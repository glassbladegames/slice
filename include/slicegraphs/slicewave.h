#pragma once

struct waveGraph
{
	slVec2 xy,wh;

	slBU window_size; // how many samples to go all the way from left to right
	slScalar extra_samples; // number of samples "past" the right side that will smoothly shift into view
	slScalar window_time_unit; // the unit of time the window covers: controls the rate at which extra_samples decreases to zero
	slScalar y_min,y_max; // information about the range of y-values

	float* values_buf; // y-values from left to right
	slBU values_count; // how many samples in values_buf
	slBU values_capacity; // storage capability of values_buf
	slBU values_writepos; // start of where new values will be written
	slBU values_readpos; // start of where new values should be read
	SDL_mutex* values_mutex;

	SDL_Color color; // color of lines

	GLuint points_vao;

	GLuint points_glbuf; // opengl buffer
	slBU glbuf_capacity; // capacity in samples
	slBU glbuf_readpos;
	slBU glbuf_writepos;
	slBU glbuf_uploaded;

	slZHook zhook;
};

void waveAppendSamples (waveGraph* graph, float* sample_buf, slBU sample_count);
void waveSetGraphDims (waveGraph* graph, slVec2 xy, slVec2 wh, Uint8 z);
waveGraph* waveCreateGraph (slBU window_size, slScalar window_time_unit = 1, slScalar y_min = -1, slScalar y_max = 1);
void waveDestroyGraph (waveGraph* graph);
namespace graphs_internal
{
	void waveInit ();
	void waveQuit ();
}
