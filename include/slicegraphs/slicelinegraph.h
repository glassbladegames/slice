#pragma once

struct lineGraph
{
	slVec2 xy,wh;

	slScalar y_min,y_max; // information about the range of y-values

	float* values_buf; // y-values from left to right
	slBU values_count; // how many samples in values_buf
	slBU values_capacity; // storage capability of values_buf
	SDL_mutex* values_mutex;

	SDL_Color color; // color of lines

	GLuint points_vao;

	GLuint points_glbuf; // opengl buffer
	slBU glbuf_capacity; // capacity in samples
	bool glbuf_stale; // tells that new data should be uploaded

	slZHook zhook;
};

void lineSetSamples (lineGraph* graph, float* sample_buf, slBU sample_count);
void lineSetGraphDims (lineGraph* graph, slVec2 xy, slVec2 wh, Uint8 z);
lineGraph* lineCreateGraph (slScalar y_min = 0, slScalar y_max = 1);
void lineDestroyGraph (lineGraph* graph);
namespace graphs_internal
{
	void lineInit ();
	void lineQuit ();
}
