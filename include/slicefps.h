#pragma once
slScalar fpsGetCurrent (); // Current framerate (last calculated).
slScalar fpsGetTotal (); // Total framerate (over the application's entire lifespan).
void fpsSetIndicatorEnabled (bool to);
void fpsInit (slVec2 indicator_xy = 0.01, slVec2 indicator_wh = slVec2(0.12,0.03));
void fpsQuit ();
/// Framerate updates on a one-second interval.
