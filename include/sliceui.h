#ifndef SLICEUI_H_INCLUDED
#define SLICEUI_H_INCLUDED

struct uiBox;
struct uiBoxRef;
#include <slice.h> // for mat4

#define uiColorFuncDefs(GetterName,FuncName,varname,\
                     HoverGetterName,HoverFuncName,use_hover_varname,hover_varname,DisableHoverFuncName)\
    inline SDL_Color GetterName () slForceInline;\
    inline uiBoxRef FuncName (SDL_Color color) slForceInline;\
    inline uiBoxRef FuncName (Uint8 r, Uint8 g, Uint8 b, Uint8 a) slForceInline;\
    inline uiBoxRef FuncName (Uint8 r, Uint8 g, Uint8 b) slForceInline;\
    inline uiBoxRef FuncName (Uint8 gray, Uint8 a) slForceInline;\
    inline uiBoxRef FuncName (Uint8 gray) slForceInline;\
    inline SDL_Color HoverGetterName () slForceInline;\
    inline uiBoxRef HoverFuncName (SDL_Color color) slForceInline;\
    inline uiBoxRef HoverFuncName (Uint8 r, Uint8 g, Uint8 b, Uint8 a) slForceInline;\
    inline uiBoxRef HoverFuncName (Uint8 r, Uint8 g, Uint8 b) slForceInline;\
    inline uiBoxRef HoverFuncName (Uint8 gray, Uint8 a) slForceInline;\
    inline uiBoxRef HoverFuncName (Uint8 gray) slForceInline;\
    inline uiBoxRef DisableHoverFuncName () slForceInline;

#define uiColorFuncs(GetterName,FuncName,varname,\
                     HoverGetterName,HoverFuncName,use_hover_varname,hover_varname,DisableHoverFuncName)\
    inline SDL_Color uiBox::GetterName ()\
    {\
        return varname;\
    }\
    inline uiBoxRef uiBox::FuncName (SDL_Color color)\
    {\
        varname = color;\
        return this;\
    }\
    inline uiBoxRef uiBox::FuncName (Uint8 r, Uint8 g, Uint8 b, Uint8 a)\
    {\
        varname = {r,g,b,a};\
        return this;\
    }\
    inline uiBoxRef uiBox::FuncName (Uint8 r, Uint8 g, Uint8 b)\
    {\
        varname = {r,g,b,255};\
        return this;\
    }\
    inline uiBoxRef uiBox::FuncName (Uint8 gray, Uint8 a)\
    {\
        varname = {gray,gray,gray,a};\
        return this;\
    }\
    inline uiBoxRef uiBox::FuncName (Uint8 gray)\
    {\
        varname = {gray,gray,gray,255};\
        return this;\
    }\
    inline SDL_Color uiBox::HoverGetterName ()\
    {\
        return use_hover_varname ? hover_varname : varname;\
    }\
    inline uiBoxRef uiBox::HoverFuncName (SDL_Color color)\
    {\
        hover_varname = color;\
        use_hover_varname = true;\
        return this;\
    }\
    inline uiBoxRef uiBox::HoverFuncName (Uint8 r, Uint8 g, Uint8 b, Uint8 a)\
    {\
        hover_varname = {r,g,b,a};\
        use_hover_varname = true;\
        return this;\
    }\
    inline uiBoxRef uiBox::HoverFuncName (Uint8 r, Uint8 g, Uint8 b)\
    {\
        hover_varname = {r,g,b,255};\
        use_hover_varname = true;\
        return this;\
    }\
    inline uiBoxRef uiBox::HoverFuncName (Uint8 gray, Uint8 a)\
    {\
        hover_varname = {gray,gray,gray,a};\
        use_hover_varname = true;\
        return this;\
    }\
    inline uiBoxRef uiBox::HoverFuncName (Uint8 gray)\
    {\
        hover_varname = {gray,gray,gray,255};\
        use_hover_varname = true;\
        return this;\
    }\
    inline uiBoxRef uiBox::DisableHoverFuncName ()\
    {\
        use_hover_varname = false;\
        return this;\
    }

#define uiAlignFuncDefs(var_name,getter_name,setter_name,setter_x_name,setter_y_name)\
	inline uiAlignXY getter_name () slForceInline;\
	inline uiBoxRef setter_name (uiAlignXY align) slForceInline;\
	inline uiBoxRef setter_name (Uint8 align_x, Uint8 align_y) slForceInline;\
	inline uiBoxRef setter_x_name (Uint8 align_x) slForceInline;\
	inline uiBoxRef setter_y_name (Uint8 align_y) slForceInline;

#define uiAlignFuncs(var_name,getter_name,setter_name,setter_x_name,setter_y_name,staling_code)\
	inline uiAlignXY uiBox::getter_name ()\
	{\
		return var_name;\
	}\
	inline uiBoxRef uiBox::setter_name (uiAlignXY align)\
	{\
		var_name = align;\
        staling_code\
		return this;\
	}\
	inline uiBoxRef uiBox::setter_name (Uint8 align_x, Uint8 align_y)\
	{\
		var_name = uiAlignXY(align_x,align_y);\
        staling_code\
		return this;\
	}\
	inline uiBoxRef uiBox::setter_x_name (Uint8 align_x)\
	{\
		var_name.align_x = align_x;\
        staling_code\
		return this;\
	}\
	inline uiBoxRef uiBox::setter_y_name (Uint8 align_y)\
	{\
		var_name.align_y = align_y;\
        staling_code\
		return this;\
	}

class uiUI;
extern uiUI* uiMainUI;

struct uiAlignXY
{
	Uint8 align_x : 4,
		  align_y : 4;
	inline uiAlignXY () slForceInline
	{
		align_x = slAlignCenter;
		align_y = slAlignCenter;
	}
	inline uiAlignXY (Uint8 align_x, Uint8 align_y) slForceInline
	{
		this->align_x = align_x;
		this->align_y = align_y;
	}
};

struct uiBoxRef;
class uiBox : public slRefCounted
{
    void OnZero () override;

    bool clips_descendants = false;
    Uint8 target_stencil;
    bool re_sort = false;

    slScalar target_aspect = -1;
    slVec2 topleft_pos, size;
    slScalar rotate_degrees = 0;

    // Local to screen: how to transform corner points when rendering.
    // Screen to local: how to determine mouse-over.
    GLmat4 local_to_screen, screen_to_local;

    slLLNodeData<uiBox> nodedata;
    slLinkedList<uiBox> boxes = slLinkedList<uiBox>(&uiBox::nodedata);

    uiBox* parent = NULL;

    SDL_Color border_color = {0};
    bool use_hover_border_color = false;
    SDL_Color hover_border_color;

    SDL_Color back_color = {0};
    bool use_hover_back_color = false;
    SDL_Color hover_back_color;

    slTexSwapChain tex_swapchain;
    bool use_hover_tex = false;
    slTexSwapChain hover_tex_swapchain;

    SDL_Color tex_mask = {255,255,255,255};
    bool use_hover_tex_mask = false;
    SDL_Color hover_tex_mask;

    bool draw_over_descendants = false;
    Uint8 z = 127;

    slScalar final_local_aspect;
    bool hovered;
    bool captures_hover = false;

    slVec2 ThroughMat4 (slVec2 in_vec, GLmat4 mat);
    void RecalcThis (slScalar parent_aspect);
    void RecalcTraverseST (slScalar parent_aspect);
    void RecalcTraverseMT (slWorkerTask* task, slScalar parent_aspect);
    void StencilThis ();
    void DrawThis ();
    void Draw ();
    bool CheckHover (slVec2 media_mouse, bool already_taken = false);

	uiAlignXY aspect_align, tex_align;
    slVec2 tex_wh;

    bool stale, was_stale;
    slScalar previous_parent_aspect = -1;
    bool strong_parent;

    ~uiBox ();
    friend class uiBoxRef;
    friend class uiUI;
    friend class uiRecalcCall;

    slVec2 topleft, topright, bottomleft, bottomright;

public:

    uiBox* Detach (); // Cannot return uiBoxRef since it's called during destruction.
    uiBoxRef AttachTo (uiBoxRef parent, bool strong_parent = true);
    inline uiBoxRef AttachTo (uiUI* ui) slForceInline;
    inline uiBoxRef Attach () slForceInline;

	uiAlignFuncDefs(aspect_align,GetAspectAlign,SetAspectAlign,SetAspectAlignX,SetAspectAlignY)
	uiAlignFuncDefs(tex_align,GetTexAlign,SetTexAlign,SetTexAlignX,SetTexAlignY)

    inline slVec2 GetPos () slForceInline;
    inline uiBoxRef SetPos (slVec2 vec) slForceInline;
    inline uiBoxRef SetPos (slScalar x, slScalar y) slForceInline;

    inline slVec2 GetSize () slForceInline;
    inline uiBoxRef SetSize (slVec2 vec) slForceInline;
    inline uiBoxRef SetSize (slScalar x, slScalar y) slForceInline;

    inline Uint8 GetZ () slForceInline;
    inline uiBoxRef SetZ (Uint8 z) slForceInline;

    inline slScalar GetRotation () slForceInline;
    inline uiBoxRef SetRotation (slScalar rotate_degrees) slForceInline;

    inline slScalar GetTargetAspect () slForceInline;
    inline uiBoxRef SetTargetAspect (slScalar target_aspect) slForceInline;

    uiColorFuncDefs(GetBorderColor,SetBorderColor,border_color,
                    GetHoverBorderColor,SetHoverBorderColor,use_hover_border_color,hover_border_color,DisableHoverBorderColor)

    uiColorFuncDefs(GetBackColor,SetBackColor,back_color,
                    GetHoverBackColor,SetHoverBackColor,use_hover_back_color,hover_back_color,DisableHoverBackColor)

    inline slTexRef GetTexture () slForceInline;
    inline uiBoxRef SetTexture (slTexRef tex) slForceInline;
	inline uiBoxRef CapturesHover (bool capture = true) slForceInline;
    inline slTexRef GetHoverTexture () slForceInline;
    inline uiBoxRef SetHoverTexture (slTexRef hover_tex) slForceInline;
    inline uiBoxRef DisableHoverTexture () slForceInline;

    uiColorFuncDefs(GetTexMask,SetTexMask,tex_mask,
                    GetHoverTexMask,SetHoverTexMask,use_hover_tex_mask,hover_tex_mask,DisableHoverTexMask)
};

struct uiBoxRef : slRef<uiBox>
{
    using slRef<uiBox>::slRef;
    inline uiBoxRef () slForceInline : slRef<uiBox>(new uiBox) {}
};



uiAlignFuncs(aspect_align,GetAspectAlign,SetAspectAlign,SetAspectAlignX,SetAspectAlignY,stale = true;)
uiAlignFuncs(tex_align,GetTexAlign,SetTexAlign,SetTexAlignX,SetTexAlignY,)

inline slVec2 uiBox::GetPos ()
{
    return topleft_pos;
}
inline uiBoxRef uiBox::SetPos (slVec2 vec)
{
    topleft_pos = vec;
    stale = true;
    return this;
}
inline uiBoxRef uiBox::SetPos (slScalar x, slScalar y)
{
    topleft_pos = slVec2(x,y);
    stale = true;
    return this;
}

inline slVec2 uiBox::GetSize ()
{
    return size;
}
inline uiBoxRef uiBox::SetSize (slVec2 vec)
{
    size = vec;
    stale = true;
    return this;
}
inline uiBoxRef uiBox::SetSize (slScalar x, slScalar y)
{
    size = slVec2(x,y);
    stale = true;
    return this;
}

inline Uint8 uiBox::GetZ ()
{
    return z;
}
inline uiBoxRef uiBox::SetZ (Uint8 z)
{
    this->z = z;
    if (parent) parent->re_sort = true;
    return this;
}

inline slScalar uiBox::GetRotation ()
{
    return rotate_degrees;
}
inline uiBoxRef uiBox::SetRotation (slScalar rotate_degrees)
{
    this->rotate_degrees = rotate_degrees;
    stale = true;
    return this;
}

inline slScalar uiBox::GetTargetAspect ()
{
    return target_aspect;
}
inline uiBoxRef uiBox::SetTargetAspect (slScalar target_aspect)
{
    this->target_aspect = target_aspect;
    stale = true;
    return this;
}

uiColorFuncs(GetBorderColor,SetBorderColor,border_color,
             GetHoverBorderColor,SetHoverBorderColor,use_hover_border_color,hover_border_color,DisableHoverBorderColor)

uiColorFuncs(GetBackColor,SetBackColor,back_color,
             GetHoverBackColor,SetHoverBackColor,use_hover_back_color,hover_back_color,DisableHoverBackColor)

inline slTexRef uiBox::GetTexture ()
{
    return tex_swapchain.Step();
}
inline uiBoxRef uiBox::SetTexture (slTexRef tex)
{
    tex_swapchain.Queue(tex);
    return this;
}
inline uiBoxRef uiBox::CapturesHover (bool capture = true)
{
    captures_hover = capture;
    return this;
}
inline slTexRef uiBox::GetHoverTexture ()
{
    return use_hover_tex ? hover_tex_swapchain.Step() : tex_swapchain.Step();
}
inline uiBoxRef uiBox::SetHoverTexture (slTexRef hover_tex)
{
    hover_tex_swapchain.Queue(hover_tex);
    use_hover_tex = true;
    return this;
}
inline uiBoxRef uiBox::DisableHoverTexture ()
{
    hover_tex_swapchain.Queue(slNoTexture); /* Might as well potentially free up memory. */
    use_hover_tex = false;
    return this;
}
uiColorFuncs(GetTexMask,SetTexMask,tex_mask,
             GetHoverTexMask,SetHoverTexMask,use_hover_tex_mask,hover_tex_mask,DisableHoverTexMask)



class uiUI
{
    uiBoxRef root_box;

    void RecalcST (slScalar media_aspect);
    void RecalcMT (slScalar media_aspect);
    void CheckHover (slVec2 media_mouse);

public:

    uiUI (slScalar target_aspect = -1);
    inline uiBoxRef GetRootBox () slForceInline { return root_box; }
    void Draw ();
};

inline uiBoxRef uiBox::AttachTo (uiUI* ui) { return AttachTo(ui->GetRootBox(), false); }
inline uiBoxRef uiBox::Attach () { return AttachTo(uiMainUI); }

void uiInit ();
void uiQuit ();

#endif // SLICEUI_H_INCLUDED
