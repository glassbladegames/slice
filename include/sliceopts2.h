#pragma once

#include <sliceui.h>

class o2Menu
{
    uiBoxRef root_box;
public:
    inline uiBoxRef GetRootBox () slForceInline { return root_box; }
    o2Menu ();
    ~o2Menu ();
};
