#pragma once

#define slDefaultWindowX 1280
#define slDefaultWindowY 720

#define likely(cond) __builtin_expect(cond,1)
#define unlikely(cond) __builtin_expect(cond,0)
#define slForceInline __attribute__((always_inline))

#include <glad/glad.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#ifndef _GNU_SOURCE
	#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <time.h>



void slDeadBeef ();
void* sl_malloc (size_t n);
void* sl_realloc (void* prev, size_t n);
void slSetMemLimit (size_t limit); // Limits size of any single allocation. Default: 0 (no artificial limit).
#define malloc sl_malloc
#define realloc sl_realloc
#define SDL_main main



#include <slice/getarg.h>
#include <slice/slicemath.h>
#include <slice/GLmat4.h>
#include <slice/slicegc.h>
#include <slice/slList.h>
#include <slice/slBlockAllocator.h>
#include <slice/slicestr.h>
#include <slice/sliceworkers.h>
#include <slice/queues.h>
#include <slice/slTreeSet.h>
#include <slice/slTreeMap.h>
#include <slice/slLinkedList.h>
#include <slice/slicekeybinds.h>
#include <slice/slicetextures.h>



void slLoadScreenOrtho ();
void slLoadScreenOrtho_Aspected ();

#include <slice/sliceaudio.h>

void slFatal (char* msg, int returncode);
void slInit (char* appname = "Slice Game Engine", char* settings_path = "settings");

void slUseContext (); // Switch back to Slice's OpenGL context after you were doing something else.

void slDoNothing ();

void slDefer (void (*func) (void* arg), void* arg); // Deferred function will run (once) upon next slCycle.
void slCycle ();
void slQuit ();
void slSetIcon (char* path);

void slSort (slList* list, bool (*inorder) (void* first, void* second));

inline slForceInline void slSetTextureClamping (GLint mode)
{
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,mode);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,mode);
}

struct slHSVA
{
	// Hue, Saturation, Value, Alpha
	Uint8 h,s,v,a;
};
SDL_Color slHSVAtoRGBA (slHSVA in);
slHSVA slRGBAtoHSVA (SDL_Color rgba);

#include <slice/ui_shaders.h>

bool slGetFullscreen ();
void slSetFullscreen (bool to);
void slToggleFullscreen ();

bool slGetVSync ();
void slSetVSync (bool to);
void slToggleVSync ();

slScalar slGetDelta ();

bool slGetExitReq ();
#define slGetReqt slGetExitReq
void slSignalExit ();
void slRejectExit ();

slVec2 slGetMouse ();
slVec2 slGetMouseMaintainAspect ();
void slGetMouse (slScalar* x, slScalar* y); // Deprecated

SDL_Window* slGetWindow ();
bool slGetWindowFocused ();
slInt2 slGetWindowResolution ();
slScalar slGetWindowAspect ();

void slSetAppDrawStage (void (*func) ());

void slCaptureNextInput (void (*onnextinput) (slKeyInfo input));

void slSetMiscEventHandler (void (*on_misc) (SDL_Event event));
void slSetAllEventsHandler (void (*on_any) (SDL_Event event));
void slSetFileDropHandler (void (*on_filedrop) (char* path));



/// Do not attempt to "Unhook" a function that is not currently "Hooked"!

// Return true from `func` to prevent garbage collection from deleting the object.
void slHook_TextureUsed (bool (*is_used) (slTexture* tex));
void slUnhook_TextureUsed (bool (*is_used) (slTexture* tex));
void slHook_SoundSourceUsed (bool (*func) (slSoundSource* src));
void slUnhook_SoundSourceUsed (bool (*func) (slSoundSource* src));

struct slBox;
void slHook_PreRender (void (*func) (slBox* hoverbox));
void slUnhook_PreRender (void (*func) (slBox* hoverbox));



// slCycle won't return until every hooked Frame Conclude function returns.
void slHook_FrameConclude (void (*func) ());
void slUnhook_FrameConclude (void (*func) ());



#include <slice/sliceanim.h>



#define slTypingEvent_Character 0
#define slTypingEvent_Enter 1
#define slTypingEvent_Backspace 2
#define slTypingEvent_Delete 3
#define slTypingEvent_Home 4
#define slTypingEvent_End 5
#define slTypingEvent_LeftArrow 6
#define slTypingEvent_RightArrow 7
#define slTypingEvent_UpArrow 8
#define slTypingEvent_DownArrow 9
#define slTypingEvent_PageUp 10
#define slTypingEvent_PageDown 11
#define slTypingEvent_Tab 12
#define slTypingEvent_Deselect 13 // Fired when the callback is about to be set to something else (including NULL).
#define slTypingEvent_Copy 14
#define slTypingEvent_Paste 15
#define slTypingEvent_Cut 16
#define slTypingEvent_MouseDrag 17

void slSetTypingInputCallback (void (*callback) (void* userdata, int event_type, char ch), void* userdata = NULL);
/// Setting this to a function enables typing mode. Setting it to NULL disables typing mode.
/// If event_type is slTypingEvent_Character, then ch represents a character value entered.
/// If event_type is slTypingEvent_Deselect, slTypingEvent_Copy, slTypingEvent_Paste, or slTypingEvent_MouseDrag then ch is meaningless.
/// Otherwise, ch represents whether or not one or more of the Shift keys are currently pressed.
/// To do: add support for clipboard.



/*  Queue for tasks that need an OpenGL context and must be completed before a
    frame is next rendered, so they run in the main thread.
    What this is useful for: issuing GL calls that should complete very quickly,
    but from functions that want to be callable from any thread.  */
class slNextFrameTasksProto : public slAsyncNonblockingQueue
{
public:
    void Flush ();
//public:
    slNextFrameTasksProto ();
    void Push (void (*task_func) (void* task_data), void* task_data, SDL_atomic_t* items_in_queue /* NULL is OK */);

    //friend void slCycle (); // Flushes every engine cycle.
    //friend void slQuit (); // Flushes before engine shutdown.
};
extern slNextFrameTasksProto* slNextFrameTasks;

/*  Queue for tasks that need an OpenGL context but can run in the background,
    for example uploading large buffers.  */
class slBackgroundUploaderProto : public slConsumerQueue
{
    void PreLoop () override;
    void ItemFunc (void* queue_item) override;
public:
    slBackgroundUploaderProto ();
    void Push (void (*task_func) (void* task_data), void* task_data, SDL_atomic_t* items_in_queue /* NULL is OK */);
};
extern slBackgroundUploaderProto* slBackgroundUploader;
void slWaitBackgroundUploads(SDL_atomic_t* items_in_queue);

#define slAlignCenter 0
#define slAlignLeft 1
#define slAlignRight 2
#define slAlignTop 1
#define slAlignBottom 2

#define slAlignTopLeft 		slAlignLeft,	slAlignTop
#define slAlignTopCenter 	slAlignCenter,	slAlignTop
#define slAlignTopRight 	slAlignRight,	slAlignTop
#define slAlignCenterLeft 	slAlignLeft,	slAlignCenter
#define slAlignCenterCenter	slAlignCenter,	slAlignCenter
#define slAlignCenterRight 	slAlignRight,	slAlignCenter
#define slAlignBottomLeft 	slAlignLeft,	slAlignBottom
#define slAlignBottomCenter	slAlignCenter,	slAlignBottom
#define slAlignBottomRight	slAlignRight,	slAlignBottom

#include <sliceui.h>
#include <slice/sliceboxes.h>
#include <slice/slicetypingboxes.h>
#include <slice/slicesliders.h>
#include <slice/slicetoggle.h>
#include <slice/slicescroll.h>
#include <slice/slicetext_ext.h>
#include <slice/splash.h>
#include <slice/slicelines.h>
