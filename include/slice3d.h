#pragma once
#include <slice.h>

#include <slice3d/load-obj.h> // just for objColor definition



struct GLcolor3
{
    GLfloat r,g,b;
    GLcolor3 (GLfloat gray) slForceInline : r(gray), g(gray), b(gray) {}
    GLcolor3 (GLfloat r, GLfloat g, GLfloat b) slForceInline : r(r), g(g), b(b) {}
    /* TO DO: use vector math to speed up conversion from bytes to floats */
    GLcolor3 (SDL_Color rgb) slForceInline : r(rgb.r * slInv255), g(rgb.g * slInv255), b(rgb.b * slInv255) {}
    GLcolor3 (objColor obj) slForceInline : r(obj.r), g(obj.g), b(obj.b) {}
};

struct s3dMesh
{
    slTexture* ambient_texref;
	slTexture* diffuse_texref;
    slTexture* specular_texref;
    slTexture* opacity_texref;
    slTexture* normal_texref;
	GLuint vertex_buffer;
	uint32_t triangle_count;

    GLcolor3 ambient_color, diffuse_color, specular_color;
    GLfloat specular_exponent;
    bool mag_filter_nearest; // if set to true, use GL_NEAREST as texture magnification filter. Otherwise use LINEAR.
    bool ignore_environment_ambient; // Do not modulate lighting by global color values. (Useful for televisions etc.)
};

struct s3dObject;
struct s3dModel : public slRefCounted
{
	s3dMesh* meshes;
	slBU _index_;
	uint32_t meshcount = 0; // will be less than the true count during loading
    bool done_loading = false; // Is set on success or failure alike, so that
                               // anything waiting on this can proceed instead
                               // of getting stuck waiting for meshcount to not
                               // be zero anymore (it stays 0 on failure).
    slScalar max_dist_from_center = 0; // Calculated during loading.

    s3dModel ();

    void OnZero () override;
    // Works the same way as it does with slTexture:
    virtual void Unregister (); // Happens before (separately from) destruction.
    // ^^ If you override this, make sure to call the overridden function!!
    virtual ~s3dModel ();

    GLuint matrix_buffer;
    GLuint matrix_texture;
    slBU matrix_count;
    slBU matrix_residency_capacity; // capacity of s3dObject** buffer
    slBU matrix_buffer_capacity; // capacity of OpenGL matrix buffer
    s3dObject** matrix_residency;
    SDL_mutex* matrix_mutex; // prevents residency from getting corrupted

    void AddMatrix (s3dObject* add_obj);
    void RemoveMatrix (s3dObject* remove_obj);
    void UpdateMatrix (s3dObject* update_obj);
};
struct s3dModelRef : slRef<s3dModel>
{
    using slRef<s3dModel>::slRef;
};

struct s3dLoadedModelInfo
{
    char* filepath;
};
struct s3dLoadedModel : s3dModel
{
    s3dLoadedModelInfo tree_key;
    slTreeNodeData<s3dLoadedModel> tree_data;
	s3dLoadedModel (s3dLoadedModelInfo search_key);
    void Unregister () override;
};
s3dModelRef s3dLoadObj (char* basepath, char* filename);

s3dModelRef s3dSpriteModel (objMaterial material = objFallbackMaterial);
s3dModelRef s3dSpriteModel (
    // Convenience function. These values are just used to make objMaterial
    // which is then passed to the other s3dSpriteModel function.
    char* sprite_texpath,
    bool mag_filter_nearest = true,
    float ambient = 0,
    float diffuse = 1,
    float specular = 0,
    float spec_exp = objFallbackMaterial.specular_exponent,
    bool ignore_environment_ambient = false
);

// Very similar to slNoTexture, this s3dNullModel type is needed since objects
// are no longer allowed to have NULL model pointers.
class s3dNullModel : public s3dModel
{
    inline s3dNullModel () slForceInline
    {
        /*  We very little to do to set this up,
            so it's unnecessary for it to be its own type, but it being distinct
            shouldn't harm anything either.  */
        meshes = malloc(0);
        done_loading = true;
    }
    static s3dNullModel* instance;
    friend struct s3dNullModelRef;
    friend void s3dInit ();
    friend void s3dQuit ();
};
struct s3dNullModelRef : public s3dModelRef
{
	s3dNullModelRef () : s3dModelRef(s3dNullModel::instance) {}
};
// Convenience macro in case you prefer to write "s3dNoModel" for readability.
#define s3dNoModel (s3dNullModelRef())

struct s3dCamera;
class s3dObject
{
    s3dModelRef model = s3dNoModel;
	GLmat4 world_transform;
    slBU matrix_residence; // Where, in the s3dModel's buffer(s), is the matrix from this object?
    bool visible;

public:
	void SetModel (s3dModelRef set_to);
    void SetWorldTransform (GLmat4 world_transform); // Combine origin, rotation, and scaling yourself however you wish.
    void SetVisible (bool visible);

    slBU _index_; // You shouldn't mess with this but it has to be public.

    friend s3dObject* s3dCreateObject (s3dModelRef model);
    friend void s3dDestroyObject (s3dObject* object);
    friend void s3dApplyWorldTransform (s3dObject* obj);
    friend class s3dModel;
};
s3dObject* s3dCreateObject (s3dModelRef model);
void s3dDestroyObject (s3dObject* object);

struct s3dCamera
{
private:
    GLmat4 projection_matrix = GLmat4_perspective(90,0.2,300);
    // ^ Aspect ratio compensation is done separately. Give a square-shaped projection here.
	s3dVec3 origin = 0;
    s3dVec3 yaw_pitch_roll = 0; // Rotation in degrees.
    GLfloat znear=0.2,zfar=300;
    bool is_ortho = false;
    union
    {
        GLfloat fov=90;
        GLfloat view_half_size;
    };

    bool dirty = true; // Indicates something changed, need to recalculate final_matrix.
    GLmat4 final_view_matrix;
    GLmat4 final_view_undo;
    void Recalculate ();

public:
    bool cull_backfaces = true;
    s3dVec3 GetNormal (); // Calculates normal from yaw & pitch.
    inline GLmat4 GetViewMatrix () slForceInline
    {
        if (dirty) Recalculate();
        return final_view_matrix;
    }
    inline GLmat4 GetViewUndo () slForceInline
    {
        if (dirty) Recalculate();
        return final_view_undo;
    }
    GLmat4 GetProjectionMatrix (slInt2 target_wh);
    inline GLfloat* NearFarZ () slForceInline { return &znear; }
    inline bool IsOrtho () slForceInline { return is_ortho; }
    inline GLfloat CornerMultiplier () slForceInline
    {
        return is_ortho
            ? view_half_size
            : tan(slDegToRad_F(fov) * 0.5f);
    }
    inline void SetYaw (slScalar yaw_degrees) slForceInline
    {
        yaw_pitch_roll.x = yaw_degrees;
        dirty = true;
    }
    inline void SetPitch (slScalar pitch_degrees) slForceInline
    {
        yaw_pitch_roll.y = pitch_degrees;
        dirty = true;
    }
    inline void SetRoll (slScalar roll_degrees) slForceInline
    {
        yaw_pitch_roll.z = roll_degrees;
        dirty = true;
    }
    inline void SetYPR (s3dVec3 ypr_degrees) slForceInline
    {
        yaw_pitch_roll = ypr_degrees;
        dirty = true;
    }
    inline slScalar GetYaw () slForceInline
    {
        return yaw_pitch_roll.x;
    }
    inline slScalar GetPitch () slForceInline
    {
        return yaw_pitch_roll.y;
    }
    inline slScalar GetRoll () slForceInline
    {
        return yaw_pitch_roll.z;
    }
    inline s3dVec3 GetYPR () slForceInline
    {
        return yaw_pitch_roll;
    }
    inline void SetOrigin (s3dVec3 origin) slForceInline
    {
        this->origin = origin;
        dirty = true;
    }
    inline s3dVec3 GetOrigin () slForceInline
    {
        return origin;
    }
    inline void Ortho (GLfloat view_half_size, GLfloat near_clip, GLfloat far_clip) slForceInline
    {
        projection_matrix = GLmat4_ortho(-view_half_size,view_half_size,-view_half_size,view_half_size,near_clip,far_clip);
        is_ortho = true;
        this->view_half_size = view_half_size;
        znear = near_clip;
        zfar = far_clip;
        dirty = true;
    }
    inline void Perspective (GLfloat fov, GLfloat near_clip, GLfloat far_clip) slForceInline
    {
        projection_matrix = GLmat4_perspective(fov,near_clip,far_clip);
        is_ortho = false;
        this->fov = fov;
        znear = near_clip;
        zfar = far_clip;
        dirty = true;
    }
};
class s3dRenderCamera : public s3dCamera
{
    slFrame* vis_frame = new slFrame(false);
    slTargetTexRef vis_color;

    slFrame* gbuf_frame = new slFrame(false);
    slTargetTexRef gbuf_depth    = gbuf_frame->CreateDepthOutput();
    slTargetTexRef gbuf_diffuse  = gbuf_frame->CreateColorOutput(1,false);
    slTargetTexRef gbuf_specular = gbuf_frame->CreateColorOutput(2,true);
    slTargetTexRef gbuf_normal   = gbuf_frame->CreateColorOutput(3,false);

    friend void s3dRender (s3dRenderCamera*);

public:

    inline s3dRenderCamera (bool create_alpha_channel = false) slForceInline
        : vis_color(vis_frame->CreateColorOutput(0,create_alpha_channel))
    {
        gbuf_frame->AttachColorOutput(0,vis_color);
    }
    inline ~s3dRenderCamera () slForceInline
    {
        delete vis_frame;
        delete gbuf_frame;
    }

    inline void Resize (slVec2 screen_wh) slForceInline
    {
        // Scaled by the screen resolution.
        slInt2 pixels_wh = screen_wh * slGetWindowResolution();
        Resize(pixels_wh);
    }
    inline void Resize (slInt2 pixels_wh) slForceInline
    {
        // Directly use a specific resolution.
        vis_frame->Resize(pixels_wh);
        gbuf_frame->Resize(pixels_wh);
    }

    inline void Render () slForceInline
    {
        s3dRender(this);
    }

    inline slTexRef GetColor () slForceInline { return vis_color; }
    inline slTexRef GetDepth () slForceInline { return gbuf_depth; }
    inline slTexRef GetNormalBuffer () slForceInline { return gbuf_normal; }
};

void s3dInit ();
void s3dQuit ();
void s3dSetDistanceFog (bool enabled, GLcolor3 rgb = 0, GLfloat multiplier = 0.25);
void s3dSetEnvironmentAmbientColor (GLcolor3 color);
void s3dSetDiffuseVoxeling (bool voxeling_enabled, GLfloat voxel_size);
void s3dSetSpecularVoxeling (bool voxeling_enabled, GLfloat voxel_size);
void s3dSetFogVoxeling (bool voxeling_enabled, GLfloat voxel_size);

#define s3dLight_Ortho 0
#define s3dLight_Point 1
#define s3dLight_Spot 2
class s3dLight
{
    Uint8 type = s3dLight_Point;
    // Assume it's a point light until SetHalfSpan or SetCoverageAngle are called.

//    GLvec4 pos;
    // This is even relevant for ortho lights (shadow renders from this point
    // regardless of projection). W component ignored, just using GLvec4 so it
    // doesn't need to be cast aside from when it's set.

//    GLvec4 dir;
    // Relevant to ortho and spot lights, ignored for point lights.

    GLfloat coverage_angle; // Stored in degrees.
    // Angle from center to lit edge. If angle is small enough, engine can avoid
    // rendering some sides of the shadow cubemap. Relevant to spot lights only.

    GLfloat half_span;
    // Angle from center to edge, for ortho lights specifically. A larger span
    // means a lower shadow quality when resolution remains the same. Anything
    // outside the span is lit.

    bool redraw_static = true;
    // Signals that the shadow maps for static geometry need to be redrawn. This
    // is likely because the light itself is new, or has moved from its previous
    // location.

    GLvec4 pos_static_drawn;
    // Used to determine whether to trigger a redraw of the shadow maps for
    // static geometry because it has moved too far from the light position when
    // this was last performed.

    GLfloat movement_tolerated_before_redraw_static = 0;
    // Maximum distance this light can be moved without triggering a redrew of
    // shadow maps for static geometry.

    int shadow_tex_res = 256;

    slFrame* frame = new slFrame(false);
    slTargetTexRef frame_depth = frame->CreateDepthOutput();
    s3dCamera camera;

    s3dLight ();
    ~s3dLight ();

    slBU _index_;
    static slList Lights;

    friend void s3dUpdateShadows();
    friend void s3dRender (s3dRenderCamera*);

public:
    GLcolor3 color = 1;

    GLfloat strength = 10;
    // Determines how far light is cast from a point or spot light. Set to a
    // negative value for infinite strength.

    inline slTargetTexRef GetDepth () slForceInline { return frame_depth; }
    inline s3dCamera* GetCamera () slForceInline { return &camera; }
    inline void SetShadowRes (int res) slForceInline { shadow_tex_res = res; }

    static s3dLight* Create ();
    static void Destroy (s3dLight* light);

    void SetType (Uint8 type);
    // 'type' should be one of: {s3dLight_Ortho, s3dLight_Point, s3dLight_Spot}

    void SetCoverageAngle (slScalar angle);
    // Given in degrees. Only relevant to spot lights.

    void SetHalfSpan (slScalar span);
    // Only relevant to ortho lights.

    void SetMovementTolerance (slScalar max_movement);
    // Defaults to no tolerance (always redraws when position changes at all).
};

void s3dUpdateShadows ();
// App calls this once per frame explicitly. If it were called by s3dRender then
// performance would go to waste updating the shadows for each additional camera
// during the rendering of one overall frame.



struct s3dBumpInfo
{
    char* filepath;
    inline s3dBumpInfo (char* filepath) slForceInline
    {
        this->filepath = filepath;
    }
};
class s3dBumpTexture : public slTexture
{
    s3dBumpInfo bump_tree_key;
    slTreeNodeData<s3dBumpTexture> bump_tree_data;
    friend class s3dBumpTexturesTreeProto;

    s3dBumpTexture* next_load;
    friend class s3dBumpAsyncQueueProto;

public:
    s3dBumpTexture (s3dBumpInfo search_key);
    ~s3dBumpTexture () override;
    void Unregister () override;
};
struct s3dBumpTexRef : public slTexRef
{
	s3dBumpTexRef (s3dBumpTexture* raw_ref) : slTexRef(raw_ref) {}
};
s3dBumpTexRef s3dLoadBumpMap (char* path);

#include <slice3d/slicephys3d.h>
