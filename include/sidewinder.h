#pragma once
#include <slice.h>
struct swThread
{
	//SDL_Thread* thread;
	slBU _index_;
	slScalar awaken_t;
	bool dead;
	SDL_sem* start;
	void (*main) (swThread* thread);

	slScalar slept_time; // Time elapsed since last call to swSleep.
};
void swSleep (swThread* thread, slScalar awaken_t = 0);
void swSpawn (void (*func) (swThread* self));
void swExecute ();
void swInit ();
void swKillThread (swThread* thread);
void swKillAll ();
