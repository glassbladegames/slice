#pragma once

#include <slice.h>

template <class Key, class Value>
class slTreeMap
{
    Key Value::* embedded_key;
    slTreeNodeData<Value> Value::* embedded_nodedata;
    Value* root = NULL;
    int total_values = 0;

    int Recount (Value* local_root);
    void PrintInOrder (Value* local_root, int depth, char pre);

public:
    inline slTreeMap (Key Value::* embedded_key,
                      slTreeNodeData<Value> Value::* embedded_nodedata) slForceInline
    {
        this->embedded_key = embedded_key;
        this->embedded_nodedata = embedded_nodedata;
    }
    void Insert (Value* value);
    void Remove (Value* value);
    Value* Lookup (Key* key);

    inline void PrintInOrder () slForceInline
    {
        PrintInOrder(root,0,0xC4);
    }
    inline int Recount () slForceInline
    {
        return Recount(root);
    }

    /* Return 1 for in-order, -1 for reverse-order, 0 for equality. */
    virtual int Compare (Key* key_a, Key* key_b) = 0;
    virtual void PrintKey (Key* key) = 0;
};

#include <slice/slTreeMapImpl.cpp>
