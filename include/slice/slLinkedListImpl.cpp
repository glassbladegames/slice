#include <slice/slLinkedList.h>

template <class Value>
void slLinkedList<Value>::Prepend (Value* value)
{
    NextOf(value) = first;
    PrevOf(value) = NULL;
    if (first) PrevOf(first) = value;
    else       last          = value;
    first                    = value;
    total_values++;
}

template <class Value>
void slLinkedList<Value>::Append (Value* value)
{
    NextOf(value) = NULL;
    PrevOf(value) = last;
    if (last) NextOf(last) = value;
    else      first        = value;
    last                   = value;
    total_values++;
}

template <class Value>
void slLinkedList<Value>::InsertBefore (Value* value, Value* before)
{
    if (!before)
    {
        /* Before nothing. insert at end. */
        Append(value);
    }
    else
    {
        NextOf(value) = before;
        PrevOf(value) = PrevOf(before);
        if (PrevOf(before)) NextOf(PrevOf(before)) = value;
        else                first                  = value;
        PrevOf(before)                             = value;
        total_values++;
    }
}

template <class Value>
void slLinkedList<Value>::InsertAfter (Value* value, Value* after)
{
    if (!after)
    {
        /* After nothing. insert at beginning. */
        Prepend(value);
    }
    else
    {
        NextOf(value) = NextOf(after);
        PrevOf(value) = after;
        if (NextOf(after)) PrevOf(NextOf(after)) = value;
        else               last                  = value;
        NextOf(after)                            = value;
        total_values++;
    }
}

template <class Value>
void slLinkedList<Value>::Remove (Value* value)
{
    if (NextOf(value)) PrevOf(NextOf(value)) = PrevOf(value);
    else               last                  = PrevOf(value);
    if (PrevOf(value)) NextOf(PrevOf(value)) = NextOf(value);
    else               first                 = NextOf(value);
    total_values--;
}

template <class Value>
void slLinkedList<Value>::Sort (bool (*inorder) (Value* prior, Value* after))
{
    Value* next_insert = first;
    while (next_insert)
    {
        Value* inserting = next_insert;
        next_insert = NextOf(next_insert);

        Value* compareto = inserting;
        while (compareto = PrevOf(compareto))
            if (inorder(compareto,inserting))
                break;

        if (PrevOf(inserting) != compareto)
        {
            Remove(inserting);
            InsertAfter(inserting,compareto);
        }
    }
}
