#ifndef SLICEWORKERS_H_INCLUDED
#define SLICEWORKERS_H_INCLUDED

#include <slice/queues.h>

slBU slCeil16 (slBU size);
//struct slWorker;
typedef slAsyncNonblockingQueue slWorker; // To reduce required refactoring.
void slSchedule (void (*func) (void*), void* arg, slWorker* worker);
struct slBulkJobDef
{
	slBU thread_userdata_size;
	void (*init_thread_userdata) (void* thread_userdata);//, slBU core_id);
	void (*quit_thread_userdata) (void* thread_userdata);//, slBU core_id);
	//void (*thread_prework) (void* thread_userdata, slBU core_id);
	//void (*thread_postwork) (void* thread_userdata, slBU core_id);
	void (*main_prework) (void* all_threads_userdata, slBU thread_userdata_size, void* main_userdata, slBU core_count);
	void (*main_postwork) (void* all_threads_userdata, slBU thread_userdata_size, void* main_userdata, slBU core_count);
	//void (*per_item) (void* item, void* main_userdata, slWorker* worker);

	void (*thread_work) (slBU start, slBU count, void* thread_userdata, void* main_userdata, slWorker* worker);
};
#pragma pack(push,16)
struct slBulkJob
{
	slBulkJobDef def;
	//void* all_threads_userdata;
};
#pragma pack(pop)
//inline void* slGetJobThreadData (slBulkJob* job) {return (void*)job + sizeof(slBulkJob);}
slBulkJob* slCreateBulkJob (slBulkJobDef def);
void slDestroyBulkJob (slBulkJob* job);
void slExecuteBulkJob (slBulkJob* job, slBU count, void* main_userdata, slBU threading_threshold = 16);
void slDoArrayWork (void* array, slBU count, slBU item_size, void (*peritem) (void* item, void* userdata, slWorker* worker), void* userdata, slBU threading_threshold = 16);
void slDoSimpleWork (slBU count, void (*peritem) (slBU start, slBU end, void* userdata, slWorker* worker), void* userdata, slBU threading_threshold = 16);
void slDoWork (slList* list, void (*peritem) (void* item, void* userdata, slWorker* worker), void* userdata, slBU threading_threshold = 16);

class slWorkerTask
{
    SDL_atomic_t in_progress = {0};
    SDL_sem* completion;
    friend class slWorkerCall;
public:
    slWorkerTask ();
    ~slWorkerTask (); // Destruction *is* the way to await completion.
};
int slGetWorkerCount ();
struct slWorkerCall
{
    slWorkerTask* task;
    slWorkerCall* next;

    slWorkerCall (slWorkerTask* task);
    void Send ();
    virtual void WorkFunc () = 0;
    virtual ~slWorkerCall ();
};

namespace sl_internal
{
	void slWorkersInit ();
	void slWorkersQuit ();
}

#endif // SLICEWORKERS_H_INCLUDED
