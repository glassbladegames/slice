#pragma once
struct tglToggler
{
	slBox* text_area;
	slBox* toggle_back;
	slBox* toggle_front;
	slTexture* on_tex;
	slTexture* off_tex;
	void (*onchange) (tglToggler* toggler);
	slBU index,alloc_index;
	slScalar animation_progress;
	slScalar toggle_front_left_pos;
	SDL_Color on_color;
	SDL_Color off_color;
	bool state,animating;
	void SetState (bool new_state);
};

/// Deprecated - use the object oriented call instead for new code.
inline slForceInline void tglSetToggler (tglToggler* toggler, bool state) { toggler->SetState(state); }

tglToggler* tglCreateToggler (
	char* text, slBox* parent, bool initial_state, slScalar text_ratio = 0.8,
	slTexRef on_tex = slRenderText("ON"), slTexRef off_tex = slRenderText("OFF"),
	SDL_Color on_color = {0,255,0,255}, SDL_Color off_color = {255,0,0,255}
);
void tglDestroyToggler (tglToggler* toggler);
namespace sl_internal
{
	void tglInit ();
	void tglQuit ();
}
