#include <slice/slTreeMap.h>

template <class Key, class Value>
void slTreeMap<Key,Value>::Insert (Value* value)
{
    Value** inspect_ptr = &root;
    Value* inspect;
    while (inspect = *inspect_ptr)
    {
        int comparison = Compare(&(inspect->*embedded_key),&(value->*embedded_key));

        // 0 means value is a duplicate, which is forbidden
        if (!comparison)
        {
            printf("Can't insert value because it is already in the TreeMap\n");
            return;
        }

        // 1 means value > *inspect_ptr, so should go on the right
        // -1 means value < *inspect_ptr, so should go on the left
        slTreeNodeData<Value>* inspect_data = &(inspect->*embedded_nodedata);
        if (comparison > 0) inspect_ptr = &inspect_data->right;
        else if (comparison < 0) inspect_ptr = &inspect_data->left;
    }

    total_values++;
    *inspect_ptr = value;
    slTreeNodeData<Value>* value_data = &(value->*embedded_nodedata);
    value_data->left = NULL;
    value_data->right = NULL;
}
template <class Key, class Value>
void slTreeMap<Key,Value>::Remove (Value* value)
{
    //printf("Want to remove: ");
    //Printvalue(value);

    Value** inspect_ptr = &root;
    Value* inspect;
    while (inspect = *inspect_ptr)
    {
        slTreeNodeData<Value>* inspect_data = &(inspect->*embedded_nodedata);

        int comparison = Compare(&(inspect->*embedded_key),&(value->*embedded_key));

        // 0 means this is the value to remove
        if (!comparison)
        {
            total_values--;

            Value* immediate_left_value = inspect_data->left;
            Value* immediate_right_value = inspect_data->right;
            if (!immediate_left_value && !immediate_right_value)
            {
                //printf("leaf node removal\n");
                *inspect_ptr = NULL;
            }
            else if (immediate_left_value && !immediate_right_value)
            {
                //printf("basic replace with left node\n");
                *inspect_ptr = immediate_left_value;
            }
            else if (!immediate_left_value && immediate_right_value)
            {
                //printf("basic replace with right node\n");
                *inspect_ptr = immediate_right_value;
            }
            else
            {
                // See if either the left or right node, each of which exists,
                // can immediately be moved up. This requires them to not have
                // any subtree that is closer to the removed node's value.
                // The right one would be handled by the generic loop below, but
                // we'd have to deal with it as a slightly weird edge case.
                // Also checking the left one can be a slight optimization since
                // we wouldn't consider it otherwise with the generic loop.
                slTreeNodeData<Value>* immediate_left_data = &(immediate_left_value->*embedded_nodedata);
                slTreeNodeData<Value>* immediate_right_data = &(immediate_right_value->*embedded_nodedata);
                if (!immediate_left_data->right)
                {
                    //printf("special replace with left node\n");
                    *inspect_ptr = immediate_left_value;
                    immediate_left_data->right = immediate_right_value;
                }
                else if (!immediate_right_data->left)
                {
                    //printf("special replace with right node\n");
                    *inspect_ptr = immediate_right_value;
                    immediate_right_data->left = immediate_left_value;
                }
                else
                {
                    //printf("replace with leftmost node from right subtree\n");

                    // Find the right subtree's closest value and put it here.
                    Value** subtree_inspect_ptr = &inspect_data->right;
                    Value* subtree_inspect;
                    while (subtree_inspect = *subtree_inspect_ptr) // Note: this should never be what breaks the loop.
                    {
                        slTreeNodeData<Value>* subtree_inspect_data = &(subtree_inspect->*embedded_nodedata);
                        if (subtree_inspect_data->left)
                        {
                            subtree_inspect_ptr = &subtree_inspect_data->left;
                        }
                        else
                        {
                            // Make the pointer to the removed value point at the node
                            // that we are moving into place.
                            *inspect_ptr = subtree_inspect;

                            // If there was a right subtree of the node that's moving,
                            // this will make it the left subtree of the parent of the
                            // moved node. If there wasn't a right subtree, this also
                            // clear the left pointer to NULL instead of still pointing
                            // at the moved node.
                            *subtree_inspect_ptr = subtree_inspect_data->right;

                            // The left and right pointers of the moved node need to be
                            // copied from the node that's getting removed.
                            *subtree_inspect_data = *inspect_data;

                            break;
                        }
                    }
                }
            }

            return;
        }

        // 1 means value > *inspect_ptr, so would be to the right
        // -1 means value < *inspect_ptr, so would be to the left
        if (comparison > 0) inspect_ptr = &(inspect_data->right);
        else if (comparison < 0) inspect_ptr = &(inspect_data->left);
    }

    printf("Can't remove value because it wasn't in the TreeMap\n");

    *inspect_ptr = value;
    slTreeNodeData<Value>* value_data = &(value->*embedded_nodedata);
    value_data->left = NULL;
    value_data->right = NULL;
}
template <class Key, class Value>
Value* slTreeMap<Key,Value>::Lookup (Key* key)
{
    Value* inspect = root;
    while (inspect)
    {
        int comparison = Compare(&(inspect->*embedded_key),key);

        // 0 means an exact match
        if (!comparison) return inspect;

        // 1 means value > *inspect_ptr, so would be to the right
        // -1 means value < *inspect_ptr, so would be to the left
        slTreeNodeData<Value>* inspect_data = &(inspect->*embedded_nodedata);
        if (comparison > 0) inspect = inspect_data->right;
        else if (comparison < 0) inspect = inspect_data->left;
    }
    return NULL;
}
template <class Key, class Value>
void slTreeMap<Key,Value>::PrintInOrder (Value* local_root, int depth, char pre)
{
    if (!depth) printf("<begin tree set...>\n");
    if (local_root)
    {
        slTreeNodeData<Value>* data = &(local_root->*embedded_nodedata);
        PrintInOrder(data->left, depth + 1, 0xDA);
        for (int i = 0; i < depth; i++) putchar(' ');
        putchar(pre);
        Printvalue(local_root);
        PrintInOrder(data->right, depth + 1, 0xC0);
    }
    if (!depth) printf("<...end tree set>\n");
}
template <class Key, class Value>
int slTreeMap<Key,Value>::Recount (Value* local_root)
{
    if (local_root)
    {
        slTreeNodeData<Value>* data = &(local_root->*embedded_nodedata);
        return Recount(data->left) + 1 + Recount(data->right);
    }
    else return 0;
}
