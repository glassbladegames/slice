#pragma once

#include <slice.h>



class slAsyncNonblockingQueue
{
    SDL_mutex* sync;
    int offsetof_next;
    void* first = NULL;
    void** last_ptr = &first;

public:

    slAsyncNonblockingQueue (int offsetof_next);
    ~slAsyncNonblockingQueue ();

    void Push (void* item); // Do not pass NULL to this.
    void* Get (); // Will immediately return NULL if there is nothing.
};

class slAsyncBlockingQueue : slAsyncNonblockingQueue
{
    SDL_sem* waiting;

public:

    slAsyncBlockingQueue (int offsetof_next);
    ~slAsyncBlockingQueue ();

    void Push (void* item); // Push(NULL) will safely post to the semaphore.
    void* Get (); // Will wait until a corresponding Push has occurred.
};

class slConsumerQueue : slAsyncBlockingQueue
{
    int consumer_count;
    SDL_atomic_t next_consumer_id = {0};
    SDL_sem* shutdown_complete;

public:

    slConsumerQueue (int offsetof_next, int consumer_count = 1);
    void Start ();            // Example: (MyQueue = new MyQueueProto())->Start();
    slConsumerQueue* Stop (); // Example: delete MyQueue->Stop();
    virtual ~slConsumerQueue ();
    inline int GetConsumerCount () slForceInline { return consumer_count; }

    void Push (void* item); // Do not pass NULL to this.

protected:

    static __thread int consumer_id;

private:

    // Allow setup & cleanup around the loop.
    virtual void PreLoop ();
    virtual void PostLoop ();
    virtual void ItemFunc (void* item) = 0;

    void Consume ();
    static void Consume_Wrap (slConsumerQueue* self);
};
