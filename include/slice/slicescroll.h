#pragma once
struct scrScroll
{
	slBox* container;
	slBox* upbutton;
	slBox* downbutton;
	slSlider* bar;
	void (*update) (scrScroll* scroll);
	slList boxes;
	slBU _index_,_alloc_index_;
	slBU itemsshown;
	slBU offset;
	slScalar barwidth;
	bool visi;
	bool autohidebar;
	void SetVisible (bool visible);
	void AddBox (slBox* box);
	void RemoveBox (slBox* box);
	void ScrollTo (slBU row);
	//slBU GetRow (slBox* box);
	inline void ScrollToEnd () slForceInline { ScrollTo(boxes.itemcount - 1); }
	void Reposition ();
	void ApplyOffset ();
	void ScrollUp ();
	void ScrollDown ();
};

scrScroll* scrCreateScroll (slBox* container, slBU itemsshown, slScalar barwidth = 0.02);
void scrDestroyScroll (scrScroll* scroll);

namespace sl_internal
{
	scrScroll* scrGetHoveredScroll (slVec2 mousepos);
	void scrInit ();
	void scrQuit ();
}
