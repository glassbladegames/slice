#ifndef SLICELINES_H_INCLUDED
#define SLICELINES_H_INCLUDED


namespace sl_internal
{
	void slLinesInit ();
	void slLinesQuit ();
}
struct slLine
{
	GLvec2 p0,p1;
};
struct slCoordsTransform
{
	GLvec2 view_xy;
	GLvec2 view_wh;
	GLfloat view_rotate;
};
slCoordsTransform slUICoordsTransform ();
slCoordsTransform slUIAspectCoordsTransform (); // provides "maintainaspect" behavior
struct slLineGroup
{
	slLine* lines_buf;
	slBU lines_capacity;
	slBU lines_usage;
	slBU stale_start;
	GLuint vao,glbuf;
	slBU glbuf_capacity;
	slZHook zhook;
	SDL_Color color;
	bool visible;
	slCoordsTransform (*get_transform) (); // defaults to slUICoordsTransform

	void Clear ();
	void Draw (GLvec2 p0, GLvec2 p1);
};
slLineGroup* slCreateLineGroup (Uint8 z, SDL_Color color = {255,255,255,255});
void slDestroyLineGroup (slLineGroup* group);

#endif // SLICELINES_H_INCLUDED
