#ifndef SLICEANIM_H_INCLUDED
#define SLICEANIM_H_INCLUDED


struct animAnimation
{
	void* reftoset;
	bool ref_is_swapchain;
	slTexture** frames;
	slBU _index_;
	slBU _alloc_index_;
	slBU framecount;
	slScalar pos;
	slScalar framerate;
};
//animAnimation* animCreateAnimation (void* reftoset, char* path_fmtstr, slBU framecount, slScalar framerate, bool ref_is_swapchain); // reftoset should be slTexture** or slTexSwapChain*.
animAnimation* animCreateAnimation (slTexture** reftoset, char* path_fmtstr, slBU framecount, slScalar framerate);
animAnimation* animCreateAnimation (slTexSwapChain* swapchain, char* path_fmtstr, slBU framecount, slScalar framerate);
void animDestroyAnimation (animAnimation* anim);
namespace sl_internal
{
	void animInit ();
	void animQuit ();
}


#endif // SLICEANIM_H_INCLUDED
