#include <slice/slTreeSet.h>

template <class Value>
Value* slTreeSet<Value>::Insert (Value* item)
{
    Value* parent = NULL;
    Value** inspect_ptr = &root;
    Value* inspect;
    while (inspect = *inspect_ptr)
    {
        int comparison = Compare(inspect,item);

        // 0 means item is a duplicate, which is forbidden
        if (!comparison)
        {
            printf("Can't insert item because it is already in the TreeSet\n");
            return NULL;
        }

        parent = inspect;
        // 1 means item > *inspect_ptr, so should go on the right
        // -1 means item < *inspect_ptr, so should go on the left
        slTreeNodeData<Value>* inspect_data = GetTreeData(inspect);
        if (comparison > 0) inspect_ptr = &inspect_data->right;
        else if (comparison < 0) inspect_ptr = &inspect_data->left;
    }

    total_items++;
    *inspect_ptr = item;
    slTreeNodeData<Value>* item_data = GetTreeData(item);
    item_data->left = NULL;
    item_data->right = NULL;
    return parent;
}

template <class Value>
void slTreeSet<Value>::Remove (Value* item)
{
    //printf("Want to remove: ");
    //PrintItem(item);

    void** inspect_ptr = &root;
    void* inspect;
    while (inspect = *inspect_ptr)
    {
        slTreeNodeData<Value>* inspect_data = GetTreeData(inspect);

        int comparison = Compare(inspect,item);

        // 0 means this is the item to remove
        if (!comparison)
        {
            total_items--;

            if (!inspect_data->left && !inspect_data->right)
            {
                //printf("leaf node removal\n");
                *inspect_ptr = NULL;
            }
            else if (inspect_data->left && !inspect_data->right)
            {
                //printf("basic replace with left node\n");
                *inspect_ptr = inspect_data->left;
            }
            else if (!inspect_data->left && inspect_data->right)
            {
                //printf("basic replace with right node\n");
                *inspect_ptr = inspect_data->right;
            }
            else
            {
                // See if either the left or right node, each of which exists,
                // can immediately be moved up. This requires them to not have
                // any subtree that is closer to the removed node's value.
                // The right one would be handled by the generic loop below, but
                // we'd have to deal with it as a slightly weird edge case.
                // Also checking the left one can be a slight optimization since
                // we wouldn't consider it otherwise with the generic loop.
                slTreeNodeData<Value>* immediate_left_data = GetTreeData(inspect_data->left);
                slTreeNodeData<Value>* immediate_right_data = GetTreeData(inspect_data->right);
                if (!immediate_left_data->right)
                {
                    //printf("special replace with left node\n");
                    *inspect_ptr = inspect_data->left;
                    immediate_left_data->right = inspect_data->right;
                }
                else if (!immediate_right_data->left)
                {
                    //printf("special replace with right node\n");
                    *inspect_ptr = inspect_data->right;
                    immediate_right_data->left = inspect_data->left;
                }
                else
                {
                    //printf("replace with leftmost node from right subtree\n");

                    // Find the right subtree's closest value and put it here.
                    void** subtree_inspect_ptr = &inspect_data->right;
                    void* subtree_inspect;
                    while (subtree_inspect = *subtree_inspect_ptr) // Note: this should never be what breaks the loop.
                    {
                        slTreeNodeData<Value>* subtree_inspect_data = GetTreeData(subtree_inspect);
                        if (subtree_inspect_data->left)
                        {
                            subtree_inspect_ptr = &subtree_inspect_data->left;
                        }
                        else
                        {
                            // Make the pointer to the removed item point at the node
                            // that we are moving into place.
                            *inspect_ptr = subtree_inspect;

                            // If there was a right subtree of the node that's moving,
                            // this will make it the left subtree of the parent of the
                            // moved node. If there wasn't a right subtree, this also
                            // clear the left pointer to NULL instead of still pointing
                            // at the moved node.
                            *subtree_inspect_ptr = subtree_inspect_data->right;

                            // The left and right pointers of the moved node need to be
                            // copied from the node that's getting removed.
                            *subtree_inspect_data = *inspect_data;

                            break;
                        }
                    }
                }
            }

            return;
        }

        // 1 means item > *inspect_ptr, so would be to the right
        // -1 means item < *inspect_ptr, so would be to the left
        if (comparison > 0) inspect_ptr = &inspect_data->right;
        else if (comparison < 0) inspect_ptr = &inspect_data->left;
    }

    printf("Can't remove item because it wasn't in the TreeSet\n");

    *inspect_ptr = item;
    slTreeNodeData<Value>* item_data = GetTreeData(item);
    item_data->left = NULL;
    item_data->right = NULL;
}

template <class Value>
int slTreeSet<Value>::LookupCompare (Value* intree_item, Value* search_item)
{
    return Compare(intree_item, search_item);
}

template <class Value>
Value* slTreeSet<Value>::Lookup (Value* item)
{
    void* inspect = root;
    while (inspect)
    {
        int comparison = LookupCompare(inspect,item);

        // 0 means an exact match
        if (!comparison) return inspect;

        // 1 means item > *inspect_ptr, so would be to the right
        // -1 means item < *inspect_ptr, so would be to the left
        slTreeNodeData<Value>* inspect_data = GetTreeData(inspect);
        if (comparison > 0) inspect = inspect_data->right;
        else if (comparison < 0) inspect = inspect_data->left;
    }
    return NULL;
}

template <class Value>
void slTreeSet<Value>::PrintInOrder (Value* local_root, int depth, char pre)
{
    if (!depth) printf("<begin tree set...>\n");
    if (local_root)
    {
        slTreeNodeData<Value>* data = GetTreeData(local_root);
        PrintInOrder(data->left, depth + 1, 0xDA);
        for (int i = 0; i < depth; i++) putchar(' ');
        putchar(pre);
        PrintItem(local_root);
        PrintInOrder(data->right, depth + 1, 0xC0);
    }
    if (!depth) printf("<...end tree set>\n");
}

template <class Value>
int slTreeSet<Value>::Recount (Value* local_root)
{
    if (local_root)
    {
        slTreeNodeData<Value>* data = GetTreeData(local_root);
        return Recount(data->left) + 1 + Recount(data->right);
    }
    else return 0;
}
