#ifndef SLICEKEYBINDS_H_INCLUDED
#define SLICEKEYBINDS_H_INCLUDED

#define slKeyInfo_Unbound 0
#define slKeyInfo_KeyCode 1
#define slKeyInfo_MouseButton 2
#define slKeyInfo_WheelAction 3
#define slWheelForward 1
#define slWheelBackward 0
struct slKeyInfo
{
	union
	{
		Uint32 keycode;
		Uint8 mousebutton;
		Uint8 wheelaction;
	}
	id;
	Uint8 type;
};
#define slKeyCode(id) ((slKeyInfo){{.keycode = (id)},slKeyInfo_KeyCode})
#define slMouseButton(id) ((slKeyInfo){{.mousebutton = (id)},slKeyInfo_MouseButton})
#define slWheelAction(id) ((slKeyInfo){{.wheelaction = (id)},slKeyInfo_WheelAction})
#define slUnbound() ((slKeyInfo){{0},slKeyInfo_Unbound})
struct slKeyBind
{
	char* name;
	void (*onpress) (slKeyBind*);
	void (*onrelease) (slKeyBind*);
	slKeyInfo _info_;
	void SetInfo (slKeyInfo info);
	bool down;
};
slKeyBind* slGetKeyBind (char* name, slKeyInfo dflt = slUnbound());
char* slDescribeKeyInfo (slKeyInfo info); // Returns a newly allocated string.



// The function is called when any key bind is create or an existing one is modified.
// Upon registering, it will also be called on every existing bind to "catch up" to the present moment.
// Value of "is_new" is true for any key bind the callback hasn't been called for yet.
// It differentiates 'create' versus 'modify' events, not whether the 'create' event happened prior to callback registration.
// Listening can be helpful for implementing options menus as well as updating tool tips associated with specific key binds.
void slListenKeyBinds (void (*on_event) (slKeyBind* bind, bool is_new));

// Function will no longer be called on key bind creation and modification.
void slUnlistenKeyBinds (void (*on_event) (slKeyBind* bind, bool is_new));



bool slBoundToSame (slKeyBind* bind0, slKeyBind* bind1);

namespace sl_internal
{
	void slCheckBoundKeysDown (slKeyInfo event);
	void slCheckBoundKeysUp (slKeyInfo event);

	void slLoadKeys (FILE* keysfile);
	void slSaveKeys (FILE* keysfile);

	void slKeysQuit ();
}

#endif // SLICEKEYBINDS_H_INCLUDED
