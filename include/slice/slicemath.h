#ifndef SLICEMATH_H_INCLUDED
#define SLICEMATH_H_INCLUDED



//#define SLICE_USE_FLOAT



typedef Uint64 slBU;
typedef Sint64 slBS;
#include <math.h>

#ifdef SLICE_USE_FLOAT
	typedef float slScalar;

	#define slfmax fmaxf
	#define slfmin fminf
	#define slfabs fabsf
	#define slfloor floorf
	#define slceil ceilf
	#define slfmod fmodf
	#define slpow powf
	#define slsqrt sqrtf

	#define slsin sinf
	#define slcos cosf
	#define sltan tanf
	#define slasin asinf
	#define slacos acosf
	#define slatan atanf
	#define slatan2 atan2f

	#define slcopysign copysignf

	#define SL_PI M_PI_F
#else
	typedef double slScalar;

	#define slfmax fmax
	#define slfmin fmin
	#define slfabs fabs
	#define slfloor floor
	#define slceil ceil
	#define slfmod fmod
	#define slpow pow
	#define slsqrt sqrt

	#define slsin sin
	#define slcos cos
	#define sltan tan
	#define slasin asin
	#define slacos acos
	#define slatan atan
	#define slatan2 atan2

	#define slcopysign copysign

	#define SL_PI M_PI
#endif
#define M_PI_F ((float)M_PI)
#define fclamp(value,lower,upper) fmin(fmax(value,lower),upper)
#define fclampf(value,lower,upper) fminf(fmaxf(value,lower),upper)
#define slfclamp(value,lower,upper) slfmin(slfmax(value,lower),upper)

const slScalar slDegConv = 180 / M_PI;
const slScalar slRadConv = M_PI / 180;
#define slRadToDeg(n) ((n) * slDegConv)
#define slDegToRad(n) ((n) * slRadConv)
const float slDegConv_F = 180 / M_PI;
const float slRadConv_F = M_PI / 180;
#define slRadToDeg_F(n) ((n) * slDegConv_F)
#define slDegToRad_F(n) ((n) * slRadConv_F)



//typedef slScalar slVec2 __attribute__((vector_size(sizeof(slScalar)*2)));
//typedef GLfloat slQuadGLfloat __attribute__((vector_size(sizeof(GLfloat)*4)));
#ifndef SLICE_USE_FLOAT
struct GLvec2;
#endif
struct slInt2;



typedef slScalar slRawVec2 __attribute__((vector_size(sizeof(slScalar)*2)));
struct slVec2
{
	union
	{
		slRawVec2 xy,wh;
		struct { slScalar x,y; };
		struct { slScalar w,h; };
	};
	slVec2 () slForceInline {}
	slVec2 (slRawVec2 raw) slForceInline : xy(raw) {}
	slVec2 (slScalar _x, slScalar _y) slForceInline : xy((slRawVec2){_x,_y}) {}
	slVec2 (slScalar both) slForceInline : xy((slRawVec2){both,both}) {}
	slVec2 operator+ (slVec2 other) slForceInline { return slVec2(xy + other.xy); }
	slVec2 operator- (slVec2 other) slForceInline { return slVec2(xy - other.xy); }
	slVec2 operator* (slVec2 other) slForceInline { return slVec2(xy * other.xy); }
	slVec2 operator/ (slVec2 other) slForceInline { return slVec2(xy / other.xy); }
	slVec2 operator+ (slScalar add) slForceInline { return slVec2(xy + add); }
	slVec2 operator- (slScalar minus) slForceInline { return slVec2(xy - minus); }
	slVec2 operator* (slScalar scale) slForceInline { return slVec2(xy * scale); }
	slVec2 operator/ (slScalar scale) slForceInline { return slVec2(xy / scale); }
	slVec2 operator+= (slVec2 other) slForceInline { xy += other.xy; return *this; }
	slVec2 operator-= (slVec2 other) slForceInline { xy -= other.xy; return *this; }
	slVec2 operator*= (slVec2 other) slForceInline { xy *= other.xy; return *this; }
	slVec2 operator/= (slVec2 other) slForceInline { xy /= other.xy; return *this; }
	slVec2 operator+= (slScalar add) slForceInline { xy += add; return *this; }
	slVec2 operator-= (slScalar minus) slForceInline { xy -= minus; return *this; }
	slVec2 operator*= (slScalar scale) slForceInline { xy *= scale; return *this; }
	slVec2 operator/= (slScalar scale) slForceInline { xy /= scale; return *this; }
	slVec2 operator- () slForceInline { return slVec2(-xy); }

	/*
	inline bool operator<= (slVec2 other) slForceInline { return xy >= other.xy; }
	inline bool operator>= (slVec2 other) slForceInline { return xy <= other.xy; }
	inline bool operator< (slVec2 other) slForceInline { return xy < other.xy; }
	inline bool operator> (slVec2 other) slForceInline { return xy > other.xy; }
	*/

	inline bool operator<= (slVec2 other) slForceInline { return x <= other.x && y <= other.y; }
	inline bool operator>= (slVec2 other) slForceInline { return x >= other.x && y >= other.y; }
	inline bool operator< (slVec2 other) slForceInline { return x < other.x && y < other.y; }
	inline bool operator> (slVec2 other) slForceInline { return x > other.x && y > other.y; }

	slScalar len2 () slForceInline
	{
		slRawVec2 sqrd = xy * xy;
		return sqrd[0] + sqrd[1];
	}
	slScalar len () slForceInline { return slsqrt(len2()); }
	slScalar dist2 (slVec2 other) slForceInline
	{
		//return (*this - other).len2(); }
		slRawVec2 sqrd = (xy - other.xy);
		sqrd *= sqrd;
		return sqrd[0] + sqrd[1];
	}
	slScalar dist (slVec2 other) slForceInline { return slsqrt(dist2(other)); }
	static inline slVec2 FromTCD (slScalar angle) slForceInline // Top Clockwise Degrees
	{
		angle = slDegToRad(90 - angle);
		return slVec2(slcos(angle),slsin(angle));
	}
	inline slScalar TCD () slForceInline // Top Clockwise Degrees
	{
		return 90 - slRadToDeg(slatan2(y,x));
	}
    inline slVec2 RotatedByTCD (slScalar angle) slForceInline // Top Clockwise Degrees
    {
        return FromTCD(angle) * y + FromTCD(angle + 90) * x;
    }
	inline slScalar Area () slForceInline { return w * h; }
	inline slScalar Perimeter () slForceInline { return (w + h) * 2; }



	#ifndef SLICE_USE_FLOAT
	inline slVec2 (GLvec2 vec) slForceInline;
	#endif
	inline slVec2 (slInt2 vec) slForceInline;
};



#ifdef SLICE_USE_FLOAT
typedef slRawVec2 GLvec2_raw;
typedef slVec2 GLvec2;
#else
typedef GLfloat GLvec2_raw __attribute__((vector_size(sizeof(GLfloat)*2)));
struct GLvec2
{
	union
	{
		GLvec2_raw xy,wh;
		struct { GLfloat x,y; };
		struct { GLfloat w,h; };
	};
	GLvec2 () slForceInline {}
	GLvec2 (GLvec2_raw raw) slForceInline : xy(raw) {}
	GLvec2 (GLfloat _x, GLfloat _y) slForceInline : xy((GLvec2_raw){_x,_y}) {}
	GLvec2 (GLfloat both) slForceInline : xy((GLvec2_raw){both,both}) {}
	GLvec2 operator+ (GLvec2 other) slForceInline { return GLvec2(xy + other.xy); }
	GLvec2 operator- (GLvec2 other) slForceInline { return GLvec2(xy - other.xy); }
	GLvec2 operator* (GLvec2 other) slForceInline { return GLvec2(xy * other.xy); }
	GLvec2 operator/ (GLvec2 other) slForceInline { return GLvec2(xy / other.xy); }
	GLvec2 operator+ (GLfloat add) slForceInline { return GLvec2(xy + add); }
	GLvec2 operator- (GLfloat minus) slForceInline { return GLvec2(xy - minus); }
	GLvec2 operator* (GLfloat scale) slForceInline { return GLvec2(xy * scale); }
	GLvec2 operator/ (GLfloat scale) slForceInline { return GLvec2(xy / scale); }
	GLvec2 operator+= (GLvec2 other) slForceInline { xy += other.xy; return *this; }
	GLvec2 operator-= (GLvec2 other) slForceInline { xy -= other.xy; return *this; }
	GLvec2 operator*= (GLvec2 other) slForceInline { xy *= other.xy; return *this; }
	GLvec2 operator/= (GLvec2 other) slForceInline { xy /= other.xy; return *this; }
	GLvec2 operator+= (GLfloat add) slForceInline { xy += add; return *this; }
	GLvec2 operator-= (GLfloat minus) slForceInline { xy -= minus; return *this; }
	GLvec2 operator*= (GLfloat scale) slForceInline { xy *= scale; return *this; }
	GLvec2 operator/= (GLfloat scale) slForceInline { xy /= scale; return *this; }
	GLvec2 operator- () slForceInline { return GLvec2(-xy); }

	/*
	inline bool operator<= (GLvec2 other) slForceInline { return xy >= other.xy; }
	inline bool operator>= (GLvec2 other) slForceInline { return xy <= other.xy; }
	inline bool operator< (GLvec2 other) slForceInline { return xy < other.xy; }
	inline bool operator> (GLvec2 other) slForceInline { return xy > other.xy; }
	*/

	inline bool operator<= (GLvec2 other) slForceInline { return x <= other.x && y <= other.y; }
	inline bool operator>= (GLvec2 other) slForceInline { return x >= other.x && y >= other.y; }
	inline bool operator< (GLvec2 other) slForceInline { return x < other.x && y < other.y; }
	inline bool operator> (GLvec2 other) slForceInline { return x > other.x && y > other.y; }

	GLfloat len2 () slForceInline
	{
		GLvec2_raw sqrd = xy * xy;
		return sqrd[0] + sqrd[1];
	}
	GLfloat len () slForceInline { return sqrtf(len2()); }
	GLfloat dist2 (GLvec2 other) slForceInline
	{
		//return (*this - other).len2(); }
		GLvec2_raw sqrd = (xy - other.xy);
		sqrd *= sqrd;
		return sqrd[0] + sqrd[1];
	}
	GLfloat dist (GLvec2 other) slForceInline { return sqrtf(dist2(other)); }
	static inline GLvec2 FromTCD (GLfloat angle) slForceInline // Top Clockwise Degrees
	{
		angle = slDegToRad_F(90 - angle);
		return GLvec2(cosf(angle),sinf(angle));
	}
	inline GLfloat TCD () slForceInline // Top Clockwise Degrees
	{
		return 90 - slRadToDeg_F(atan2f(y,x));
	}
	inline GLfloat Area () slForceInline { return w * h; }
	inline GLfloat Perimeter () slForceInline { return (w + h) * 2; }



	GLvec2 (slVec2 vec) slForceInline
	{
		float out4 [4];
		_mm_store_ps(out4,_mm_cvtpd_ps(_mm_load_pd((const double*)&vec)));
		*this = *(GLvec2*)out4;
	}
	inline GLvec2 (slInt2 vec) slForceInline;
};
slVec2::slVec2 (GLvec2 vec)
{
	float in4 [4];
	*(GLvec2*)in4 = vec;
	_mm_store_pd((double*)this,_mm_cvtps_pd(_mm_load_ps(in4)));
}
#endif



typedef slBS slRawInt2 __attribute__((vector_size(sizeof(slBS)*2)));
struct slInt2
{
	//protected:
	union
	{
		slRawInt2 xy,wh;
		struct { slBS x,y; };
		struct { slBS w,h; };
	};
	//public:
	slInt2 () slForceInline {}
	slInt2 (slRawInt2 raw) slForceInline : xy(raw) {}
	slInt2 (slBS _x, slBS _y) slForceInline : xy((slRawInt2){_x,_y}) {}
	slInt2 (slBS both) slForceInline : xy((slRawInt2){both,both}) {}
	slInt2 operator+ (slInt2 other) slForceInline { return slInt2(xy + other.xy); }
	slInt2 operator- (slInt2 other) slForceInline { return slInt2(xy - other.xy); }
	slInt2 operator* (slInt2 other) slForceInline { return slInt2(xy * other.xy); }
	slInt2 operator/ (slInt2 other) slForceInline { return slInt2(xy / other.xy); }
	slInt2 operator+ (slBS add) slForceInline { return slInt2(xy + add); }
	slInt2 operator- (slBS minus) slForceInline { return slInt2(xy - minus); }
	slInt2 operator* (slBS scale) slForceInline { return slInt2(xy * scale); }
	slInt2 operator/ (slBS scale) slForceInline { return slInt2(xy / scale); }
	slInt2 operator+= (slInt2 other) slForceInline { xy += other.xy; return *this; }
	slInt2 operator-= (slInt2 other) slForceInline { xy -= other.xy; return *this; }
	slInt2 operator*= (slInt2 other) slForceInline { xy *= other.xy; return *this; }
	slInt2 operator/= (slInt2 other) slForceInline { xy /= other.xy; return *this; }
	slInt2 operator+= (slBS add) slForceInline { xy += add; return *this; }
	slInt2 operator-= (slBS minus) slForceInline { xy -= minus; return *this; }
	slInt2 operator*= (slBS scale) slForceInline { xy *= scale; return *this; }
	slInt2 operator/= (slBS scale) slForceInline { xy /= scale; return *this; }
	slInt2 operator- () slForceInline { return slInt2(-xy); }

	/*
	inline bool operator== (slInt2 other) slForceInline { return xy == other.xy; }
	inline bool operator!= (slInt2 other) slForceInline { return xy != other.xy; }
	inline bool operator<= (slInt2 other) slForceInline { return xy >= other.xy; }
	inline bool operator>= (slInt2 other) slForceInline { return xy <= other.xy; }
	inline bool operator< (slInt2 other) slForceInline { return xy < other.xy; }
	inline bool operator> (slInt2 other) slForceInline { return xy > other.xy; }
	*/

	inline bool operator== (slInt2 other) slForceInline { return x == other.x && y == other.y; }
	inline bool operator!= (slInt2 other) slForceInline { return x != other.x || y != other.y; }
	//inline bool operator<= (slInt2 other) slForceInline { return x <= other.x && y <= other.y; }
	//inline bool operator>= (slInt2 other) slForceInline { return x >= other.x && y >= other.y; }
	//inline bool operator< (slInt2 other) slForceInline { return x < other.x && y < other.y; }
	//inline bool operator> (slInt2 other) slForceInline { return x > other.x && y > other.y; }

	slBS len2 () slForceInline
	{
		slRawInt2 sqrd = xy * xy;
		return sqrd[0] + sqrd[1];
	}
	slBS len () slForceInline { return slsqrt(len2()); }
	slBS dist2 (slInt2 other) slForceInline
	{
		//return (*this - other).len2(); }
		slRawInt2 sqrd = (xy - other.xy);
		sqrd *= sqrd;
		return sqrd[0] + sqrd[1];
	}
	slBS dist (slInt2 other) slForceInline { return slsqrt(dist2(other)); }
	inline slInt2 Rotate (Uint8 turns) slForceInline // Each turn is 90 degrees clockwise.
	{
		switch (turns & 3)
		{
			case 1: // 1,4 -> 4,-1
			return slInt2(y,-x);

			case 2: // 1,4 -> -1,-4
			return -*this;

			case 3: // 1,4 -> -4,1
			return slInt2(-y,x);

			default: // No Change
			return *this;
		}
	}
	inline slInt2 RotateAround (Uint8 turns, slInt2 around) slForceInline
	{
		return (*this - around).Rotate(turns) + around;
	}
	inline slBS Area () slForceInline { return w * h; }
	inline slBS Perimeter () slForceInline { return (w + h) * 2; }



	inline slInt2 (slVec2 vec) slForceInline
	{
		// To do eventually: replace scalar code with SIMD intrinsics.
		x = vec.x;
		y = vec.y;
	}
	#ifndef SLICE_USE_FLOAT
	inline slInt2 (GLvec2 vec) slForceInline
	{
		// To do eventually: replace scalar code with SIMD intrinsics.
		x = vec.x;
		y = vec.y;
	}
	#endif
};
slVec2::slVec2 (slInt2 vec)
{
	// To do eventually: replace scalar code with SIMD intrinsics.
	x = vec.x;
	y = vec.y;
}
GLvec2::GLvec2 (slInt2 vec)
{
	// To do eventually: replace scalar code with SIMD intrinsics.
	x = vec.x;
	y = vec.y;
}



#define slInv255 (1.f/255)

Uint8 slClamp255 (slScalar n);

void slAspectifyPoint (slScalar* x, slScalar* y);
void slDeAspectifyPoint (slScalar* x, slScalar* y);
void slAspectifyPoint (slVec2* point);
void slDeAspectifyPoint (slVec2* point);


void slRotatePoint (slScalar* x, slScalar* y, slScalar ang);
slVec2 slRotatePoint (slVec2 point, slScalar ang);
void slRotatePointAroundPoint (slScalar* x, slScalar* y, slScalar ang, slScalar center_x, slScalar center_y);



/*struct SDL_Color_Vec
{
	union
	{
		SDL_Color sdl_color;
		Uint8 rgba_vec __attribute__((vector_size(sizeof(Uint8)*4)));
	};
	inline SDL_Color_Vec (SDL_Color color) slForceInline { sdl_color = color; }
};*/
struct GLrgba_f
{
	union
	{
		struct { GLfloat r,g,b,a; };
		GLfloat rgba_vec __attribute__((vector_size(sizeof(GLfloat)*4)));
	};
	inline GLrgba_f () slForceInline {} // No-initialization constructor.
	inline GLrgba_f (SDL_Color in) slForceInline
	{
		// Use obnoxious intrinsic to do all uint8->float conversions at once.
		/*union
		{
			__m64 bytes;
			SDL_Color / *high_pad,* /low;
		};
		low = in;
		_mm_store_ps((float*)&rgba_vec,_mm_cvtpu8_ps(bytes));*/
		// Actually this doesn't seem to work properly anyway.

		// Scalar equivalent of the above code:
		r = in.r; g = in.g; b = in.b; a = in.a;



		rgba_vec *= slInv255; // Scale 255 to 1.f
	}
};
struct GLxywh { GLfloat x,y,w,h; };
inline slForceInline void slUniformFromColor (GLuint location, SDL_Color color)
{
	GLrgba_f color_f = color;
	glUniform4fv(location,1,(GLfloat*)&color_f);
}



inline slForceInline slBS slRound (slScalar n)
{
	return n + slcopysign(0.5,n);
}

inline slForceInline slScalar sl0360 (slScalar ang)
{
	ang = slfmod(ang,360);
	//if (ang < 0) ang += 360;
	if (signbit(ang)) ang += 360;
	return ang;
}

#endif // SLICEMATH_H_INCLUDED
