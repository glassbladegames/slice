#ifndef SLICETEXT_EXT_H_INCLUDED
#define SLICETEXT_EXT_H_INCLUDED

#define txtAlignLeft 0
#define txtAlignCenter 1
#define txtAlignRight 2
slTexRef txtRenderMultiline (char* text, Uint8 align = txtAlignCenter, SDL_Color color = {255,255,255,255}, slFont* font = slLoadFont(slGetDefaultFontPath()));


#endif // SLICETEXT_EXT_H_INCLUDED
