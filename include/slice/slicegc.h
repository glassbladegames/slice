#ifndef SLICEGC_H_INCLUDED
#define SLICEGC_H_INCLUDED



struct slGC_Action
{
	void (*action_func) (void* arg);
	void* arg;
	char* name;
	void Hook (void (*action_func) (void* arg), void* arg = NULL, char* name = "(unnamed)");
	void Unhook ();
};


//void slSetCustomGCEveryFrame (bool to);
void slGC_SetInterval (slScalar to);


namespace sl_internal
{
	void slGC_Step ();
	void slCollectAllGarbageNow ();

	void slInitGC ();
	void slQuitGC ();
}

#endif // SLICEGC_H_INCLUDED
