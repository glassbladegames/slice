#ifndef SLICEBOXES_H_INCLUDED
#define SLICEBOXES_H_INCLUDED

void slSetUIAspect (slScalar ui_aspect); // Default is 1 (square).
slScalar slGetUIAspect ();

struct slZHook
{
	void (*func) (void* userdata);
	void* userdata;
	Uint8 z;

	uiBox* workaround_box;
    slFrame* workaround_frame;
};
bool slZHooksInOrder (slZHook* zhook1, slZHook* zhook2);
void slAddZHook (slZHook* zhook);
void slRemoveZHook (slZHook* zhook);

#define slBlankColor {0,0,0,0}

struct slBox
{
    uiBox* workaround_box;
    uiBox* workaround_texbox;

	slTexSwapChain texref_swapchain;
	slTexSwapChain hovertexref_swapchain;

	void* userdata;
	void (*     onclick) (slBox*) = NULL;
	void (*onrightclick) (slBox*) = NULL;
	slBU _index_;

	slScalar rotangle = 0; // Used only if `rot` is true.
	slVec2 xy = 0, wh = 0;
	bool maintainaspect = false;
	Uint8 aspect_align_x : 2 = slAlignCenter, aspect_align_y : 2 = slAlignCenter;
    struct
    {
        slScalar rotangle;
        slVec2 xy, wh = 0;
        bool maintainaspect;
    	Uint8 aspect_align_x : 2, aspect_align_y : 2;
    }
    prev_txform;
    slVec2 prev_tex_wh = 0;
    Uint8 prev_z = 127;

    slVec2 tex_wh = 1;
    slVec2 rotpoint; // Used only if `rot` is true, and `rotcenter` is false.

	SDL_Color
        backcolor = slBlankColor,
        bordercolor = slBlankColor,
        hoverbackcolor = slBlankColor,
        hoverbordercolor = slBlankColor,
        /*gapcolor = slBlankColor,*/
        texbordercolor = slBlankColor,
        hovertexbordercolor = slBlankColor;
	SDL_Color
        drawmask = {255,255,255,255},
        hoverdrawmask = {255,255,255,255};
	bool rot = false;
	bool rotcenter; // Used only if `rot` is true.
	bool hoverable = false;
	bool visible = true;

	Uint8 z = 127; // Lower is Closer
	Uint8    tex_align_x : 2 = slAlignCenter,    tex_align_y : 2 = slAlignCenter;
	Uint8 border_thickness = 1;

    // Clearing this mid-frame is dicey, try to avoid doing that.
	slBox* scissor_box = NULL; // NULL for no scissor. Pointer of another box to
                               // restrict drawing to that box's screen area.
                               // Be careful about destroying the other box if
                               // this one stays around for another frame.



	inline void SetTexRef (slTexRef texref) slForceInline
	{
        texref_swapchain.Queue(texref);
	}
	inline void SetHoverTexRef (slTexRef hovertexref) slForceInline
	{
        hovertexref_swapchain.Queue(hovertexref);
	}
	inline void SetDims (slVec2 xy, slVec2 wh, Uint8 z) slForceInline
	{
		this->xy = xy;
		this->wh = wh;
		this->z = z;
	}
	inline void SetRots (bool rot = false, slScalar rotangle = 0, bool rotcenter = true, slVec2 rotpoint = 0) slForceInline
	{
		this->rot = rot;
		this->rotangle = rotangle;
		this->rotcenter = rotcenter;
		this->rotpoint = rotpoint;
	}
	inline void AspectAnchor (Uint8 tex_align_x, Uint8 tex_align_y) slForceInline
	{
		maintainaspect = true;
		this->tex_align_x = tex_align_x;
		this->tex_align_y = tex_align_y;
	}
	void PutWithin (slBox* outer); // Does not (yet) take rotation into account.
};
slBox* slCreateBox (slTexRef tex = slNoTexture, slTexRef hovertex = slNoTexture);
void slDestroyBox (slBox* todel);

void slBoxHoverAbsorb (slBox* box);
bool slPointOnBox (slBox* box, slVec2 point);
inline void slSetBoxDims (slBox* box, slVec2 pos, slVec2 dims, Uint8 z) { box->SetDims(pos,dims,z); } // Deprecated
inline void slSetBoxDims (slBox* box, slScalar x, slScalar y, slScalar w, slScalar h, Uint8 z) { box->SetDims(slVec2(x,y),slVec2(w,h),z); } // Very Deprecated
inline void slSetBoxRots (slBox* box, bool rot = false, slScalar rotangle = 0, bool rotcenter = true, slVec2 rotpoint = slVec2(0)) { box->SetRots(rot,rotangle,rotcenter,rotpoint); } // Deprecated
inline void slRelBoxDims (slBox* box, slBox* rel) { box->PutWithin(rel); } // Deprecated
struct slCorners
{
	//slVec2 p00,p01,p10,p11;
	slScalar p00x,p00y,p01x,p01y,p10x,p10y,p11x,p11y;
};
slCorners slGetBoxCorners (slBox* item);

void slDrawUI ();
	// If you define an AppDrawStage, you must call this in it somewhere to draw the UI (this allows you to choose to draw things after UI).
	// If an AppDrawStage is not defined, slDrawUI will be called directly instead, as a default.



slBox* slGetHoveredBox (slVec2 cursor);
namespace sl_internal
{
	void slInitUI ();
	void slQuitUI ();
	bool slOnClickUI (slVec2 cursor, bool leftclick);
	void slRenderPrepareAll (slBox* hoverbox);
}
void slBindFullscreenArray ();

#endif // SLICEBOXES_H_INCLUDED
