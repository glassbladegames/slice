#pragma once

#include <slice.h>

template <class Value>
struct slLLNodeData
{
    Value* next;
    Value* prev;
};

template <class Value>
class slLLCursor;

template <class Value>
class slLinkedList
{
    slLLNodeData<Value> Value::* embedded_nodedata;
    Value* first = NULL;
    Value* last  = NULL;
    int total_values = 0;

public:
    inline slLinkedList (slLLNodeData<Value> Value::* embedded_nodedata) slForceInline
    {
        this->embedded_nodedata = embedded_nodedata;
    }
    inline Value* First () slForceInline { return first; }
    inline Value* Last  () slForceInline { return last ; }
    inline int    Count () slForceInline { return total_values; }
    inline Value*& NextOf (Value* of) slForceInline
    {
        return (of->*embedded_nodedata).next;
    }
    inline Value*& PrevOf (Value* of) slForceInline
    {
        return (of->*embedded_nodedata).prev;
    }
    void InsertBefore (Value* value, Value* before);
    void InsertAfter  (Value* value, Value* after );
    void Prepend (Value* value);
    void Append  (Value* value);
    void Remove  (Value* value);
    void Sort (bool (*inorder) (Value* first, Value* second));
};

template <class Value>
class slLLCursor
{
    slLinkedList<Value>* list;
    Value* current;
public:
    inline slLLCursor (slLinkedList<Value>* list)
    {
        this->list = list;
        current = list->First();
    }
    inline slLLCursor (slLinkedList<Value>* list, Value* where)
    {
        this->list = list;
        current = where;
    }
    inline Value* Here () slForceInline
    {
        return current;
    }
    inline void Forward () slForceInline
    {
        if (current) current = list->NextOf(current);
    }
    inline void Back () slForceInline
    {
        if (current) current = list->PrevOf(current);
    }
    inline void To (Value* value) slForceInline
    {
        current = value;
    }
};

#include <slice/slLinkedListImpl.cpp>
