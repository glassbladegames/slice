#ifndef SLICETYPINGBOXES_H_INCLUDED
#define SLICETYPINGBOXES_H_INCLUDED

#include <slice.h>
#include <vector>
struct slTypingChar
{
	char ch;
	slBox* box;
	slScalar aspect;
	slTypingChar (char ch, SDL_Color text_color, slFont* font, slBox* scissor_box, bool sensitive);
	~slTypingChar ();
};
struct slTypingText
{
	bool sensitive = false; // Replaces the text with dots, useful for sensitive information.
	std::vector<slTypingChar*> line;
	SDL_Color text_color;
	slFont* font;
	slBox* scissor_box; // Storage for the scissor of the container, which all cells have applied so they don't overflow its area.
	void InsertChar (slBU pos, char ch);
	void DeleteChar (slBU pos);
	void DeleteRangeChars (slBU del_start, slBU del_stop);
	void Clear ();
	char* GenText ();
	char* GenRangeText (slBU start, slBU stop);
};
struct slTypingBox
{
	void (*onenter) (slTypingBox* typbox); // The user hits enter.
	void (*ontextchange) (slTypingBox* typbox); // The user types something.
	void (*ontab) (slTypingBox* typbox); // The user presses tab. This usually means go to next field.
	void (*onshifttab) (slTypingBox* typbox); // The user presses tab while holding shift. This usually means go to previous field.
	void (*onselect) (slTypingBox* typbox); // The user clicks the box.
	void (*onabandon) (slTypingBox* typbox); // The user clicks outside the box.
	void* userdata;
	slBU _index_;

	slBox* container; // Bounding box that the view is within.
	slBU cursor_strpos = 0; // Current cursor position, not relative to the view offsets.
	slScalar cursor_offset = 0; // Cursor position in cell-heights. Needed since the text is not fixed-width.
	slBU select_strpos = 0; // If there is no selection, these are equal to cursor_x and cursor_y.
	slScalar select_offset = 0;
	slScalar view_offset = 0; // How far the first character box is pushed to the left.

	slBox* highlight;
	slBox* caret;
	slScalar caret_blink_elapsed;
	bool focused = false;
	slScalar caret_width = 0.04; // Relative to character height.
	slScalar caret_blink_interval = 0.5; // Seconds per visibility toggle.

	slTypingText text;
	slTexture* hint_tex = NULL;

	void SetText (char* str);
	char* GenText (); /// Returns an allocated string. You should free it when you're done with it.
	slTypingBox (slBox* container, SDL_Color text_color, slFont* font); /// Use slCreateTypingBox, not `new`.
	void SetHint (char* str, SDL_Color color = {0x7F,0x7F,0x7F,0x7F});
	void CheckCursorBounds ();
	void CursorToMouse ();
	static void OnContainerClick (slBox* box);
	void ShowCursor ();
	void Focus ();

	void HighlightStartEnd (slBU* start_strpos, slScalar* start_offset, slBU* end_strpos, slScalar* end_offset);

	void SetTextColor (SDL_Color color);

	void UpdateCells ();

	void Step ();

	char* GenSelectedText ();
	bool DeleteSelection (); // returns true if it deleted something, false if it did nothing.
	void PutCharAtCursor (char ch);
	void NoSelection ();
	void OnTextInput (int event_type, char ch);
	static void OnTextInput_Wrap (slTypingBox* typing_box, int event_type, char ch);
	~slTypingBox ();
};





namespace sl_internal
{
	void slTypingBoxesInit ();
	void slTypingBoxesQuit ();
}
slTypingBox* slCreateTypingBox (slBox* from, SDL_Color text_color = {0xFF,0xFF,0xFF,0xFF}, slFont* font = slLoadFont(slGetDefaultFontPath()));
void slDestroyTypingBox (slTypingBox* todel);




/*





#ifndef SLICETYPINGBOXES_H_INCLUDED
#define SLICETYPINGBOXES_H_INCLUDED

struct slTypingBox
{
	slBox* visual;
	char* text;
	void* userdata;
	void (*ontextchange) (slTypingBox* typbox); // The user types something.
	void (*ontextsubmit) (slTypingBox* typbox); // The user hits enter.
	void (*onselect) (slTypingBox* typbox); // The user clicks the box.
	void (*onabandon) (slTypingBox* typbox); // The user clicks outside the box.
	slBU _index_;
	SDL_Color textcolor,hovertextcolor,typingtextcolor;

	bool clearonselect;
		// Dictates whether clicking on the box should clear its text.

	void Focus (bool clear = false);
		// Allows you to enter typing mode on this box, without the user clicking on it.

	void SetText (char* new_text);
		// `new_text` is cloned, so you can free your copy.

	char* CaptureText ();
		// Sets `text` to NULL and gives you its original value.
		// Otherwise you'd have to clone `text` to safely use that string after destroying the typing box.
};
slTypingBox* slCreateTypingBox (slBox* from, bool grab = false);
void slDestroyTypingBox (slTypingBox* todel);



namespace sl_internal
{
	void slTypingBoxesInit ();
	void slTypingBoxesQuit ();
	void slTypingBoxesStep ();
	bool slTypingBoxEvent (Uint32 event_key_keysym_sym);
	void slDeselectTypingBox ();
	void slOnTypingBoxInput (char* event_text_text);
}

*/

#endif // SLICETYPINGBOXES_H_INCLUDED
