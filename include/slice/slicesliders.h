#ifndef SLICESLIDERS_H_INCLUDED
#define SLICESLIDERS_H_INCLUDED

struct slSlider
{
	slBox* behind;
	slBox* mark;
	void (*onchange) (slSlider* slider);
	void* userdata;
	slBU _index_;
	slScalar minvalue,maxvalue,curvalue;
	bool vertical,isbar,invert;
	void SetPos (slScalar to);
	void SetValue (slScalar to);
};
slSlider* slCreateSlider (slBox* behind, slBox* mark, bool vertical = false, bool isbar = false, bool invert = false);
void slDestroySlider (slSlider* slider);




namespace sl_internal
{
	void slSlidersInit ();
	void slSlidersQuit ();
	void slSlidersMouseUp (slVec2 mouse);
	void slSlidersStep (slVec2 mouse);
}

#endif // SLICESLIDERS_H_INCLUDED
