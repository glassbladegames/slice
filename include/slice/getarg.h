#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

char* GetArg (int argc, char** argv, char* prefix);
/**

	Usage:

		command line: myprogram -number_arg=5 -string_arg=HelloWorld -argwithnovalue

		C:

			if (GetArg(argc,argv,"-argwithnovalue")) DoSomething();

			char* number_arg = GetArg(argc,argv,"-number_arg=");
			slBU number = DEFAULT_NUMBER;
			if (number_arg) slScanDecU(number_arg,&number);
			SomethingWithNumber(number);

			char* string_arg = GetArg(argc,argv,"-string_arg=");
			if (!string_arg) string_arg = DEFAULT_STRING;
			SomethingWithString(string_arg);

**/

#ifdef __cplusplus
}
#endif
