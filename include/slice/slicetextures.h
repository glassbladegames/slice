#ifndef SLICETEXTURES_H_INCLUDED
#define SLICETEXTURES_H_INCLUDED



struct slFont
{
	TTF_Font* font;
	char* path;
};
slFont* slLoadFont (char* path);
char* slGetDefaultFontPath ();
void slSetDefaultFontPath (char* path);



namespace sl_internal
{
    void slTexturesInit ();
    void slTexturesQuit ();
}



class slRefCounted
{
    SDL_atomic_t usages = {0};
protected:
    inline bool IsZero () slForceInline { return !SDL_AtomicGet(&usages); }
    virtual void OnZero () = 0;
public:
    // inline slRefCounted () {
    //     printf("new refcounted %llX\n", this);
    // }
    inline void Reserve () slForceInline
    {
        SDL_AtomicIncRef(&usages);
    }
    inline void Abandon () slForceInline
    {
        if (SDL_AtomicDecRef(&usages)) {
            // printf("counted %llX reached zero\n", this);
            OnZero();
        }
    }
};
template <class Counted>
class slRef
{
protected:
    Counted* raw_ref;
public:
    inline slRef (Counted* raw_ref) slForceInline
    {
        this->raw_ref = raw_ref;
        raw_ref->Reserve();
    }
    inline slRef (const slRef& copy_from) slForceInline
    {
        raw_ref = copy_from.ReserveGetRaw();
    }
    inline slRef& operator= (const slRef& copy_from) slForceInline
    {
        Counted* prior_ref = raw_ref;

        raw_ref = copy_from.ReserveGetRaw();

        prior_ref->Abandon();

        return *this;
    }
    inline ~slRef () slForceInline
    {
        raw_ref->Abandon();
    }
    inline Counted* ReserveGetRaw () slForceInline
    {
        raw_ref->Reserve();
        return raw_ref;
    }
    inline Counted* operator-> () slForceInline
    {
        return raw_ref;
    }
    inline Counted& operator* () slForceInline
    {
        return *raw_ref;
    }
    inline bool operator!= (slRef& other) slForceInline
    {
        return raw_ref != other.raw_ref;
    }
};



#define slTextures slTexture::Textures
class slTexture : public slRefCounted
{
    slBU _index_; // in slTextures list... AKA slTexture::Textures list.
    static slList Textures;
    void OnZero () override;
    static void Shutdown ();
    friend void sl_internal::slTexturesQuit ();

protected:
    slTexture ();
    virtual ~slTexture ();
    friend void slReleaseTexture (slTexture* tex);

    virtual void Unregister (); // Happens before (separately from) destruction.
    // ^^ If you override this, make sure to call the overridden function!!

public:
	slScalar aspect; // Only is valid is dims_valid is true. If texture can't be loaded, dims_valid will still become true and this will be set to 1.
    GLuint tex;
	volatile bool ready = false; // Becomes true once texture is completely uploaded to OpenGL.
	volatile bool dims_valid = false; // Becomes true as soon as "aspect" is valid, significantly before "ready" is true.

    inline void WaitReady () slForceInline
    {
        // Useful if you want to delay rendering a frame until a custom texture is uploaded.
        while (!ready) SDL_Delay(0);
    }
    inline slScalar WaitDimsValid () slForceInline
    {
        // Useful if you want to safely use the 'aspect' value in some calculations.
        while (!ready) SDL_Delay(0);
        return aspect; // So that you can do 'aspect = texture->WaitDimsValid();' on one line.
    }
};
struct slTexRef : slRef<slTexture>
{
    using slRef<slTexture>::slRef;
};
/*  For thread safety, avoid this pattern:
          Object: "class A { slTexRef texture; } A a;"
        Thread 1: "a.texref = SomeTexRef;"
        Thread 2: "a.texref = OtherTexRef;"
    unless you ensure these actions do not occur simultaneously.  */



class slNullTexture : public slTexture
{
    inline slNullTexture () slForceInline
    {
        tex = 0;
        ready = true;
        aspect = 1;
        dims_valid = true;
    }
    static slNullTexture* instance;
    friend struct slNullTexRef;
    friend void sl_internal::slTexturesInit ();
    friend void sl_internal::slTexturesQuit ();
};
/*  slNullTexture should only be used to represent "no texture". Do not try to
    use it as "transparent texture" or "black texture". Since there is nothing
    specific to any individual place where slNullTexture would be used, only one
    is created at engine startup and deleted at engine shutdown. If you need a
    raw pointer to it, simply get the raw_ref of an slNullTexRef.  */
struct slNullTexRef : public slTexRef
{
	slNullTexRef () : slTexRef(slNullTexture::instance) {}
};
// Convenience macro in case you prefer to write "slNoTexture" for readability.
#define slNoTexture (slNullTexRef())



struct slTextInfo
{
    char* text;
    slFont* font;
    SDL_Color text_color, back_color/*unused, for now*/;
    inline slTextInfo (char* text, slFont* font, SDL_Color text_color, SDL_Color back_color) slForceInline
    {
        this->text = text;
        this->font = font;
        this->text_color = text_color;
        this->back_color = back_color;
    }
};
struct slTextTexRef;
class slTextTexture : public slTexture
{
    slTextInfo text_tree_key;
    slTreeNodeData<slTextTexture> text_tree_data;
    friend class slTextTexturesTreeProto;

    slTextTexture* next_load;
    friend class slTextAsyncQueueProto;

    slTextTexture (slTextInfo search_key);
    ~slTextTexture () override;

public:
    void Unregister () override;

    static slTextTexRef Render (
        char* str,
        SDL_Color color = {255,255,255,255},
        slFont* font = slLoadFont(slGetDefaultFontPath())
    );
};
struct slTextTexRef : public slTexRef
{
	slTextTexRef (slTextTexture* raw_ref) : slTexRef(raw_ref) {}
};
inline slForceInline slTextTexRef slRenderText (
    char* str,
    SDL_Color color = {255,255,255,255},
    slFont* font = slLoadFont(slGetDefaultFontPath())
)
{
    return slTextTexture::Render(str,color,font);
}



struct slImageInfo
{
    char* loaded_from;
    inline slImageInfo (char* loaded_from) slForceInline
    {
        this->loaded_from = loaded_from;
    }
};
struct slImageTexRef;
class slImageTexture : public slTexture
{
    slImageInfo image_tree_key;
    slTreeNodeData<slImageTexture> image_tree_data;
    friend class slImageTexturesTreeProto;

    slTextTexture* next_load;
    friend class slImageAsyncQueueProto;

    slImageTexture (slImageInfo search_key);
    ~slImageTexture () override;

public:
    void Unregister () override;

    static slImageTexRef Load (char* path);
};
struct slImageTexRef : public slTexRef
{
	slImageTexRef (slImageTexture* raw_ref) : slTexRef(raw_ref) {}
};
inline slForceInline slImageTexRef slLoadTexture (char* path)
{
    return slImageTexture::Load(path);
}



struct slMemoryTexRef;
class slMemoryTexture : public slTexture
{
public:
    void Update (slInt2 wh, void* colorbuf);

    static slMemoryTexRef Create ();
};
struct slMemoryTexRef : public slTexRef
{
	slMemoryTexRef (slMemoryTexture* raw_ref) : slTexRef(raw_ref) {}

    inline void Update (slInt2 wh, void* colorbuf) slForceInline
        { ((slMemoryTexture*)raw_ref)->Update(wh,colorbuf); }
};
inline slForceInline slMemoryTexRef slCreateCustomTexture (slBU w, slBU h, void* proto_data)//, bool alpha, Uint8 row_alignment)
{
    slMemoryTexRef ref_out = slMemoryTexture::Create();
    ref_out.Update(slInt2(w,h),proto_data);
    return ref_out;
}
inline slForceInline slMemoryTexRef slSolidColorTexture (SDL_Color color)
{
	return slCreateCustomTexture(1,1,&color);
}



/** Do not attempt to create or use any slTargetTexture off the main thread. **/
struct slTargetTexRef;
class slTargetTexture : public slTexture
{
    int gl_internalformat,gl_format,gl_type;

    slTargetTexture (slInt2 wh, int gl_internalformat, int gl_format, int gl_type);
    static slTargetTexRef Create (slInt2 wh, int gl_internalformat, int gl_format, int gl_type);

    void Resize (slInt2 wh);

    friend class slFrame;
};
struct slTargetTexRef : public slTexRef
{
	slTargetTexRef (slTargetTexture* raw_ref) : slTexRef(raw_ref) {}
};



/** Do not attempt to create or use any slFrame off the main thread. **/
class slFrame
{
    slInt2 dims = 1;
    GLuint framebuffer;

    GLuint depth_renderbuffer = 0;
    slTargetTexture* depth_texture = NULL;

    GLuint* color_buffers = NULL;
    slTargetTexture** color_textures = NULL;
    int color_count = 0;

public:
    void CreateDepthBuffer ();
    void DeleteDepthBuffer ();

    slFrame (bool gen_depth_buffer);
    ~slFrame ();

    slTargetTexRef CreateDepthOutput ();
    void AttachDepthOutput (slTargetTexRef existing);
    slTargetTexRef GetDepthOutput (); // not safe if no depth texture
    void DeleteDepthOutput ();

    slTargetTexRef CreateColorOutput (int id, bool alpha, bool floating = false);
    void AttachColorOutput (int id, slTargetTexRef existing);
    slTargetTexRef GetColorOutput (int id); // not safe if passing invalid id
    void DeleteColorOutput (int id);

    void Resize (slInt2 wh); // Canvas size: (wh.w, wh.h) pixels.
    void Resize (); // Canvas size: (screen.x, screen.y) pixels.
    void Resize (slVec2 wh); // Canvas size: (screen.x * wh.w, screen.y * wh.h) pixels.
    inline slInt2 GetDims () slForceInline { return dims; }

    void DrawTo ();
};
void slDrawToScreen ();



void slQueueTexUpload (slTexture* tex, SDL_Surface* surf);
SDL_Surface* slLoadTexture_Surf (char* path); /// May return NULL.
SDL_Surface* slRenderText_Surf (char* str, SDL_Color color, slFont* font); /// May return NULL.
SDL_Surface* slConvertSurface_RGBA32 (SDL_Surface* original); /// Don't pass NULL as `original`. May return NULL.
SDL_Surface* slConvertSurface_RGBA32_Free (SDL_Surface* original); /// Convenience function, frees `original`. Safe to pass NULL as `original` to this function.
//#define slColorMatch(color1,color2) (color1.r == color2.r && color1.g == color2.g && color1.b == color2.b && color1.a == color2.a)
#define slColorMatch(color1,color2) (*(Uint32*)&(color1) == *(Uint32*)&(color2))



struct slTexReplacement
{
    slTexRef replacement;
    inline slTexReplacement (slTexRef tex) slForceInline : replacement(tex) {}
    slLLNodeData<slTexReplacement> nodedata;
};
class slTexSwapChain
{
	slTexRef current = slNoTexture;
    slLinkedList<slTexReplacement> replacements = slLinkedList<slTexReplacement>(&slTexReplacement::nodedata);
	SDL_mutex* mutex = SDL_CreateMutex();
    void Clear ();
public:
    ~slTexSwapChain ();
    void Queue (slTexRef latest);
    slTexRef Step ();
};



#endif // SLICETEXTURES_H_INCLUDED
