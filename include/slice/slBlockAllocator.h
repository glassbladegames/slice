#ifndef SLBLOCKALLOCATOR_H_INCLUDED
#define SLBLOCKALLOCATOR_H_INCLUDED



#define slDefaultFirstBlockItems 64 // The value used by slInitBlockAllocator if its first_block_items argument is 0.
struct slBlockAllocator
{
	void* Allocate ();
	/// Allocates an item and returns a pointer to it.

	void Release (void* item);
	/// Releases `item`, which *MUST* be a valid pointer to an allocated item (and may not be NULL).

	void ReleaseAllItems ();
	/// Logically releases all items, but does not free the blocks.

	void FreeAllBlocks ();
	/// Same effect as ReleaseAll, but also frees all the blocks' memory.



	slBlockAllocator (char* name, slBU itemsize, slBU blockitems = 0);
		// If itemsize < sizeof(void*) it will be set to that size.
		// Providing blockitems = 0 will cause a default (slAllocDefaultBlockItems) to be used as the items per block.
		// `blockitems` does not have to be a power of two, or a multiple of anything.

	/// This causes crashes. You should be freeing the memory explicitly anyway, so to hell with the automatic nonsense.
	//inline slForceInline ~slBlockAllocator () { FreeAllBlocks(); }



	private:

	char* name;
	void** blocks;
	void* next_available;
	// Not implemented: //SDL_mutex* mutex; // not required. if exists, will safeguard against thread issues.
	slBU item_size,first_block_items,block_count;

	void CreateNewBlock ();
};

#endif // SLBLOCKALLOCATOR_H_INCLUDED
