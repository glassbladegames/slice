#ifndef SLLIST_H_INCLUDED
#define SLLIST_H_INCLUDED

/// Some flags that show how this Slice distribution will behave under "unsafe" list and alloc operations.
#define SL_LIST_INDEXED_DUPLICATE_CHECK
	/// Low performance cost: checks list index for incoming item's index value.
#define SL_LIST_INDEX_CHECK
	/// Low performance cost: checks that the list index does point to the item.
#define SL_LIST_STARTING_CAPACITY 16



#define slNoIndex -1
struct slList
{
	//#if defined(SL_LIST_INDEX_CHECK) || defined(SL_LIST_INDEXED_DUPLICATE_CHECK)
	char* name;
	//#endif

	void** items; // the list.
	slBU itemcount; // how many items are currently in the list?
	slBS indexoffset; // offset of index within object structure. slNoIndex if none.
	slBU capacity; // how many slots currently allocated
	bool preserveorder; // whether to preserve ordering

	// Not implemented: //SDL_mutex* mutex; // not required. if exists, will safeguard against thread issues.

	slList (char* name, slBS indexoffset, bool preserveorder = false);
	void Add (void* item);
	void RemoveIndex (slBU index);
	void Remove (void* item);
    bool Has (void* item);
	void Shrink ();
	void Clear (void (*clr_func) (void* item), bool free_buffer = true);
	void UntilEmpty (void (*clr_func) (void* item), bool free_buffer = true);
	inline void* operator[] (slBU index) slForceInline { return items[index]; }

	slGC_Action autoshrink_action;
	void HookAutoShrink ();
	void UnhookAutoShrink ();
};


#endif // SLLIST_H_INCLUDED
