#pragma once

// Must define this now so it's defined when slTreeMap.h is included by slice.h
template <class Value>
struct slTreeNodeData
{
    Value* left;
    Value* right;
};

#include <slice.h>

template <class Value>
class slTreeSet
{
    slTreeNodeData<Value> Value::* embedded_nodedata;
    Value* root = NULL;
    int total_items = 0;
    inline slTreeNodeData<Value>* GetTreeData (Value* item) slForceInline
    {
        return &(item->*embedded_nodedata);
    }
    void PrintInOrder (Value* local_root, int depth, char pre);
    int Recount (Value* local_root);

public:
    inline slTreeSet (slTreeNodeData<Value> Value::* embedded_nodedata) slForceInline
    {
        this->embedded_nodedata = embedded_nodedata;
    }
    Value* Insert (Value* item);
    /*  Returns the parent this item is now under, or NULL if the item is the
        root of the tree (can only happen if tree was completely empty).
        Inserting duplicates will cause this to return NULL also, but you should
        never do that in the first place.  */
    void Remove (Value* item);
    Value* Lookup (Value* item);

    inline void PrintInOrder () slForceInline
    {
        PrintInOrder(root,0,0xC4);
    }

    inline int Recount () slForceInline
    {
        return Recount(root);
    }

    /* Return 1 for in-order, -1 for reverse-order, 0 for equality. */
    virtual int Compare (Value* item_a, Value* item_b) = 0;
    virtual void PrintItem (Value* item) = 0;

    /*  The default version of this just calls Compare(intree_item,search_item),
        which is what Insert and Remove use directly. Overriding this allows you
        to change the behavior of Lookup.  */
    virtual int LookupCompare (Value* intree_item, Value* search_item);
};

#include <slice/slTreeSetImpl.cpp>
