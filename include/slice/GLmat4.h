#include <slice.h>
#ifndef _INCLUDE_GLMAT4_H_
#define _INCLUDE_GLMAT4_H_



typedef slScalar s3dRawVec3 __attribute__((vector_size(sizeof(slScalar)*4)));
struct GLvec4;
struct s3dVec3
{
	union
	{
		s3dRawVec3 xyz;
		struct { slScalar x,y,z; };
	};
	s3dVec3 () slForceInline : xyz((s3dRawVec3){0,0,0}) {}
	s3dVec3 (slScalar x, slScalar y, slScalar z) slForceInline : x(x), y(y), z(z) {}
	s3dVec3 (slScalar xyz) slForceInline : x(xyz), y(xyz), z(xyz) {}
	static s3dVec3 from_float_array (float* rawfloats) slForceInline
	{
		s3dVec3 out;
		out.xyz = (s3dRawVec3)
		{
			*rawfloats,
			rawfloats[1],
			rawfloats[2]
		};
		return out;
	}
	s3dVec3 operator+ (slScalar other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz += other;
		return out;
	}
	s3dVec3 operator- (slScalar other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz -= other;
		return out;
	}
	s3dVec3 operator* (slScalar other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz *= other;
		return out;
	}
	s3dVec3 operator/ (slScalar other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz /= other;
		return out;
	}
	s3dVec3 operator+= (slScalar other) slForceInline
	{
		this->xyz += other;
		return *this;
	}
	s3dVec3 operator-= (slScalar other) slForceInline
	{
		this->xyz -= other;
		return *this;
	}
	s3dVec3 operator*= (slScalar other) slForceInline
	{
		this->xyz *= other;
		return *this;
	}
	s3dVec3 operator/= (slScalar other) slForceInline
	{
		this->xyz /= other;
		return *this;
	}
	s3dVec3 operator+ (s3dVec3 other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz += other.xyz;
		return out;
	}
	s3dVec3 operator- (s3dVec3 other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz -= other.xyz;
		return out;
	}
	s3dVec3 operator* (s3dVec3 other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz *= other.xyz;
		return out;
	}
	s3dVec3 operator/ (s3dVec3 other) slForceInline
	{
		s3dVec3 out = *this;
		out.xyz /= other.xyz;
		return out;
	}
	s3dVec3 operator+= (s3dVec3 other) slForceInline
	{
		this->xyz += other.xyz;
		return *this;
	}
	s3dVec3 operator-= (s3dVec3 other) slForceInline
	{
		this->xyz -= other.xyz;
		return *this;
	}
	s3dVec3 operator*= (s3dVec3 other) slForceInline
	{
		this->xyz *= other.xyz;
		return *this;
	}
	s3dVec3 operator/= (s3dVec3 other) slForceInline
	{
		this->xyz /= other.xyz;
		return *this;
	}
	s3dVec3 operator- ()
	{
		s3dVec3 out = *this;
		out.xyz = -out.xyz;
		return out;
	}
	inline s3dVec3 (GLvec4 from) slForceInline;
	slScalar length_squared () slForceInline
	{
		s3dVec3 squared = *this * *this;
		return squared.x + squared.y + squared.z;
	}
	slScalar length () slForceInline
	{
		return slsqrt(length_squared());
	}
	slScalar dist_squared (s3dVec3 other) slForceInline
	{
		return (other - *this).length_squared();
	}
	slScalar dist (s3dVec3 other) slForceInline
	{
		return slsqrt(dist_squared(other));
	}
	s3dVec3 normalized () slForceInline
	{
		slScalar len = length();
		if (len == 0) return s3dVec3(0,1,0);
		else return *this / len;
	}
};
typedef GLfloat GLvec4_raw __attribute__((vector_size(sizeof(GLfloat)*4)));
#include <xmmintrin.h>
struct GLvec4
{
	union
	{
		struct { GLfloat x,y,z,w; };
		GLvec4_raw data;
	};
	GLvec4 () slForceInline {}
	GLvec4 (GLvec4_raw in) slForceInline : data(in) {}
	//GLvec4 (GLvec3 in) slForceInline : data(in.data) {w = 1;}
	GLvec4 (GLfloat x, GLfloat y, GLfloat z, GLfloat w) slForceInline : x(x), y(y), z(z), w(w) {}
	GLvec4 (GLfloat x, GLfloat y, GLfloat z) slForceInline : x(x), y(y), z(z), w(1) {}
	GLvec4 (s3dVec3 in) slForceInline : x(in.x), y(in.y), z(in.z), w(1)
	{
		/// Do this properly with an intrinsic later.
	}
	GLfloat sum4 () slForceInline
	{
		/// Use an MMX intrinsic for summing the four values... it will speed this up quite nicely.
		/// For now though I can't access the damn documentation.
		return x + y + z + w;
	}
	void print (); // Just prints the numbers to console, for debugging purposes.
};
inline slForceInline s3dVec3::s3dVec3 (GLvec4 from)
{
	x = from.x;
	y = from.y;
	z = from.z;
}
typedef GLfloat GLvec16_raw __attribute__((vector_size(sizeof(GLfloat)*16)));
struct GLmat4;
GLmat4 GLmat4_mul (GLmat4& a, GLmat4& b);
struct GLmat4
{
	union
	{
		GLfloat data [16];
		struct { GLvec4_raw row1,row2,row3,row4; };
		GLvec16_raw entire_vec; /// Use this for addition to take advantage of AVX-512 more easily.
	};
	GLmat4 operator* (GLmat4 second) slForceInline
	{
		return GLmat4_mul(*this,second);
	}
	GLmat4 operator*= (GLmat4 second) slForceInline
	{
		*this = GLmat4_mul(*this,second);
		return *this;
	}
	GLmat4 operator+ (GLmat4 second) slForceInline
	{
		GLmat4 out;
		out.entire_vec = entire_vec + second.entire_vec;
		return out;
	}
	GLmat4 operator+= (GLmat4 second) slForceInline
	{
		entire_vec += second.entire_vec;
		return *this;
	}
	GLvec4 operator* (GLvec4 vec)
	{
		return GLvec4
		(
			GLvec4(row1 * vec.data).sum4(),
			GLvec4(row2 * vec.data).sum4(),
			GLvec4(row3 * vec.data).sum4(),
			GLvec4(row4 * vec.data).sum4()
		);
	}
	void print (); // Just prints the numbers to console, for debugging purposes.
	GLmat4 transpose ();
	void transpose_in_place ();
};
inline GLmat4 GLmat4_identity () slForceInline;
GLmat4 GLmat4_identity ()
{
	GLmat4 out;
	out.entire_vec = (GLvec16_raw){1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1};
	return out;
}
GLmat4 GLmat4_scaling (GLvec4 scalevec);
GLmat4 GLmat4_xrotate (GLfloat rads);
GLmat4 GLmat4_yrotate (GLfloat rads);
GLmat4 GLmat4_zrotate (GLfloat rads);
GLmat4 GLmat4_offset (GLfloat x, GLfloat y, GLfloat z);
inline slForceInline GLmat4 GLmat4_offset (GLvec4 vec) // w component is ignored.
{
	return GLmat4_offset(vec.x,vec.y,vec.z);
}
GLmat4 GLmat4_ortho (GLfloat left, GLfloat right, GLfloat bottom, GLfloat top, GLfloat near_clip, GLfloat far_clip);
//GLmat4 GLmat4_ortho_depth (GLfloat near_z, GLfloat far_z);
GLmat4 GLmat4_perspective (GLfloat fov, GLfloat znear, GLfloat zfar); // fov is in degrees.



#endif
