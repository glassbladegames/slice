#ifndef SLICEAUDIO_H_INCLUDED
#define SLICEAUDIO_H_INCLUDED

#define slSoundSource_Loading 0
#define slSoundSource_Incomplete 1
#define slSoundSource_Ready 2
struct slSoundSource
{
	float* samples; // Interleaved.
	char* loadedfrom;
	slBU _index_;
	Uint32 samplecount;
	Uint32 persecond;
	Uint8 ready;
	Uint8 channelcount;
};
slSoundSource* slLoadSWAG (char* path);
slSoundSource* slLoadOGG (char* path);
struct slSound
{
	slSoundSource* src;
	void (*onfinish) (slSound* sound);
	/// ONLY fires when the sound stops playing because it reached the end.
	/// Does not fire if you stop the sound manually, and also does not fire if the sound is set to repeat.
	// WARNING: Runs directly in the audio mixer, so not thread-safe for doing anything state-affecting.
	// If you want to do anything state-affecting (or multiple-read state-reliant), then you should
	// use this to send a signal to your main thread, to have the work done there instead.
	void* userdata;
	slBU _index_;
	slBU cursample;
	float volume;
	float pitch;
	float cursubsample;
	bool playing;
	bool loop;
	bool destroy;
	float (*pitch_modulate) (float seconds);
};
slSound* slCreateSound (slSoundSource* src = NULL, bool destroy = true, bool play = true, bool loop = false, slScalar volume = 1, slScalar pitch = 1);
void slDestroySound (slSound* sound);
void slSetCustomMixStage (void (*func) (float* buf, slBU len, Uint8 channels, slScalar persample));
void slCloseAudio ();
void slOpenAudio (bool enabled = true, slScalar buffer_length = 1./20, Uint8 channels = 2, slBU rate = 96000, Uint32 format = AUDIO_F32);
void slPlaySound (slSound* sound);

void slSetMuteWhileAway (bool to);
bool slGetMuteWhileAway ();
void slSetMasterVolume (slScalar to);
slScalar slGetMasterVolume ();

namespace sl_internal
{
	void slAudioInit ();
	void slAudioQuit ();
	void slAudioStep ();
}

#endif // SLICEAUDIO_H_INCLUDED
