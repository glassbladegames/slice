#pragma once
#include <stdlib.h>
struct FlexArray
{
	private:
	void* mem;
	size_t usage;
	size_t itemsize;
	size_t capacity;
	size_t initial_capacity;
	public:
	FlexArray (size_t itemsize, size_t initial_capacity = 16);
	void* Append ();
	void Remove (size_t index);
	void RemoveItem (void* item);
	inline void* Get (size_t index)
	{
		return mem + index * itemsize;
	}
	inline size_t Length ()
	{
		return usage;
	}
	void Trim ();
	inline void RemoveAll ()
	{
		usage = 0;
	}
	inline ~FlexArray ()
	{
		free(mem);
	}
};
