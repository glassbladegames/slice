#include <slice2d.h>


struct stFixed2
{
	union
	{
		slRawInt2 xy,wh;
		struct { slBS x,y; };
		struct { slBS w,h; };
	};
    inline stFixed2 () slForceInline {}
    inline stFixed2 (slRawInt2 xy) slForceInline { this->xy = xy; }
    inline stFixed2 (slInt2 tileid, slInt2 inner) slForceInline
    {
        //xy = (slRawInt2){tileid.x << 16, tileid.y << 16} + inner.xy;
        xy = (tileid.xy << 16) | inner.xy;
    }

    inline stFixed2 operator+ (stFixed2 other) slForceInline
    {
        return stFixed2(xy + other.xy);
    }
    inline stFixed2 operator- (stFixed2 other) slForceInline
    {
        return stFixed2(xy - other.xy);
    }
    inline stFixed2 operator+= (stFixed2 other) slForceInline
    {
        xy += other.xy;
        return *this;
    }
    inline stFixed2 operator-= (stFixed2 other) slForceInline
    {
        xy -= other.xy;
        return *this;
    }
    inline stFixed2 operator- () slForceInline
    {
        return stFixed2(-xy);
    }

    inline slVec2 ToFloats () slForceInline
    {
		slVec2 tile_id(x >> 16, y >> 16);
		slVec2 inner(x & 0xFFFF, y & 0xFFFF);
		return tile_id + inner / 0x10000;
    }
    inline static stFixed2 FromFloats (slVec2 floating) slForceInline
    {
        //printf("%lf, %lf -> ",floating.x,floating.y);
        slInt2 tileid(slfloor(floating.x),slfloor(floating.y));
        slInt2 inner = (floating - tileid) * 0x10000;

        if (inner.x < 0) inner.x = 0;
        else if (inner.x > 0xFFFF) inner.x = 0xFFFF;
        if (inner.y < 0) inner.y = 0;
        else if (inner.y > 0xFFFF) inner.y = 0xFFFF;

        /*return*/ stFixed2 out = stFixed2(
            (slRawInt2){
                (tileid.x << 16) | inner.x,
                (tileid.y << 16) | inner.y
            }
        );
        //printf("%lld, %lld\n",out.x,out.y);
        return out;
    }
    inline slInt2 TileID () slForceInline
    {
		return slInt2(x >> 16, y >> 16);
    }
    inline slInt2 Inner () slForceInline
    {
		return slInt2(x & 0xFFFF, y & 0xFFFF);
    }
};


struct stTile
{
    s2dInstance* visual; // TEMPORARY, will be rendered differently later.

    bool collide;
};

struct stAllTiles
{
    /* this is separate from the world because it may be useful to make tiles
       be grouped into many blocks that are only loaded in when entities are
       near them. For now there is only one block of tiles though. */
    slInt2 blc; // top-left corner
    slInt2 wh; // how far to the right and up
    stTile* tiles; /* w*h tiles.
                    *tiles is the top-left tile,
                    tiles[1] is directly right of *tiles,
                    and tiles[wh.w] is directly below *tiles. */
    stAllTiles (slInt2 blc, slInt2 wh, bool* protobuf);
    ~stAllTiles ();
    bool Blocked (slInt2 pos);
};

struct stWorld;
class stEntity
{
    slBU _index_;

    void SpatialRegisterX ();
    void SpatialRegisterY ();
    void SpatialUnregisterX ();
    void SpatialUnregisterY ();

    friend class stWorld;

public:
    stFixed2 blc, trc;
    /*  TO DO: Restrict access to the position data. Updating them requires
        the trees and linked lists to be updated.  */

    s2dInstance* visual;

    bool collide = true;
    slVec2 vel = 0;
    slScalar bouncy = 0.5;

    /* The center values are purely used as output. */
    slVec2 center_now;
	slVec2 center_prev; // Store entity's position during previous step
                        // for smooth interpolation on frames between steps.

    stWorld* world;

      slLLNodeData<stEntity>    up_list_data;
      slLLNodeData<stEntity>  down_list_data;
      slLLNodeData<stEntity> right_list_data;
      slLLNodeData<stEntity>  left_list_data;
    slTreeNodeData<stEntity>    up_tree_data;
    slTreeNodeData<stEntity>  down_tree_data;
    slTreeNodeData<stEntity> right_tree_data;
    slTreeNodeData<stEntity>  left_tree_data;

    //stEntity (stWorld* world, slTexRef tex, slVec2 wh, slVec2 xy);
    stEntity (stWorld* world, slTexRef tex, stFixed2 blc, stFixed2 trc);
    ~stEntity ();

};

struct stDebugBox
{
    /*  Just a box that is spawned as things are happening and fades away after
        some time, to aid with visual debugging.  */
    s2dInstance* box;
    slScalar sustain_duration = 2, fade_duration = 0.5, age = 0;
    slBU _index_;
    stDebugBox (slVec2 xy, slVec2 wh, slTexRef tex)
    {
        box = s2dCreateInstance(tex);
        box->SetDims(xy,wh,199);
    }
    ~stDebugBox ()
    {
        s2dDestroyInstance(box);
    }
};

struct stEntitiesUpTree : slTreeSet<stEntity>
{
    inline stEntitiesUpTree () slForceInline : slTreeSet(&stEntity::up_tree_data) {}
    int Compare (stEntity* entity_a, stEntity* entity_b) override;
    int LookupCompare (stEntity* intree_entity, stEntity* search_entity) override;
    void PrintItem (stEntity* entity) override;
    stEntity* FirstFrom (Sint64 y);
};
struct stEntitiesDownTree : slTreeSet<stEntity>
{
    inline stEntitiesDownTree () slForceInline : slTreeSet(&stEntity::down_tree_data) {}
    int Compare (stEntity* entity_a, stEntity* entity_b) override;
    int LookupCompare (stEntity* intree_entity, stEntity* search_entity) override;
    void PrintItem (stEntity* entity) override;
    stEntity* FirstFrom (Sint64 y);
};
struct stEntitiesRightTree : slTreeSet<stEntity>
{
    inline stEntitiesRightTree () slForceInline : slTreeSet(&stEntity::right_tree_data) {}
    int Compare (stEntity* entity_a, stEntity* entity_b) override;
    int LookupCompare (stEntity* intree_entity, stEntity* search_entity) override;
    void PrintItem (stEntity* entity) override;
    stEntity* FirstFrom (Sint64 x);
};
struct stEntitiesLeftTree : slTreeSet<stEntity>
{
    inline stEntitiesLeftTree () slForceInline : slTreeSet(&stEntity::left_tree_data) {}
    int Compare (stEntity* entity_a, stEntity* entity_b) override;
    int LookupCompare (stEntity* intree_entity, stEntity* search_entity) override;
    void PrintItem (stEntity* entity) override;
    stEntity* FirstFrom (Sint64 x);
};

struct stWorld
{
    slList debug_boxes = slList(// items[i]: stDebugBox*
        "tile-oriented physics: visual-debugging boxes",
        offsetof(stDebugBox,_index_)
    );

    stAllTiles tiles;

    /*  Lock the mutex when an entity is registering or deregistering,
        and when the physics step is running (iterating through lists).
        Maybe this mutex can be split into several for the main list vs. the
        spatial ones, or one for the main list plus one for each spatial list,
        although the trees and lists must stay synchronized with their
        counterparts.  */
    /*  We do need the separate slList (could turn it into an slLinkedList
        though?) for the physics step since during it, the spatial lists are
        being rearranged so iterating over one of them could cause items to be
        stepped more than once or skipped over.  */
    SDL_mutex* entities_listmutex;
    slList entities = slList(// items[i]: stEntity*
        "tile-oriented physics: entities",
        offsetof(stEntity,_index_)
    );
    slLinkedList<stEntity>    up_list = slLinkedList<stEntity>(&stEntity::   up_list_data);
    slLinkedList<stEntity>  down_list = slLinkedList<stEntity>(&stEntity:: down_list_data);
    slLinkedList<stEntity> right_list = slLinkedList<stEntity>(&stEntity::right_list_data);
    slLinkedList<stEntity>  left_list = slLinkedList<stEntity>(&stEntity:: left_list_data);
          stEntitiesUpTree    up_tree;
        stEntitiesDownTree  down_tree;
       stEntitiesRightTree right_tree;
        stEntitiesLeftTree  left_tree;

    slVec2 gravity = 0;

    slScalar
        stepping_interval = 1/120.f, // Crisp 120fps physics by default.
        stepping_elapsed = 0,
        stepping_multiplier = 1,
        stepping_maxcalctime = 1/30.f; // Below about 30 fps, allow time loss.

    // Very high velocities can take very high processing times if entities
    // bounce through tight corridors, such as an entity which is sized
    // 0.FFFF X 0.FFFF surrounded on two sides by tiles, constantly bouncing
    // back and forth. MAX_VEL = 100 is recommended for that specific scenario
    // for a single entity (tested on Ryzen 5800H).
    slScalar MAX_VEL = 1000000; // Set to -1 to allow any velocity.
    bool MAX_VEL_COMBINED_AXES = false; // Set to true to enforce MAX_VEL as
                                        // the magnitude and scale down both
                                        // components of the velocity by the
                                        // same proportion. Otherwise the value
                                        // for each axis is simply clamped.
    bool MAX_VEL_EXPORT = true;  // Set to true to set the entity's velocity
                                 // to the capped value, otherwise the entity's
                                 // velocity is unchanged (capped internally).

    slScalar max_substep_calctime = 0.1; // Set to -1 to uncap processing time.

    stWorld (slInt2 tlc, slInt2 wh, bool* protobuf);
    ~stWorld ();
    bool Blocked (slInt2 pos);
    bool HorizontalStrikefaceClear (slBS x_low, slBS x_high, slBS y);
    bool VerticalStrikefaceClear (slBS x, slBS y_low, slBS y_high);
    void DiscreteStep ();
    void Step ();
    void Draw ();
    void SetCamPos (slVec2 pos);
    void SetCamSize(slScalar size);
    void SetGravity (slVec2 gravity);
};

void stInit ();
void stQuit ();
