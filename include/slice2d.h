#pragma once
#include <slice.h>

//#define s2dEnableBatchCulling

struct s2dTexInfo
{
	GLfloat x,y,w,h,rot;
};
struct s2dInstanceRender
{
	bool draw;
	GLfloat sinvalue,cosvalue;
	GLvec2 rotpoint;
	GLxywh xywh;
	GLfloat texinfocos,texinfosin;
    slTexRef texref = slNoTexture; // prevents artifacting if swapchain texture
                                   // changes after render prepare
};
struct s2dInstance
{
	void* userdata;
	//slTexture* texref;
	slTexSwapChain texref_swapchain;
	void SetTexRef (slTexRef texref) slForceInline
	{
        texref_swapchain.Queue(texref);
	}

	s2dTexInfo* texinfo = NULL;
    // NULL to draw textures normally, if exists is not freed automatically.
    // THREAD-UNSAFE to set from valid pointer to NULL during render.

    slBU _index_;
	slBU _alloc_index_;
	slVec2 xy, wh;
    slVec2 rotpoint; // Used only if `rot` is true and `rotcenter` is false.
	slScalar rotangle; // Used only if `rot` is true.
	SDL_Color drawmask = {255,255,255,255};
	bool rot : 1 = false;
	bool rotcenter : 1; // Used only if `rot` is true.
	bool keepratio : 1 = true;
	bool visible = true;
	Uint8 z; // Lower is Closer
	s2dInstanceRender render;
	inline void SetDims (slVec2 xy, slVec2 wh, Uint8 z) slForceInline
	{
		this->xy = xy;
		this->wh = wh;
		this->z = z;
	}
	inline void SetRots (bool rot = false, slScalar rotangle = 0, bool rotcenter = true, slVec2 rotpoint = 0) slForceInline
	{
		this->rot = rot;
		this->rotangle = rotangle;
		this->rotcenter = rotcenter;
		this->rotpoint = rotpoint;
	}
	void PutWithin (s2dInstance* outer); // Does not (yet) take rotation into account.
};
s2dInstance* s2dCreateInstance (slTexRef tex = slNoTexture, s2dTexInfo* texinfo = NULL);
void s2dDestroyInstance (s2dInstance* todel);

#include <slice2d/s2dBatch.h>

slCorners s2dGetInstCorners (s2dInstance* item);

bool s2dPointOnInstance (s2dInstance* inst, slVec2 point);

inline slForceInline void s2dSetInstDims (s2dInstance* inst, slVec2 pos, slVec2 dims, Uint8 z)
{
    /// This function is deprecated.
    inst->SetDims(pos,dims,z);
}
inline slForceInline void s2dSetInstDims (s2dInstance* inst, slScalar x, slScalar y, slScalar w, slScalar h, Uint8 z)
{
    /// This function is deprecated.
	s2dSetInstDims(inst,slVec2(x,y),slVec2(w,h),z);
}
inline slForceInline void s2dRelInstDims (s2dInstance* inst, s2dInstance* rel)
{
    /// This function is deprecated.
    inst->PutWithin(rel);
}
inline slForceInline void s2dSetInstRots (s2dInstance* inst, bool rot = false, slScalar rotangle = 0, bool rotcenter = true, slVec2 rotpoint = slVec2(0))
{
    /// This function is deprecated.
    inst->SetRots(rot,rotangle,rotcenter,rotpoint);
}

void s2dScreenToWorld (slBox* screen, s2dInstance* world);
void s2dWorldToScreen (s2dInstance* world, slBox* screen);

void s2dAddZHook (slZHook* zhook);
void s2dRemoveZHook (slZHook* zhook);

void s2dInit ();
void s2dQuit ();
void s2dRender ();
void s2dRender (slFrame* target_frame);

void s2dUpdateRelMouse ();

slVec2 s2dGetRelMouse ();
void s2dGetRelMouse (slScalar* relx, slScalar* rely); // Deprecated

void s2dSetCamXY (slVec2 xy);
void s2dSetCamXY (slScalar x, slScalar y); // Deprecated
slVec2 s2dGetCamXY ();
void s2dGetCamXY (slScalar* x, slScalar* y); // Deprecated

void s2dSetCamSize (slScalar w);
slScalar s2dGetCamSize ();
#define s2dCamWH_SetWto1 0
#define s2dCamWH_SetHto1 1
#define s2dCamWH_CutArea 2
#define s2dCamWH_PadArea 3
void s2dSetCamWH_Mode (Uint8 mode);
slVec2 s2dGetCamWH ();
void s2dGetCamWH (slScalar* w, slScalar* h); // Deprecated

void s2dSetCamRot (slScalar rot);
slScalar s2dGetCamRot ();

slCoordsTransform s2dWorldCoordsTransform ();
inline slForceInline slLineGroup* s2dCreateLineGroup (Uint8 z, SDL_Color color = {255,255,255,255})
{
	slLineGroup* out = slCreateLineGroup(z,color);
	out->get_transform = s2dWorldCoordsTransform;
	return out;
}

void s2dSetGlobalDrawMask (SDL_Color drawmask); // Does not currently do anything.

#include <slice2d/world_shaders.h>



#include <slice2d/slicephys2d.h>
#include <slice2d/sliceparticles.h>
