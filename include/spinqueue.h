#pragma once
#include <stdlib.h>
struct SpinQueue
{
	size_t capacity,length,readpos,writepos,itemsize;
	void* buffer;
};
#define QueueInitialItems 16
SpinQueue QueueInit (size_t itemsize);
void* QueueGet (SpinQueue* queue);
void* QueueAppend (SpinQueue* queue, void* copyin);
void QueueClear (SpinQueue* queue, bool free_buffer = true);
