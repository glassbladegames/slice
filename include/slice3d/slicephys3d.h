#pragma once
#include <btBulletDynamicsCommon.h>
btDiscreteDynamicsWorld* phys3dGetWorld ();
struct phys3dShape
{
	/// This allows the application to not worry about deleting collision shapes.

	btCollisionShape* shape;
	slBU _index_;

	SDL_atomic_t usages;
	slForceInline void Reserve () // Reserve once.
	{
		SDL_AtomicIncRef(&usages);
	}
	slForceInline void Abandon () // Abandon from one reservation.
	{
		SDL_AtomicDecRef(&usages);
	}

	btVector3 local_inertia;
};
phys3dShape* phys3dMakeShape (btCollisionShape* proto_shape); // Don't re-use the btCollisionShape you pass in here.
struct phys3dObject
{
	s3dObject* visual; // Can be NULL.
	phys3dShape* shape;
	btRigidBody* body;
	btMotionState* body_motion_state;
	slBU _index_;
	GLmat4 base_transform; // Mesh transformation at the origin, which will be combined with the calculated world transform each frame.

	btVector3 GetBodyCenter ();
};
phys3dObject* phys3dCreateObject (s3dObject* visual, phys3dShape* shape, s3dVec3 starting_pos, slScalar mass, int* group=NULL, int* mask=NULL); /// `visual` is allowed to be NULL.
void phys3dDestroyObject (phys3dObject* obj);
namespace s3d_internal
{
	void phys3dInit ();
	void phys3dQuit ();
}
void phys3dSetRateMul (slScalar mul);
void phys3dSetInterval (slScalar stepping_fps = 100, slScalar catch_up_fps = 30);
