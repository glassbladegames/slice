#pragma once
#include "slice.h"

bool opGetVisi ();
void opSetCustomEscapeCallback (bool (*func) ());
void opOpen ();
void opInformCustomVisi (bool visi);
void opSetVisibilityCallback (void (*visibility_callback) (bool menu_visible));

void opInit (
	SDL_Color MainPanelColor = {79,79,79,255},
	SDL_Color PanelColor = {47,47,47,255},
	SDL_Color TextColor = {255,255,255,255},
	SDL_Color HoverColor = {63,127,255,191}
);
void opQuit ();
void opSetAppOptsCallback (void (*custom_setvisi) (bool to));
void opSetExitVisible (bool visi);
/// Primary options menu components reside in Z=7 and Z=8.
/// Secondary options menu components reside in Z=4 through Z=6.
