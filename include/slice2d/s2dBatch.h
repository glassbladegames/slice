#ifndef S2DBATCH_H_INCLUDED
#define S2DBATCH_H_INCLUDED


struct s2dBatchMember;
struct s2dBatch;
void s2dBatchNewTexture (s2dBatch* batch);
struct s2dBatch
{
	slBlockAllocator allocator;
	//slList members; // all s2dBatchMember objects
	s2dBatchMember** active_members;
	s2dBatchMember** dormant_members;
	//SDL_mutex* vertex_data_lock; // protects SetPosAndDims, SetRotation, etc
	slTexture* tex; // Don't directly modify this.
	size_t active_member_count,dormant_member_count;
	size_t active_member_capacity,dormant_member_capacity;
	//slList members; // s2dBatchMember objects that are 'visible' (in buffer).
	//slList members_dormant; // s2dBatchMember objects that are 'invisible' (not in buffer).
	//GLenum index_type;
	//GLfloat* raw_vertices;
	size_t /*buf_member_count,*/vbo_capacity/*,rawbuf_capacity*/;
	GLuint vao,vbo;//,index_vbo;


	Uint8 z; // You can directly modify this.
	bool keepratio; // This too.

	// You can call this function to set the texture.
	inline void SetTexture (slTexRef tex_ref) slForceInline
	{
        if (tex) tex->Abandon();
        tex = tex_ref.ReserveGetRaw();
		s2dBatchNewTexture(this);
	}
};
s2dBatch* s2dCreateBatch (slTexRef tex, Uint8 z);
void s2dDestroyBatch (s2dBatch* batch);
struct s2dBatchMember
{
	// Don't modify any of these from an application, it won't do any good.
	// You can read them if you want to obviously, that doesn't hurt anything.
	private: slVec2 xy,wh; public: /// private to prevent user from obvious mistakes
	slVec2 rotpoint;
	s2dBatch* parent_batch;
	slBU index;
	private: slScalar rotangle; public: /// private to prevent user from obvious mistakes
	bool rot,rotcenter;
	//bool needs_update;
	#ifdef s2dEnableBatchCulling
	bool visible; /// You can modify this (and only this) directly.
	#else
	bool visible; /// Actually, *don't* modify this directly.
	#endif
	bool is_active;
	GLfloat vertexdata [8];
	GLxywh GetTexXYWH ();


	// Use these functions.
	//void SetPosition (slVec2 pos);
	//void SetDimensions (slVec2 dims);
	void SetPosAndDims (slVec2 pos, slVec2 dims); /// More efficient than calling SetPosition and SetDimensions separately.
	inline void SetPos (slVec2 pos) slForceInline { SetPosAndDims(pos,wh); }
	inline void SetDims (slVec2 dims) slForceInline { SetPosAndDims(xy,dims); }
	void SetRotation (bool rot = false, slScalar rotangle = 0, bool rotcenter = true, slVec2 rotpoint = slVec2(0));
	void SetVisible (bool visible); // not thread safe. actually none of these are since OpenGL hates threads
	inline slVec2 GetPos () slForceInline { return xy; }
	inline slVec2 GetDims () slForceInline { return wh; }
};
s2dBatchMember* s2dCreateBatchMember (s2dBatch* batch);
void s2dDestroyBatchMember (s2dBatch* batch, s2dBatchMember* member);
/*void s2dSetBatchMemberDims (s2dBatchMember* member, slScalar x, slScalar y, slScalar w, slScalar h);
void s2dSetBatchMemberDims (s2dBatchMember* member, slVec2 xy, slVec2 wh);
void s2dSetBatchMemberRots (s2dBatchMember* member, bool rot = false, slScalar rotangle = 0, bool rotcenter = true, slVec2 rotpoint = slVec2(0));*/

namespace s2d_internal
{
	GLuint s2dRenderBatch (s2dBatch* batch);
}

#endif // S2DBATCH_H_INCLUDED
