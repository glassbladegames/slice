#pragma once


namespace s2d_internal
{
	void s2dSetDrawTexture (GLuint tex);
	extern slShaderProgram s2dBatchProgram,s2dWorldRotateProgram,s2dTiledRotateProgram;
	extern GLuint
		s2dWorldRotateProgram_XYWH,
		s2dWorldRotateProgram_CamPos,
		s2dWorldRotateProgram_InvHalfCamDims,
		s2dWorldRotateProgram_CamRotCos,
		s2dWorldRotateProgram_CamRotSin,
		s2dWorldRotateProgram_SinValue,
		s2dWorldRotateProgram_CosValue,
		s2dWorldRotateProgram_RotPoint;
	extern GLuint
		s2dBatchProgram_CamPos,
		s2dBatchProgram_InvHalfCamDims,
		s2dBatchProgram_CamRotCos,
		s2dBatchProgram_CamRotSin;
	extern GLuint
		s2dTiledRotateProgram_XYWH,
		s2dTiledRotateProgram_CamPos,
		s2dTiledRotateProgram_InvHalfCamDims,
		s2dTiledRotateProgram_CamRotCos,
		s2dTiledRotateProgram_CamRotSin,
		s2dTiledRotateProgram_SinValue,
		s2dTiledRotateProgram_CosValue,
		s2dTiledRotateProgram_RotPoint,
		s2dTiledRotateProgram_TexXYWH,
		s2dTiledRotateProgram_TexCosSin;
	extern GLuint
		s2dWorldRotateProgram_Tex,
		s2dWorldRotateProgram_Mask,
		s2dBatchProgram_Tex,
		s2dBatchProgram_Mask,
		s2dTiledRotateProgram_Tex,
		s2dTiledRotateProgram_Mask;
	void s2dUniformFromColor (GLuint uniform, SDL_Color color);
	void s2dInitWorldProgram ();
	void s2dQuitWorldProgram ();
	void s2dLoadWorldOrtho ();
}
