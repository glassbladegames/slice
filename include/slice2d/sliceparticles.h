#pragma once

struct ptclLayer;
struct ptclParticle
{
	GLvec2 pos,vel;
	union
	{
		struct { GLfloat r,g,b; } rgb;
		struct { GLfloat red,green,blue; };
	}; // This prevents easily mistyping "r" for "radius".
	//GLfloat r,g,b;
	GLfloat radius;

	bool render; // This SUCKS for structure size.
    // To do: remove it and use the sign bit of radius for this purpose.
};
//struct ptclRemoves;
struct ptclDrawBuffer
{
	slBU buf_len;
	GLuint buf;
	GLuint vao;
	slBU buf_available;
};
struct ptclLayer
{
	void* ptcl_array;
	slBU ptcl_capacity,ptcl_usage,ptcl_size;
	bool (*step_particle) (ptclParticle* particle, float delta);//, ptclRemoves* removes);
		/// Return true to delete particle, false to leave it.
	slZHook zhook;
	slGC_Action gc_action;
	//bool* render_states; // Better way to store "bool render;" which was in ptclParticle.
	ptclDrawBuffer* draw_buffers;
	slBU draw_buffers_used;
};
inline void* ptclUserdata (void* particle)
{
	return particle + sizeof(ptclParticle);
}
ptclLayer* ptclCreateLayer (Uint8 z, bool (*step_particle) (ptclParticle* particle, float delta), slBU userdata_size);
void ptclDestroyLayer (ptclLayer* layer);
ptclParticle* ptclSpawnParticle (ptclLayer* layer);
//void ptclKillParticle (ptclParticle* part, ptclRemoves* removes);
namespace s2d_internal
{
	void ptclInit ();
	void ptclQuit ();
}



const float RAND_MAX_INV = 1.f / RAND_MAX;
inline slScalar Deviation (slScalar lowest, slScalar highest)
{
	return (rand() * RAND_MAX_INV) * (highest - lowest) + lowest;
}
inline GLfloat Deviation_F (GLfloat lowest, GLfloat highest)
{
	return (rand() * RAND_MAX_INV) * (highest - lowest) + lowest;
}
void ptclPhysAdvance (ptclParticle* part, float delta, bool (*ignore) (b2Fixture* fxt) = NULL, float restitution = 0.3, void (*oncollide) (ptclParticle* particle) = NULL);
inline slForceInline void ptclSimpleAdvance (ptclParticle* part, float delta)
{
	part->pos += part->vel * delta;
}
//void ptclAccelerate (ptclParticle* part, float delta, GLvec2 accel = GLvec2(0,physGetGravity()));
inline slForceInline void ptclAccelerate (ptclParticle* part, float delta, GLvec2 accel)
{
	part->vel += accel * delta;
}
