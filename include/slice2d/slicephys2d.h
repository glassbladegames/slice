#pragma once
#include <Box2D/Box2D.h>

/// Use meters when interfacing with the physics engine.
// Handy macros for converting between units. Compiler should fold constants.
#define physInchesPerMeter 39.3701f
#define physInchesToMeters(n) (n * physMetersPerInch)
#define physMetersToInches(n) (n * (1.f / physMetersPerInch))
#define physFeetToMeters(n) (n * (12.f * physMetersPerInch))
#define physMetersToFeet(n) (n * (1.f / (physMetersPerInch * 12.f)))

/// Default Interval: 120 FPS
void physSetInterval (slScalar to); // Set physics interval. Smaller = more precision and smoothness, larger = less work.
slScalar physGetInterval (); // Get the physics interval. Use this, instead of slGetDelta, during callbacks hooked to physPreStep.

/// Defaults to 1. Set to < 1 for slow motion effects, etc.
void physSetRateMul (slScalar mul);

struct physInstance
{
	s2dInstance* visual;
	b2Body* body;
	slBU _index_,_alloc_index_;
	b2BodyDef body_def;
	bool canmove,canrotate;
	slVec2 prev_pos,next_pos; slScalar prev_rot,next_rot;
};

// Used to create shapes for passing to physCreateInstance.
b2PolygonShape physRectShape (s2dInstance* visual, slTexture* shrink_to = NULL, b2Vec2* capture = NULL);

// Used to filter collisions between objects.
struct physCollInfo
{
	Uint16 objtype;
	Uint16 collidewith;
};
#define physDefaultCollInfo {0x0001,0xFFFF}

// `visual` is "consumed" by physCreateInstance. Do not destroy `visual`, physDestroyInstance will do that itself.
// The initial position of the physInstance will be set to the current position of `visual`.
physInstance* physCreateInstance
(
	s2dInstance* visual,
	b2PolygonShape shape,
	bool canmove = false,
	physCollInfo collinfo = physDefaultCollInfo,
	bool canrotate = true,
	float32 friction = 0.5,
	float32 density = 1
);
void physDestroyInstance (physInstance* instance);

namespace s2d_internal
{
	void physInit ();
	void physQuit ();
}



// The "PreCycle" functions will only be called if physics simulation is going to occur during the cycle.
// If, during a physics cycle, multiple steps must be performed, PreCycle will only be called once prior to those steps.
void physHook_PreCycle (void (*func) ());
void physUnhook_PreCycle (void (*func) ());

// The "PostCycle" functions will be called after physics objects' visual positions
// are updated from their physical counterparts.
void physHook_PostCycle (void (*func) ());
void physUnhook_PostCycle (void (*func) ());

// The "PreStep" functions are called immediately after each and every physics step.
void physHook_PreStep (void (*func) ());
void physUnhook_PreStep (void (*func) ());

// There may be visual frames drawn, during which PreCycle, PreStep, or PostCycle are not even called.



b2World* physGetWorld (); // Useful if you want to directly manipulate the world or perform queries on it.
#define physGetGravity() (physGetWorld()->GetGravity().y) // Convenience macro for getting world's vertical gravity.

void physSetSmoothMode (bool smooth_mode);
