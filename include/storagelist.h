#include <stdint.h>
struct StorageList_BlockHeader
{
	uint32_t occupied_count;
};
struct StorageList_ItemHeader
{
	bool occupied;
	uint32_t block_index;
	StorageList_ItemHeader* next_available;
};
struct StorageList
{
	uint32_t usage;
	uint32_t itemsize_item;
	uint32_t itemsize_storage;
	uint32_t block_itemcount;
	uint32_t blocksize_bytes;
	void** blocks;
	uint32_t block_count;
	void* first_available;
};
void StorageList_FreeAllStorage (StorageList* list)
{
	for (uint32_t block_id = 0; block_id < list->block_count; block_id++) free(list->blocks[block_id]);
	free(list->blocks);
	list->blocks = NULL;
	list->block_count = 0;
	list->first_available = NULL;
}
void StorageList_RemoveAllItems (StorageList* list)
{
	StorageList_ItemHeader** prev_pointer = &(list->first_available);
	for (uint32_t block_id = 0; block_id < list->block_count; block_id++)
	{
		void* item_header_ptr = list->blocks[block_id] + sizeof(StorageList_BlockHeader);
		for (uint32_t block_index = 0; block_index < list->block_itemcount; block_index++)
		{
			StorageList_ItemHeader* item_header = item_header_ptr;
			item_header_ptr += list->itemsize_storage;
			item_header->occupied = false;
			*prev_pointer = item_header;
			prev_pointer = &(item_header->next_available);
		}
	}
	*prev_pointer = NULL;
}
void* StorageList_Alloc (StorageList* list)
{
	if (list->first_available)
	{

	}
	else
	{
		list->blocks = realloc(list->blocks,sizeof(void*) * ++list->block_count);
	}
}
