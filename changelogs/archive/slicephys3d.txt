0.1.1.1
 - Internal modifications to conform to the Slice 2.6.9.0 allocator and list API changes.

0.1.1.0 
 - Added configuration options for the physics framerate.
 - Default physics framerate increased from 60 to 100.
 - Max physics steps for rendered frame is no longer a fixed value (3) but instead determined by the physics framerate and a specified framerate at and above which the physics simulation should not "lose time" which defaults to 30 FPS.
 - Physics stepping is now cycle-asynchronous (executes while rendering is performed within slCycle and is waited on before control returns to the application code).
 - Added convenience function to get the center position of a phys3dObject.
 - Added function to get access to the Bullet dynamics world in use.

0.1.0.0
 - Library released with very basic functionality.