TARGET_INCLUDE_DIR ?= /usr/local/include
TARGET_LIB_DIR ?= /usr/local/lib

INCLUDES += \
	-Iinclude \
	-I$(TARGET_INCLUDE_DIR)
# The lone -Ithird-party with nothing trailing it is for Box2D
# since it gets included like so: '#include <Box2D/Box2D.h>'

ifeq ($(OPTIMIZATIONS),1)
CFLAGS += -fPIC -O2 -fomit-frame-pointer -s
else
CFLAGS += -fPIC -w -g
endif
CXXFLAGS += $(CFLAGS) -fpermissive

NAMEVER = package

PRODUCTS = libslice.a \
	libslice2d.a \
	libslicetiles.a \
	libslice3d.a \
	libsliceopts.a \
	libslicefps.a \
	libslicegraphs.a \
	libspinqueue.a

SLICE_OBJS = $(patsubst %.cpp,%.o,$(shell find src/slice -iname '*.cpp'))
SLICE_OBJS += $(patsubst %.c,%.o,$(shell find src/slice -iname '*.c'))
SLICE_OBJS += $(patsubst %.cpp,%.o,$(shell find src/sliceui -iname '*.cpp'))
SLICE_OBJS += $(patsubst %.cpp,%.o,$(shell find src/sliceopts2 -iname '*.cpp'))

SLICE2D_OBJS = $(patsubst %.cpp,%.o,$(shell find src/slice2d -iname '*.cpp'))

SLICETILES_OBJS = $(patsubst %.cpp,%.o,$(shell find src/slicetiles -iname '*.cpp'))

SLICE3D_OBJS = $(patsubst %.cpp,%.o,$(shell find src/slice3d -iname '*.cpp'))

SLICEGRAPHS_OBJS = $(patsubst %.cpp,%.o,$(shell find src/slicegraphs -iname '*.cpp'))

all: $(PRODUCTS)
	mkdir -p bin
	g++ -fpermissive src/s3dm-to-obj/s3dm-src-to-obj.cpp -o bin/s3dm-src-to-obj
	g++ -fpermissive src/s3dm-to-obj/s3dm-to-obj.cpp -o bin/s3dm-to-obj

install: all
	install $(PRODUCTS) $(TARGET_LIB_DIR)
	cp -r include/* $(TARGET_INCLUDE_DIR)
	install bin/* /usr/local/bin

libslice.a: $(SLICE_OBJS) third-party/glad-3.1/src/glad.o
	ar -cr $@ $^
libslice2d.a: $(SLICE2D_OBJS)
	ar -cr $@ $^
libslicetiles.a: $(SLICETILES_OBJS)
	ar -cr $@ $^
libslice3d.a: $(SLICE3D_OBJS)
	ar -cr $@ $^
libsliceopts.a: src/sliceopts/sliceopts.o
	ar -cr $@ $^
libslicefps.a: src/slicefps/slicefps.o
	ar -cr $@ $^
libslicegraphs.a: $(SLICEGRAPHS_OBJS)
	ar -cr $@ $^
libspinqueue.a: src/spinqueue/spinqueue.o
	ar -cr $@ $^

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $(INCLUDES) $< -o $@

%.o: %.c
	$(CC) -c $(CFLAGS) $(INCLUDES) $< -o $@

clean:
	@rm -rvf $(shell find . -iname '*.o') *.a
