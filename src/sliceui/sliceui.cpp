#include <sliceui.h>



SDL_mutex* uiStructuralMutex;



GLmat4 AspectCorrectionLocalToScreen (slScalar media_aspect, slScalar target_aspect, uiAlignXY aspect_align = uiAlignXY())
{
    slScalar aspects_meta_ratio = media_aspect / target_aspect;
    GLmat4 scaling =   aspects_meta_ratio > 1
                     ? GLmat4_scaling(GLvec4(1 / aspects_meta_ratio, 1, 1))
                     : GLmat4_scaling(GLvec4(1,     aspects_meta_ratio, 1));

    if (aspects_meta_ratio > 1)
    {
        if (aspect_align.align_x == slAlignCenter) return scaling;

        slScalar gap = media_aspect - target_aspect;
        if (aspect_align.align_x == slAlignLeft) gap = -gap;
        gap /= target_aspect;
        return scaling * GLmat4_offset(gap, 0, 0);
    }
    else
    {
        if (aspect_align.align_y == slAlignCenter) return scaling;

        slScalar gap = 1 / media_aspect - 1 / target_aspect;
        if (aspect_align.align_y == slAlignBottom) gap = -gap;
        gap *= target_aspect;
        return scaling * GLmat4_offset(0, gap, 0);
    }
}
GLmat4 AspectCorrectionScreenToLocal (slScalar media_aspect, slScalar target_aspect, uiAlignXY aspect_align = uiAlignXY())
{
    slScalar aspects_meta_ratio = media_aspect / target_aspect;
    GLmat4 scaling =   aspects_meta_ratio > 1
                     ? GLmat4_scaling(GLvec4(aspects_meta_ratio,     1, 1))
                     : GLmat4_scaling(GLvec4(1, 1 / aspects_meta_ratio, 1));

    if (aspects_meta_ratio > 1)
    {
        if (aspect_align.align_x == slAlignCenter) return scaling;

        slScalar gap = media_aspect - target_aspect;
        if (aspect_align.align_x == slAlignRight) gap = -gap;
        gap *= target_aspect;
        return GLmat4_offset(gap, 0, 0) * scaling;
    }
    else
    {
        if (aspect_align.align_y == slAlignCenter) return scaling;

        slScalar gap = 1 / media_aspect - 1 / target_aspect;
        if (aspect_align.align_y == slAlignBottom) gap = -gap;
        gap /= target_aspect;
        return GLmat4_offset(0, gap, 0) * scaling;
    }
}



struct uiRecalcCall;
struct uiRecalcCallRecyclerProto
{
    uiRecalcCall* first_free = NULL;
    SDL_mutex* mgmt_mutex;
    slGC_Action gc_action;
    void Init ();
    void Quit ();
    void FreeUnused ();
    static void FreeUnused_Wrap (uiRecalcCallRecyclerProto* self);
    void* Acquire (size_t sz);
    void Release (void* ptr);
}
uiRecalcCallRecycler;
struct uiRecalcCall : slWorkerCall
{
    uiBox* box;
    slScalar parent_aspect;
    uiRecalcCall (slWorkerTask* task, uiBox* box, slScalar parent_aspect)
        : slWorkerCall(task)
    {
        this->box = box;
        this->parent_aspect = parent_aspect;
        Send();
    }
    void WorkFunc () override
    {
        box->RecalcTraverseMT(task,parent_aspect);
    }

    uiRecalcCall* __next_free__;
    void* operator new (size_t sz)
    {
        return uiRecalcCallRecycler.Acquire(sz);
    }
    void operator delete (void* ptr)
    {
        uiRecalcCallRecycler.Release(ptr);
    }
};
void uiRecalcCallRecyclerProto::Init ()
{
    mgmt_mutex = SDL_CreateMutex();
    gc_action.Hook(FreeUnused_Wrap,this,"recalc call recycler");
}
void uiRecalcCallRecyclerProto::Quit ()
{
    gc_action.Unhook();
    FreeUnused();
    SDL_DestroyMutex(mgmt_mutex);
}
void uiRecalcCallRecyclerProto::FreeUnused ()
{
    SDL_LockMutex(mgmt_mutex);
    while (first_free)
    {
        uiRecalcCall* rem = first_free;
        first_free = rem->__next_free__;
        free(rem);
    }
    SDL_UnlockMutex(mgmt_mutex);
}
static void uiRecalcCallRecyclerProto::FreeUnused_Wrap (uiRecalcCallRecyclerProto* self)
{
    self->FreeUnused();
}
void* uiRecalcCallRecyclerProto::Acquire (size_t sz)
{
    SDL_LockMutex(mgmt_mutex);
    if (first_free)
    {
        uiRecalcCall* out = first_free;
        first_free = out->__next_free__;
        SDL_UnlockMutex(mgmt_mutex);
        return out;
    }
    else
    {
        SDL_UnlockMutex(mgmt_mutex);
        return malloc(sz);
    }
}
void uiRecalcCallRecyclerProto::Release (void* ptr)
{
    uiRecalcCall* call = ptr;
    SDL_LockMutex(mgmt_mutex);
    call->__next_free__ = first_free;
    first_free = call;
    SDL_UnlockMutex(mgmt_mutex);
}



void uiBox::OnZero ()
{
    delete this;
}
uiBox* uiBox::Detach ()
{
    SDL_LockMutex(uiStructuralMutex);

    if (parent)
    {
        parent->boxes.Remove(this);
        parent = NULL;
        if (strong_parent) Abandon();
    }

    SDL_UnlockMutex(uiStructuralMutex);

    return this;
}
uiBoxRef uiBox::AttachTo (uiBoxRef parent, bool strong_parent)
{
    Detach();

    SDL_LockMutex(uiStructuralMutex);

    parent->boxes.Append(this);
    this->parent = parent.operator->();
    if (this->strong_parent = strong_parent) Reserve();
    parent->re_sort = true;

    SDL_UnlockMutex(uiStructuralMutex);

    stale = true; // New parent dimensions may differ when ours are unchanged.

    return this;
}
uiBox::~uiBox ()
{
    Detach();
    while (uiBox* inner = boxes.First()) inner->Detach();
}
bool uiBoxesInOrder (uiBox* first, uiBox* second)
{
    return first->GetZ() >= second->GetZ();
}
void uiBox::RecalcThis (slScalar parent_aspect)
{
    if (re_sort)
    {
        re_sort = false;
        boxes.Sort(uiBoxesInOrder);
    }

    if (parent) target_stencil = parent->target_stencil;
    else target_stencil = 0;
    if (clips_descendants) target_stencil++;

    was_stale = stale || (parent_aspect != previous_parent_aspect);
    if (!was_stale) return;
    previous_parent_aspect = parent_aspect;
    stale = false;

    slScalar adjusted_aspect = target_aspect < 0 ? parent_aspect : target_aspect;
    GLmat4 rot_correction_lts = AspectCorrectionLocalToScreen(adjusted_aspect,1);
    GLmat4 rot_correction_stl = AspectCorrectionScreenToLocal(adjusted_aspect,1);
    final_local_aspect = adjusted_aspect * (size.x / size.y);
    slVec2 center_pos = topleft_pos * 2 - 1 + size;
    local_to_screen =   GLmat4_offset(center_pos.x, -center_pos.y, 0)
                      * rot_correction_lts
                      * GLmat4_zrotate(-slDegToRad_F(rotate_degrees))
                      * rot_correction_stl
                      * GLmat4_scaling(GLvec4(size.x, size.y, 1));
    screen_to_local =   GLmat4_scaling(GLvec4(1/size.x, 1/size.y, 1))
                      * rot_correction_lts
                      * GLmat4_zrotate(slDegToRad_F(rotate_degrees))
                      * rot_correction_stl
                      * GLmat4_offset(-center_pos.x, center_pos.y, 0);
    if (target_aspect >= 0)
    {
        GLmat4 main_correction_lts = AspectCorrectionLocalToScreen(parent_aspect,target_aspect,aspect_align);
        GLmat4 main_correction_stl = AspectCorrectionScreenToLocal(parent_aspect,target_aspect,aspect_align);
        local_to_screen = main_correction_lts * local_to_screen;
        screen_to_local = screen_to_local * main_correction_stl;
    }
    if (parent)
    {
        local_to_screen = parent->local_to_screen * local_to_screen;
        screen_to_local = screen_to_local * parent->screen_to_local;
    }

    topleft = ThroughMat4(slVec2(-1,1),local_to_screen),
    topright = ThroughMat4(slVec2(1,1),local_to_screen),
    bottomleft = ThroughMat4(slVec2(-1,-1),local_to_screen),
    bottomright = ThroughMat4(slVec2(1,-1),local_to_screen);
}
void uiBox::RecalcTraverseST (slScalar parent_aspect)
{
    RecalcThis(parent_aspect);
    for (slLLCursor<uiBox> csr(&boxes); uiBox* inner_box = csr.Here(); csr.Forward())
    {
        if (was_stale) inner_box->stale = true;
        inner_box->RecalcTraverseST(final_local_aspect);
    }
}
void uiBox::RecalcTraverseMT (slWorkerTask* task, slScalar parent_aspect)
{
    RecalcThis(parent_aspect);
    for (slLLCursor<uiBox> csr(&boxes); uiBox* inner_box = csr.Here(); csr.Forward())
    {
        if (was_stale) inner_box->stale = true;
        new uiRecalcCall(task,inner_box,final_local_aspect);
    }
}
slVec2 uiBox::ThroughMat4 (slVec2 in_vec, GLmat4 mat)
{
    GLvec4 in_v4(in_vec.x,in_vec.y,0);
    GLvec4 out_v4 = mat * in_v4;
    return slVec2(out_v4.x,out_v4.y);
}
uiBox* uiStenciledBox;
/*  During a frame, address of most recently stenciled box. If the box about
    to be stenciled is at this address, we can use the stencil buffer as-is.
*/
void uiBox::StencilThis ()
{
    if (!target_stencil)
    {
        glDisable(GL_STENCIL_TEST);
        return;
    }

    glEnable(GL_STENCIL_TEST);

    /*uiBox* above = this;
    while (above)
    {
        if (uiStenciledBox == above)
        {
            / *  Stencil the boxes below the stenciled box.  * /

            goto NORMAL_DRAWING;
        }
    }*/

    /*  The stenciled box is not an ancestor.  */
    glClearStencil(0);
    glClear(GL_STENCIL_BUFFER_BIT);

    glStencilOp(GL_INCR,GL_INCR,GL_INCR);
    glStencilFunc(GL_ALWAYS,0,~(GLuint)0);
    glColorMask(GL_FALSE,GL_FALSE,GL_FALSE,GL_FALSE);

    uiBox* clipper = this;
    while (clipper)
    {
        if (clipper->clips_descendants)
        {
            glBegin(GL_TRIANGLE_FAN);
            glColor4ub(0xFF,0xFF,0xFF,0x3F);
            glVertex2f(clipper->topleft.x,clipper->topleft.y);
            glVertex2f(clipper->topright.x,clipper->topright.y);
            glVertex2f(clipper->bottomright.x,clipper->bottomright.y);
            glVertex2f(clipper->bottomleft.x,clipper->bottomleft.y);
            glEnd();
        }

        clipper = clipper->parent;
    }

    glStencilOp(GL_KEEP,GL_KEEP,GL_KEEP);
    glStencilFunc(GL_EQUAL,target_stencil,~(GLuint)0);
    glColorMask(GL_TRUE,GL_TRUE,GL_TRUE,GL_TRUE);
}
void uiBox::DrawThis ()
{
    SDL_Color back_color = hovered ? GetHoverBackColor() : GetBackColor();
    slTexRef tex = hovered ? GetHoverTexture() : GetTexture();
    SDL_Color tex_mask = hovered ? GetHoverTexMask() : GetTexMask();
    SDL_Color border_color = hovered ? GetHoverBorderColor() : GetBorderColor();
    if (!back_color.a && (!tex->tex || !tex_mask.a) && !border_color.a)
        return;

    if (parent) parent->StencilThis();
    else glDisable(GL_STENCIL_TEST);

    if (back_color.a)
    {
        //printf("back ");
        glDisable(GL_TEXTURE_2D);
        glColor4ubv((GLubyte*)&back_color);
        glBegin(GL_TRIANGLE_FAN);
        glVertex2f(topleft.x,topleft.y);
        glVertex2f(topright.x,topright.y);
        glVertex2f(bottomright.x,bottomright.y);
        glVertex2f(bottomleft.x,bottomleft.y);
        glEnd();
    }

    if (tex->tex && tex_mask.a) /* i.e. tex isn't slNoTexture */
    {
        //printf("tex ");
        glColor4ubv((GLubyte*)&tex_mask);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,tex->tex);
		slSetTextureClamping(GL_CLAMP_TO_EDGE);
        GLmat4 tex_correction_lts = AspectCorrectionLocalToScreen(final_local_aspect,tex->aspect,tex_align);
        GLmat4 txform = local_to_screen * tex_correction_lts;
        slVec2 topleft = ThroughMat4(slVec2(-1,1),txform),
               topright = ThroughMat4(slVec2(1,1),txform),
               bottomleft = ThroughMat4(slVec2(-1,-1),txform),
               bottomright = ThroughMat4(slVec2(1,-1),txform);
        glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f(0,1);
        glVertex2f(topleft.x,topleft.y);
        glTexCoord2f(1,1);
        glVertex2f(topright.x,topright.y);
        glTexCoord2f(1,0);
        glVertex2f(bottomright.x,bottomright.y);
        glTexCoord2f(0,0);
        glVertex2f(bottomleft.x,bottomleft.y);
        glEnd();
    }

    if (border_color.a)
    {
        //printf("border ");
        glDisable(GL_TEXTURE_2D);
        glColor4ubv((GLubyte*)&border_color);
        glBegin(GL_LINE_LOOP);
        glVertex2f(topleft.x,topleft.y);
        glVertex2f(topright.x,topright.y);
        glVertex2f(bottomright.x,bottomright.y);
        glVertex2f(bottomleft.x,bottomleft.y);
        glEnd();
    }
    //putchar('\n');
}
bool uiBox::CheckHover (slVec2 media_mouse, bool already_taken)
{
    if (!already_taken)
    {
        slVec2 local_mouse = ThroughMat4(slVec2(media_mouse.x,1-media_mouse.y) * 2 - 1,
                                         screen_to_local);
        hovered =    local_mouse.x >= -1
                  && local_mouse.x <=  1
                  && local_mouse.y >= -1
                  && local_mouse.y <=  1;
        if (draw_over_descendants) already_taken = hovered;
    }
    else hovered = false;

    for (slLLCursor<uiBox> csr(&boxes, boxes.Last()); uiBox* box = csr.Here(); csr.Back())
    {
        if (box->CheckHover(media_mouse, already_taken))
        {
            already_taken = true;
            hovered = false;
        }
    }

    // Return value indicates whether we, or any descendant, took the hover.
    return hovered && captures_hover;
}
void uiBox::Draw ()
{
    if (!draw_over_descendants) DrawThis();
    for (slLLCursor<uiBox> csr(&boxes); uiBox* box = csr.Here(); csr.Forward())
    {
        box->Draw();
    }
    if (draw_over_descendants) DrawThis();
}



uiUI::uiUI (slScalar target_aspect)
{
    root_box
        ->SetPos(0)
        ->SetSize(1)
        ->SetTargetAspect(target_aspect)
    ;
}
void uiUI::RecalcST (slScalar media_aspect)
{
    root_box->RecalcTraverseST(media_aspect);
}
void uiUI::RecalcMT (slScalar media_aspect)
{
    slWorkerTask task;
    root_box->RecalcTraverseMT(&task,media_aspect);
}
void uiUI::CheckHover (slVec2 media_mouse)
{
    root_box->CheckHover(media_mouse);
}
void uiUI::Draw ()
{
    SDL_LockMutex(uiStructuralMutex);
    /*  Don't allow the tree structure to be modified
        while we are traversing it.  */

    //Uint64 start = SDL_GetPerformanceCounter();
    RecalcST(slGetWindowAspect());
    //Uint64 stop = SDL_GetPerformanceCounter();
    CheckHover(slGetMouse());
    //Uint64 stop2 = SDL_GetPerformanceCounter();
    //printf("drawing...\n");
    glUseProgram(0);
    glActiveTexture(GL_TEXTURE0);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_SCISSOR_TEST);

    uiStenciledBox = NULL;

    root_box->Draw();

    //printf("done\n");
    //Uint64 stop3 = SDL_GetPerformanceCounter();

    SDL_UnlockMutex(uiStructuralMutex);

    //Uint64 cycles = stop - start;
    //double seconds = cycles / (double)SDL_GetPerformanceFrequency();
    //Uint64 cycles2 = stop2 - stop;
    //double seconds2 = cycles2 / (double)SDL_GetPerformanceFrequency();
    //Uint64 cycles3 = stop3 - stop2;
    //double seconds3 = cycles3 / (double)SDL_GetPerformanceFrequency();
    //printf("time spent recalculating: %lf seconds (%llu cycles)\n",seconds,cycles);
    //printf("           hoverchecking: %lf seconds (%llu cycles)\n",seconds2,cycles2);
    //printf("                 drawing: %lf seconds (%llu cycles)\n",seconds3,cycles3);
    //printf("                -> total: %lf seconds (%llu cycles)\n",seconds+seconds2+seconds3,cycles+cycles2+cycles3);
}



uiUI* uiMainUI;
void uiInit ()
{
    uiStructuralMutex = SDL_CreateMutex();
    uiRecalcCallRecycler.Init();
    uiMainUI = new uiUI;
}
void uiQuit ()
{
    delete uiMainUI;
    uiRecalcCallRecycler.Quit();
    SDL_DestroyMutex(uiStructuralMutex);
}
