#include <slicetiles.h>

stAllTiles::stAllTiles (slInt2 blc, slInt2 wh, bool* protobuf)
{
    this->blc = blc;
    this->wh = wh;
    tiles = malloc(sizeof(stTile) * wh.w * wh.h);

    stTile* tile = tiles;
    bool* proto = protobuf;

    Uint8 pixels [] = {
        0xFF,0x00,0x00,0xFF, 0x7F,0x7F,0x7F,0xFF, 0x7F,0x7F,0x7F,0xFF, 0x00,0xFF,0x00,0xFF,
        0x7F,0x7F,0x7F,0xFF, 0xFF,0xFF,0xFF,0xFF, 0xFF,0xFF,0xFF,0xFF, 0x7F,0x7F,0x7F,0xFF,
        0x7F,0x7F,0x7F,0xFF, 0xFF,0xFF,0xFF,0xFF, 0xFF,0xFF,0xFF,0xFF, 0x7F,0x7F,0x7F,0xFF,
        0x00,0x00,0xFF,0xFF, 0x7F,0x7F,0x7F,0xFF, 0x7F,0x7F,0x7F,0xFF, 0xFF,0xFF,0x00,0xFF,
    };
    slTexRef tex_some = slCreateCustomTexture(4,4,pixels);//slLoadTexture("#");
    slTexRef tex_none = slNoTexture;
    for (slBS y = 0; y < wh.h; y++)
    {
        for (slBS x = 0; x < wh.w; x++, tile++, proto++)
        {
            tile->collide = *proto;
            tile->visual = s2dCreateInstance(*proto ? tex_some : tex_none);
            slInt2 tilepos = blc + slInt2(x,y);
            tile->visual->SetDims(slVec2(0.5) + tilepos,1,200);
        }
    }
}
stAllTiles::~stAllTiles ()
{
    slBU total = wh.w * wh.h;
    stTile* tile = tiles;
    for (slBU i = 0; i < total; i++, tile++)
    {
        s2dDestroyInstance(tile->visual);
    }
    free(tiles);
}
bool stAllTiles::Blocked (slInt2 pos)
{
    pos -= blc;
    if (pos.x < 0 || pos.y < 0 || pos.x >= wh.w || pos.y >= wh.h) return false;
    return tiles[pos.y * wh.w + pos.x].collide;
}

slVec2 CornersVisCenter (stFixed2 blc, stFixed2 trc)
{
    trc.xy++;
    return (trc + blc).ToFloats() * 0.5;
}
slVec2 CornersVisHeight (stFixed2 blc, stFixed2 trc)
{
    trc.xy++;
    return (trc - blc).ToFloats();
}
// stEntity::stEntity (stWorld* world, slTexRef tex, slVec2 wh, slVec2 xy)
// {
//
// }
void stEntity::SpatialRegisterX ()
{
    stEntity* right_parent = world->right_tree.Insert(this);
    if (!right_parent) world->right_list.Append(this);
    else if (right_parent->right_tree_data.left == this) world->right_list.InsertBefore(this,right_parent);
    else world->right_list.InsertAfter(this,right_parent);
    stEntity* left_parent = world->left_tree.Insert(this);
    if (!left_parent) world->left_list.Append(this);
    else if (left_parent->left_tree_data.left == this) world->left_list.InsertBefore(this,left_parent);
    else world->left_list.InsertAfter(this,left_parent);
}
void stEntity::SpatialRegisterY ()
{
    stEntity* up_parent = world->up_tree.Insert(this);
    if (!up_parent) world->up_list.Append(this);
    else if (up_parent->up_tree_data.left == this) world->up_list.InsertBefore(this,up_parent);
    else world->up_list.InsertAfter(this,up_parent);
    stEntity* down_parent = world->down_tree.Insert(this);
    if (!down_parent) world->down_list.Append(this);
    else if (down_parent->down_tree_data.left == this) world->down_list.InsertBefore(this,down_parent);
    else world->down_list.InsertAfter(this,down_parent);
}
void stEntity::SpatialUnregisterX ()
{
    world->right_tree.Remove(this);
    world->right_list.Remove(this);
    world->left_tree.Remove(this);
    world->left_list.Remove(this);
}
void stEntity::SpatialUnregisterY ()
{
    world->up_tree.Remove(this);
    world->up_list.Remove(this);
    world->down_tree.Remove(this);
    world->down_list.Remove(this);
}
stEntity::stEntity (stWorld* world, slTexRef tex, stFixed2 blc, stFixed2 trc)
{
    this->world = world;
    world->entities.Add(this);

    visual = s2dCreateInstance(tex);
    this->blc = blc;
    this->trc = trc;
    center_now = CornersVisCenter(blc,trc);
    center_prev = this->center_now;
    visual->SetDims(center_now,CornersVisHeight(blc,trc),100);

    SpatialRegisterX();
    SpatialRegisterY();
}
stEntity::~stEntity ()
{
    SpatialUnregisterX();
    SpatialUnregisterY();

    s2dDestroyInstance(visual);
    world->entities.Remove(this);
}



/*  Reason for using memory address as tie breaker in Compare functions:

    If we return 0, it means the entities are identical to the
    tree implementation. Therefore to prevent inserting duplicates
    we will simply use the memory addresses as the tie-breaker.

    The insertion of duplicates is forbidden because it would cause
    problems with removal, and because true duplicates [same memory
    address] cannot exist in the tree twice without clobbering the node
    data from the first insertion.  */

int stEntitiesUpTree::Compare (stEntity* entity_a, stEntity* entity_b)
{
    /* Return 1 for in-order, -1 for reverse-order, 0 for equality. */
    if (entity_a->blc.y < entity_b->blc.y) return  1;
    if (entity_a->blc.y > entity_b->blc.y) return -1;
    /* Memory address as tie breaker. */
    if (entity_a < entity_b) return  1;
    if (entity_a > entity_b) return -1;
    return 0;
}
int stEntitiesUpTree::LookupCompare (stEntity* intree_entity, stEntity* search_entity)
{
    /*  We're looking for the "edge" item, the very first one that matches the
        search criteria.

        If the item we're on is "in" and the item before it is "out" (including
        if it does not exist), this is what we  want to return from Lookup
        (comparison should yield 0).

        If the item we're on is "in", but so is the item before it, then we are
        to the right of the "edge". Since the in-tree entity is "after" the
        edge, comparison should yield -1.

        If the item we're on is "out", then the in-tree entity is "before" the
        edge, so comparison should yield 1.  */

    if (intree_entity->blc.y <= search_entity->trc.y)
    {
        /* We're below the minimum y-value, so this item is "out". */
        return 1;
    }
    /* We're "in". */
    stEntity* prev_entity = intree_entity->up_list_data.prev;
    if (!prev_entity)
    {
        /* There is no previous item, we're the first. */
        return 0;
    }
    /* A previous item exists. */
    if (prev_entity->blc.y <= search_entity->trc.y)
    {
        /* Previous item is "out", we're the first. */
        return 0;
    }
    /* Previous item is "in". */
    return -1;
}
void stEntitiesUpTree::PrintItem (stEntity* entity)
{
    printf("bottom y = %lld:%X, addr=%llX\n",
        (long long)entity->blc.TileID().y,
        (unsigned int)entity->blc.Inner().y,
        entity
    );
}

int stEntitiesDownTree::Compare (stEntity* entity_a, stEntity* entity_b)
{
    /* Return 1 for in-order, -1 for reverse-order, 0 for equality. */
    if (entity_a->trc.y > entity_b->trc.y) return  1;
    if (entity_a->trc.y < entity_b->trc.y) return -1;
    /* Memory address as tie breaker. */
    if (entity_a < entity_b) return  1;
    if (entity_a > entity_b) return -1;
    return 0;
}
int stEntitiesDownTree::LookupCompare (stEntity* intree_entity, stEntity* search_entity)
{
    if (intree_entity->trc.y >= search_entity->blc.y)
        return 1; /* Above maximum y-value, so this item is "out". */

    /* We're "in". */

    stEntity* prev_entity = intree_entity->down_list_data.prev;
    if (!prev_entity)
        return 0; /* There is no previous item, we're the first. */

    /* A previous item exists. */

    if (prev_entity->trc.y >= search_entity->blc.y)
        return 0; /* Previous item is "out", we're the first. */

    return -1; /* Previous item is "in". */
}
void stEntitiesDownTree::PrintItem (stEntity* entity)
{
    printf("top y = %lld:%X, addr=%llX\n",
        (long long)entity->trc.TileID().y,
        (unsigned int)entity->trc.Inner().y,
        entity
    );
}

int stEntitiesRightTree::Compare (stEntity* entity_a, stEntity* entity_b)
{
    /* Return 1 for in-order, -1 for reverse-order, 0 for equality. */
    if (entity_a->blc.x < entity_b->blc.x) return  1;
    if (entity_a->blc.x > entity_b->blc.x) return -1;
    /* Memory address as tie breaker. */
    if (entity_a < entity_b) return  1;
    if (entity_a > entity_b) return -1;
    return 0;
}
int stEntitiesRightTree::LookupCompare (stEntity* intree_entity, stEntity* search_entity)
{
    if (intree_entity->blc.x <= search_entity->trc.x)
        return 1; /* Above maximum x-value, so this item is "out". */

    /* We're "in". */

    stEntity* prev_entity = intree_entity->right_list_data.prev;
    if (!prev_entity)
        return 0; /* There is no previous item, we're the first. */

    /* A previous item exists. */

    if (prev_entity->blc.x <= search_entity->trc.x)
        return 0; /* Previous item is "out", we're the first. */

    return -1; /* Previous item is "in". */
}
void stEntitiesRightTree::PrintItem (stEntity* entity)
{
    printf("left x = %lld:%X, addr=%llX\n",
        (long long)entity->blc.TileID().x,
        (unsigned int)entity->blc.Inner().x,
        entity
    );
}

int stEntitiesLeftTree::Compare (stEntity* entity_a, stEntity* entity_b)
{
    /* Return 1 for in-order, -1 for reverse-order, 0 for equality. */
    if (entity_a->trc.x > entity_b->trc.x) return  1;
    if (entity_a->trc.x < entity_b->trc.x) return -1;
    /* Memory address as tie breaker. */
    if (entity_a < entity_b) return  1;
    if (entity_a > entity_b) return -1;
    return 0;
}
int stEntitiesLeftTree::LookupCompare (stEntity* intree_entity, stEntity* search_entity)
{
    if (intree_entity->trc.x >= search_entity->blc.x)
        return 1; /* Above maximum x-value, so this item is "out". */

    /* We're "in". */

    stEntity* prev_entity = intree_entity->left_list_data.prev;
    if (!prev_entity)
        return 0; /* There is no previous item, we're the first. */

    /* A previous item exists. */

    if (prev_entity->trc.x >= search_entity->blc.x)
        return 0; /* Previous item is "out", we're the first. */

    return -1; /* Previous item is "in". */
}
void stEntitiesLeftTree::PrintItem (stEntity* entity)
{
    printf("right x = %lld:%X, addr=%llX\n",
        (long long)entity->trc.TileID().x,
        (unsigned int)entity->trc.Inner().x,
        entity
    );
}



stWorld::stWorld (slInt2 tlc, slInt2 wh, bool* protobuf) : tiles(tlc,wh,protobuf)
{

}
typedef void (*stDeleterFunc) (void*);
stWorld::~stWorld ()
{
    while (entities.itemcount) delete entities.items[0];
    //entities.UntilEmpty((stDeleterFunc)stEntity::~stEntity);
}
bool stWorld::Blocked (slInt2 pos)
{
    return tiles.Blocked(pos);
}

bool stWorld::HorizontalStrikefaceClear (slBS x_low, slBS x_high, slBS y)
{
    for (slBS scan_x = x_low; scan_x <= x_high; scan_x++)
        if (tiles.Blocked(slInt2(scan_x,y)))
            return false;
    return true;
}
bool stWorld::VerticalStrikefaceClear (slBS x, slBS y_low, slBS y_high)
{
    for (slBS scan_y = y_low; scan_y <= y_high; scan_y++)
        if (tiles.Blocked(slInt2(x,scan_y)))
            return false;
    return true;
}

inline slForceInline slBS stIntRoundedDiv (slBS dividend, slBS divisor)
{
    // >> has unwanted effects on negatives values so use proper / instead..
    if (dividend >= 0 == divisor >= 0) dividend += divisor / 2;
    else                               dividend -= divisor / 2;
    return dividend / divisor;
}
struct Corner_Handle : public stFixed2
{
    stEntity* entity;
    stFixed2 dims;
    Corner_Handle (stEntity* entity, stFixed2 corner) : stFixed2(corner)
    {
        this->entity = entity;
        dims = entity->trc - entity->blc;
        xy = corner.xy;
    }
    inline stFixed2 operator= (stFixed2 from) slForceInline
    {
        xy = from.xy;
        return *this;
    }
};
class TRC_Handle : public Corner_Handle
{
public:
    TRC_Handle (stEntity* entity) : Corner_Handle(entity, entity->trc) {}
    ~TRC_Handle ()
    {
        entity->blc = *this - dims;
        entity->trc = *this;
    }
    using Corner_Handle::operator=;
};
class TLC_Handle : public Corner_Handle
{
public:
    TLC_Handle (stEntity* entity) : Corner_Handle(entity, (slRawInt2){entity->blc.x, entity->trc.y}) {}
    ~TLC_Handle ()
    {
        entity->blc = (slRawInt2){x, y - dims.y};
        entity->trc = (slRawInt2){x + dims.x, y};
    }
    using Corner_Handle::operator=;
};
class BRC_Handle : public Corner_Handle
{
public:
    BRC_Handle (stEntity* entity) : Corner_Handle(entity, (slRawInt2){entity->trc.x, entity->blc.y}) {}
    ~BRC_Handle ()
    {
        entity->blc = (slRawInt2){x - dims.x, y};
        entity->trc = (slRawInt2){x, y + dims.y};
    }
    using Corner_Handle::operator=;
};
class BLC_Handle : public Corner_Handle
{
public:
    BLC_Handle (stEntity* entity) : Corner_Handle(entity, entity->blc) {}
    ~BLC_Handle ()
    {
        entity->blc = *this;
        entity->trc = *this + dims;
    }
    using Corner_Handle::operator=;
};
class Dest_BLC : public stFixed2
{
    stFixed2* dest_trc;
    stFixed2 dims;
public:
    Dest_BLC (stFixed2* dest_trc, stFixed2 dims) : stFixed2(*dest_trc - dims)
    {
        this->dest_trc = dest_trc;
        this->dims = dims;
    }
    ~Dest_BLC ()
    {
        *dest_trc = *this + dims;
    }
};
class Dest_BRC : public stFixed2
{
    stFixed2* dest_trc;
    stFixed2 dims;
public:
    Dest_BRC (stFixed2* dest_trc, stFixed2 dims) : stFixed2((slRawInt2){dest_trc->x, dest_trc->y - dims.y})
    {
        this->dest_trc = dest_trc;
        this->dims = dims;
    }
    ~Dest_BRC ()
    {
        *dest_trc = (slRawInt2){x, y + dims.y};
    }
};
class Dest_TLC : public stFixed2
{
    stFixed2* dest_trc;
    stFixed2 dims;
public:
    Dest_TLC (stFixed2* dest_trc, stFixed2 dims) : stFixed2((slRawInt2){dest_trc->x - dims.x, dest_trc->y})
    {
        this->dest_trc = dest_trc;
        this->dims = dims;
    }
    ~Dest_TLC ()
    {
        *dest_trc = (slRawInt2){x + dims.x, y};
    }
};
void BounceTarget (slBS& target, slBS hit, slScalar bouncy, slScalar& vel_component)
{
    slBS prediff = target - hit;
    slBS absdiff = prediff > 0 ? prediff : -prediff;
    slBS bouncediff = absdiff * bouncy;
    if (bouncediff > absdiff) bouncediff = absdiff;
    bouncediff--;
    if (bouncediff < 0) bouncediff = 0;
    if (prediff < 0) bouncediff = -bouncediff;
    target = hit - bouncediff;
    vel_component *= -bouncy;
}
inline slForceInline slBS LowestInTile (slBS coord)
{
    return coord & (slBS)~0xFFFF;
}
inline slForceInline slBS HighestInTile (slBS coord)
{
    return LowestInTile(coord) | 0xFFFF;
}
#define ClampInTile(from,intercept,axis)                \
    if (intercept.TileID().axis < from.TileID().axis)   \
        intercept.axis = LowestInTile(from.axis);       \
    if (intercept.TileID().axis > from.TileID().axis)   \
        intercept.axis = HighestInTile(from.axis);
stFixed2 GridInterceptRight (stFixed2 from, stFixed2 to)
{
    slBS room_to_move = 0xFFFF - from.Inner().x;
    stFixed2 direction = to - from;
    stFixed2 intercept = from;
    intercept.x += room_to_move;
    intercept.y += room_to_move * stIntRoundedDiv(direction.y,direction.x);
    ClampInTile(from,intercept,y)
    return intercept;
}
stFixed2 GridInterceptUp (stFixed2 from, stFixed2 to)
{
    slBS room_to_move = 0xFFFF - from.Inner().y;
    stFixed2 direction = to - from;
    stFixed2 intercept = from;
    intercept.x += room_to_move * stIntRoundedDiv(direction.x,direction.y);
    intercept.y += room_to_move;
    ClampInTile(from,intercept,x)
    return intercept;
}
stFixed2 GridInterceptLeft (stFixed2 from, stFixed2 to)
{
    slBS room_to_move = from.Inner().x;
    stFixed2 direction = to - from;
    stFixed2 intercept = from;
    intercept.x -= room_to_move;
    intercept.y -= room_to_move * stIntRoundedDiv(direction.y,direction.x);
    ClampInTile(from,intercept,y)
    return intercept;
}
stFixed2 GridInterceptDown (stFixed2 from, stFixed2 to)
{
    slBS room_to_move = from.Inner().y;
    stFixed2 direction = to - from;
    stFixed2 intercept = from;
    intercept.x -= room_to_move * stIntRoundedDiv(direction.x,direction.y);
    intercept.y -= room_to_move;
    ClampInTile(from,intercept,x)
    return intercept;
}
void ToRightIntercept (stFixed2& from, stFixed2& to, stFixed2 intercept, stEntity* entity)
{
    from = intercept;
    bool passable = entity->world->VerticalStrikefaceClear(
        from.TileID().x + 1,
        entity->blc.TileID().y, entity->trc.TileID().y
    );
    if (passable) from.x++;
    else BounceTarget(to.x, from.x, entity->bouncy, entity->vel.x);
}
void ToUpIntercept (stFixed2& from, stFixed2& to, stFixed2 intercept, stEntity* entity)
{
    from = intercept;
    bool passable = entity->world->HorizontalStrikefaceClear(
        entity->blc.TileID().x, entity->trc.TileID().x,
        from.TileID().y + 1
    );
    if (passable) from.y++;
    else BounceTarget(to.y, from.y, entity->bouncy, entity->vel.y);
}
void ToLeftIntercept (stFixed2& from, stFixed2& to, stFixed2 intercept, stEntity* entity)
{
    from = intercept;
    bool passable = entity->world->VerticalStrikefaceClear(
        from.TileID().x - 1,
        entity->blc.TileID().y, entity->trc.TileID().y
    );
    if (passable) from.x--;
    else BounceTarget(to.x, from.x, entity->bouncy, entity->vel.x);
}
void ToDownIntercept (stFixed2& from, stFixed2& to, stFixed2 intercept, stEntity* entity)
{
    from = intercept;
    bool passable = entity->world->HorizontalStrikefaceClear(
        entity->blc.TileID().x, entity->trc.TileID().x,
        from.TileID().y - 1
    );
    if (passable) from.y--;
    else BounceTarget(to.y, from.y, entity->bouncy, entity->vel.y);
}
#define TryLocalWarp2(from,to,axis,cursor,cursor_corner,cursor_compare)     \
    if (    to.TileID().axis == from.TileID().axis                          \
         && (    !cursor.Here()                                             \
              || to.axis cursor_compare cursor.Here()->cursor_corner.axis)) \
    {                                                                       \
        steppart = "warp-"#axis;                                            \
        from.axis = to.axis;                                                \
        continue;                                                           \
    }
bool DetectConflict (stWorld* world, stEntity* ent)
{
    /* Not intended for production. Would result in O(N^2) physics substep. */
    for (slBU i = 0; i < world->entities.itemcount; i++)
    {
        stEntity* other = world->entities.items[i];
        if (other == ent) continue;
        bool clear =    ent->trc.x < other->blc.x || ent->blc.x > other->trc.x
                     || ent->trc.y < other->blc.y || ent->blc.y > other->trc.y;
        if (!clear) return true;
    }
    return false;
}
struct StringBuilderString
{
    slLLNodeData<StringBuilderString> node_data;
    char* text;
};
struct StringBuilder
{
    slLinkedList<StringBuilderString> strings =
        slLinkedList<StringBuilderString>(&StringBuilderString::node_data);
    void Add (char* fmt, ...)
    {
        //printf("printing\n");
        StringBuilderString* str = new StringBuilderString;
        va_list args;
        va_start(args,fmt);
        vasprintf(&str->text,fmt,args);
        va_end(args);
        strings.Append(str);
        //printf("printed\n");
    }
    char* Get ()
    {
        //printf("get all\n");
        size_t len = 0;
        for (slLLCursor<StringBuilderString> cursor(&strings);
             StringBuilderString* str = cursor.Here();
             cursor.Forward())
        {
            len += strlen(str->text);
        }
        size_t pos = 0;
        char* out = malloc(len + 1);
        for (slLLCursor<StringBuilderString> cursor(&strings);
             StringBuilderString* str = cursor.Here();
             cursor.Forward())
        {
            size_t local_len = strlen(str->text);
            memcpy(out + pos, str->text, local_len);
            pos += local_len;
        }
        out[len] = 0;
        //printf("got all\n");
        return out;
    }
    void Clear ()
    {
        //printf("clearing\n");
        slLLCursor<StringBuilderString> cursor(&strings);
        while (StringBuilderString* del = cursor.Here())
        {
            cursor.Forward();
            strings.Remove(del);
            free(del->text);
            delete del;
        }
        //printf("cleared\n");
    }
    ~StringBuilder ()
    {
        Clear();
    }
};
void stWorld::DiscreteStep ()
{
    slVec2 step_gravity = gravity * stepping_interval;

    for (slBU i = 0; i < entities.itemcount; i++)
    {
        stEntity* entity = entities.items[i];

        entity->vel += step_gravity;

        slVec2 capped_vel = entity->vel;
        if (MAX_VEL >= 0)
        {
            if (MAX_VEL_COMBINED_AXES)
            {
                slVec2 vel_sq = capped_vel * capped_vel;
                slScalar vel_mag = slsqrt(vel_sq.x + vel_sq.y);
                if (vel_mag > MAX_VEL) capped_vel *= (MAX_VEL / vel_mag);
            }
            else
            {
                capped_vel.x = slfclamp(capped_vel.x, -MAX_VEL, MAX_VEL);
                capped_vel.y = slfclamp(capped_vel.y, -MAX_VEL, MAX_VEL);
            }
            if (MAX_VEL_EXPORT) entity->vel = capped_vel;
        }

        stFixed2 dims = entity->trc - entity->blc;

        slVec2 total_motion = capped_vel * stepping_interval;
        stFixed2 final_trc = entity->trc + stFixed2::FromFloats(total_motion);

        bool moving_x = final_trc.x != entity->trc.x,
             moving_y = final_trc.y != entity->trc.y;
        if (moving_x) entity->SpatialUnregisterX();
        if (moving_y) entity->SpatialUnregisterY();
        slLLCursor<stEntity>    up_cursor(&   up_list,NULL);
        slLLCursor<stEntity>  down_cursor(& down_list,NULL);
        slLLCursor<stEntity> right_cursor(&right_list,NULL);
        slLLCursor<stEntity>  left_cursor(& left_list,NULL);
        bool up_cursor_valid = false, down_cursor_valid = false,
          right_cursor_valid = false, left_cursor_valid = false;

        Uint64 substep_started = SDL_GetPerformanceCounter();
        char* stepname = "none";
        char* steppart = "n/a";

        bool was_conflict = DetectConflict(this,entity);
        StringBuilder pres;
        while (1)
        {
            bool is_conflict = DetectConflict(this,entity);
            if (is_conflict && !was_conflict)
            {
                char* pres_str = pres.Get();
                printf("%s",pres_str);
                free(pres_str);
                pres.Clear();
                printf("conflict directly after %s[%s]\n",stepname,steppart);
            }
            else
            {
                pres.Add("%s[%s](%s)\n",stepname,steppart,is_conflict?"<conf>":"");
            }
            was_conflict = is_conflict;

            stepname = "???";
            steppart = "???";

            if (max_substep_calctime >= 0)
            {
                slScalar elapsed = (SDL_GetPerformanceCounter() - substep_started) / (slScalar)SDL_GetPerformanceFrequency();
                if (elapsed > max_substep_calctime)
                {
                    final_trc = entity->trc;
                    printf("Physics substep reached processing time limit. Last step: %s\n",stepname);
                }
            }
            if (final_trc.x > entity->trc.x)
            {
                if (final_trc.y > entity->trc.y)
                {
                    stepname = "up-right";
                    /* Moving right. Moving up. */
                    TRC_Handle e_trc(entity);
                    stFixed2 intercept_xgrid = GridInterceptRight(e_trc, final_trc),
                             intercept_ygrid = GridInterceptUp   (e_trc, final_trc);
                    if (!right_cursor_valid) right_cursor.To(right_tree.Lookup(entity));
                    if (!   up_cursor_valid)    up_cursor.To(   up_tree.Lookup(entity));
                    right_cursor_valid = true; left_cursor_valid = false;
                       up_cursor_valid = true; down_cursor_valid = false;
                    TryLocalWarp2(e_trc,final_trc,x,right_cursor,blc,<)
                    TryLocalWarp2(e_trc,final_trc,y,   up_cursor,blc,<)
                    stFixed2 intercept_xcoll, intercept_ycoll;
                    stFixed2 direction = final_trc - e_trc;
                    bool intercept_xcoll_valid, intercept_ycoll_valid;
                    if (intercept_xcoll_valid = !!right_cursor.Here())
                    {
                        intercept_xcoll.x = right_cursor.Here()->blc.x - 1;
                        if (intercept_xcoll_valid = (intercept_xcoll.TileID().x == e_trc.TileID().x))
                        {
                            intercept_xcoll.y =   e_trc.y
                                                + (   (intercept_xcoll.x - e_trc.x)
                                                    * stIntRoundedDiv(direction.y,direction.x));
                            ClampInTile(e_trc,intercept_xcoll,y)
                        }
                    }
                    if (intercept_ycoll_valid = !!up_cursor.Here())
                    {
                        intercept_ycoll.y = up_cursor.Here()->blc.y - 1;
                        if (intercept_ycoll_valid = (intercept_ycoll.TileID().y == e_trc.TileID().y))
                        {
                            intercept_ycoll.x =   e_trc.x
                                                + (   (intercept_ycoll.y - e_trc.y)
                                                    * stIntRoundedDiv(direction.x,direction.y));
                            ClampInTile(e_trc,intercept_ycoll,x)
                        }
                    }
                    if (intercept_ycoll_valid && intercept_xcoll.y > intercept_ycoll.y)
                        intercept_xcoll.y = intercept_ycoll.y;
                    if (intercept_xcoll_valid && intercept_ycoll.x > intercept_xcoll.x)
                        intercept_ycoll.x = intercept_xcoll.x;
                    if (    intercept_xgrid.x < intercept_ygrid.x
                         && (!intercept_xcoll_valid || intercept_xgrid.x < intercept_xcoll.x)
                         && (!intercept_ycoll_valid || intercept_xgrid.x < intercept_ycoll.x))
                    {
                        steppart = "grid-x";
                        ToRightIntercept(e_trc, final_trc, intercept_xgrid, entity);
                    }
                    else if (    (!intercept_xcoll_valid || intercept_ygrid.y < intercept_xcoll.y)
                              && (!intercept_ycoll_valid || intercept_ygrid.y < intercept_ycoll.y))
                    {
                        steppart = "grid-y";
                        ToUpIntercept(e_trc, final_trc, intercept_ygrid, entity);
                    }
                    else if (    intercept_xcoll_valid
                              && (!intercept_ycoll_valid || intercept_xcoll.x < intercept_ycoll.x))
                    {
                        steppart = "coll-x";
                        e_trc = intercept_xcoll;
                        bool passable =    (e_trc.y - dims.y) > right_cursor.Here()->trc.y
                                        ||  e_trc.y           < right_cursor.Here()->blc.y;
                        if (passable) right_cursor.Forward();
                        else BounceTarget(final_trc.x, e_trc.x, entity->bouncy, entity->vel.x);
                    }
                    else
                    {
                        steppart = "coll-y";
                        e_trc = intercept_ycoll;
                        bool passable =    (e_trc.x - dims.x) > up_cursor.Here()->trc.x
                                        ||  e_trc.x           < up_cursor.Here()->blc.x;
                        if (passable) up_cursor.Forward();
                        else BounceTarget(final_trc.y, e_trc.y, entity->bouncy, entity->vel.y);
                    }
                }
                else if (final_trc.y < entity->trc.y)
                {
                    stepname = "down-right";
                    /* Moving right. Moving down. */
                    Dest_BRC final_brc(&final_trc,dims);
                    BRC_Handle e_brc(entity);
                    stFixed2 intercept_xgrid = GridInterceptRight(e_brc, final_brc),
                             intercept_ygrid = GridInterceptDown (e_brc, final_brc);
                    if (!right_cursor_valid) right_cursor.To(right_tree.Lookup(entity));
                    if (! down_cursor_valid)  down_cursor.To( down_tree.Lookup(entity));
                    right_cursor_valid = true;  left_cursor_valid = false;
                       up_cursor_valid = false; down_cursor_valid = true;
                    TryLocalWarp2(e_brc,final_brc,x,right_cursor,blc,<)
                    TryLocalWarp2(e_brc,final_brc,y, down_cursor,trc,>)
                    stFixed2 intercept_xcoll, intercept_ycoll;
                    stFixed2 direction = final_brc - e_brc;
                    bool intercept_xcoll_valid, intercept_ycoll_valid;
                    if (intercept_xcoll_valid = !!right_cursor.Here())
                    {
                        intercept_xcoll.x = right_cursor.Here()->blc.x - 1;
                        if (intercept_xcoll_valid = (intercept_xcoll.TileID().x == e_brc.TileID().x))
                        {
                            intercept_xcoll.y =   e_brc.y
                                                + (   (intercept_xcoll.x - e_brc.x)
                                                    * stIntRoundedDiv(direction.y,direction.x));
                            ClampInTile(e_brc,intercept_xcoll,y)
                        }
                    }
                    if (intercept_ycoll_valid = !!down_cursor.Here())
                    {
                        intercept_ycoll.y = down_cursor.Here()->trc.y + 1;
                        if (intercept_ycoll_valid = (intercept_ycoll.TileID().y == e_brc.TileID().y))
                        {
                            intercept_ycoll.x =   e_brc.x
                                                + (   (intercept_ycoll.y - e_brc.y)
                                                    * stIntRoundedDiv(direction.x,direction.y));
                            ClampInTile(e_brc,intercept_ycoll,x)
                        }
                    }
                    if (intercept_ycoll_valid && intercept_xcoll.y < intercept_ycoll.y)
                        intercept_xcoll.y = intercept_ycoll.y;
                    if (intercept_xcoll_valid && intercept_ycoll.x > intercept_xcoll.x)
                        intercept_ycoll.x = intercept_xcoll.x;
                    if (    intercept_xgrid.x < intercept_ygrid.x
                         && (!intercept_xcoll_valid || intercept_xgrid.x < intercept_xcoll.x)
                         && (!intercept_ycoll_valid || intercept_xgrid.x < intercept_ycoll.x))
                    {
                        steppart = "grid-x";
                        ToRightIntercept(e_brc, final_brc, intercept_xgrid, entity);
                    }
                    else if (    (!intercept_xcoll_valid || intercept_ygrid.y > intercept_xcoll.y)
                              && (!intercept_ycoll_valid || intercept_ygrid.y > intercept_ycoll.y))
                    {
                        steppart = "grid-y";
                        ToDownIntercept(e_brc, final_brc, intercept_ygrid, entity);
                    }
                    else if (    intercept_xcoll_valid
                              && (!intercept_ycoll_valid || intercept_xcoll.x < intercept_ycoll.x))
                    {
                        steppart = "coll-x";
                        e_brc = intercept_xcoll;
                        bool passable =     e_brc.y           > right_cursor.Here()->trc.y
                                        || (e_brc.y + dims.y) < right_cursor.Here()->blc.y;
                        if (passable) right_cursor.Forward();
                        else BounceTarget(final_brc.x, e_brc.x, entity->bouncy, entity->vel.x);
                    }
                    else
                    {
                        steppart = "coll-y";
                        e_brc = intercept_ycoll;
                        bool passable =    (e_brc.x - dims.x) > down_cursor.Here()->trc.x
                                        ||  e_brc.x           < down_cursor.Here()->blc.x;
                        if (passable) down_cursor.Forward();
                        else BounceTarget(final_brc.y, e_brc.y, entity->bouncy, entity->vel.y);
                    }
                }
                else
                {
                    stepname = "right";
                    /* Moving right. No vertical motion. */
                    TRC_Handle e_trc(entity);
                    stFixed2 intercept_xgrid = (slRawInt2){HighestInTile(e_trc.x), e_trc.y};
                    if (!right_cursor_valid) right_cursor.To(right_tree.Lookup(entity));
                    right_cursor_valid = true; left_cursor_valid = false;
                    TryLocalWarp2(e_trc,final_trc,x,right_cursor,blc,<)
                    stFixed2 intercept_xcoll;
                    if (right_cursor.Here())
                        intercept_xcoll = (slRawInt2){right_cursor.Here()->blc.x - 1, e_trc.y};
                    if (!right_cursor.Here() || intercept_xgrid.x < intercept_xcoll.x)
                    {
                        steppart = "grid-x";
                        ToRightIntercept(e_trc, final_trc, intercept_xgrid, entity);
                    }
                    else
                    {
                        steppart = "coll-x";
                        e_trc = intercept_xcoll;
                        bool passable =    (e_trc.y - dims.y) > right_cursor.Here()->trc.y
                                        ||  e_trc.y           < right_cursor.Here()->blc.y;
                        if (passable) right_cursor.Forward();
                        else BounceTarget(final_trc.x, e_trc.x, entity->bouncy, entity->vel.x);
                    }
                }
            }
            else if (final_trc.x < entity->trc.x)
            {
                if (final_trc.y > entity->trc.y)
                {
                    stepname = "up-left";
                    /* Moving left. Moving up. */
                    Dest_TLC final_tlc(&final_trc,dims);
                    TLC_Handle e_tlc(entity);
                    stFixed2 intercept_xgrid = GridInterceptLeft(e_tlc, final_tlc),
                             intercept_ygrid = GridInterceptUp  (e_tlc, final_tlc);
                    if (!left_cursor_valid) left_cursor.To(left_tree.Lookup(entity));
                    if (!  up_cursor_valid)   up_cursor.To(  up_tree.Lookup(entity));
                    right_cursor_valid = false; left_cursor_valid = true;
                       up_cursor_valid = true;  down_cursor_valid = false;
                    TryLocalWarp2(e_tlc,final_tlc,x,left_cursor,trc,>)
                    TryLocalWarp2(e_tlc,final_tlc,y,  up_cursor,blc,<)
                    stFixed2 intercept_xcoll, intercept_ycoll;
                    stFixed2 direction = final_tlc - e_tlc;
                    bool intercept_xcoll_valid, intercept_ycoll_valid;
                    if (intercept_xcoll_valid = !!left_cursor.Here())
                    {
                        intercept_xcoll.x = left_cursor.Here()->trc.x + 1;
                        if (intercept_xcoll_valid = (intercept_xcoll.TileID().x == e_tlc.TileID().x))
                        {
                            intercept_xcoll.y =   e_tlc.y
                                                + (   (intercept_xcoll.x - e_tlc.x)
                                                    * stIntRoundedDiv(direction.y,direction.x));
                            ClampInTile(e_tlc,intercept_xcoll,y)
                        }
                    }
                    if (intercept_ycoll_valid = !!up_cursor.Here())
                    {
                        intercept_ycoll.y = up_cursor.Here()->blc.y - 1;
                        if (intercept_ycoll_valid = (intercept_ycoll.TileID().y == e_tlc.TileID().y))
                        {
                            intercept_ycoll.x =   e_tlc.x
                                                +   (   (intercept_ycoll.y - e_tlc.y)
                                                      * stIntRoundedDiv(direction.x,direction.y));
                            ClampInTile(e_tlc,intercept_ycoll,x)
                        }
                    }
                    if (intercept_ycoll_valid && intercept_xcoll.y > intercept_ycoll.y)
                        intercept_xcoll.y = intercept_ycoll.y;
                    if (intercept_xcoll_valid && intercept_ycoll.x < intercept_xcoll.x)
                        intercept_ycoll.x = intercept_xcoll.x;
                    if (    intercept_xgrid.x > intercept_ygrid.x
                         && (!intercept_xcoll_valid || intercept_xgrid.x > intercept_xcoll.x)
                         && (!intercept_ycoll_valid || intercept_xgrid.x > intercept_ycoll.x))
                    {
                        steppart = "grid-x";
                        ToLeftIntercept(e_tlc, final_tlc, intercept_xgrid, entity);
                    }
                    else if (    (!intercept_xcoll_valid || intercept_ygrid.y < intercept_xcoll.y)
                              && (!intercept_ycoll_valid || intercept_ygrid.y < intercept_ycoll.y))
                    {
                        steppart = "grid-y";
                        ToUpIntercept(e_tlc, final_tlc, intercept_ygrid, entity);
                    }
                    else if (    intercept_xcoll_valid
                              && (!intercept_ycoll_valid || intercept_xcoll.x > intercept_ycoll.x))
                    {
                        steppart = "coll-x";
                        e_tlc = intercept_xcoll;
                        bool passable =    (e_tlc.y - dims.y) > left_cursor.Here()->trc.y
                                        ||  e_tlc.y           < left_cursor.Here()->blc.y;
                        if (passable) left_cursor.Forward();
                        else BounceTarget(final_tlc.x, e_tlc.x, entity->bouncy, entity->vel.x);
                    }
                    else
                    {
                        steppart = "coll-y";
                        e_tlc = intercept_ycoll;
                        bool passable =     e_tlc.x           > up_cursor.Here()->trc.x
                                        || (e_tlc.x + dims.x) < up_cursor.Here()->blc.x;
                        if (passable) up_cursor.Forward();
                        else BounceTarget(final_tlc.y, e_tlc.y, entity->bouncy, entity->vel.y);
                    }
                }
                else if (final_trc.y < entity->trc.y)
                {
                    stepname = "down-left";
                    /* Moving left. Moving down. */
                    Dest_BLC final_blc(&final_trc,dims);
                    BLC_Handle e_blc(entity);
                    stFixed2 intercept_xgrid = GridInterceptLeft(e_blc, final_blc),
                             intercept_ygrid = GridInterceptDown(e_blc, final_blc);
                    if (!left_cursor_valid) left_cursor.To(left_tree.Lookup(entity));
                    if (!down_cursor_valid) down_cursor.To(down_tree.Lookup(entity));
                    right_cursor_valid = false; left_cursor_valid = true;
                       up_cursor_valid = false; down_cursor_valid = true;
                    TryLocalWarp2(e_blc,final_blc,x,left_cursor,trc,>)
                    TryLocalWarp2(e_blc,final_blc,y,down_cursor,trc,>)
                    stFixed2 intercept_xcoll, intercept_ycoll;
                    stFixed2 direction = final_blc - e_blc;
                    bool intercept_xcoll_valid, intercept_ycoll_valid;
                    if (intercept_xcoll_valid = !!left_cursor.Here())
                    {
                        intercept_xcoll.x = left_cursor.Here()->trc.x + 1;
                        if (intercept_xcoll_valid = (intercept_xcoll.TileID().x == e_blc.TileID().x))
                        {
                            intercept_xcoll.y =   e_blc.y
                                                + (   (intercept_xcoll.x - e_blc.x)
                                                    * stIntRoundedDiv(direction.y,direction.x));
                            ClampInTile(e_blc,intercept_xcoll,y)
                        }
                    }
                    if (intercept_ycoll_valid = !!down_cursor.Here())
                    {
                        intercept_ycoll.y = down_cursor.Here()->trc.y + 1;
                        if (intercept_ycoll_valid = (intercept_ycoll.TileID().y == e_blc.TileID().y))
                        {
                            intercept_ycoll.x =   e_blc.x
                                                + (   (intercept_ycoll.y - e_blc.y)
                                                    * stIntRoundedDiv(direction.x,direction.y));
                            ClampInTile(e_blc,intercept_ycoll,x)
                        }
                    }
                    if (intercept_ycoll_valid && intercept_xcoll.y < intercept_ycoll.y)
                        intercept_xcoll.y = intercept_ycoll.y;
                    if (intercept_xcoll_valid && intercept_ycoll.x < intercept_xcoll.x)
                        intercept_ycoll.x = intercept_xcoll.x;
                    if (    intercept_xgrid.x > intercept_ygrid.x
                         && (!intercept_xcoll_valid || intercept_xgrid.x > intercept_xcoll.x)
                         && (!intercept_ycoll_valid || intercept_xgrid.x > intercept_ycoll.x))
                    {
                        steppart = "grid-x";
                        ToLeftIntercept(e_blc, final_blc, intercept_xgrid, entity);
                    }
                    else if (    (!intercept_xcoll_valid || intercept_ygrid.y > intercept_xcoll.y)
                              && (!intercept_ycoll_valid || intercept_ygrid.y > intercept_ycoll.y))
                    {
                        steppart = "grid-y";
                        ToDownIntercept(e_blc, final_blc, intercept_ygrid, entity);
                    }
                    else if (    intercept_xcoll_valid
                              && (!intercept_ycoll_valid || intercept_xcoll.x > intercept_ycoll.x))
                    {
                        steppart = "coll-x";
                        e_blc = intercept_xcoll;
                        bool passable =     e_blc.y           > left_cursor.Here()->trc.y
                                        || (e_blc.y + dims.y) < left_cursor.Here()->blc.y;
                        if (passable) left_cursor.Forward();
                        else BounceTarget(final_blc.x, e_blc.x, entity->bouncy, entity->vel.x);
                    }
                    else
                    {
                        steppart = "coll-y";
                        e_blc = intercept_ycoll;
                        bool passable =     e_blc.x           > down_cursor.Here()->trc.x
                                        || (e_blc.x + dims.x) < down_cursor.Here()->blc.x;
                        if (passable) down_cursor.Forward();
                        else BounceTarget(final_blc.y, e_blc.y, entity->bouncy, entity->vel.y);
                    }
                }
                else
                {
                    stepname = "left";
                    /* Moving left. No vertical motion. */
                    BLC_Handle e_blc(entity);
                    Dest_BLC final_blc(&final_trc,dims);
                    stFixed2 intercept_xgrid = (slRawInt2){LowestInTile(e_blc.x), e_blc.y};
                    if (!left_cursor_valid) left_cursor.To(left_tree.Lookup(entity));
                    right_cursor_valid = false; left_cursor_valid = true;
                    TryLocalWarp2(e_blc,final_blc,x,left_cursor,trc,>)
                    stFixed2 intercept_xcoll;
                    if (left_cursor.Here())
                        intercept_xcoll = (slRawInt2){left_cursor.Here()->trc.x + 1, e_blc.y};
                    if (!left_cursor.Here() || intercept_xgrid.x > intercept_xcoll.x)
                    {
                        steppart = "grid-x";
                        ToLeftIntercept(e_blc, final_blc, intercept_xgrid, entity);
                    }
                    else
                    {
                        steppart = "coll-x";
                        e_blc = intercept_xcoll;
                        bool passable =     e_blc.y           > left_cursor.Here()->trc.y
                                        || (e_blc.y + dims.y) < left_cursor.Here()->blc.y;
                        if (passable) left_cursor.Forward();
                        else BounceTarget(final_blc.x, e_blc.x, entity->bouncy, entity->vel.x);
                    }
                }
            }
            else
            {
                if (final_trc.y > entity->trc.y)
                {
                    stepname = "up";
                    /* No horizontal motion. Moving up. */
                    TRC_Handle e_trc(entity);
                    stFixed2 intercept_ygrid = (slRawInt2){e_trc.x, HighestInTile(e_trc.y)};
                    if (!up_cursor_valid) up_cursor.To(up_tree.Lookup(entity));
                    up_cursor_valid = true; down_cursor_valid = false;
                    TryLocalWarp2(e_trc,final_trc,y,up_cursor,blc,<)
                    stFixed2 intercept_ycoll;
                    if (up_cursor.Here())
                        intercept_ycoll = (slRawInt2){e_trc.x, up_cursor.Here()->blc.y - 1};
                    if (!up_cursor.Here() || intercept_ygrid.y < intercept_ycoll.y)
                    {
                        steppart = "grid-y";
                        ToUpIntercept(e_trc, final_trc, intercept_ygrid, entity);
                    }
                    else
                    {
                        steppart = "coll-y";
                        e_trc = intercept_ycoll;
                        bool passable =    (e_trc.x - dims.x) > up_cursor.Here()->trc.x
                                        ||  e_trc.x           < up_cursor.Here()->blc.x;
                        if (passable) up_cursor.Forward();
                        else BounceTarget(final_trc.y, e_trc.y, entity->bouncy, entity->vel.y);
                    }
                }
                else if (final_trc.y < entity->trc.y)
                {
                    stepname = "down";
                    /* No horizontal motion. Moving down. */
                    BLC_Handle e_blc(entity);
                    Dest_BLC final_blc(&final_trc,dims);
                    stFixed2 intercept_ygrid = (slRawInt2){e_blc.x, LowestInTile(e_blc.y)};
                    if (!down_cursor_valid) down_cursor.To(down_tree.Lookup(entity));
                    up_cursor_valid = false; down_cursor_valid = true;
                    TryLocalWarp2(e_blc,final_blc,y,down_cursor,trc,>)
                    stFixed2 intercept_ycoll;
                    if (down_cursor.Here())
                        intercept_ycoll = (slRawInt2){e_blc.x, down_cursor.Here()->trc.y + 1};
                    if (!down_cursor.Here() || intercept_ygrid.y > intercept_ycoll.y)
                    {
                        steppart = "grid-y";
                        ToDownIntercept(e_blc, final_blc, intercept_ygrid, entity);
                    }
                    else
                    {
                        steppart = "coll-y";
                        e_blc = intercept_ycoll;
                        bool passable =     e_blc.x           > down_cursor.Here()->trc.x
                                        || (e_blc.x + dims.x) < down_cursor.Here()->blc.x;
                        if (passable) down_cursor.Forward();
                        else BounceTarget(final_blc.y, e_blc.y, entity->bouncy, entity->vel.y);
                    }
                }
                else break; /* No horizontal motion. No vertical motion. */
            }
        }
        if (moving_x) entity->SpatialRegisterX();
        if (moving_y) entity->SpatialRegisterY();
        entity->center_prev = entity->center_now;
        entity->center_now = CornersVisCenter(entity->blc,entity->trc);
    }
}
void stWorld::Step ()
{
    stepping_elapsed += slGetDelta() * stepping_multiplier;
    Uint64 start_time = SDL_GetPerformanceCounter();
    while (stepping_elapsed >= stepping_interval)
    {
        /*  If the physics calculations have already taken
            'stepping_maxcalctime' seconds or more, give up on keeping the
            physics in sync with real time.  */
        Uint64 processing_elapsed = (SDL_GetPerformanceCounter() - start_time)
                                    / (slScalar)SDL_GetPerformanceFrequency();
        if (processing_elapsed >= stepping_maxcalctime)
        {
            stepping_elapsed = stepping_interval;
            break;
        }
        stepping_elapsed -= stepping_interval;

        DiscreteStep();
    }

    slScalar late = stepping_elapsed / stepping_interval;
    slScalar early = 1 - late;
    for (slBU i = 0; i < entities.itemcount; i++)
    {
        stEntity* entity = entities.items[i];
        entity->visual->xy = entity->center_prev * early + entity->center_now * late;
    }
}
void stWorld::Draw ()
{
    s2dRender();
}
void stWorld::SetCamPos (slVec2 pos)
{
    s2dSetCamXY(pos);
}
void stWorld::SetCamSize(slScalar size)
{
    s2dSetCamSize(size);
}
void stWorld::SetGravity (slVec2 gravity)
{
    this->gravity = gravity;
}

void stInit ()
{
    s2dInit();
}
void stQuit ()
{
    s2dQuit();
}
