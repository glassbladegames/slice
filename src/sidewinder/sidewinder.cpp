#include "sidewinder.h"
slList swThreads("Sidewinder threads",offsetof(swThread,_index_));
SDL_atomic_t swRemaining;
void swSleep (swThread* thread, slScalar awaken_t)
{
	thread->awaken_t = awaken_t;
	thread->slept_time = 0;
	SDL_AtomicDecRef(&swRemaining);
	//printf("sleep starting\n");
	SDL_SemWait(thread->start);
	//printf("sleep ended\n");
}
int swThreadProc (swThread* self)
{
	SDL_SemWait(self->start);
	self->main(self);
	self->dead = true;
	SDL_AtomicDecRef(&swRemaining);
	return 0;
}
void swSpawn (void (*func) (swThread* self))
{
	swThread* thread = malloc(sizeof(swThread));
	thread->dead = false;
	thread->slept_time = 0;
	thread->start = SDL_CreateSemaphore(0);
	thread->main = func;
	thread->awaken_t = 0;
	swThreads.Add(thread);
	SDL_DetachThread(SDL_CreateThread(swThreadProc,"SideWinder Thread",thread));
}

void swExecute ()
{
	//printf("executing\n");
	slScalar delta = slGetDelta();
	for (slBU cur = 0; cur < swThreads.itemcount; cur++)
	{
		swThread* thread = *(swThreads.items + cur);
		thread->awaken_t -= delta;
		thread->slept_time += delta;
		if (thread->awaken_t <= 0)
		{
			//printf("woke thread\n");
			SDL_AtomicIncRef(&swRemaining);
			SDL_SemPost(thread->start);
		}
	}
	//printf("waiting for all done\n");
	while (SDL_AtomicGet(&swRemaining)) SDL_Delay(0);
	//printf("cleaning up\n");
	for (slBU cur = 0; cur < swThreads.itemcount; cur++)
	{
		swThread* thread = *(swThreads.items + cur);
		if (thread->dead)
		{
			printf("one found dead\n");
			swThreads.Remove(thread);
			free(thread);
		}
	}
	//printf("execute complete\n");
}
void swInit ()
{
	SDL_AtomicSet(&swRemaining,0); /// Is this necessary?

}
/*void swKillThread (swThread* thread)
{
	printf("killing thread...\n");
	thread->dead = true;
	SDL_AtomicIncRef(&swRemaining);
	SDL_SemPost(thread->start);
	while (SDL_AtomicGet(&swRemaining)) SDL_Delay(0);
	slListRemove(&swThreads,thread);
	free(thread);
	printf("killed thread\n");
}*/
void swKillAll ()
{
	//printf("killing %llu threads\n",swThreads.itemcount);
	SDL_AtomicSet(&swRemaining,swThreads.itemcount);
	for (slBU cur = 0; cur < swThreads.itemcount; cur++)
	{
		swThread* thread = *(swThreads.items + cur);
		thread->dead = true;
		SDL_SemPost(thread->start);
	}
	//printf("waiting for all dead\n");
	while (SDL_AtomicGet(&swRemaining)) SDL_Delay(0);
	//printf("cleaning up deads\n");
	swThreads.Clear(free);
	//printf("all threads killed\n");
}
