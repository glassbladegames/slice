#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#define SDL_main main
#include <glad/vulkan.h>
#define SLICE_VK_VER(era,major,minor,patch) ((era<<12)|(major<<8)|(minor<<4)|(patch))
#include <stdio.h>
int main (int argc, char** argv)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) return -1;
	SDL_Window* window = SDL_CreateWindow("vt",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,1280,720,SDL_WINDOW_VULKAN);
	if (!window) return -2;
	char exit_req = 0;

	unsigned int extcount;
	SDL_Vulkan_GetInstanceExtensions(window,&extcount,NULL);
	const char** extnames = malloc(sizeof(char*) * extcount);
	SDL_Vulkan_GetInstanceExtensions(window,&extcount,extnames);

	void* getprocaddr = SDL_Vulkan_GetVkGetInstanceProcAddr();
	if (!getprocaddr) return 10;
	gladLoadVulkan(NULL,getprocaddr);

	VkApplicationInfo app_info;
	app_info.apiVersion = VK_API_VERSION_1_1;
	app_info.applicationVersion = 0;
	app_info.engineVersion = SLICE_VK_VER(3,0,0,0);
	app_info.pApplicationName = "Slice Vulkan Test";
	app_info.pEngineName = "Slice Game Engine";
	app_info.pNext = NULL;
	app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;

	VkInstanceCreateInfo inst_info;
	inst_info.flags = 0;
	inst_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	inst_info.pNext = NULL;
	inst_info.pApplicationInfo = &app_info;
	inst_info.enabledLayerCount = 0;
	inst_info.ppEnabledLayerNames = NULL;
	inst_info.enabledExtensionCount = extcount;
	inst_info.ppEnabledExtensionNames = extnames;

	VkInstance inst;
	VkResult result = vkCreateInstance(&inst_info,NULL,&inst);
	free(extnames);
	if (result != VK_SUCCESS) return -3;

	while (!exit_req)
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT) exit_req = 1;
		}
	}

	gladLoaderUnloadVulkan();
	SDL_DestroyWindow(window);
	return 0;
}
