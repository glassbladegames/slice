#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
int convert_one (char* in_path)
{
    char* obj_path;
    asprintf(&obj_path,"%s.obj",in_path);
    char* mtl_path;
    asprintf(&mtl_path,"%s.mtl",in_path);
	FILE* in_file = fopen(in_path,"rb");
    FILE* obj_file = fopen(obj_path,"w");
    FILE* mtl_file = fopen(mtl_path,"w");
    free(obj_path);
    free(mtl_path);
    if (!in_file || !obj_file || !mtl_file)
    {
        if (!in_file) printf("(!) Can't open input file '%s'\n",in_path);
        else fclose(in_file);
        if (!obj_file) printf("(!) Can't open output file '%s'\n",obj_path);
        else fclose(obj_file);
        if (!mtl_file) printf("(!) Can't open output file '%s'\n",mtl_path);
        else fclose(mtl_file);
        return 1;
    }

    fprintf(obj_file,"mtllib %s.mtl\n",in_path);
	uint32_t meshcount;
	fread(&meshcount,4,1,in_file);
	printf("Mesh Count: %u\n\n",meshcount);
    int global_v_id = 1;
    int mat_name = 0;
	while (meshcount--)
	{
        // Declare a new material and refer to it in the main file.
        fprintf(mtl_file,"newmtl %d\n",mat_name);
        fprintf(obj_file,"usemtl %d\n",mat_name);
        mat_name++;

        // Put the texture path into the material definition,
        // as ambient and diffuse textures.
		uint32_t len;
		fread(&len,4,1,in_file);
        char* texture_name = malloc(len + 1);
        texture_name[len] = 0;
        fread(texture_name,1,len,in_file);
        fprintf(mtl_file,"map_Ka %s\nmap_Kd %s\n",texture_name,texture_name);
        free(texture_name);
        fprintf(mtl_file,"mag_filter_nearest\n");

		uint32_t tricount;
		fread(&tricount,4,1,in_file);
		printf("\n\tTriangle Count: %u\n",tricount);
		while (tricount--)
		{
			float values [15];
			fread(values,4,15,in_file);
            for (int offset = 0; offset < 15; offset += 5)
            {
                fprintf(obj_file, "v %f %f %f\n", values[offset+0], values[offset+1], values[offset+2]);
                fprintf(obj_file, "vt %f %f\n", values[offset+3], 1-values[offset+4]);
            }
            int a = global_v_id++;
            int b = global_v_id++;
            int c = global_v_id++;
            fprintf(obj_file, "f %d/%d %d/%d %d/%d\n", a, a, b, b, c, c);
		}
	}

    fprintf(obj_file,"# This line indicates the file is complete.\n");
    fprintf(mtl_file,"# This line indicates the file is complete.\n");

    fclose(in_file);
    fclose(obj_file);
    fclose(mtl_file);
	return 0;
}
int main (int argc, char** argv)
{
	if (argc < 2)
	{
		printf("(!) No input file path(s) given.\n");
		return -1;
	}
    int all_result = 0;
    int successful = 0;
    for (int i = 1; i < argc; i++)
    {
        char* in_path = argv[i];
        printf("Converting '%s'\n",in_path);
        int one_result = convert_one(in_path);
        if (one_result)
        {
            printf("Failed to convert '%s'!\n",in_path);
            all_result = 1;
        }
        else successful++;
    }
    printf("%d/%d succeeded\n",successful,argc-1);
    return all_result;
}
