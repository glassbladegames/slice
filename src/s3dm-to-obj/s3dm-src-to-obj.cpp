#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
bool isspace (char cc)
{
	return cc == ' ' || cc == '\t' || cc == '\n';
}
bool isfloatstart (char cc)
{
	return (cc >= '0' && cc <= '9') || cc == '.' || cc == '-' || cc == '+';
}
int GetNonspaceChar (FILE* file)
{
	int cc;
	while ((cc = fgetc(file)) != EOF) if (!isspace(cc)) break;
	return cc;
}
char* strclone (char* str)
{
	size_t size = strlen(str) + 1;
	char* out = malloc(size);
	memcpy(out,str,size);
	return out;
}
int convert_one (char* in_path)
{
	FILE* src = fopen(in_path,"r");
	if (!src)
	{
		printf("(!) Cannot open source file.\n");
		return -2;
	}
	char* outpath;
	size_t inlen = strlen(in_path);
	if (inlen < 5) outpath = strclone(".s3dm");
	else
	{
		outpath = strclone(in_path);
		outpath[inlen - 4] = 0;
	}
	printf("Output Path: \"%s\"\n\n",outpath);
	FILE* outfile = fopen(outpath,"wb");
	if (!outfile)
	{
		printf("(!) Cannot write to output file.\n");
		return -3;
	}
	size_t MeshCount = 0;
	fseek(outfile,4,SEEK_SET);
	while (true)
	{
		while (true) switch (GetNonspaceChar(src))
		{
			case EOF: goto SUPERDONE;
			case '{': goto ENTER;
			default: printf("(!) Illegal character encountered, expected '{'.\n");
		}
		ENTER: printf("{\n");



		/// Get texture path.
		while (true) switch(GetNonspaceChar(src))
		{
			case EOF: printf("(!) Section ended very early, expected '\"'.\n"); goto SUPERDONE;
			case '"': goto QUOTESTART;
			default: printf("(!) Illegal character encountered, expected '\"'.\n");
		}
		QUOTESTART:
		size_t texstr_grow = 32;
		size_t texstr_size = texstr_grow;
		char* texstr = malloc(texstr_size);
		size_t texstr_len = 0;
		int cc;
		while (true) switch(cc = GetNonspaceChar(src))
		{
			case EOF: free(texstr); printf("(!) String not closed, expected '\"'.\n"); goto SUPERDONE;
			case '"': goto QUOTEEND;
			default:
				if (texstr_len >= texstr_size) texstr = realloc(texstr,texstr_size += texstr_grow);
				texstr[texstr_len++] = cc;
		}
		QUOTEEND:
		texstr[texstr_len] = 0;
		printf("\t\"%s\"\n",texstr);
		uint32_t lenvalue = texstr_len;
		fwrite(&lenvalue,4,1,outfile);
		fwrite(texstr,1,lenvalue,outfile);
		free(texstr);



		/// Get triangles.
		int tricount_offset = 4;
		size_t TriCount = 0;
		fseek(outfile,4,SEEK_CUR);
		while (true)
		{
			while (true) switch (cc = GetNonspaceChar(src))
			{
				case EOF: printf("\n\t(!) Section not closed, expected '}'.\n"); goto SUPERDONE;
				case '}': goto DONE;
				default:
					if (isfloatstart(cc))
					{
						ungetc(cc,src);
						goto MORE;
					}
					printf("\n\t(!) Illegal character encountered, expected floating-point number.\n");
			}
			DONE: break;
			MORE:
			float x1,y1,z1,u1,v1,x2,y2,z2,u2,v2,x3,y3,z3,u3,v3;
			if (fscanf(src," %f %f %f / %f %f , %f %f %f / %f %f , %f %f %f / %f %f ;",&x1,&y1,&z1,&u1,&v1,&x2,&y2,&z2,&u2,&v2,&x3,&y3,&z3,&u3,&v3) < 15)
				printf("\n\t(!) Invalid triangle.\n");
			else
			{
				printf("\n\t%.2f %.2f %.2f / %.2f %.2f,\n\t%.2f %.2f %.2f / %.2f %.2f,\n\t%.2f %.2f %.2f / %.2f %.2f;\n",x1,y1,z1,u1,v1,x2,y2,z2,u2,v2,x3,y3,z3,u3,v3);
				float values [15] = {x1,y1,z1,u1,v1,x2,y2,z2,u2,v2,x3,y3,z3,u3,v3};
				fwrite(values,4,15,outfile);
				TriCount++;
				tricount_offset += 4 * 15;
			}
		}
		fseek(outfile,-tricount_offset,SEEK_CUR);
		uint32_t tricount_value = TriCount;
		fwrite(&tricount_value,4,1,outfile);
		fseek(outfile,tricount_offset - 4,SEEK_CUR);
		printf("}\n\n");
		MeshCount++;
	}
	SUPERDONE:;
	fseek(outfile,0,SEEK_SET);
	uint32_t sectioncount = MeshCount;
	fwrite(&sectioncount,4,1,outfile);
    fclose(outfile); // Must close this before running the other program,
    // otherwise the file could appear to be empty or incomplete since not all
    // contents would have been flushed to disk potentially.
	printf("Finished compiling S3DM\n");

    printf("Converting compiled S3DM to Obj\n");
    char* cmd;
    asprintf(&cmd,"s3dm-to-obj '%s'\n",outpath);
    int to_obj_result = system(cmd);
    free(cmd);
    if (to_obj_result) printf("FAILED to convert S3DM to Obj ('%s').\n",outpath);
    else printf("Finished converting S3DM to Obj\n");

    remove(outpath); // delete the compiled s3dm file since it was temporary
    free(outpath);
	return to_obj_result;
}
int main (int argc, char** argv)
{
	if (argc < 2)
	{
		printf("(!) No input file path(s) given.\n");
		return -1;
	}
    int all_result = 0;
    int successful = 0;
    for (int i = 1; i < argc; i++)
    {
        char* in_path = argv[i];
        printf("Converting '%s'\n",in_path);
        int one_result = convert_one(in_path);
        if (one_result)
        {
            printf("Failed to convert '%s'!\n",in_path);
            all_result = 1;
        }
        else successful++;
    }
    printf("%d/%d succeeded\n",successful,argc-1);
    return all_result;
}
