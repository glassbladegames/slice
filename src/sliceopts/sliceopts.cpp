#include "sliceopts.h"
SDL_Color PanelColor, TextColor, HoverColor;
bool opBindsVisi = false;
slBox* opBindsPanel;
slBox* opBindsBack;
scrScroll* opBindsScroll;
struct opBindEntry
{
	slKeyBind* binding;
	slBox* description;
	slBox* valuebox;
	slBox* scrollbox; // Everything is positioned relative to this box, b/c it is moved around by Slice-Scroll.
};
slList opBindEntries("SliceOpts: Bind Entries",slNoIndex,true);
opBindEntry* opKeyToRebind;
void opOnRebindInput (slKeyInfo input)
{
	opKeyToRebind->valuebox->backcolor = slBlankColor;
	opKeyToRebind->valuebox->hoverable = true;
	opKeyToRebind->binding->SetInfo(input);
}
void opRebindKey (slBox* box)
{
	//printf("clicked box. z: %i. texref: %llX. tex-scale: %f,%f.\n",(int)box->z,box->texref,(float)box->tex_w,(float)box->tex_h);
	//if (box->texref) printf("\ttexture id: %u\n",box->texref->tex);
	opBindEntry* entry = box->userdata;
	//entry->valuebox->texref = NULL;
    entry->valuebox->SetTexRef(slNoTexture);
	entry->valuebox->backcolor = {0,0,0,47};
	entry->valuebox->hoverable = false;
	opKeyToRebind = entry;
	slCaptureNextInput(opOnRebindInput);
}
void opUpdateBinds ()
{
	for (slBU cur = 0; cur < opBindEntries.itemcount; cur++)
	{
		opBindEntry* entry = *(opBindEntries.items + cur);
		slSetBoxDims(entry->description,0,0.05,0.7,0.9,4);
		slRelBoxDims(entry->description,entry->scrollbox);
		entry->description->visible = entry->scrollbox->visible;
		slSetBoxDims(entry->valuebox,0.725,0.05,0.25,0.9,4);
		slRelBoxDims(entry->valuebox,entry->scrollbox);
		entry->valuebox->visible = entry->scrollbox->visible;
		//entry->scrollbox->visible = false;
	}
}
void opOnBindChanged (slKeyBind* bind, bool is_new)
{
	opBindEntry* entry;
	if (is_new)
	{
		// Make new entry for this bind.

		entry = malloc(sizeof(opBindEntry));
		opBindEntries.Add(entry);
		entry->binding = bind;
		entry->description = slCreateBox(slRenderText(bind->name,{0,0,0,255}));
		entry->valuebox = slCreateBox();
		entry->valuebox->hoverable = true;
		entry->valuebox->bordercolor = {127,127,127,127};
		entry->valuebox->hoverbordercolor = {191,191,191,127};
		entry->valuebox->onclick = opRebindKey;
		entry->valuebox->userdata = entry;
		entry->scrollbox = slCreateBox();
		opBindsScroll->AddBox(entry->scrollbox);
	}
	else
	{
		// Find existing entry related to this bind.

		for (slBU cur = 0; cur < opBindEntries.itemcount; cur++)
		{
			entry = opBindEntries.items[cur];
			if (entry->binding == bind) break;
		}
	}

	char* description = slDescribeKeyInfo(bind->_info_);
	entry->valuebox->SetTexRef(slRenderText(description,{0,0,0,255}));
	free(description);
}
void opBindsSetVisi ()
{
	opBindsPanel->visible = opBindsVisi;
	opBindsBack->visible = opBindsVisi;
	opBindsScroll->SetVisible(opBindsVisi);

	opUpdateBinds();
}
void opOnBindsBack ()
{
	opBindsVisi = false;
	opBindsSetVisi();
}
void opBindsInit ()
{
	opBindsPanel = slCreateBox();
	opBindsPanel->backcolor = PanelColor;
	opBindsPanel->bordercolor = {191,191,191,255};
	slSetBoxDims(opBindsPanel,0.25,0.025,0.5,0.95,6);
	opBindsPanel->onclick = slDoNothing;
	opBindsPanel->visible = opBindsVisi;
	opBindsPanel->hoverable = true;
	opBindsPanel->hoverbackcolor = opBindsPanel->backcolor;
	opBindsPanel->hoverbordercolor = opBindsPanel->bordercolor;

	slBox* opBindsSubPanel = slCreateBox();
	slSetBoxDims(opBindsSubPanel,0.05,0.05,0.9,0.85,5);
	slRelBoxDims(opBindsSubPanel,opBindsPanel);
	opBindsScroll = scrCreateScroll(opBindsSubPanel,16);
	opBindsScroll->SetVisible(opBindsVisi);
	opBindsScroll->update = opUpdateBinds;

	opBindsBack = slCreateBox(slRenderText("Back",TextColor));
	slSetBoxDims(opBindsBack,0.375,0.92,0.25,0.06,5);
	slRelBoxDims(opBindsBack,opBindsPanel);
	opBindsBack->visible = opBindsVisi;
	opBindsBack->onclick = opOnBindsBack;
	opBindsBack->hoverable = true;
	opBindsBack->backcolor = {31,31,31,191};
	opBindsBack->bordercolor = {255,255,255,255};
	opBindsBack->hoverbackcolor = HoverColor;
	opBindsBack->hoverbordercolor = {255,255,255,255};
}
void opDeallocBindEntry (opBindEntry* entry)
{
	slDestroyBox(entry->description);
	slDestroyBox(entry->valuebox);
	free(entry);
}
void opBindsQuit ()
{
	slDestroyBox(opBindsBack);
	slDestroyBox(opBindsPanel);
	scrDestroyScroll(opBindsScroll);
	opBindEntries.Clear(opDeallocBindEntry);
}
bool opVisi = false;
bool opCustomVisi = false;
bool opVideoVisiStatus;
bool opAudioVisiStatus;
void opVideoVisi (bool visi);
void opAudioVisi (bool visi);
slBox* opPanel;
slBox* opExit;
slBox* opVideoButton;
slBox* opAudioButton;
slBox* opResume;
slBox* opOpenBinds;
slBox* opCustomButton;
bool opExitVisible = true;
void (*_opCustomSetVisi_) (bool to) = NULL;
void (*opVisibilityCallback) (bool menu_visible);
void opSetVisi ()
{
	opPanel->visible = opVisi;
	opExit->visible = opVisi;
	opVideoButton->visible = opVisi;
	opAudioButton->visible = opVisi;
	opResume->visible = opVisi;
	opOpenBinds->visible = opVisi;
	if (_opCustomSetVisi_) opCustomButton->visible = opVisi;
	else opCustomButton->visible = false;
	opExit->visible = opExitVisible && opVisi;

	if (opVisibilityCallback) opVisibilityCallback(opVisi);
}
bool (*opCustomEscapeCallback) () = NULL;
void opOnEscape ()
{
	if (opCustomEscapeCallback) if (opCustomEscapeCallback()) return;
	if (opBindsVisi)
	{
		opBindsVisi = false;
		opBindsSetVisi();
	}
	else if (opVideoVisiStatus) opVideoVisi(false);
    else if (opAudioVisiStatus) opAudioVisi(false);
	else if (opCustomVisi)
	{
		opCustomVisi = false;
        printf("closing due to escape button\n");
		_opCustomSetVisi_(false);
	}
	else
	{
		opVisi = !opVisi;
		opSetVisi();
	}
}
void opOnResume ()
{
	opVisi = false;
	opSetVisi();
}
void opOnBinds ()
{
	opBindsVisi = true;
	opBindsSetVisi();
}
#define opButtonSpacing (custom_setvisi ? 0.01 : 0.015)
#define opButtonYPos(n) 0.25 + ((height * (n)) + (spacing * ((n) + 1)))
#define opButtonInfo(button)\
{\
    button->backcolor = {31,31,31,191};\
    button->bordercolor = {255,255,255,255};\
    button->hoverbackcolor = HoverColor;\
    button->hoverbordercolor = {255,255,255,255};\
    button->hoverable = true;\
}
slBox* opFullscreen;
slBox* opVSync;
slBox* opVideoPanel;
slBox* opVideoBack;
void opVideoVisi (bool visi)
{
    opVideoPanel->visible = visi;
    opFullscreen->visible = visi;
    opVSync->visible = visi;
    opVideoBack->visible = visi;
    opVideoVisiStatus = visi;
}
void opVideoOpen ()
{
    opVideoVisi(true);
}
void opVideoClose ()
{
    opVideoVisi(false);
}
#define opFullscreenTexRef() slRenderText(slGetFullscreen() ? "Disable Fullscreen" : "Enable Fullscreen",TextColor)
#define opVSyncTexRef() slRenderText(slGetVSync() ? "Disable V-Sync" : "Enable V-Sync",TextColor)
void opToggleFullscreen ()
{
    slToggleFullscreen();
    opFullscreen->SetTexRef(opFullscreenTexRef());
}
void opToggleVSync ()
{
    slToggleVSync();
    opVSync->SetTexRef(opVSyncTexRef());
}
void opVideoInit ()
{
    opVideoPanel = slCreateBox();
    slSetBoxDims(opVideoPanel,0.25,0.25,0.5,0.5,6);
	opVideoPanel->backcolor = PanelColor;
	opVideoPanel->bordercolor = {191,191,191,255};
	opVideoPanel->onclick = slDoNothing;

	opFullscreen = slCreateBox(opFullscreenTexRef());
	opButtonInfo(opFullscreen);
	opFullscreen->onclick = opToggleFullscreen;
    slSetBoxDims(opFullscreen,0.1,0.025,0.8,0.15,5);
    slRelBoxDims(opFullscreen,opVideoPanel);

	opVSync = slCreateBox(opVSyncTexRef());
	opButtonInfo(opVSync);
	opVSync->onclick = opToggleVSync;
	slSetBoxDims(opVSync,0.1,0.225,0.8,0.15,5);
	slRelBoxDims(opVSync,opVideoPanel);

	opVideoBack = slCreateBox(slRenderText("Back",TextColor));
	opButtonInfo(opVideoBack);
	opVideoBack->onclick = opVideoClose;
	slSetBoxDims(opVideoBack,0.3,0.825,0.4,0.15,5);
	slRelBoxDims(opVideoBack,opVideoPanel);

	opVideoVisi(false);
}
void opVideoQuit ()
{
    slDestroyBox(opVideoPanel);
	slDestroyBox(opFullscreen);
	slDestroyBox(opVSync);
	slDestroyBox(opVideoBack);
}
slBox* opMuteAway;
//slBox* opMasterVol_Desc;
slSlider* opMasterVol;
slBox* opMasterVol_Back;
slBox* opMasterVol_Mark;
slBox* opAudioPanel;
slBox* opAudioBack;
void opAudioVisi (bool visi)
{
    opAudioPanel->visible = visi;
    opMuteAway->visible = visi;
    //opMasterVol_Desc->visible = visi;
    opMasterVol_Back->visible = visi;
    opMasterVol_Mark->visible = visi;
    opAudioBack->visible = visi;
    opAudioVisiStatus = visi;
}
void opAudioOpen ()
{
    opAudioVisi(true);
}
void opAudioClose ()
{
    opAudioVisi(false);
}
void opMasterVolApply ()
{
    slSetMasterVolume(opMasterVol->curvalue);
    char* text;
    asprintf(&text,"Master Volume: %u%%",(int)slRound(opMasterVol->curvalue * 100));
    //opMasterVol_Back->texref = slRenderText(text,TextColor);
    //slQueueReplaceTexture(&opMasterVol_Back->texref,slRenderText(text,TextColor));
    opMasterVol_Back->SetTexRef(slRenderText(text,TextColor));
    free(text);
}
#define opMuteAwayTexRef() slRenderText(slGetMuteWhileAway() ? "Don't Mute While Away" : "Mute While Away",TextColor)
void opToggleMuteAway ()
{
    slSetMuteWhileAway(!slGetMuteWhileAway());
    opMuteAway->SetTexRef(opMuteAwayTexRef());
}
void opAudioInit ()
{
    opAudioPanel = slCreateBox();
    slSetBoxDims(opAudioPanel,0.25,0.25,0.5,0.5,6);
	opAudioPanel->backcolor = PanelColor;
	opAudioPanel->bordercolor = {191,191,191,255};
	opAudioPanel->onclick = slDoNothing;

	opMuteAway = slCreateBox(opMuteAwayTexRef());
	opButtonInfo(opMuteAway);
	opMuteAway->onclick = opToggleMuteAway;
    slSetBoxDims(opMuteAway,0.1,0.025,0.8,0.15,5);
    slRelBoxDims(opMuteAway,opAudioPanel);

    /*
    opMasterVol_Desc = slCreateBox(slRenderText("Master Volume"));
    slSetBoxDims(opMasterVol_Desc,0.1,0.225,0.8,0.035,5);
    slRelBoxDims(opMasterVol_Desc,opAudioPanel);
    */

	opMasterVol_Back = slCreateBox();
	opButtonInfo(opMasterVol_Back);
	opMasterVol_Back->backcolor = slBlankColor;
	opMasterVol_Back->hoverbackcolor = slBlankColor;
	slSetBoxDims(opMasterVol_Back,0.1,0.225,0.8,0.15,4);
	slRelBoxDims(opMasterVol_Back,opAudioPanel);

	opMasterVol_Mark = slCreateBox();
	opButtonInfo(opMasterVol_Mark);
	opMasterVol_Mark->hoverable = true;
	slSetBoxDims(opMasterVol_Mark,0.1,0.225,0.8,0.15,5);
	slRelBoxDims(opMasterVol_Mark,opAudioPanel);
	opMasterVol_Mark->backcolor = opMasterVol_Mark->hoverbackcolor;

	opMasterVol = slCreateSlider(opMasterVol_Back,opMasterVol_Mark,false,true);
	opMasterVol->minvalue = 0;
	opMasterVol->maxvalue = 1;
	opMasterVol->onchange = opMasterVolApply;
	opMasterVol->SetValue(slGetMasterVolume());
	opMasterVolApply();

	opAudioBack = slCreateBox(slRenderText("Back",TextColor));
	opButtonInfo(opAudioBack);
	opAudioBack->onclick = opAudioClose;
	slSetBoxDims(opAudioBack,0.3,0.825,0.4,0.15,5);
	slRelBoxDims(opAudioBack,opAudioPanel);

	opAudioVisi(false);
}
void opAudioQuit ()
{
    slDestroyBox(opAudioPanel);
	slDestroyBox(opMuteAway);
	//slDestroyBox(opMasterVol_Desc);
	slDestroySlider(opMasterVol);
	slDestroyBox(opAudioBack);
}
#define opSetButton(button)\
	opButtonInfo(button);\
	button->wh.w = 0.5;\
	button->xy.x = 0.25;\
	button->z = 7;
void opOnCustomButton ()
{
	opCustomVisi = true;
    printf("opening due to button click\n");
	_opCustomSetVisi_(true);
}
void opRestackMainButtons ()
{
    slScalar spacing;
    slScalar height;
    if (opExitVisible && _opCustomSetVisi_)
    {
        spacing = 0.01;
        height = 7;
    }
    else if (opExitVisible || _opCustomSetVisi_)
    {
        spacing = 0.015;
        height = 6;
    }
    else
    {
        spacing = 0.0225;
        height = 5;
    }
    height = (0.5 - (spacing * height)) / (height - 1);
    opResume->wh.h = height;
	opResume->xy.y = opButtonYPos(0);
    opCustomButton->wh.h = height;
    opCustomButton->xy.y = opButtonYPos(1);
    opVideoButton->wh.h = height;
	opVideoButton->xy.y = opButtonYPos(_opCustomSetVisi_ ? 2 : 1);
    opAudioButton->wh.h = height;
	opAudioButton->xy.y = opButtonYPos(_opCustomSetVisi_ ? 3 : 2);
    opOpenBinds->wh.h = height;
	opOpenBinds->xy.y = opButtonYPos(_opCustomSetVisi_ ? 4 : 3);
    opExit->wh.h = height;
    opExit->xy.y = opButtonYPos(_opCustomSetVisi_ ? 5 : 4);
}
void opSetExitVisible (bool visi)
{
    opExitVisible = visi;
    opExit->visible = visi && opVisi;
    opRestackMainButtons();
}
void opSetAppOptsCallback (void (*custom_setvisi) (bool to))
{
    opCustomVisi = false;
    _opCustomSetVisi_ = custom_setvisi;
    if (custom_setvisi)
    {
        opCustomButton->onclick = custom_setvisi;
        opCustomButton->visible = opVisi;
        printf("closing during init\n");
        _opCustomSetVisi_(false);
    }
    else opCustomButton->visible = false;
    opRestackMainButtons();
}
slKeyBind* PauseBind;
void opInit (SDL_Color MainPanelColor, SDL_Color _PanelColor, SDL_Color _TextColor, SDL_Color _HoverColor)
{
	PanelColor = _PanelColor;
	TextColor = _TextColor;
	HoverColor = _HoverColor;

	opPanel = slCreateBox();
	opPanel->backcolor = MainPanelColor;
	slSetBoxDims(opPanel,0,0,1,1,8);
	opPanel->onclick = slDoNothing;
	opPanel->hoverable = true;
	opPanel->hoverbackcolor = opPanel->backcolor;
	opPanel->hoverbordercolor = opPanel->bordercolor;

	opResume = slCreateBox(slRenderText("Resume",TextColor));
	opSetButton(opResume);
	opResume->onclick = opOnResume;

    opCustomButton = slCreateBox(slRenderText("App Settings",TextColor));
    opSetButton(opCustomButton);
    opCustomButton->onclick = opOnCustomButton;

    opVideoInit();

	opVideoButton = slCreateBox(slRenderText("Video Options",TextColor));
	opSetButton(opVideoButton);
	opVideoButton->onclick = opVideoOpen;

	opAudioInit();

	opAudioButton = slCreateBox(slRenderText("Audio Options",TextColor));
	opSetButton(opAudioButton);
	opAudioButton->onclick = opAudioOpen;

	opOpenBinds = slCreateBox(slRenderText("Edit Key Binds",TextColor));
	opSetButton(opOpenBinds);
	opOpenBinds->onclick = opOnBinds;

    opExit = slCreateBox(slRenderText("Exit",TextColor));
    opSetButton(opExit);
    opExit->onclick = slSignalExit;

    opSetAppOptsCallback(_opCustomSetVisi_);
	opSetVisi();
	PauseBind = slGetKeyBind("Pause Menu",slKeyCode(SDLK_ESCAPE));
	PauseBind->onpress = opOnEscape;
	opBindsInit();

	slListenKeyBinds(opOnBindChanged);
}
void opQuit ()
{
	slUnlistenKeyBinds(opOnBindChanged);
	//slCancelTextureReplacements(&opMasterVol_Back->texref);

	PauseBind->onpress = NULL;
    opVideoQuit();
    opAudioQuit();
	opBindsQuit();
	slDestroyBox(opPanel);
	slDestroyBox(opExit);
	slDestroyBox(opResume);
	slDestroyBox(opVideoButton);
	slDestroyBox(opAudioButton);
	slDestroyBox(opOpenBinds);
	slDestroyBox(opCustomButton);
}

bool opGetVisi ()
{
	return opVisi;
}
void opSetCustomEscapeCallback (bool (*func) ())
{
	opCustomEscapeCallback = func;
}
void opOpen ()
{
	opVisi = true;
	opSetVisi();
}
void opInformCustomVisi (bool visi)
{
	opCustomVisi = visi;
}
void opSetVisibilityCallback (void (*visibility_callback) (bool menu_visible))
{
	opVisibilityCallback = visibility_callback;
}
