#include "flexarray.h"
#include <string.h>
FlexArray::FlexArray (size_t itemsize, size_t initial_capacity = 16)
{
	if (!initial_capacity) initial_capacity = 1;
	this->initial_capacity = initial_capacity;
	capacity = initial_capacity;
	this->itemsize = itemsize;
	mem = malloc(itemsize * capacity);
	usage = 0;
}
void* FlexArray::Append ()
{
	if (usage >= capacity) mem = realloc(mem,itemsize * (capacity <<= 1));
	size_t index = usage++;
	void* out = mem + itemsize * index;
	//if (index_offset >= 0)
	//	*(size_t*)(out + index_offset) = index;
	return out;
}
void FlexArray::Remove (size_t index)
{
	void* last_item = mem + itemsize * --usage;
	void* this_item = mem + itemsize * index;
	if (last_item == this_item) return;
	memcpy(this_item,last_item,itemsize);
	//if (index_offset < 0) return;
	//*(size_t*)(this_item + index_offset) = index;
}
void FlexArray::RemoveItem (void* item)
{
	size_t index = ((size_t)item - (size_t)mem) / itemsize;
	Remove(index);
}
extern void* FlexArray::Get (size_t index);
extern size_t FlexArray::Length ();
void FlexArray::Trim ()
{
	size_t trim_to = capacity;
	while (trim_to >= usage) trim_to >>= 1;
	trim_to <<= 1;
	if (trim_to < initial_capacity) trim_to = initial_capacity;
	if (trim_to < capacity) mem = realloc(mem,itemsize * (capacity = trim_to));
}
extern void FlexArray::RemoveAll ();
extern FlexArray::~FlexArray ();
