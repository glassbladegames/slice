#include <slicegraphs.h>

void waveDiscardAncientSamples (waveGraph* graph)
{
	slBU keep_count = graph->window_size + graph->extra_samples;
	if (graph->values_count > keep_count)
	{
		slBU discard_count = graph->values_count - keep_count;
		graph->values_readpos += discard_count;
		if (graph->values_readpos >= graph->values_capacity) graph->values_readpos -= graph->values_capacity;
		graph->values_count = keep_count;
		if (graph->glbuf_uploaded > discard_count)
		{
			graph->glbuf_uploaded -= discard_count;
			graph->glbuf_readpos += discard_count;
		}
		else
		{
			graph->glbuf_uploaded = 0;
			graph->glbuf_readpos = 0;
			graph->glbuf_writepos = 0;
		}
	}
}
void waveEnsureSpace (waveGraph* graph, slBU required_capacity)
{
	if (required_capacity > graph->values_capacity)
	{
		slBU old_capacity = graph->values_capacity;
		while (graph->values_capacity < required_capacity) graph->values_capacity <<= 1;
		graph->values_buf = realloc(graph->values_buf,sizeof(float) * graph->values_capacity);
		if (graph->values_readpos + graph->values_count >= old_capacity)
		{
			// It was wrapped around previously, so un-wrap it. It's at least twice the size now so the second segment is guaranteed to fit contiguously now.
			slBU move_count = graph->values_writepos;
			memcpy(graph->values_buf + old_capacity,graph->values_buf,move_count * sizeof(float));
			graph->values_writepos = old_capacity + move_count;
		}
	}
}
void waveAppendSamples (waveGraph* graph, float* sample_buf, slBU sample_count)
{
	SDL_LockMutex(graph->values_mutex);

	waveDiscardAncientSamples(graph);
	graph->extra_samples += sample_count;
	slBU required_capacity = graph->values_count + sample_count;
	waveEnsureSpace(graph,required_capacity);
	slBU write_to = graph->values_writepos + sample_count;
	if (write_to >= graph->values_capacity)
	{
		slBU first_count = graph->values_capacity - graph->values_writepos;
		memcpy(graph->values_buf + graph->values_writepos,sample_buf,first_count * sizeof(float));
		memcpy(graph->values_buf,sample_buf + first_count,(sample_count - first_count) * sizeof(float));
		graph->values_writepos = write_to - graph->values_capacity;
	}
	else
	{
		memcpy(graph->values_buf + graph->values_writepos,sample_buf,sample_count * sizeof(float));
		graph->values_writepos = write_to;
	}
	graph->values_count += sample_count;

	SDL_UnlockMutex(graph->values_mutex);
}



slShaderProgram waveProgram;
GLuint
	waveXYLocation,
	waveWHLocation,
	waveYMinLocation,
	waveYRangeLocation,
	waveColorLocation;
char* waveLineVertSrc = R"(#version 130

in float yvalue;

uniform vec2 XY,WH;
	// These are not the actual graph XYWH.
	// They are provided transformed such that X + W * gl_VertexID is the final X coordinate.

uniform float y_min,y_range;

void main ()
{
	float y01 = (yvalue - y_min) / y_range;
	y01 = 1 - clamp(y01,0,1);
	vec2 pos = XY + WH * vec2(gl_VertexID,y01);
	pos.y = 1 - pos.y;
	gl_Position = vec4(pos * 2 - 1,0,1);
}
)";
char* waveLineFragSrc = R"(#version 130

uniform vec4 Color;

void main ()
{
	gl_FragColor = Color;
}
)";
void waveBindProgramAttribs (GLuint program)
{
	glBindAttribLocation(program,0,"yvalue");
}
void waveDrawGraph (waveGraph* graph)
{
	SDL_LockMutex(graph->values_mutex);

	graph->extra_samples -= slGetDelta() / graph->window_time_unit * graph->window_size;
	if (graph->extra_samples < 0.1) graph->extra_samples = 0.1;

	waveDiscardAncientSamples(graph);

	glBindBuffer(GL_ARRAY_BUFFER,graph->points_glbuf);

	// Always ensure the GL-owned buffer has adequate capacity: twice the CPU-owned buffer's size.
	if (graph->glbuf_capacity <= graph->values_capacity)
	{
		glBufferData(GL_ARRAY_BUFFER,(graph->glbuf_capacity = graph->values_capacity << 1) * sizeof(float),NULL,GL_DYNAMIC_DRAW);
		graph->glbuf_readpos = 0;
		graph->glbuf_writepos = 0;
		graph->glbuf_uploaded = 0;
	}

	// See if there are new samples to upload.
	if (graph->glbuf_uploaded < graph->values_count)
	{
		// The GL-owned buffer isn't allowed to wrap around. If it would, start it over and reupload everything.
		if (graph->glbuf_readpos + graph->values_count > graph->glbuf_capacity)
		{
			graph->glbuf_readpos = 0;
			graph->glbuf_writepos = 0;
			graph->glbuf_uploaded = 0;
		}

		slBU upload_length = graph->values_count - graph->glbuf_uploaded;
		slBU src_upload_start = graph->values_readpos + graph->glbuf_uploaded;
		if (src_upload_start >= graph->values_capacity) src_upload_start -= graph->values_capacity;
		if (src_upload_start + upload_length > graph->values_capacity)
		{
			slBU first_segment_len = graph->values_capacity - src_upload_start;
			slBU second_segment_len = upload_length - first_segment_len;
			glBufferSubData(GL_ARRAY_BUFFER,graph->glbuf_writepos * sizeof(float),first_segment_len * sizeof(float),graph->values_buf + src_upload_start);
			graph->glbuf_writepos += first_segment_len;
			glBufferSubData(GL_ARRAY_BUFFER,graph->glbuf_writepos * sizeof(float),second_segment_len * sizeof(float),graph->values_buf);
			graph->glbuf_writepos += second_segment_len;
		}
		else
		{
			glBufferSubData(GL_ARRAY_BUFFER,graph->glbuf_writepos * sizeof(float),upload_length * sizeof(float),graph->values_buf + src_upload_start);
			graph->glbuf_writepos += upload_length;
		}
		graph->glbuf_uploaded = graph->values_count;
	}

	slBU first_vertex = graph->glbuf_readpos;
	//slBU vertex_count = graph->glbuf_uploaded;
	//if (vertex_count > graph->window_size) vertex_count = graph->window_size;
	slBU vertex_count = graph->glbuf_uploaded - (slBU)graph->extra_samples;

	SDL_UnlockMutex(graph->values_mutex);



	glBindVertexArray(graph->points_vao);
	glUseProgram(waveProgram.program);

	GLvec2 xy = GLvec2(graph->xy);
	GLvec2 wh = GLvec2(graph->wh);
	xy.x += wh.w;
	wh.w /= graph->window_size - 1;
	xy.x -= wh.w * (vertex_count + first_vertex);
	glUniform2fv(waveXYLocation,1,(GLfloat*)&xy);
	glUniform2fv(waveWHLocation,1,(GLfloat*)&wh);
	glUniform1f(waveYMinLocation,graph->y_min);
	glUniform1f(waveYRangeLocation,graph->y_max - graph->y_min);
	slUniformFromColor(waveColorLocation,graph->color);

	//printf("drawing %d samples starting at %d\nxywh [%f, %f] [%f, %f]\ny: %f -> %f\n",vertex_count,first_vertex,xy.x,xy.y,wh.w,wh.h,graph->y_min,graph->y_max);
	glDrawArrays(GL_LINE_STRIP,first_vertex,vertex_count);
}
void waveSetGraphDims (waveGraph* graph, slVec2 xy, slVec2 wh, Uint8 z)
{
	graph->xy = xy;
	graph->wh = wh;
	graph->zhook.z = z;
}
waveGraph* waveCreateGraph (slBU window_size, slScalar window_time_unit, slScalar y_min, slScalar y_max)
{
	waveGraph* graph = malloc(sizeof(waveGraph));
	graph->window_size = window_size;
	graph->window_time_unit = window_time_unit;
	graph->y_min = y_min;
	graph->y_max = y_max;
	graph->extra_samples = 0;

	graph->values_capacity = 16;
	graph->values_buf = malloc(sizeof(float) * graph->values_capacity);
	graph->values_readpos = 0;
	graph->values_writepos = 0;
	graph->values_count = 0;

	glGenVertexArrays(1,&graph->points_vao);
	glBindVertexArray(graph->points_vao);
	glGenBuffers(1,&graph->points_glbuf);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER,graph->points_glbuf);
	glVertexAttribPointer(0,1,GL_FLOAT,false,sizeof(float),0);
	graph->glbuf_capacity = 0;
	graph->glbuf_uploaded = 0;
	graph->glbuf_readpos = 0;
	graph->glbuf_writepos = 0;

	graph->values_mutex = SDL_CreateMutex();
	graph->zhook.func = waveDrawGraph;
	graph->zhook.userdata = graph;
	slAddZHook(&graph->zhook);

	graph->color = {255,255,255,255};

	return graph;
}
void waveDestroyGraph (waveGraph* graph)
{
	slRemoveZHook(&graph->zhook);
	SDL_DestroyMutex(graph->values_mutex);

	glDeleteVertexArrays(1,&graph->points_vao);
	glDeleteBuffers(1,&graph->points_glbuf);

	free(graph->values_buf);

	free(graph);
}
namespace graphs_internal
{
	void waveInit ()
	{
		waveProgram = slCreateShaderProgram("Slice Wave Rendering",waveLineVertSrc,waveLineFragSrc,waveBindProgramAttribs);
		waveXYLocation = slLocateUniform(waveProgram,"XY");
		waveWHLocation = slLocateUniform(waveProgram,"WH");
		waveYMinLocation = slLocateUniform(waveProgram,"y_min");
		waveYRangeLocation = slLocateUniform(waveProgram,"y_range");
		waveColorLocation = slLocateUniform(waveProgram,"Color");
	}
	void waveQuit ()
	{
		slDestroyShaderProgram(waveProgram);
	}
}
