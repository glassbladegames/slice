#include <slicegraphs.h>

void lineSetSamples (lineGraph* graph, float* sample_buf, slBU sample_count)
{
	SDL_LockMutex(graph->values_mutex);

	if (graph->values_capacity != sample_count)
		graph->values_buf = realloc(graph->values_buf,sizeof(float) * (graph->values_capacity = sample_count));
	memcpy(graph->values_buf,sample_buf,sample_count * sizeof(float));
	graph->glbuf_stale = true;

	SDL_UnlockMutex(graph->values_mutex);
}



slShaderProgram lineProgram;
GLuint
	lineXYLocation,
	lineWHLocation,
	lineYMinLocation,
	lineYRangeLocation,
	lineColorLocation;
char* lineLineVertSrc = R"(#version 130

in float yvalue;

uniform vec2 XY,WH;
	// These are not the actual graph XYWH.
	// They are provided transformed such that X + W * gl_VertexID is the final X coordinate.

uniform float y_min,y_range;

void main ()
{
	float y01 = (yvalue - y_min) / y_range;
	y01 = 1 - clamp(y01,0,1);
	vec2 pos = XY + WH * vec2(gl_VertexID,y01);
	pos.y = 1 - pos.y;
	gl_Position = vec4(pos * 2 - 1,0,1);
}
)";
char* lineLineFragSrc = R"(#version 130

uniform vec4 Color;
void main ()
{
	gl_FragColor = Color;
}
)";
void lineBindProgramAttribs (GLuint program)
{
	glBindAttribLocation(program,0,"yvalue");
}
void lineDrawGraph (lineGraph* graph)
{
	glBindBuffer(GL_ARRAY_BUFFER,graph->points_glbuf);

	if (graph->glbuf_stale)
	{
		SDL_LockMutex(graph->values_mutex);

		if (graph->glbuf_capacity != graph->values_capacity)
			glBufferData(GL_ARRAY_BUFFER,sizeof(float) * (graph->glbuf_capacity = graph->values_capacity),graph->values_buf,GL_DYNAMIC_DRAW);
		else
			glBufferSubData(GL_ARRAY_BUFFER,0,sizeof(float) * graph->glbuf_capacity,graph->values_buf);

		SDL_UnlockMutex(graph->values_mutex);
	}

	glBindVertexArray(graph->points_vao);
	glUseProgram(lineProgram.program);

	GLvec2 xy = GLvec2(graph->xy);
	GLvec2 wh = GLvec2(graph->wh);
	wh.w /= graph->glbuf_capacity - 1;
	glUniform2fv(lineXYLocation,1,(GLfloat*)&xy);
	glUniform2fv(lineWHLocation,1,(GLfloat*)&wh);
	glUniform1f(lineYMinLocation,graph->y_min);
	glUniform1f(lineYRangeLocation,graph->y_max - graph->y_min);
	slUniformFromColor(lineColorLocation,graph->color);

	//printf("drawing %d samples\nxywh [%f, %f] [%f, %f]\ny: %f -> %f\n",vertex_count,xy.x,xy.y,wh.w,wh.h,graph->y_min,graph->y_max);
	glDrawArrays(GL_LINE_STRIP,0,graph->glbuf_capacity);
}
void lineSetGraphDims (lineGraph* graph, slVec2 xy, slVec2 wh, Uint8 z)
{
	graph->xy = xy;
	graph->wh = wh;
	graph->zhook.z = z;
}
lineGraph* lineCreateGraph (slScalar y_min, slScalar y_max)
{
	lineGraph* graph = malloc(sizeof(lineGraph));
	graph->y_min = y_min;
	graph->y_max = y_max;

	graph->values_capacity = 0;
	graph->values_buf = NULL;

	glGenVertexArrays(1,&graph->points_vao);
	glBindVertexArray(graph->points_vao);
	glGenBuffers(1,&graph->points_glbuf);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER,graph->points_glbuf);
	glVertexAttribPointer(0,1,GL_FLOAT,false,sizeof(float),0);
	graph->glbuf_capacity = 0;

	graph->values_mutex = SDL_CreateMutex();
	graph->zhook.func = lineDrawGraph;
	graph->zhook.userdata = graph;
	slAddZHook(&graph->zhook);

	graph->color = {255,255,255,255};

	return graph;
}
void lineDestroyGraph (lineGraph* graph)
{
	slRemoveZHook(&graph->zhook);
	SDL_DestroyMutex(graph->values_mutex);

	glDeleteVertexArrays(1,&graph->points_vao);
	glDeleteBuffers(1,&graph->points_glbuf);

	if (graph->values_buf) free(graph->values_buf);

	free(graph);
}
namespace graphs_internal
{
	void lineInit ()
	{
		lineProgram = slCreateShaderProgram("Slice line Rendering",lineLineVertSrc,lineLineFragSrc,lineBindProgramAttribs);
		lineXYLocation = slLocateUniform(lineProgram,"XY");
		lineWHLocation = slLocateUniform(lineProgram,"WH");
		lineYMinLocation = slLocateUniform(lineProgram,"y_min");
		lineYRangeLocation = slLocateUniform(lineProgram,"y_range");
		lineColorLocation = slLocateUniform(lineProgram,"Color");
	}
	void lineQuit ()
	{
		slDestroyShaderProgram(lineProgram);
	}
}
