#include <slicegraphs.h>
using namespace graphs_internal;
slBU graphsInitCount = 0;
void graphsInit ()
{
	if (graphsInitCount++) return;
	lineInit();
	waveInit();
}
void graphsQuit ()
{
	if (--graphsInitCount) return;
	lineQuit();
	waveQuit();
}
