#include <slice3d.h>
#include <slice3d/load-obj.h>
using namespace s3d_internal;

slList s3dModels("Slice3D - Models",offsetof(s3dModel,_index_),false);
SDL_mutex* s3dModelListMutex;
SDL_atomic_t s3dModelsQueuedForGC = {0};

void s3dInitMeshBuffer (s3dMesh* mesh)
{
	glGenBuffers(1,&mesh->vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER,mesh->vertex_buffer);
}
void s3dInitModelTransforms_GL (s3dModel* model)
{
    // Create buffer.
    model->matrix_buffer_capacity = 1;
    glGenBuffers(1,&model->matrix_buffer);
    glBindBuffer(GL_COPY_WRITE_BUFFER,model->matrix_buffer);
    glBufferData(
        GL_COPY_WRITE_BUFFER,
        sizeof(GLmat4) * model->matrix_buffer_capacity,
        NULL, // No initial copy from memory.
        GL_DYNAMIC_DRAW
    );

    // Create buffer texture.
    glGenTextures(1,&model->matrix_texture);
    glBindTexture(GL_TEXTURE_BUFFER,model->matrix_texture);
    glTexBuffer(GL_TEXTURE_BUFFER,GL_RGBA32F,model->matrix_buffer);
}
void s3dInitModelTransforms (s3dModel* model)
{
    model->matrix_mutex = SDL_CreateMutex();
    model->matrix_count = 0;
    model->matrix_residency_capacity = 1;
    model->matrix_residency = malloc(sizeof(s3dObject*) * model->matrix_residency_capacity);
    slNextFrameTasks->Push(s3dInitModelTransforms_GL,model,NULL);
}
void s3dQuitModelTransforms (s3dModel* model)
{
    glDeleteTextures(1,&model->matrix_texture);
    glDeleteBuffers(1,&model->matrix_buffer);
}
s3dModel::s3dModel ()
{
	s3dModels.Add(this);
    s3dInitModelTransforms(this);
}
s3dModel::~s3dModel () {}

s3dVec3 cross (s3dVec3 a, s3dVec3 b)
{
    /* todo: make this part of s3dVec3 */
    /* todo: vector math for performance */
    return s3dVec3(
        a.y * b.z - a.z * b.y,
        -(a.x * b.z - a.z * b.x),
        a.x * b.y - a.y * b.x
    );
}
void s3dMeshMaterialInfo (s3dMesh* outmesh, objMaterial* mat)
{
    outmesh->ambient_color      = mat->ambient_color;
    outmesh->diffuse_color      = mat->diffuse_color;
    outmesh->specular_color     = mat->specular_color;
    outmesh->specular_exponent  = mat->specular_exponent;
    outmesh->mag_filter_nearest = mat->mag_filter_nearest;
    outmesh->ignore_environment_ambient = mat->ignore_environment_ambient;

    outmesh->ambient_texref  = mat->ambient_texpath  ?  slLoadTexture(mat->ambient_texpath ).ReserveGetRaw() : NULL;
    outmesh->diffuse_texref  = mat->diffuse_texpath  ?  slLoadTexture(mat->diffuse_texpath ).ReserveGetRaw() : NULL;
    outmesh->specular_texref = mat->specular_texpath ?  slLoadTexture(mat->specular_texpath).ReserveGetRaw() : NULL;
    outmesh->opacity_texref  = mat->opacity_texpath  ?  slLoadTexture(mat->opacity_texpath ).ReserveGetRaw() : NULL;
    outmesh->normal_texref   = mat->bump_texpath     ? s3dLoadBumpMap(mat->bump_texpath    ).ReserveGetRaw() : NULL;
}
struct s3dObjLoadingData
{
    char* basepath_cloned;
    char* filename_cloned;

    s3dModelRef output;
    s3dObjLoadingData (s3dModelRef model, char* basepath, char* filename)
        : output(model)
    {
        basepath_cloned = slStrAlwaysClone(basepath);
        filename_cloned = slStrAlwaysClone(filename);
    }
    ~s3dObjLoadingData ()
    {
        // These are freed earlier on in the loading process.
        //free(basepath_cloned);
        //free(filename_cloned);
    }
    objModel* input;
    float** each_mesh_vertices_buf;

    void* next;
};
void s3dFinalizeObj (s3dObjLoadingData* data)
{
    objModel* input = data->input;
	s3dModelRef output = data->output;
    if (input)
    {
        for (int i = 0; i < input->group_count; i++)
        {
            s3dMesh* outmesh = output->meshes + i;
            float* vertices_buf = data->each_mesh_vertices_buf[i];
            objGroup* group = input->group_buf + i;

            s3dMeshMaterialInfo(outmesh,group->material);
    		s3dInitMeshBuffer(outmesh);
    		size_t vertices_size = sizeof(float) * 24 * outmesh->triangle_count;
            glBufferData(GL_ARRAY_BUFFER,vertices_size,vertices_buf,GL_STATIC_DRAW);
            free(vertices_buf);
            glFinish();
            output->meshcount++;
        }
        free(data->each_mesh_vertices_buf);
        objFree(input);
    }
	output->done_loading = true;
    delete data; // Deleting loading data object releases model reference.
}
SDL_atomic_t s3dModelsInFinalization = {0};
class s3dObjLoadersProto : public slConsumerQueue
{
public:
    s3dObjLoadersProto () : slConsumerQueue(offsetof(s3dObjLoadingData,next)) {}
private:
    void ItemFunc (void* data_ptr) override
    {
        s3dObjLoadingData* data = data_ptr;

        objModel* input = objLoad(data->basepath_cloned,data->filename_cloned);
        free(data->basepath_cloned);
        free(data->filename_cloned);

        data->input = input;
        s3dModelRef output = data->output;
        if (input)
        {
            output->meshes = malloc(sizeof(s3dMesh) * input->group_count);
            int* tricounts = malloc(sizeof(int) * input->group_count);
            for (int i = 0; i < input->group_count; i++)
            {
                objGroup* object = input->group_buf + i;
                int tricount = 0;
                for (int j = 0; j < object->face_count; j++)
                {
                    objFace* face = object->face_buf + j;
                    tricount += face->vertex_ref_count - 2;
                }
                tricounts[i] = tricount;
            }

            data->each_mesh_vertices_buf = malloc(sizeof(float*) * input->group_count);

            const int tri_floats = 8 * 3; // 3 vertices * {pos_xyz, tex_uv, norm_xyz}

            for (int obj_id = 0; obj_id < input->group_count; obj_id++)
            {
                objGroup* group = input->group_buf + obj_id;
                int tricount = tricounts[obj_id];
                float* vertices_buf = malloc(sizeof(float) * tri_floats * tricount);
                data->each_mesh_vertices_buf[obj_id] = vertices_buf;
                float* vertexpointer = vertices_buf;
                bool calc_normal = false;
                for (int fid = 0; fid < group->face_count; fid++)
                {
                    objFace* face = group->face_buf + fid;
                    for (int vertexid = 2; vertexid < face->vertex_ref_count; vertexid++)
                    {
                        bool should_calc_normal = false;
                        for (int id_offset = -2; id_offset <= 0; id_offset++)
                        {
                            // first vertex of each triangle should be the first vertex of
                            // the face, since we can assemble a 'triangle fan' in this way
                            objVertexRef* ref;
                            if (id_offset == -2) ref = face->vertex_ref_buf;
                            else ref = face->vertex_ref_buf + vertexid + id_offset;

                            should_calc_normal = should_calc_normal || !ref->normal_id;
                        }
                        s3dVec3 calculated_normal;
                        if (should_calc_normal)
                        {
                            s3dVec3 positions [3];
                            for (int id_offset = -2; id_offset <= 0; id_offset++)
                            {
                                // first vertex of each triangle should be the first vertex of
                                // the face, since we can assemble a 'triangle fan' in this way
                                objVertexRef* ref;
                                if (id_offset == -2) ref = face->vertex_ref_buf;
                                else ref = face->vertex_ref_buf + vertexid + id_offset;

                                // pos_id guaranteed to be >= 1
                                objVertexPos* pos = input->vertex_pos_buf + ref->pos_id - 1;
                                positions[id_offset + 2] = s3dVec3(pos->x,pos->y,pos->z);
                            }
                            s3dVec3 dir_1 = positions[1] - positions[0];
                            s3dVec3 dir_2 = positions[2] - positions[1];
                            calculated_normal = cross(dir_1,dir_2);
                        }

                        for (int id_offset = -2; id_offset <= 0; id_offset++)
                        {
                            // first vertex of each triangle should be the first vertex of
                            // the face, since we can assemble a 'triangle fan' in this way
                            objVertexRef* ref;
                            if (id_offset == -2) ref = face->vertex_ref_buf;
                            else ref = face->vertex_ref_buf + vertexid + id_offset;

                            // pos_id guaranteed to be >= 1
                            objVertexPos* pos = input->vertex_pos_buf + ref->pos_id - 1;
                            *vertexpointer++ = pos->x;
                            *vertexpointer++ = pos->y;
                            *vertexpointer++ = pos->z;
                            // we don't support position w component

							slScalar dist_from_center = slsqrt(pos->x * pos->x + pos->y * pos->y + pos->z * pos->z);
							if (output->max_dist_from_center < dist_from_center)
								output->max_dist_from_center = dist_from_center;

                            // tex_id may be 0 if it was unspecified
                            if (ref->tex_id)
                            {
                                objVertexTex* tex = input->vertex_tex_buf + ref->tex_id - 1;
                                *vertexpointer++ = tex->u;
                                *vertexpointer++ = tex->v;
                                // we don't support texture w component
                            }
                            else
                            {
                                // Texture probably isn't used or is a solid color.
                                // Could leave this as uninitialized junk, or zeroes,
                                // but for bump mapping math to work, these actually
                                // need to be distinct for each vertex, even if they're
                                // just distinct junk values.
                                float junk = vertexpointer - vertices_buf;
                                *vertexpointer++ = junk;
                                *vertexpointer++ = junk;
                            }

                            // normal_id may be 0 if it was unspecified
                            if (ref->normal_id)
                            {
                                objVertexNormal* normal = input->vertex_normal_buf + ref->normal_id - 1;
                                *vertexpointer++ = normal->x;
                                *vertexpointer++ = normal->y;
                                *vertexpointer++ = normal->z;
                            }
                            else
                            {
                                *vertexpointer++ = calculated_normal.x;
                                *vertexpointer++ = calculated_normal.y;
                                *vertexpointer++ = calculated_normal.z;
                            }
                        }
                    }
                }

                s3dMesh* outmesh = output->meshes + obj_id;
                outmesh->triangle_count = tricount;
            }

            free(tricounts);
        }
        else
        {
            output->meshes = malloc(0);
        }

        slBackgroundUploader->Push(s3dFinalizeObj,data,&s3dModelsInFinalization);
    }
};
s3dObjLoadersProto* s3dObjLoaders;

struct s3dLoadedModelsTreeProto : slTreeMap<s3dLoadedModelInfo,s3dLoadedModel>
{
    s3dLoadedModelsTreeProto ()
        : slTreeMap(&s3dLoadedModel::tree_key,&s3dLoadedModel::tree_data)
    {}
    int Compare (s3dLoadedModelInfo* data_a, s3dLoadedModelInfo* data_b) override
    {
        /* Return 1 for in-order, -1 for reverse-order, 0 for equality. */
        return strcmp(data_a->filepath,data_b->filepath);
    }
    void PrintKey (s3dLoadedModelInfo* data) override
    {
        printf("obj@'%s'\n",data->filepath);
    }
}
s3dLoadedModelsTree;
s3dLoadedModel::s3dLoadedModel (s3dLoadedModelInfo search_key)
{
    /*  s3dLoadObj allocates the filepath string, so if we copied it here
        that function would have to free its own copy anyway, so don't bother
        copying (more efficient this way).  */
    tree_key.filepath = search_key.filepath;//slStrAlwaysClone(search_key.filepath);

    // Uses key of the inserted object, which is now set up by this point.
    s3dLoadedModelsTree.Insert(this);
    //printf("count:   %d\n",s3dLoadedModelsTree.total_items);
    //printf("recount: %d\n",s3dLoadedModelsTree.Recount());
    //s3dLoadedModelsTree.PrintInOrder();
}
void s3dLoadedModel::Unregister ()
{
    s3dModel::Unregister();

    s3dLoadedModelsTree.Remove(this);
    free(tree_key.filepath);
}

s3dModelRef s3dLoadObj (char* basepath, char* filename)
{
    /* basepath can be NULL, filename expected to always exist */
    if (!basepath) basepath = "";
    char* fullpath = slStrAlwaysConcat(basepath,filename);

    SDL_LockMutex(s3dModelListMutex);

    s3dLoadedModelInfo search_key;
    search_key.filepath = fullpath;
    s3dLoadedModel* found_model = s3dLoadedModelsTree.Lookup(&search_key);
    if (found_model)
    {
        // Make it a Ref before unlocking in case unlocking is holding up an
        // Abandon that would delete it, because if so we'd try to Reserve an
        // object that's about to be eliminated.
        s3dModelRef ref_out = found_model;
        SDL_UnlockMutex(s3dModelListMutex);
        free(fullpath);
        return found_model;
    }
    s3dModelRef output = new s3dLoadedModel(search_key);
    SDL_UnlockMutex(s3dModelListMutex);

    s3dObjLoaders->Push(new s3dObjLoadingData(output,basepath,filename));
	return output;
}

s3dModelRef s3dSpriteModel (objMaterial material)
{
    SDL_LockMutex(s3dModelListMutex);
    /*  Assigning the new'ed s3dModel directly into a s3dModelRef on one line
        thanks to operator* & operator-> overloads on slRef. Kills two birds
        with one stone, both of which need to be within the mutex guard anyway:
          - The constructor adds the new model to the list of all models which
            requires synchronization.
          - The reference needs to be constructed to increment the refcount in
            the newly created model, before mutex is unlocked and conceivably
            something else could be unblocked and free it (sure it seems
            unlikely but it's technically possible nonetheless).  */
    s3dModelRef model = new s3dModel;
    SDL_UnlockMutex(s3dModelListMutex);

    /*
        TO DO: Search existing sprite models to find one that had an identical
        material, and return it instead. (The factory_free won't need to free
        anything, but will need to remove from the sprite models collection).
    */

	model->meshes = malloc(sizeof(s3dMesh));
	s3dMesh* mesh = model->meshes;
	float vertices [48] =
	{
		-.5,-.5,0,	0,0,   0,0,1,
		 .5,-.5,0,	1,0,   0,0,1,
		 .5, .5,0,	1,1,   0,0,1,

		 .5, .5,0,	1,1,   0,0,1,
		-.5, .5,0,	0,1,   0,0,1,
		-.5,-.5,0,	0,0,   0,0,1,
	};
	mesh->triangle_count = 2;
	s3dInitMeshBuffer(mesh);
	glBufferData(GL_ARRAY_BUFFER,sizeof(vertices),vertices,GL_STATIC_DRAW);
    s3dMeshMaterialInfo(mesh,&material);
    model->meshcount = 1;
    /*
        To do: push the above loading code to slNextFrameTasks instead of doing
        it right there, so that this function can be called from any thread.
    */

	return model;
}
s3dModelRef s3dSpriteModel (char* sprite_texpath, bool mag_filter_nearest, float ambient, float diffuse, float specular, float spec_exp, bool ignore_environment_ambient)
{
    objMaterial mat = objFallbackMaterial;
    mat.ambient_texpath = sprite_texpath;
    mat.diffuse_texpath = sprite_texpath;
    mat.mag_filter_nearest = mag_filter_nearest;
    mat.ambient_color = {ambient,ambient,ambient};
    mat.diffuse_color = {diffuse,diffuse,diffuse};
    mat.specular_color = {specular,specular,specular};
    mat.specular_exponent = spec_exp;
    mat.ignore_environment_ambient = ignore_environment_ambient;
    return s3dSpriteModel(mat);
}

void s3dReleaseModel (s3dModel* model)
{
    //printf("releasing model %llX\n",model);

	s3dMesh* mesh = model->meshes;
	for (slBU cur = 0; cur < model->meshcount; cur++,mesh++)
	{
        if (mesh->ambient_texref) mesh->ambient_texref->Abandon();
		if (mesh->diffuse_texref) mesh->diffuse_texref->Abandon();
		if (mesh->specular_texref) mesh->specular_texref->Abandon();
        if (mesh->opacity_texref) mesh->opacity_texref->Abandon();
        if (mesh->normal_texref) mesh->normal_texref->Abandon();
		glDeleteBuffers(1,&mesh->vertex_buffer);
	}
    glFinish();

	free(model->meshes);

    SDL_DestroyMutex(model->matrix_mutex);
    free(model->matrix_residency);
    s3dQuitModelTransforms(model);

    delete model;
}
void s3dModel::Unregister ()
{
	s3dModels.Remove(this);
}
void s3dModel::OnZero ()
{
	SDL_LockMutex(s3dModelListMutex);
    /*  Must double-check the atomic to ensure that, between checking first
        time and obtaining the mutex, the usage has not become nonzero.  */
    if (IsZero())
    {
        //printf("usage hit zero on %llX\n",model);
		Unregister();
        slBackgroundUploader->Push(s3dReleaseModel,this,&s3dModelsQueuedForGC);
    }
    SDL_UnlockMutex(s3dModelListMutex);
}

struct s3dModelMatrixUpdate
{
    s3dModelRef model;
    int index;
    GLmat4 matrix;
    s3dModelMatrixUpdate (s3dModelRef model, int index, GLmat4 matrix)
        : model(model)
    {
        this->index = index;
        this->matrix = matrix;
    }
};
void s3dModelMatricesEnforceBufferSize (s3dModelRef model)
{
    if (model->matrix_count > model->matrix_buffer_capacity)
    {
        //printf("expanding\n");

        // We will need the old buffer object and size to copy (and delete),
        slBU old_size = model->matrix_buffer_capacity;
        GLuint old_buffer = model->matrix_buffer;

        // Choose a power-of-2 size that is large enough.
        while (model->matrix_count > model->matrix_buffer_capacity)
            model->matrix_buffer_capacity <<= 1;

        // Copy data from the old buffer to a new one.
        glGenBuffers(1,&model->matrix_buffer);
        glBindBuffer(GL_COPY_READ_BUFFER,old_buffer);
        glBindBuffer(GL_COPY_WRITE_BUFFER,model->matrix_buffer);
        glBufferData(
            GL_COPY_WRITE_BUFFER,
            sizeof(GLmat4) * model->matrix_buffer_capacity,
            NULL, // No initial copy from memory.
            GL_DYNAMIC_DRAW
        );
        glCopyBufferSubData(
            GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER,
            0, 0, // No offset into either the read or write buffers.
            sizeof(GLmat4) * old_size
        );

        // Bind texture to new buffer.
        glBindTexture(GL_TEXTURE_BUFFER,model->matrix_texture);
        glTexBuffer(GL_TEXTURE_BUFFER,GL_RGBA32F,model->matrix_buffer);

        // Delete old buffer.
        glDeleteBuffers(1,&old_buffer);
    }
}
void s3dModelMatrixUpdateApply (s3dModelMatrixUpdate* update)
{
    /* Optimize for obviously-redundant updates. */
    if (update->index < update->model->matrix_count)
    {
        /* Make sure buffer is large enough. */
        s3dModelMatricesEnforceBufferSize(update->model);

        /* Apply update to the buffer data. */
        glBindBuffer(GL_COPY_WRITE_BUFFER,update->model->matrix_buffer);
        glBufferSubData(
            GL_COPY_WRITE_BUFFER,
            sizeof(GLmat4) * update->index,
            sizeof(GLmat4),
            &update->matrix
        );
    }

    delete update;
}
void s3dModel::UpdateMatrix (s3dObject* update_obj)
{
    SDL_LockMutex(matrix_mutex);

    slNextFrameTasks->Push(
        s3dModelMatrixUpdateApply,
        new s3dModelMatrixUpdate(
            this,
            update_obj->matrix_residence,
            update_obj->world_transform
        ),
        NULL
    );

    SDL_UnlockMutex(matrix_mutex);
}
void s3dModel::AddMatrix (s3dObject* add_obj)
{
    SDL_LockMutex(matrix_mutex);

    add_obj->matrix_residence = matrix_count++;
    if (matrix_count >= matrix_residency_capacity)
        matrix_residency = realloc(
            matrix_residency,
            sizeof(s3dObject**) * (matrix_residency_capacity <<= 1)
        );
    matrix_residency[add_obj->matrix_residence] = add_obj;

    slNextFrameTasks->Push(
        s3dModelMatrixUpdateApply,
        new s3dModelMatrixUpdate(
            this,
            add_obj->matrix_residence,
            add_obj->world_transform
        ),
        NULL
    );

    SDL_UnlockMutex(matrix_mutex);
}
void s3dModel::RemoveMatrix (s3dObject* remove_obj)
{
    SDL_LockMutex(matrix_mutex);

    s3dObject* last_obj = matrix_residency[--matrix_count];
    last_obj->matrix_residence = remove_obj->matrix_residence;
    matrix_residency[last_obj->matrix_residence] = last_obj;

    slNextFrameTasks->Push(
        s3dModelMatrixUpdateApply,
        new s3dModelMatrixUpdate(
            this,
            last_obj->matrix_residence,
            last_obj->world_transform
        ),
        NULL
    );

    SDL_UnlockMutex(matrix_mutex);
}

slList s3dObjects("Slice3D - Objects",offsetof(s3dObject,_index_),false);
s3dObject* s3dCreateObject (s3dModelRef model_ref)
{
	s3dObject* out = new s3dObject;
    s3dObjects.Add(out);

    out->model = model_ref;

    out->visible = false;
    out->SetWorldTransform(GLmat4_identity());
    out->SetVisible(true);

	return out;
}
void s3dDestroyObject (s3dObject* object)
{
    object->SetVisible(false);

    s3dObjects.Remove(object);
    delete object;
}



s3dVec3 s3dCamera::GetNormal ()
{
    return   GLmat4_yrotate(slDegToRad_F(yaw_pitch_roll.x))
           * GLmat4_xrotate(slDegToRad_F(yaw_pitch_roll.y))
	       * GLvec4(0,0,1);
}
void s3dCamera::Recalculate ()
{
    dirty = false;
    final_view_matrix =
        GLmat4_zrotate(slDegToRad_F(-yaw_pitch_roll.z))
      * GLmat4_xrotate(slDegToRad_F(-yaw_pitch_roll.y))
      * GLmat4_yrotate(slDegToRad_F(-yaw_pitch_roll.x))
      * GLmat4_offset(GLvec4(-origin))
    ;
    final_view_undo =
        GLmat4_offset(GLvec4(origin))
      * GLmat4_yrotate(slDegToRad_F(yaw_pitch_roll.x))
      * GLmat4_xrotate(slDegToRad_F(yaw_pitch_roll.y))
      * GLmat4_zrotate(slDegToRad_F(yaw_pitch_roll.z))
    ;
}
GLmat4 s3dCamera::GetProjectionMatrix (slInt2 target_wh)
{
    // Don't apply aspect correction if it is unnecessary due to a square aspect
    // ratio, or if may involve a division by zero.
    if (!target_wh.w || !target_wh.h || target_wh.w == target_wh.h)
        return projection_matrix;

    // Negative values will not be corrected properly but there is absolutely
    // no reason why they should be used anyway.
    GLvec4 scaling = target_wh.w > target_wh.h
        ? GLvec4(target_wh.h / (GLfloat)target_wh.w, 1, 1)
        : GLvec4(1, target_wh.w / (GLfloat)target_wh.h, 1);
    return GLmat4_scaling(scaling) * projection_matrix;
}

void s3dInitShaders ();
void s3dQuitShaders ();
slBU s3dInitCount = 0;
slGC_Action s3dModelsGC_Action;
GLuint s3dVertexArray;



slShaderProgram s3dShadowProgram;
GLuint
    s3dShadowProgram_ViewMatrix,
    s3dShadowProgram_ProjectionMatrix,
    s3dShadowProgram_NearFarZ;

char* s3dShadowVertSrc = R"(#version 140

in vec3 RawVertPos;
uniform samplerBuffer TransformsTex;

uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;
out float FragZ;
uniform vec2 NearFarZ;

in vec2 VertTexCoords;
out vec2 FragTexCoords;

void main ()
{
    int lookup_pos = gl_InstanceID * 4;
    mat4 ModelMatrix = mat4(
        texelFetch(TransformsTex, lookup_pos),
        texelFetch(TransformsTex, lookup_pos + 1),
        texelFetch(TransformsTex, lookup_pos + 2),
        texelFetch(TransformsTex, lookup_pos + 3)
    );

    vec4 view_pos = ViewMatrix * ModelMatrix * vec4(RawVertPos,1);
    FragZ = (-view_pos.z - NearFarZ.x) / (NearFarZ.y - NearFarZ.x);
	gl_Position = ProjectionMatrix * view_pos;

	FragTexCoords = VertTexCoords;
}
)";

char* s3dShadowFragSrc = R"(#version 130

in float FragZ;

in vec2 FragTexCoords;
uniform sampler2D OpacityTex;

void main ()
{
    gl_FragDepth = FragZ;

    float opacity = texture(OpacityTex,FragTexCoords).r;
    if (opacity == 0) discard;
}
)";

void s3dBindShadowAttribs (GLuint program)
{
	glBindAttribLocation(program,0,"RawVertPos");
	glBindAttribLocation(program,1,"VertTexCoords");
}

void s3dSetupShadowUniforms ()
{
    s3dShadowProgram = slCreateShaderProgram("Slice3D Shadows",s3dShadowVertSrc,s3dShadowFragSrc,s3dBindShadowAttribs);
    glUniform1i(slLocateUniform(s3dShadowProgram,"TransformsTex"),0);
    glUniform1i(slLocateUniform(s3dShadowProgram,"OpacityTex"),1);
    s3dShadowProgram_ViewMatrix = slLocateUniform(s3dShadowProgram,"ViewMatrix");
    s3dShadowProgram_ProjectionMatrix = slLocateUniform(s3dShadowProgram,"ProjectionMatrix");
    s3dShadowProgram_NearFarZ = slLocateUniform(s3dShadowProgram,"NearFarZ");
}



slShaderProgram s3dDrawProgram;
GLuint
    s3dDrawProgram_ViewMatrix,
    s3dDrawProgram_ProjectionMatrix,
    s3dDrawProgram_NearFarZ,
    s3dDrawProgram_MaterialAmbientColor,
    s3dDrawProgram_MaterialDiffuseColor,
    s3dDrawProgram_MaterialSpecularColor,
    s3dDrawProgram_MaterialSpecularExponent,
    s3dDrawProgram_EnvironmentAmbientColor,
    s3dDrawProgram_IgnoreEnvironmentAmbient;

char* s3dDrawVertSrc = R"(#version 140

in vec3 RawVertPos;
uniform samplerBuffer TransformsTex;

out vec3 FragWorldPos;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;
out float FragZ;
uniform vec2 NearFarZ;

in vec2 VertTexCoords;
out vec2 FragTexCoords;

in vec3 VertNormal;
out vec3 FragNormal;

void main ()
{
    int lookup_pos = gl_InstanceID << 2;
    mat4 ModelMatrix = mat4(
        texelFetch(TransformsTex, lookup_pos),
        texelFetch(TransformsTex, lookup_pos + 1),
        texelFetch(TransformsTex, lookup_pos + 2),
        texelFetch(TransformsTex, lookup_pos + 3)
    );

	vec4 world_pos = ModelMatrix * vec4(RawVertPos,1);
    FragWorldPos = world_pos.xyz;

    vec4 view_pos = ViewMatrix * world_pos;
    FragZ = (-view_pos.z - NearFarZ.x) / (NearFarZ.y - NearFarZ.x);
    gl_Position = ProjectionMatrix * view_pos;

	FragTexCoords = VertTexCoords;
    FragNormal = mat3(ModelMatrix) * VertNormal;
}
)";

char* s3dDrawFragSrc = R"(#version 130

in vec3 FragWorldPos;
in float FragZ;
in vec2 FragTexCoords;
in vec3 FragNormal;

uniform sampler2D AmbientTex;
uniform sampler2D DiffuseTex;
uniform sampler2D SpecularTex;
uniform sampler2D OpacityTex;
uniform sampler2D NormalTex;

uniform vec3 EnvironmentAmbientColor;
uniform bool IgnoreEnvironmentAmbient;
uniform vec3 MaterialAmbientColor;
uniform vec3 MaterialDiffuseColor;
uniform vec3 MaterialSpecularColor;
uniform float MaterialSpecularExponent;

out vec4 VisibleColor; // Only lit by ambient by this shader, but will be added to by each light.
out vec4 FinalDiffuseColor; // diffuse texture color * material diffuse color
out vec4 FinalSpecularColor; // similar to this^, plus alpha value is the specular exponent
out vec4 FinalNormal;

mat3 CotangentFrame (vec3 Normal, vec3 position, vec2 texcoord)
{
    // get edge vectors of the pixel triangle
    vec3 diff_position_x = dFdx(position);
    vec3 diff_position_y = dFdy(position);
    vec2 diff_texcoord_x = dFdx(texcoord);
    vec2 diff_texcoord_y = dFdy(texcoord);

    // solve the linear system
    vec3 dp2perp = cross(diff_position_y, Normal);
    vec3 dp1perp = cross(Normal, diff_position_x);
    vec3 Tangent =
        dp2perp * diff_texcoord_x.x +
        dp1perp * diff_texcoord_y.x;
    vec3 Bitangent =
        dp2perp * diff_texcoord_x.y +
        dp1perp * diff_texcoord_y.y;

    return mat3(normalize(Tangent), normalize(Bitangent), Normal);
}

vec3 TransformByNormal (vec3 normal_from_map)
{
    mat3 transform_by_normal = CotangentFrame(normalize(FragNormal), -FragWorldPos, FragTexCoords);
    return normalize(transform_by_normal * normal_from_map);
}

void main ()
{
    gl_FragDepth = FragZ;

    float opacity = texture(OpacityTex,FragTexCoords).r;
    if (opacity == 0) discard;

    vec3 normal = texture(NormalTex,FragTexCoords).rgb * 2 - 1;
    normal = TransformByNormal(normal);
    FinalNormal = vec4(normal * .5 + .5,1);

    vec3 ambient = texture(AmbientTex,FragTexCoords).rgb;
    ambient *= MaterialAmbientColor;
    if (!IgnoreEnvironmentAmbient) ambient *= EnvironmentAmbientColor;
    VisibleColor = vec4(ambient,1);

    vec3 diffuse = texture(DiffuseTex,FragTexCoords).rgb;
    diffuse *= MaterialDiffuseColor;
    FinalDiffuseColor = vec4(diffuse,1);

    vec3 specular = texture(SpecularTex,FragTexCoords).rgb;
    specular *= MaterialSpecularColor;
    float SpecRangeScale = 200;
    FinalSpecularColor = vec4(specular,MaterialSpecularExponent / SpecRangeScale);
}
)";

void s3dBindDrawAttribs (GLuint program)
{
	glBindAttribLocation(program,0,"RawVertPos");
	glBindAttribLocation(program,1,"VertTexCoords");
	glBindAttribLocation(program,2,"VertNormal");
    glBindFragDataLocation(program,0,"VisibleColor");
    glBindFragDataLocation(program,1,"FinalDiffuseColor");
    glBindFragDataLocation(program,2,"FinalSpecularColor");
    glBindFragDataLocation(program,3,"FinalNormal");
}

void s3dSetupDrawUniforms ()
{
	s3dDrawProgram = slCreateShaderProgram("Slice3D Draw",s3dDrawVertSrc,s3dDrawFragSrc,s3dBindDrawAttribs);
    glUniform1i(slLocateUniform(s3dDrawProgram,"AmbientTex"),0);
	glUniform1i(slLocateUniform(s3dDrawProgram,"DiffuseTex"),1);
    glUniform1i(slLocateUniform(s3dDrawProgram,"SpecularTex"),2);
    glUniform1i(slLocateUniform(s3dDrawProgram,"OpacityTex"),3);
    glUniform1i(slLocateUniform(s3dDrawProgram,"NormalTex"),4);
    glUniform1i(slLocateUniform(s3dDrawProgram,"TransformsTex"),5);
    s3dDrawProgram_ViewMatrix = slLocateUniform(s3dDrawProgram,"ViewMatrix");
    s3dDrawProgram_ProjectionMatrix = slLocateUniform(s3dDrawProgram,"ProjectionMatrix");
    s3dDrawProgram_NearFarZ = slLocateUniform(s3dDrawProgram,"NearFarZ");
    s3dDrawProgram_MaterialAmbientColor = slLocateUniform(s3dDrawProgram,"MaterialAmbientColor");
    s3dDrawProgram_MaterialDiffuseColor = slLocateUniform(s3dDrawProgram,"MaterialDiffuseColor");
    s3dDrawProgram_MaterialSpecularColor = slLocateUniform(s3dDrawProgram,"MaterialSpecularColor");
    s3dDrawProgram_MaterialSpecularExponent = slLocateUniform(s3dDrawProgram,"MaterialSpecularExponent");
    s3dDrawProgram_EnvironmentAmbientColor = slLocateUniform(s3dDrawProgram,"EnvironmentAmbientColor");
    s3dDrawProgram_IgnoreEnvironmentAmbient = slLocateUniform(s3dDrawProgram,"IgnoreEnvironmentAmbient");
}



slShaderProgram s3dLightProgram;
GLuint
    s3dLightProgram_AxesCorrection,
    s3dLightProgram_CameraIsOrtho,
    s3dLightProgram_CameraNearFarZ,
    s3dLightProgram_ViewUndoMatrix,
    s3dLightProgram_LightColor,
    s3dLightProgram_LightIsDirectional,
    s3dLightProgram_CoverageAngle,
    s3dLightProgram_LightStrength,
    s3dLightProgram_ShadowViewMatrix,
    s3dLightProgram_ShadowProjectionMatrix,
    s3dLightProgram_ShadowNearFarZ,
    s3dLightProgram_CameraPos,
    s3dLightProgram_LightPos,
    s3dLightProgram_LightIsOrtho,
    s3dLightProgram_LightDir;

char* s3dLightVertSrc = R"(#version 140

out vec2 SampleCoords;

uniform vec2 AxesCorrection;
out vec2 FrustumRay;

void main ()
{
    vec2 screen_pos = vec2((gl_VertexID & 2) >> 1, gl_VertexID & 1);
    SampleCoords = screen_pos;

    screen_pos = screen_pos * 2 - 1;
    gl_Position = vec4(screen_pos,0,1);

    FrustumRay = screen_pos * AxesCorrection;
}
)";

char* s3dLightFragSrc = R"(#version 130

in vec2 SampleCoords;

in vec2 FrustumRay;
uniform bool CameraIsOrtho;
uniform vec2 CameraNearFarZ;
uniform mat4 ViewUndoMatrix;

uniform sampler2D ForwardDiffuseTex;
uniform sampler2D ForwardSpecularTex;
uniform sampler2D ForwardNormalTex;
uniform sampler2D ForwardDepthTex;
uniform mat4 InverseCameraTransform;

uniform sampler2DShadow ShadowTex;
uniform mat4 ShadowViewMatrix;
uniform mat4 ShadowProjectionMatrix;
uniform vec2 ShadowNearFarZ;

uniform vec3 CameraPos;
uniform vec3 LightPos;
uniform bool LightIsOrtho;
uniform vec3 LightDir;
uniform vec3 LightColor;
uniform bool LightIsDirectional;
uniform float CoverageAngle;
uniform float LightStrength;

out vec3 VisibleColor;

#define M_PI 3.14159265358979323846264338327950288

vec3 CalcWorldPos ()
{
    float depth = texture(ForwardDepthTex,SampleCoords).x * (CameraNearFarZ.y - CameraNearFarZ.x) + CameraNearFarZ.x;
    vec4 view_pos = vec4(FrustumRay,-depth,1);
    if (!CameraIsOrtho) view_pos.xy *= depth;
    return (ViewUndoMatrix * view_pos).xyz;
}

void main ()
{
    vec3 diffuse_color = texture(ForwardDiffuseTex,SampleCoords).rgb;
    vec4 specular_info = texture(ForwardSpecularTex,SampleCoords);
    vec3 specular_color = specular_info.rgb;
    if (diffuse_color == vec3(0,0,0) && specular_color == vec3(0,0,0)) discard;
    float SpecRangeScale = 200;
    float specular_exponent = specular_info.a * SpecRangeScale;
    vec3 normal = texture(ForwardNormalTex,SampleCoords).xyz * 2 - 1;
    vec3 world_pos = CalcWorldPos();



    float unobstructed;
    if (LightIsOrtho) {
        unobstructed = 1;
    } else {
        vec4 shadow_view_pos = ShadowViewMatrix * vec4(world_pos,1);
        vec4 shadow_proj_pos = ShadowProjectionMatrix * shadow_view_pos;
        vec3 shadow_coords = vec3(
            shadow_proj_pos.xy / abs(shadow_proj_pos.w) * 0.5 + 0.5,
            (-shadow_view_pos.z - ShadowNearFarZ.x) / (ShadowNearFarZ.y - ShadowNearFarZ.x)
        );

        if (shadow_coords.x >= 0 && shadow_coords.x <= 1 &&
            shadow_coords.y >= 0 && shadow_coords.y <= 1 &&
            shadow_coords.z >= 0)
        {
            float ShadowBias = 0.1 / (ShadowNearFarZ.y - ShadowNearFarZ.x);
            shadow_coords.z -= ShadowBias;
            unobstructed = texture(ShadowTex,shadow_coords);

            if (unobstructed == 0) discard;
        }
        else
        {
            discard;//unobstructed = 0;
        }
    }



    vec3 light_dir;
    if (LightIsOrtho) light_dir = normalize(LightPos);
    else light_dir = normalize(LightPos - world_pos);

    float diffuse_value = max(dot(light_dir, normal), 0);

    if (diffuse_value == 0) discard;

    float specular_value;
    if (LightIsOrtho) {
        specular_value = 0;
    } else {
        vec3 view_dir = normalize(CameraPos - world_pos);
        vec3 halfway_dir = normalize(light_dir + view_dir);
        specular_value = pow(max(dot(normal,halfway_dir),0),specular_exponent);
    }



    float strength_value = unobstructed;
    if (LightIsDirectional)
    {
        float angle_from_center = acos(max(dot(light_dir,LightDir),0));
        if (angle_from_center > CoverageAngle) discard;
        strength_value *= cos(angle_from_center / CoverageAngle * M_PI * 0.5);
    }

    if (!LightIsOrtho) // Ortho lights are infinite strength by distance.
    {
        float distance_from_light = length(LightPos - world_pos);
        float distance_multiplier = exp(distance_from_light / LightStrength);
        distance_multiplier = clamp(distance_multiplier,0,1);

        strength_value *= distance_multiplier;
    }



    vec3 unscaled = diffuse_color * diffuse_value + specular_color * specular_value;
    gl_FragColor = vec4(unscaled * strength_value * LightColor, 1);
}
)";

void s3dBindLightAttribs (GLuint program)
{
    // Nothing to bind.
}

void s3dSetupLightUniforms ()
{
    s3dLightProgram = slCreateShaderProgram("Slice3D Lights",s3dLightVertSrc,s3dLightFragSrc,s3dBindLightAttribs);
    glUniform1i(slLocateUniform(s3dLightProgram,"ForwardDiffuseTex"),0);
    glUniform1i(slLocateUniform(s3dLightProgram,"ForwardSpecularTex"),1);
    glUniform1i(slLocateUniform(s3dLightProgram,"ForwardNormalTex"),2);
    glUniform1i(slLocateUniform(s3dLightProgram,"ForwardDepthTex"),3);
    glUniform1i(slLocateUniform(s3dLightProgram,"ShadowTex"),4);
    s3dLightProgram_AxesCorrection = slLocateUniform(s3dLightProgram,"AxesCorrection");
    s3dLightProgram_CameraIsOrtho = slLocateUniform(s3dLightProgram,"CameraIsOrtho");
    s3dLightProgram_CameraNearFarZ = slLocateUniform(s3dLightProgram,"CameraNearFarZ");
    s3dLightProgram_ViewUndoMatrix = slLocateUniform(s3dLightProgram,"ViewUndoMatrix");
    s3dLightProgram_LightColor = slLocateUniform(s3dLightProgram,"LightColor");
    s3dLightProgram_LightIsDirectional = slLocateUniform(s3dLightProgram,"LightIsDirectional");
    s3dLightProgram_CoverageAngle = slLocateUniform(s3dLightProgram,"CoverageAngle");
    s3dLightProgram_LightStrength = slLocateUniform(s3dLightProgram,"LightStrength");
    s3dLightProgram_ShadowViewMatrix = slLocateUniform(s3dLightProgram,"ShadowViewMatrix");
    s3dLightProgram_ShadowProjectionMatrix = slLocateUniform(s3dLightProgram,"ShadowProjectionMatrix");
    s3dLightProgram_ShadowNearFarZ = slLocateUniform(s3dLightProgram,"ShadowNearFarZ");
    s3dLightProgram_CameraPos = slLocateUniform(s3dLightProgram,"CameraPos");
    s3dLightProgram_LightPos = slLocateUniform(s3dLightProgram,"LightPos");
    s3dLightProgram_LightIsOrtho = slLocateUniform(s3dLightProgram,"LightIsOrtho");
    s3dLightProgram_LightDir = slLocateUniform(s3dLightProgram,"LightDir");
}



slTexture* s3dWhiteTexture;
slTexture* s3dUpVecTexture;
inline slForceInline slTexture* s3dTexOrWhite (slTexture* texref)
{
    if (!texref) return s3dWhiteTexture;
    if (!texref->ready) return s3dWhiteTexture;
    return texref;
}
inline slForceInline slTexture* s3dTexOrUpVec (slTexture* texref)
{
    if (!texref) return s3dUpVecTexture;
    if (!texref->ready) return s3dUpVecTexture;
    return texref;
}
GLfloat s3dTextureAnisotropicValue;
void s3dInitShaders ()
{
    glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT,&s3dTextureAnisotropicValue);
    s3dWhiteTexture = slSolidColorTexture({0xFF,0xFF,0xFF,0xFF}).ReserveGetRaw();
    s3dUpVecTexture = slSolidColorTexture({0x80,0x80,0xFF,0xFF}).ReserveGetRaw();

    s3dSetupShadowUniforms();
    s3dSetupDrawUniforms();
    s3dSetupLightUniforms();

    /* TO DO: Setup shaders for any post-processing effects, such as distance-fog. */
    /*s3dDrawProgram_DistanceFogEnabled = slLocateUniform(s3dDrawProgram,"DistanceFogEnabled");
    s3dDrawProgram_DistanceFogColor = slLocateUniform(s3dDrawProgram,"DistanceFogColor");
    s3dDrawProgram_DistanceFogMultiplier = slLocateUniform(s3dDrawProgram,"DistanceFogMultiplier");*/
}
void s3dQuitShaders ()
{
    s3dWhiteTexture->Abandon();
    s3dUpVecTexture->Abandon();

	slDestroyShaderProgram(s3dDrawProgram);
    slDestroyShaderProgram(s3dShadowProgram);
}



inline slForceInline void s3dUseTex (int texid, slTexture* texref, int mag_filter)
{
    glActiveTexture(GL_TEXTURE0 + texid);
    glBindTexture(GL_TEXTURE_2D,texref->tex);
    slSetTextureClamping(GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,mag_filter);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAX_ANISOTROPY_EXT,s3dTextureAnisotropicValue);
}
void s3dDrawTo (slFrame* frame)
{
    if (frame) frame->DrawTo();
    else slDrawToScreen();
}
void s3dRender (s3dRenderCamera* cam)
{
    slInt2 target_dims = cam->vis_frame->GetDims();



    cam->gbuf_frame->DrawTo();

	if (cam->cull_backfaces) glEnable(GL_CULL_FACE);
	else glDisable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(s3dDrawProgram.program);

	glUniformMatrix4fv(
        s3dDrawProgram_ViewMatrix,1,GL_TRUE,
        cam->GetViewMatrix().data
        //GLmat4_identity().data
    );
    glUniformMatrix4fv(
        s3dDrawProgram_ProjectionMatrix,1,GL_TRUE,
        cam->GetProjectionMatrix(target_dims).data
        //GLmat4_identity().data
    );
    glUniform2fv(s3dDrawProgram_NearFarZ,1,cam->NearFarZ());

	glBindVertexArray(s3dVertexArray);

    for (slBU model_i = 0; model_i < s3dModels.itemcount; model_i++)
    {
        s3dModel* model = s3dModels.items[model_i];

        /*  Don't bother to draw if there are no transforms.
            (Models with no transforms aren't necessarily doomed for garbage
            collection, something may reference them but be set as invisible
            or have a temporary reference in some thread etc.)  */
        /*  To do: have a list of models that have a matrix_count of at least 1
            so that this 'if' is unneeded and those items are entirely avoided
            by the render loop. Add and remove models to that list at the moment
            their matrix_count changes from zero to nonzero and vice versa.  */
        if (model->matrix_count)
        {
            glActiveTexture(GL_TEXTURE0 + 5);
            glBindTexture(GL_TEXTURE_BUFFER,model->matrix_texture);

			s3dMesh* mesh = model->meshes;
			for (uint32_t mesh_i = 0; mesh_i < model->meshcount; mesh_i++, mesh++)
			{
                slTexture* ambient_texref = s3dTexOrWhite(mesh->ambient_texref);
                slTexture* diffuse_texref = s3dTexOrWhite(mesh->diffuse_texref);
                slTexture* specular_texref = s3dTexOrWhite(mesh->specular_texref);
                slTexture* opacity_texref = s3dTexOrWhite(mesh->opacity_texref);
                slTexture* normal_texref = s3dTexOrUpVec(mesh->normal_texref);

                glUniform1i(s3dDrawProgram_IgnoreEnvironmentAmbient,mesh->ignore_environment_ambient);
                glUniform3fv(s3dDrawProgram_MaterialAmbientColor,1,(GLfloat*)&mesh->ambient_color);
                glUniform3fv(s3dDrawProgram_MaterialDiffuseColor,1,(GLfloat*)&mesh->diffuse_color);
                glUniform3fv(s3dDrawProgram_MaterialSpecularColor,1,(GLfloat*)&mesh->specular_color);
                glUniform1f(s3dDrawProgram_MaterialSpecularExponent,mesh->specular_exponent);

                int mag_filter = mesh->mag_filter_nearest ? GL_NEAREST : GL_LINEAR;
                s3dUseTex(0,ambient_texref,mag_filter);
                s3dUseTex(1,diffuse_texref,mag_filter);
                s3dUseTex(2,specular_texref,mag_filter);
                s3dUseTex(3,opacity_texref,mag_filter);
                s3dUseTex(4,normal_texref,mag_filter);

				glBindBuffer(GL_ARRAY_BUFFER,mesh->vertex_buffer);
            	void* offset = 0;
            	size_t vertsize = sizeof(float) * 8;
            	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,vertsize,offset); offset += sizeof(GLfloat) * 3;
            	glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,vertsize,offset); offset += sizeof(GLfloat) * 2;
            	glVertexAttribPointer(2,3,GL_FLOAT,GL_FALSE,vertsize,offset); offset += sizeof(GLfloat) * 3;

				glDrawArraysInstanced(GL_TRIANGLES,0,mesh->triangle_count * 3,model->matrix_count);
			}
        }
    }



    cam->vis_frame->DrawTo();

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE,GL_ONE);

    glUseProgram(s3dLightProgram.program);

    GLvec4 cam_origin = cam->GetOrigin();
    glUniform3fv(s3dLightProgram_CameraPos,1,(GLfloat*)&cam_origin);

    glUniformMatrix4fv(
        s3dLightProgram_ViewUndoMatrix,1,GL_TRUE,
        cam->GetViewUndo().data
    );
    glUniform2fv(s3dLightProgram_CameraNearFarZ,1,cam->NearFarZ());

    slVec2 aspect_correction = target_dims.w > target_dims.h
        ? slVec2(target_dims.w / (GLfloat)target_dims.h, 1)
        : slVec2(1, target_dims.h / (GLfloat)target_dims.w);
    aspect_correction *= cam->CornerMultiplier();
    glUniform2f(s3dLightProgram_AxesCorrection,aspect_correction.x,aspect_correction.y);
    glUniform1i(s3dLightProgram_CameraIsOrtho,cam->IsOrtho());

    glActiveTexture(GL_TEXTURE0 + 0);
    glBindTexture(GL_TEXTURE_2D,cam->gbuf_diffuse->tex);
    glActiveTexture(GL_TEXTURE0 + 1);
    glBindTexture(GL_TEXTURE_2D,cam->gbuf_specular->tex);
    glActiveTexture(GL_TEXTURE0 + 2);
    glBindTexture(GL_TEXTURE_2D,cam->gbuf_normal->tex);
    glActiveTexture(GL_TEXTURE0 + 3);
    glBindTexture(GL_TEXTURE_2D,cam->gbuf_depth->tex);

    for (int i = 0; i < s3dLight::Lights.itemcount; i++)
    {
        s3dLight* light = s3dLight::Lights.items[i];
        s3dCamera* light_cam = light->GetCamera();

    	glUniformMatrix4fv(
            s3dLightProgram_ShadowViewMatrix,1,GL_TRUE,
            light_cam->GetViewMatrix().data
        );
    	glUniformMatrix4fv(
            s3dLightProgram_ShadowProjectionMatrix,1,GL_TRUE,
            light_cam->GetProjectionMatrix(light->frame->GetDims()).data
        );
        glUniform2fv(s3dLightProgram_ShadowNearFarZ,1,light_cam->NearFarZ());

        glUniform1i(s3dLightProgram_LightIsOrtho,light->type == s3dLight_Ortho);
        glUniform1i(s3dLightProgram_LightIsDirectional,light->type == s3dLight_Spot);

        if (light->type == s3dLight_Ortho) {
            GLvec4 light_dir = light_cam->GetNormal();
            glUniform3fv(s3dLightProgram_LightPos,1,(GLfloat*)&light_dir.data);
        } else {
            GLvec4 light_pos = light_cam->GetOrigin();
            glUniform3fv(s3dLightProgram_LightPos,1,(GLfloat*)&light_pos.data);
            GLvec4 light_dir = light_cam->GetNormal();
            glUniform3fv(s3dLightProgram_LightDir,1,(GLfloat*)&light_dir.data);

            if (light->type == s3dLight_Spot)
                glUniform1f(
                    s3dLightProgram_CoverageAngle,
                    slDegToRad_F(light->coverage_angle) * 0.5f
                );

            glUniform1f(s3dLightProgram_LightStrength,-light->strength);
        }

        glUniform3fv(s3dLightProgram_LightColor,1,(GLfloat*)&light->color);

        glActiveTexture(GL_TEXTURE0 + 4);
        glBindTexture(GL_TEXTURE_2D,light->frame_depth->tex);

        glDrawArrays(GL_TRIANGLE_STRIP,0,4);
    }
}
void s3dShadowRender (slFrame* output_frame, s3dCamera* cam)
{
    output_frame->DrawTo();
	glClear(GL_DEPTH_BUFFER_BIT);

	glEnable(GL_DEPTH_TEST);
	if (cam->cull_backfaces) glEnable(GL_CULL_FACE);
	else glDisable(GL_CULL_FACE);

	glUseProgram(s3dShadowProgram.program);
	glUniformMatrix4fv(
        s3dShadowProgram_ViewMatrix,1,GL_TRUE,
        cam->GetViewMatrix().data
    );
    glUniformMatrix4fv(
        s3dShadowProgram_ProjectionMatrix,1,GL_TRUE,
        cam->GetProjectionMatrix(output_frame->GetDims()).data
    );
    glUniform2fv(s3dShadowProgram_NearFarZ,1,cam->NearFarZ());

	glBindVertexArray(s3dVertexArray);

    for (slBU model_i = 0; model_i < s3dModels.itemcount; model_i++)
    {
        s3dModel* model = s3dModels.items[model_i];

        /*  Don't bother to draw if there are no transforms.
            (Models with no transforms aren't necessarily doomed for garbage
            collection, something may reference them but be set as invisible
            or have a temporary reference in some thread etc.)  */
        if (model->matrix_count)
        {
            glActiveTexture(GL_TEXTURE0 + 0);
            glBindTexture(GL_TEXTURE_BUFFER,model->matrix_texture);

			s3dMesh* mesh = model->meshes;
			for (uint32_t mesh_i = 0; mesh_i < model->meshcount; mesh_i++, mesh++)
			{
                slTexture* opacity_texref = s3dTexOrWhite(mesh->opacity_texref);
                int mag_filter = mesh->mag_filter_nearest ? GL_NEAREST : GL_LINEAR;
                s3dUseTex(1,opacity_texref,mag_filter);

				glBindBuffer(GL_ARRAY_BUFFER,mesh->vertex_buffer);
            	void* offset = 0;
            	size_t vertsize = sizeof(float) * 8;
            	glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,vertsize,offset); offset += sizeof(GLfloat) * 3;
            	glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,vertsize,offset); offset += sizeof(GLfloat) * 2;

				glDrawArraysInstanced(GL_TRIANGLES,0,mesh->triangle_count * 3,model->matrix_count);
			}
        }
    }

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
}
void s3dUpdateShadows ()
{
    for (int i = 0; i < s3dLight::Lights.itemcount; i++)
    {
        s3dLight* light = s3dLight::Lights.items[i];
        light->frame->Resize(slInt2(light->shadow_tex_res));
        s3dShadowRender(light->frame,&light->camera);

        glBindTexture(GL_TEXTURE_2D,light->frame_depth->tex);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_COMPARE_MODE,GL_COMPARE_REF_TO_TEXTURE);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
        /*
        glGenerateMipmap(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
        glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAX_ANISOTROPY_EXT,s3dTextureAnisotropicValue);
        */
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);

    }
}
void s3dSetDistanceFog (bool enabled, GLcolor3 rgb, GLfloat multiplier)
{
    //glUseProgram(s3dDrawProgram.program);
    //glUniform1i(s3dDrawProgram_DistanceFogEnabled,enabled);
    //glUniform3fv(s3dDrawProgram_DistanceFogColor,1,(GLfloat*)&rgb);
    //glUniform1f(s3dDrawProgram_DistanceFogMultiplier,-multiplier);
}
void s3dSetEnvironmentAmbientColor (GLcolor3 color)
{
    glUseProgram(s3dDrawProgram.program);
    glUniform3fv(s3dDrawProgram_EnvironmentAmbientColor,1,(GLfloat*)&color);
}
void s3dSetDiffuseVoxeling (bool voxeling_enabled, GLfloat voxel_size)
{
    //glUseProgram(s3dDrawProgram.program);
    //glUniform1i(s3dDrawProgram_DiffuseVoxelingEnabled,voxeling_enabled);
    //glUniform1f(s3dDrawProgram_DiffuseVoxelingSize,voxel_size);
}
void s3dSetSpecularVoxeling (bool voxeling_enabled, GLfloat voxel_size)
{
    //glUseProgram(s3dDrawProgram.program);
    //glUniform1i(s3dDrawProgram_SpecularVoxelingEnabled,voxeling_enabled);
    //glUniform1f(s3dDrawProgram_SpecularVoxelingSize,voxel_size);
}
void s3dSetFogVoxeling (bool voxeling_enabled, GLfloat voxel_size)
{
    //glUseProgram(s3dDrawProgram.program);
    //glUniform1i(s3dDrawProgram_FogVoxelingEnabled,voxeling_enabled);
    //glUniform1f(s3dDrawProgram_FogVoxelingSize,voxel_size);
}



s3dLight::s3dLight ()
{
    Lights.Add(this);
}
s3dLight::~s3dLight ()
{
    Lights.Remove(this);
    delete frame;
}
slList s3dLight::Lights = slList("Slice3D Lights",offsetof(s3dLight,_index_),false);
s3dLight* s3dLight::Create ()
{
    return new s3dLight;
}
void s3dLight::Destroy (s3dLight* light)
{
    delete light;
}
void s3dLight::SetType (Uint8 type)
{
    // Changing type may require reconfiguration of the shadow textures,
    // if we have to go from flat texture to cubemap, etc.
    this->type = type;
}
void s3dLight::SetCoverageAngle (slScalar angle)
{
    // Changing coverage angle may require reconfiguration of shadow textures,
    // since even spot lights can be narrow enough to only need a flat texture
    // or too wide and need a cubemap.
    this->coverage_angle = angle;
    SetType(s3dLight_Spot); // Calling this function implies it's a spot light.
}
void s3dLight::SetHalfSpan (slScalar span) {
    this->half_span = span;
    SetType(s3dLight_Ortho); // Calling this function implies it's an ortho light.
}



SDL_Surface* s3dBumpToNormalMap (SDL_Surface* in_surf)
{
    /* TO DO: Use a GPU compute shader to do this work. */

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    SDL_Surface* surf = SDL_CreateRGBSurface(0,in_surf->w,in_surf->h,32,0xFF<<24,0xFF<<16,0xFF<<8,0xFF);
#else
    SDL_Surface* surf = SDL_CreateRGBSurface(0,in_surf->w,in_surf->h,32,0xFF,0xFF<<8,0xFF<<16,0xFF<<24);
#endif

    int total = in_surf->w * in_surf->h;
    Uint8* in_color_ptr = in_surf->pixels;
    Uint8* out_color_ptr = surf->pixels;
    for (int y = 0; y < in_surf->h; y++)
    {
        int next_y = (y + 1) % in_surf->h;
        int off_y = (next_y - y) * in_surf->w * 4;
        for (int x = 0 ; x < in_surf->w; x++)
        {
            int next_x = (x + 1) % in_surf->w;
            int off_x = (next_x - x) * 4;

            s3dVec3 pos_here = s3dVec3(x, y, *in_color_ptr / -(slScalar)0x3F);
            s3dVec3 pos_right = s3dVec3(x + 1, y, *(in_color_ptr + off_x) / -(slScalar)0x3F);
            s3dVec3 pos_below = s3dVec3(x, y + 1, *(in_color_ptr + off_y) / -(slScalar)0x3F);
            in_color_ptr += 4;

            s3dVec3 normal = cross(pos_right - pos_here, pos_below - pos_here).normalized();

            *out_color_ptr++ = 0x80 + (int)(normal.x * 0x7F);
            *out_color_ptr++ = 0x80 + (int)(normal.y * 0x7F);
            *out_color_ptr++ = 0x80 + (int)(normal.z * 0x7F);
            *out_color_ptr++ = 0xFF;
        }
    }

    return surf;
}

struct s3dBumpTexturesTreeProto : slTreeMap<s3dBumpInfo,s3dBumpTexture>
{
    s3dBumpTexturesTreeProto ()
        : slTreeMap(&s3dBumpTexture::bump_tree_key,&s3dBumpTexture::bump_tree_data)
    {}
    int Compare (s3dBumpInfo* info_a, s3dBumpInfo* info_b) override
    {
        /* Return 1 for in-order, -1 for reverse-order, 0 for equality. */
        return strcmp(info_a->filepath,info_b->filepath);
    }
    void PrintKey (s3dBumpInfo* info) override
    {
        printf("bump '%s'\n",info->filepath);
    }
}
s3dBumpTexturesTree;

s3dBumpTexture::s3dBumpTexture (s3dBumpInfo search_key) : bump_tree_key(search_key)
{
    bump_tree_key.filepath = slStrAlwaysClone(bump_tree_key.filepath);
    s3dBumpTexturesTree.Insert(this);
    //printf("count:   %d\n",s3dBumpTexturesTree.total_items);
    //printf("recount: %d\n",s3dBumpTexturesTree.Recount());
    //s3dBumpTexturesTree.PrintInOrder();
}
s3dBumpTexture::~s3dBumpTexture ()
{
    free(bump_tree_key.filepath);
}
void s3dBumpTexture::Unregister ()
{
    slTexture::Unregister();
    s3dBumpTexturesTree.Remove(this);
    //printf("count:   %d\n",s3dBumpTexturesTree.total_items);
    //printf("recount: %d\n",s3dBumpTexturesTree.Recount());
    //s3dBumpTexturesTree.PrintInOrder();
}

extern SDL_Surface* slInvalidTextureSurf;
extern SDL_mutex* slTexListMutex;
class s3dBumpAsyncQueueProto : public slConsumerQueue
{
public:
    s3dBumpAsyncQueueProto () : slConsumerQueue(offsetof(s3dBumpTexture,next_load)) {}
private:
    void ItemFunc (void* queue_item) override
    {
        s3dBumpTexture* out_tex = queue_item;

    	//printf("bump texture is loading\n");
    	//SDL_Delay(500); // Simulate very slow disk.

        SDL_Surface* original = slLoadTexture_Surf(out_tex->bump_tree_key.filepath);
    	SDL_Surface* converted;
    	if (original)
    	{
            converted = slConvertSurface_RGBA32(original);
    		SDL_FreeSurface(original);
        }
        else converted = NULL;

        SDL_Surface* normals_surf;
        if (converted)
        {
            normals_surf = s3dBumpToNormalMap(converted);
            SDL_FreeSurface(converted);
        }
        else
        {
            normals_surf = slInvalidTextureSurf;
            printf("failure to load bump map from '%s'\n",out_tex->bump_tree_key.filepath);
        }

        slQueueTexUpload(out_tex,normals_surf);
    }
};
s3dBumpAsyncQueueProto* s3dBumpAsyncQueue;

s3dBumpTexRef s3dLoadBumpMap (char* path)
{
    SDL_LockMutex(slTexListMutex);

    s3dBumpInfo search_key(path);
    s3dBumpTexture* found_tex = s3dBumpTexturesTree.Lookup(&search_key);
    if (found_tex)
    {
        s3dBumpTexRef ref_out = found_tex;
        SDL_UnlockMutex(slTexListMutex);
        return ref_out;
    }

    s3dBumpTexture* out_tex = new s3dBumpTexture(search_key);

    out_tex->Reserve();
    s3dBumpAsyncQueue->Push(out_tex);

    s3dBumpTexRef ref_out = out_tex;
    SDL_UnlockMutex(slTexListMutex);
	return ref_out;
}

void s3dObject::SetWorldTransform (GLmat4 world_transform)
{
    this->world_transform = world_transform.transpose();
    if (visible) model->UpdateMatrix(this);
}
void s3dObject::SetVisible (bool visible)
{
    if (visible != this->visible)
    {
        this->visible = visible;
        if (visible) model->AddMatrix(this);
        else model->RemoveMatrix(this);
    }
}
void s3dObject::SetModel (s3dModelRef set_to)
{
    if (set_to != model)
    {
        if (visible) model->RemoveMatrix(this);
    	model = set_to;
        if (visible) model->AddMatrix(this);
    }
}

s3dNullModel* s3dNullModel::instance;
void s3dInit ()
{
	if (s3dInitCount++) return;

    s3dModelListMutex = SDL_CreateMutex();
    (s3dObjLoaders = new s3dObjLoadersProto)->Start();

    (s3dBumpAsyncQueue = new s3dBumpAsyncQueueProto)->Start();

	s3dInitShaders();

    glGenVertexArrays(1,&s3dVertexArray);
    glBindVertexArray(s3dVertexArray);
    glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	phys3dInit();

    // We probably don't need to lock this since we're in the engine setup,
    // but might as well for good habit.
    SDL_LockMutex(s3dModelListMutex);
    s3dNullModel::instance = new s3dNullModel;
    s3dNullModel::instance->Reserve();
    SDL_UnlockMutex(s3dModelListMutex);
}
void s3dQuit ()
{
	if (--s3dInitCount) return;

    s3dNullModel::instance->Abandon();

    phys3dQuit();

    glDeleteVertexArrays(1,&s3dVertexArray);

	s3dQuitShaders();

	s3dObjects.UntilEmpty(s3dDestroyObject);

    slNextFrameTasks->Flush();
    /*
        Destroying objects can cause transform updates to be generated.
        These updates reserve models until they have been completed, so
        we need to finish the updates in order for models to be freed.
        Otherwise we'll deadlock when we wait for all models to be unregistered
        below.
    */

    delete s3dObjLoaders->Stop();
    slWaitBackgroundUploads(&s3dModelsInFinalization);

    int sleep_ms = 100, warn_ms = 5000;
    while (s3dModels.itemcount)
    {
        SDL_Delay(sleep_ms);
        if (warn_ms > 0)
        {
            warn_ms -= sleep_ms;
            if (warn_ms <= 0)
                printf("Still waiting on models to be unregistered (possible reference leak).\n");
        }
    }
    if (warn_ms <= 0)
        printf("All models are now unregistered (nevermind about a reference leak).\n");

	// Wait until background destruction is finished.
    slWaitBackgroundUploads(&s3dModelsQueuedForGC);

    SDL_DestroyMutex(s3dModelListMutex);

    delete s3dBumpAsyncQueue->Stop();
}
