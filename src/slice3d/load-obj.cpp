#include <slice3d/load-obj.h>
objMaterial objFallbackMaterial =
{
    NULL, // name
    {1,1,1}, // ambient_color
    {1,1,1}, // diffuse_color
    {1,1,1}, // specular_color
    250, // specular_exponent
    0, // mag_filter_nearest
    0, // ignore_environment_ambient
    NULL, // ambient_texpath
    NULL, // diffuse_texpath
    NULL, // specular_texpath
    NULL, // opacity_texpath
    NULL, // bump_texpath
};
void objDiscardRemainingLine (FILE* file)
{
    // Skip to the end of the line. This is useful for ignoring comment text,
    // keyword lines, or values we don't care about (such as when we want to
    // pay attention to an 'o' object entry, but don't need to store the object
    // name that follows the 'o' keyword).
    int got;
    while (1)
    {
        got = fgetc(file);
        if (got == '\n' || got == EOF) break;
    }
    ungetc(got,file); // we do want the newline/EOF to still be there.
}
void objSkipSpacingAndComments (FILE* file)
{
    int got;
    while (1) // keep reading until we break from finding an unwanted character
    {
        got = fgetc(file);
        if (got == '#')
        {
            // comment, skip the rest of the characters on this line
            objDiscardRemainingLine(file);
            return; // save time, we'd just read and unread the newline character
        }
        if (isspace(got) && got != '\n') continue;
        break; // we found something other than an in-line space or comment symbol
    }
    ungetc(got, file); // put back the non-space char we finally hit
}
int objLexFloat (FILE* file, float* output)
{
    objSkipSpacingAndComments(file);

    int first = fgetc(file);
    int negative = first == '-';
    if (!negative)
    {
        ungetc(first,file); // we'll just read it in normally in a moment
        if (!isdigit(first)) return 1; // wasn't '-' or a digit, not a number
    }

    double value = 0; // use doubles while calculating, narrow to float at end
    int got;
    while (isdigit(got = fgetc(file)))
    {
        value *= 10;
        value += got - '0';
    }
    // we got here by hitting a non-digit char
    if (got != '.') ungetc(got,file); // it's not part of the number, put it back
    else // we have more numbers following the decimal point
    {
        double place = 1;
        while (isdigit(got = fgetc(file)))
        {
            place /= 10;
            value += (got - '0') * place;
        }
        // we got here by hitting a non-digit char
        ungetc(got,file);
    }

    if (negative) value = -value;
    *output = value;
    return 0;
}
int objLexInt (FILE* file, int* output)
{
    objSkipSpacingAndComments(file);

    // get one char to make sure we are starting with a digit
    int first = fgetc(file);
    ungetc(first,file); // we'll just read it in normally in a moment
    if (!isdigit(first)) return 1; // wasn't a digit, not a number

    int value = 0;
    int got;
    while (isdigit(got = fgetc(file)))
    {
        value *= 10;
        value += got - '0';
    }
    // we got here by hitting a non-digit char
    ungetc(got,file);

    *output = value;
    return 0;
}
int objLexExpectChar (FILE* file, int expected)
{
    objSkipSpacingAndComments(file);

    int got = fgetc(file);
    if (got != expected)
    {
        ungetc(got,file); // unexpected char, put it back
        return 1;
    }
    return 0;
}
void objLexString (FILE* file, char** output)
{
    objSkipSpacingAndComments(file);

    int got;
    int str_capacity = 16;
    char* str_buf = malloc(str_capacity);
    int str_len = 0;
    while (!isspace(got = fgetc(file)))
    {
        str_buf[str_len++] = got;
        if (str_len + 1 >= str_capacity)
            str_buf = realloc(str_buf, str_capacity <<= 1);
    }
    str_buf[str_len] = 0;
    *output = str_buf;
}
char* objFilePath (char* basepath, char* filename)
{
    if (!basepath) return filename;

    int basepath_len = strlen(basepath);
    int filename_len = strlen(filename);
    int fullpath_len = basepath_len + filename_len;
    char* fullpath = malloc(fullpath_len + 1);
    memcpy(fullpath, basepath, basepath_len);
    memcpy(fullpath + basepath_len, filename, filename_len);
    fullpath[fullpath_len] = 0;
    return fullpath;
}
FILE* objOpen (char* basepath, char* filename)
{
    char* fullpath = objFilePath(basepath, filename);
    printf("opening from '%s'\n",fullpath);
    FILE* file = fopen(fullpath, "r");
    if (basepath) free(fullpath); // only free if it was separately allocated
    return file;
}
void objForwardSlashes (char* filename)
{
    while (1)
    {
        switch (*filename)
        {
            case 0: return;
            case '\\': *filename = '/';
        }
        filename++;
    }
}
int objReadColor (FILE* lib_file, objColor* color)
{
    if (objLexFloat(lib_file,&color->r))
    {
        printf("color missing red component\n");
        return 1;
    }
    if (objLexFloat(lib_file,&color->g))
    {
        printf("color missing green component\n");
        return 1;
    }
    if (objLexFloat(lib_file,&color->b))
    {
        printf("color missing blue component\n");
        return 1;
    }
    return 0;
}
int objReadMaterials (FILE* file, objModel* model, int& material_capacity, char* basepath)
{
    objMaterial* current_material = NULL;

    while (1)
    {
        if (!objLexExpectChar(file, EOF)) break; // Stop if we successfully 'expect' an End Of File
        if (!objLexExpectChar(file, '\n')) continue; // Go to the next line if this was a blank/comment-only line.

        // There's something here. Read the keyword for this line.
        char* keyword;
        objLexString(file,&keyword); // not checking status because this can't fail (gives empty string)

        if (!strcmp(keyword,"newmtl")) // objMaterial
        {
            free(keyword);

            // advance to a new object & initialize it
            if (model->material_count >= material_capacity)
                model->material_buf = realloc(model->material_buf, sizeof(objMaterial) * (material_capacity <<= 1));
            current_material = model->material_buf + model->material_count++;
            *current_material = objFallbackMaterial; // Sets default colors, and initializes char*'s to NULL.
            objLexString(file,&current_material->name);
        }
        else if (!strcmp(keyword,"Ns"))
        {
            free(keyword);

            if (!current_material)
            {
                printf("'Ns' without prior 'newmtl'\n");
                return 1;
            }
            if (objLexFloat(file,&current_material->specular_exponent))
            {
                printf("value missing after 'Ns'");
                return 1;
            }
        }
        else if (!strcmp(keyword,"Ka"))
        {
            free(keyword);

            if (!current_material)
            {
                printf("'Ka' without prior 'newmtl'\n");
                return 1;
            }
            if (objReadColor(file,&current_material->ambient_color)) return 1;
        }
        else if (!strcmp(keyword,"Kd"))
        {
            free(keyword);

            if (!current_material)
            {
                printf("'Kd' without prior 'newmtl'\n");
                return 1;
            }
            if (objReadColor(file,&current_material->diffuse_color)) return 1;
        }
        else if (!strcmp(keyword,"Ks"))
        {
            free(keyword);

            if (!current_material)
            {
                printf("'Ks' without prior 'newmtl'\n");
                return 1;
            }
            if (objReadColor(file,&current_material->specular_color)) return 1;
        }
        else if (!strcmp(keyword,"map_Ka"))
        {
            free(keyword);

            if (!current_material)
            {
                printf("'map_Ka' without prior 'newmtl'\n");
                return 1;
            }
            if (current_material->ambient_texpath)
            {
                printf("multiple map_Ka lines for the same material\n");
                return 1;
            }

            char* filename;
            objLexString(file,&filename);
            objForwardSlashes(filename);
            current_material->ambient_texpath = objFilePath(basepath,filename);
            if (basepath) free(filename); // only free if texpath was separately allocated
        }
        else if (!strcmp(keyword,"map_Kd"))
        {
            free(keyword);

            if (!current_material)
            {
                printf("'map_Kd' without prior 'newmtl'\n");
                return 1;
            }
            if (current_material->diffuse_texpath)
            {
                printf("multiple map_Kd lines for the same material\n");
                return 1;
            }

            char* filename;
            objLexString(file,&filename);
            objForwardSlashes(filename);
            current_material->diffuse_texpath = objFilePath(basepath,filename);
            if (basepath) free(filename); // only free if texpath was separately allocated
        }
        else if (!strcmp(keyword,"map_Ks"))
        {
            free(keyword);

            if (!current_material)
            {
                printf("'map_Ks' without prior 'newmtl'\n");
                return 1;
            }
            if (current_material->specular_texpath)
            {
                printf("multiple map_Ks lines for the same material\n");
                return 1;
            }

            char* filename;
            objLexString(file,&filename);
            objForwardSlashes(filename);
            current_material->specular_texpath = objFilePath(basepath,filename);
            if (basepath) free(filename); // only free if texpath was separately allocated
        }
        else if (!strcmp(keyword,"map_d"))
        {
            free(keyword);

            if (!current_material)
            {
                printf("'map_d' without prior 'newmtl'\n");
                return 1;
            }
            if (current_material->opacity_texpath)
            {
                printf("multiple map_d lines for the same material\n");
                return 1;
            }

            char* filename;
            objLexString(file,&filename);
            objForwardSlashes(filename);
            current_material->opacity_texpath = objFilePath(basepath,filename);
            if (basepath) free(filename); // only free if texpath was separately allocated
        }
        else if (!strcmp(keyword,"bump") || !strcmp(keyword,"map_bump"))
        {
            free(keyword);

            if (!current_material)
            {
                printf("'bump'/'map_bump' without prior 'newmtl'\n");
                return 1;
            }
            if (current_material->bump_texpath)
            {
                printf("multiple 'bump'/'map_bump' lines for the same material\n");
                return 1;
            }

            char* filename;
            objLexString(file,&filename);
            objForwardSlashes(filename);
            current_material->bump_texpath = objFilePath(basepath,filename);
            if (basepath) free(filename); // only free if texpath was separately allocated
        }
        else if (!strcmp(keyword,"mag_filter_nearest")) /* custom */
        {
            free(keyword);

            current_material->mag_filter_nearest = 1;
        }
        else if (!strcmp(keyword,"ignore_environment_ambient")) /* custom */
        {
            free(keyword);

            current_material->ignore_environment_ambient = 1;
        }
        else
        {
            free(keyword);

            // we hit a keyword we don't care about, so read past the rest of the stuff on this line
            objDiscardRemainingLine(file);
        }
    }

    return 0;
}
#define objNewGroup() \
    if (model->group_count >= group_capacity) \
        model->group_buf = realloc(model->group_buf, sizeof(objGroup) * (group_capacity <<= 1)); \
    current_group = model->group_buf + model->group_count++; \
    current_group_face_capacity = 16; \
    current_group->face_buf = malloc(sizeof(objFace) * current_group_face_capacity); \
    current_group->face_count = 0; \
    current_group->material_name = NULL;
int objParseFile (FILE* file, objModel*  model, char* basepath)
{
    int vertex_pos_capacity = 16;
    model->vertex_pos_buf = malloc(sizeof(objVertexPos) * vertex_pos_capacity);
    model->vertex_pos_count = 0;

    int vertex_tex_capacity = 16;
    model->vertex_tex_buf = malloc(sizeof(objVertexTex) * vertex_tex_capacity);
    model->vertex_tex_count = 0;

    int vertex_normal_capacity = 16;
    model->vertex_normal_buf = malloc(sizeof(objVertexNormal) * vertex_normal_capacity);
    model->vertex_normal_count = 0;

    int material_capacity = 1;
    model->material_buf = malloc(sizeof(objMaterial) * material_capacity);
    model->material_count = 0;

    int group_capacity = 1;
    model->group_buf = malloc(sizeof(objGroup) * group_capacity);
    model->group_count = 0;

    objGroup* current_group;
    int current_group_face_capacity;
    objNewGroup();
    // Initialize a group before parsing, in case there are some faces declared
    // before the first 'usemtl' lines (or if there are none).

    while (1)
    {
        if (!objLexExpectChar(file, EOF)) break; // Stop if we successfully 'expect' an End Of File
        if (!objLexExpectChar(file, '\n')) continue; // Go to the next line if this was a blank/comment-only line.

        // There's something here. Read the keyword for this line.
        char* keyword;
        objLexString(file,&keyword); // not checking status because this can't fail (gives empty string)

        if (!strcmp(keyword,"f")) // objFace
        {
            free(keyword);

            if (!current_group)
            {
                printf("'f' without prior 'o'\n");
                return 1; // Can't have a Face that isn't part of an Object.
            }

            if (current_group->face_count >= current_group_face_capacity)
                current_group->face_buf = realloc(current_group->face_buf, sizeof(objFace) * (current_group_face_capacity <<= 1));
            objFace* face = current_group->face_buf + current_group->face_count++;
            int vertex_ref_capacity = 4;
            face->vertex_ref_buf = malloc(sizeof(objVertexRef) * vertex_ref_capacity);
            face->vertex_ref_count = 0;
            while (objLexExpectChar(file, EOF) && objLexExpectChar(file, '\n')) // while we encounter neither of these
            {
                if (face->vertex_ref_count >= vertex_ref_capacity)
                    face->vertex_ref_buf = realloc(face->vertex_ref_buf, sizeof(objVertexRef) * (vertex_ref_capacity <<= 1));
                objVertexRef* ref = face->vertex_ref_buf + face->vertex_ref_count++;
                if (objLexInt(file,&ref->pos_id))
                {
                    printf("vertex position index missing\n");
                    return 1; // You MUST have a Vertex Position!
                }
                if (objLexExpectChar(file,'/'))
                {
                    ref->tex_id = 0;
                    ref->normal_id = 0;
                    continue;
                }
                if (objLexInt(file,&ref->tex_id)) ref->tex_id = 0;
                if (objLexExpectChar(file,'/'))
                {
                    ref->normal_id = 0;
                    continue;
                }
                if (objLexInt(file,&ref->normal_id)) ref->normal_id = 0;
            }
            if (face->vertex_ref_count < 3)
            {
                printf("face with fewer than 3 vertices\n");
                return 1; // Face must be at least enough vertices to form a triangle.
            }
        }
        else if (!strcmp(keyword,"v")) // objVertexPos
        {
            free(keyword);

            if (model->vertex_pos_count >= vertex_pos_capacity)
                model->vertex_pos_buf = realloc(model->vertex_pos_buf, sizeof(objVertexPos) * (vertex_pos_capacity <<= 1));
            objVertexPos* pos = model->vertex_pos_buf + model->vertex_pos_count++;
            if (objLexFloat(file,&pos->x))
            {
                printf("vertex position missing x component\n");
                return 1;
            }
            if (objLexFloat(file,&pos->y))
            {
                printf("vertex position missing y component\n");
                return 1;
            }
            if (objLexFloat(file,&pos->z))
            {
                printf("vertex position missing z component\n");
                return 1;
            }
            if (objLexFloat(file,&pos->w)) pos->w = 1;
        }
        else if (!strcmp(keyword,"vt")) // objVertexTex
        {
            free(keyword);

            if (model->vertex_tex_count >= vertex_tex_capacity)
                model->vertex_tex_buf = realloc(model->vertex_tex_buf, sizeof(objVertexTex) * (vertex_tex_capacity <<= 1));
            objVertexTex* tex = model->vertex_tex_buf + model->vertex_tex_count++;
            if (objLexFloat(file,&tex->u))
            {
                printf("vertex texture position missing u component\n");
                return 1;
            }
            if (objLexFloat(file,&tex->v)) tex->v = 0;
            else if (objLexFloat(file,&tex->w)) tex->w = 0;
        }
        else if (!strcmp(keyword,"vn")) // objVertexNormal
        {
            free(keyword);

            if (model->vertex_normal_count >= vertex_normal_capacity)
                model->vertex_normal_buf = realloc(model->vertex_normal_buf, sizeof(objVertexNormal) * (vertex_normal_capacity <<= 1));
            objVertexNormal* normal = model->vertex_normal_buf + model->vertex_normal_count++;
            if (objLexFloat(file,&normal->x))
            {
                printf("vertex normal missing x component\n");
                return 1;
            }
            if (objLexFloat(file,&normal->y))
            {
                printf("vertex normal missing y component\n");
                return 1;
            }
            if (objLexFloat(file,&normal->z))
            {
                printf("vertex normal missing z component\n");
                return 1;
            }
        }
        else if (!strcmp(keyword,"mtllib")) // load materials library file
        {
            free(keyword);

            char* lib_name;
            objLexString(file,&lib_name);
            objForwardSlashes(lib_name);
            FILE* lib_file = objOpen(basepath,lib_name);
            free(lib_name);
            if (lib_file)
            {
                int parse_status = objReadMaterials(lib_file,model,material_capacity,basepath);
                fclose(lib_file);
                if (parse_status)
                {
                    printf("failed to parse materials file\n");
                    //return 1;
                }
            }
            else
            {
                printf("failed to open materials file\n");
                //return 1;
            }
        }
        else if (!strcmp(keyword,"usemtl")) // use named material in this object
        {
            free(keyword);

            if (current_group->face_count)
            {
                // advance to a new object & initialize it
                objNewGroup();
            }
            else
            {
                // The face_count of the previous group was checked, since if it
                // had no faces then we can just reuse it for this group instead
                // of keeping an empty group, which is pointless (literally).
                // We do need to check for a material string though, to avoid
                // a memory leak when there are two 'usemtl' lines with no faces
                // specified between them.
                if (current_group->material_name) free(current_group->material_name);
                // Don't have to set it to NULL, since we're about to set it
                // anyway when we read the new material name in from the file.
            }

            objLexString(file,&current_group->material_name);
        }
        else
        {
            free(keyword);

            // we hit a keyword we don't care about, so read past the rest of the stuff on this line
            objDiscardRemainingLine(file);
        }
    }

    // if the current_group doesn't have any faces, we might as well get rid of it
    // since there's no reason to keep it around during rendering
    if (!current_group->face_count)
    {
        free(current_group->face_buf);
        // possible the name is NULL (first group)
        if (current_group->material_name) free(current_group->material_name);
        model->group_count--;
    }

    if (!model->group_count)
    {
        // If there aren't any groups of faces, this (1) would be useless to draw
        // and (2) is really suspicious of something being wrong, so print a warning
        // even though this is technically a valid file.
        printf("Model contains no groups of vertices.\n");
    }

    return 0;
}

int objResolveMaterials (objModel* model)
{
    //char resolve_failure = 0;
    for (int i = 0; i < model->group_count; i++)
    {
        objGroup* group = model->group_buf + i;
        char* searching_for = group->material_name;
        if (!searching_for)
        {
            // Special case if material string is NULL, we shouldn't try to pass
            // it to strcmp and especially shouldn't try to free it.
            group->material = &objFallbackMaterial;
            //resolve_failure = 1;
            continue;
        }
        for (int j = 0; j < model->material_count; j++)
        {
            objMaterial* material = model->material_buf + j;
            if (!strcmp(searching_for, material->name))
            {
                group->material = material;
                goto FOUND;
            }
        }
        printf("Failed to resolve material '%s'\n",searching_for);
        // Material string provided but didn't match anything, use the fallback
        // material but still proceed to free this string.
        group->material = &objFallbackMaterial;
        //resolve_failure = 1;
        FOUND:;
        free(searching_for); // Free this string regardless of finding a match.
    }

    // Now free the name strings in the materials we searched against.
    for (int j = 0; j < model->material_count; j++)
        free(model->material_buf[j].name);

    return 0;//resolve_failure;
}
int objVerifyIndices (objModel* model)
{
    // We do this separately from loading to support cases where vertices are
    // specified after they were already referenced (would fail with one-pass
    // loading since the count would not have been increased yet).
    for (int obj_id = 0; obj_id < model->group_count; obj_id++)
    {
        objGroup* object = model->group_buf + obj_id;
        for (int face_id = 0; face_id < object->face_count; face_id++)
        {
            objFace* face = object->face_buf + face_id;
            for (int i = 0; i < face->vertex_ref_count; i++)
            {
                objVertexRef* ref = face->vertex_ref_buf + i;
                // Using '>' rather than '>=' because IDs start at 1.
                // We set IDs to 0 when there was no ID specified.
                // We don't need to check for negatives since objLexInt only
                // produces values >= 0.
                if (ref->pos_id > model->vertex_pos_count || !ref->pos_id) // parsed as unsigned, but 0 is *not* OK
                {
                    printf("vertex position index is out of bounds\n");
                    return 1;
                }
                if (ref->tex_id > model->vertex_tex_count) // parsed as unsigned, and 0 is OK
                {
                    printf("vertex texture index is out of bounds\n");
                    return 1;
                }
                if (ref->normal_id > model->vertex_normal_count) // parsed as unsigned, and 0 is OK
                {
                    printf("vertex normal index is out of bounds\n");
                    return 1;
                }
            }
        }
    }
    return 0;
}
void objFree (objModel* model)
{
    for (int i = 0; i < model->material_count; i++)
    {
        objMaterial* material = model->material_buf + i;
        if (material->ambient_texpath) free(material->ambient_texpath);
        if (material->diffuse_texpath) free(material->diffuse_texpath);
        if (material->specular_texpath) free(material->specular_texpath);
        if (material->bump_texpath) free(material->bump_texpath);
    }
    free(model->vertex_pos_buf);
    free(model->vertex_tex_buf);
    free(model->vertex_normal_buf);
    free(model->material_buf);
    free(model->group_buf);
    free(model);
}
objModel* objLoad (char* basepath, char* filename)
{
    FILE* file = objOpen(basepath, filename);
    if (!file)
    {
        printf(
            "failed to open '%s%s'\n",
            basepath ? basepath : "",
            filename
        );
        return NULL;
    }

    objModel* model = malloc(sizeof(objModel));
    int parse_result = objParseFile(file,model,basepath);
    fclose(file);
    if (parse_result)
    {
        // Parsing failed, free everything including temporary strings and quit.
        printf("failed to parse model file\n");
        for (int i = 0; i < model->material_count; i++)
            free(model->material_buf[i].name);
        for (int i = 0; i < model->group_count; i++)
            if (model->group_buf[i].material_name)
                free(model->group_buf[i].material_name);
        objFree(model);
        return NULL;
    }

    if (objResolveMaterials(model))
    {
        // Failed to resolve some materials, free normally and quit.
        printf("failed to resolve materials\n");
        objFree(model);
        return NULL;
    }

    if (objVerifyIndices(model))
    {
        // Index values were out of range, file is invalid.
        printf("failed to verify indices\n");
        objFree(model);
        return NULL;
    }

    printf("successfully loaded model file\n");
    return model;
}
