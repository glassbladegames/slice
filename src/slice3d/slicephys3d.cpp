#include <slice3d.h>
struct
{
	btBroadphaseInterface* broadphase;
	btDefaultCollisionConfiguration* collision_config;
	btCollisionDispatcher* collision_dispatcher;
	btSequentialImpulseConstraintSolver* solver;
	btDiscreteDynamicsWorld* world;
}
phys3dWorld;
btDiscreteDynamicsWorld* phys3dGetWorld ()
{
	return phys3dWorld.world;
}
btVector3 phys3dObject::GetBodyCenter ()
{
	btTransform trans = body->getWorldTransform();
	return trans.getOrigin();
}
slList phys3dShapes("3D Physics Shapes",offsetof(phys3dShape,_index_),false);
slBlockAllocator phys3dShapeAllocator ("3D physics shapes allocator",sizeof(phys3dShape));
phys3dShape* phys3dMakeShape (btCollisionShape* proto_shape)
{
	//phys3dShape* out = malloc(sizeof(phys3dShape));
	phys3dShape* out = phys3dShapeAllocator.Allocate();

	proto_shape->calculateLocalInertia(1,out->local_inertia);

	out->shape = proto_shape;
	SDL_AtomicSet(&out->usages,0);
	phys3dShapes.Add(out);
	return out;
}
void phys3dFreeShape (phys3dShape* shape)
{
	phys3dShapes.Remove(shape);
	delete shape->shape;
	//free(shape);
	phys3dShapeAllocator.Release(shape);
}
void phys3dShapeGC (phys3dShape* shape, void* unused_userdata, slWorker* worker)
{
	if (!SDL_AtomicGet(&shape->usages)) slSchedule(phys3dFreeShape,shape,worker);
}
void phys3dShapesGC ()
{
	slDoWork(&phys3dShapes,phys3dShapeGC,NULL);
}
slList phys3dObjects("3D Physics Objects",offsetof(phys3dObject,_index_),false);
slBlockAllocator phys3dObjectAllocator ("3D physics objects allocator",sizeof(phys3dObject));
phys3dObject* phys3dCreateObject (s3dObject* visual, phys3dShape* shape, s3dVec3 starting_pos, slScalar mass, int* group, int* mask)
{
	shape->Reserve();

	//phys3dObject* obj = malloc(sizeof(phys3dObject));
	phys3dObject* obj = phys3dObjectAllocator.Allocate();
	phys3dObjects.Add(obj);

	obj->body_motion_state = new btDefaultMotionState(btTransform(btQuaternion(btVector3(0,1,0),0),btVector3(starting_pos.xyz)));
	//obj->body_motion_state = new btDefaultMotionState(btTransform(btQuaternion(btVector3(0,1,0),0),btVector3(visual->pos.xyz)));
	obj->shape = shape;
	btRigidBody::btRigidBodyConstructionInfo construction_info(mass,obj->body_motion_state,shape->shape,shape->local_inertia);

	obj->body = new btRigidBody(construction_info);

	obj->body->setAngularFactor(1);

    if (group && mask) {
        phys3dWorld.world->addRigidBody(obj->body,*group,*mask);
    } else {
        phys3dWorld.world->addRigidBody(obj->body);
    }

	obj->base_transform = GLmat4_identity();

	obj->visual = visual;
	return obj;
}
void phys3dDestroyObject (phys3dObject* obj)
{
	obj->shape->Abandon();

	if (obj->visual) s3dDestroyObject(obj->visual);
	phys3dObjects.Remove(obj);
	phys3dWorld.world->removeRigidBody(obj->body);
	delete obj->body;
	//free(obj);
	phys3dObjectAllocator.Release(obj);
}
void phys3dUpdateVisual (phys3dObject* obj)
{
	if (obj->visual)
	{
		btTransform transform;
		obj->body_motion_state->getWorldTransform(transform);
        GLmat4 phys_mat;
		transform.getOpenGLMatrix(phys_mat.data);
		phys_mat.transpose_in_place();
		obj->visual->SetWorldTransform(phys_mat * obj->base_transform);
	}
}
slScalar phys3dRateMul = 1;
void phys3dSetRateMul (slScalar mul)
{
	phys3dRateMul = mul;
}
slScalar phys3dInterval = 1. / 100.;
Uint16 phys3dMaxSteps = 100. / 30. + 1.;
void phys3dSetInterval (slScalar stepping_fps, slScalar catch_up_fps)
{
	phys3dInterval = 1 / stepping_fps;
	phys3dMaxSteps = stepping_fps / catch_up_fps + 1;
}
SDL_sem* phys3dDoStep;
SDL_sem* phys3dDidStep;
bool phys3dStepperDie;
void phys3dStepperProc ()
{
	while (true)
	{
		SDL_SemWait(phys3dDoStep);
		if (phys3dStepperDie) break;
		phys3dWorld.world->stepSimulation(slGetDelta() * phys3dRateMul,phys3dMaxSteps,phys3dInterval);
		SDL_SemPost(phys3dDidStep);
	}
	SDL_SemPost(phys3dDidStep);
}
void phys3dStep ()
{
	slDoWork(&phys3dObjects,phys3dUpdateVisual,NULL);
	SDL_SemPost(phys3dDoStep);
	//phys3dWorld.world->stepSimulation(slGetDelta() * phys3dRateMul,phys3dMaxSteps,phys3dInterval);
}
void phys3dWaitStepDone ()
{
	SDL_SemWait(phys3dDidStep);
}
void phys3dInitStepper ()
{
	phys3dStepperDie = false;
	phys3dDoStep = SDL_CreateSemaphore(0);
	phys3dDidStep = SDL_CreateSemaphore(0);
	SDL_DetachThread(SDL_CreateThread(phys3dStepperProc,"SlicePhys3D Independent Stepper",NULL));
	slHook_FrameConclude(phys3dWaitStepDone);
}
void phys3dKillStepper ()
{
	phys3dStepperDie = true;
	SDL_SemPost(phys3dDoStep);
	SDL_SemWait(phys3dDidStep);
	SDL_DestroySemaphore(phys3dDoStep);
	SDL_DestroySemaphore(phys3dDidStep);
	slUnhook_FrameConclude(phys3dWaitStepDone);
}
slGC_Action phys3dShapesGC_Action;
namespace s3d_internal
{
	void phys3dInit ()
	{
		phys3dWorld.broadphase = new btDbvtBroadphase();
		phys3dWorld.collision_config = new btDefaultCollisionConfiguration();
		phys3dWorld.collision_dispatcher = new btCollisionDispatcher(phys3dWorld.collision_config);
		phys3dWorld.solver = new btSequentialImpulseConstraintSolver;
		phys3dWorld.world = new btDiscreteDynamicsWorld(phys3dWorld.collision_dispatcher,phys3dWorld.broadphase,phys3dWorld.solver,phys3dWorld.collision_config);
		phys3dWorld.world->setGravity(btVector3(0,-9.8,0));

		slHook_PreRender(phys3dStep);
		phys3dShapesGC_Action.Hook(phys3dShapesGC);

		phys3dShapes.HookAutoShrink();
		phys3dObjects.HookAutoShrink();

		phys3dInitStepper();
	}
	void phys3dQuit ()
	{
		phys3dKillStepper();

		phys3dObjects.UntilEmpty(phys3dDestroyObject);
		phys3dShapes.UntilEmpty(phys3dFreeShape);
		phys3dObjectAllocator.FreeAllBlocks();
		phys3dShapeAllocator.FreeAllBlocks();

		slUnhook_PreRender(phys3dStep);
		phys3dShapesGC_Action.Unhook();

		phys3dShapes.UnhookAutoShrink();
		phys3dObjects.UnhookAutoShrink();

		delete phys3dWorld.world;
		delete phys3dWorld.solver;
		delete phys3dWorld.collision_dispatcher;
		delete phys3dWorld.collision_config;
		delete phys3dWorld.broadphase;
	}
}
