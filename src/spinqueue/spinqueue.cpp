#include "spinqueue.h"
#include <stdlib.h>
#include <string.h>
SpinQueue QueueInit (size_t itemsize)
{
	//SpinQueue* out = malloc(sizeof(SpinQueue));
	SpinQueue out;
	out.itemsize = itemsize;
	out.capacity = 0;
	out.buffer = NULL;
	out.length = 0;
	out.readpos = 0;
	out.writepos = 0;
	return out;
}
void* QueueGet (SpinQueue* queue)
{
	if (!queue->length) return NULL;
	void* out = queue->buffer + queue->itemsize * queue->readpos;
	if (++queue->readpos >= queue->capacity) queue->readpos = 0;
	queue->length--;
	return out;
}
void QueueEnlarge (SpinQueue* queue)
{
	//printf("Enlarging\n");
	size_t biglen = queue->capacity << 1;
	if (!biglen) biglen = 16;
	void* bigbuf = malloc(queue->itemsize * biglen);
	//printf("size increased to %llu MB (%llu X %llu)\n",(biglen * queue->itemsize)/1000000,biglen,queue->itemsize);
	size_t ending = queue->capacity - queue->readpos;
	if (queue->length <= ending) memcpy(bigbuf,queue->buffer + queue->readpos * queue->itemsize,queue->length * queue->itemsize);
	else
	{
		//printf("doing two memcpies\n");
		size_t startsize = ending * queue->itemsize;
		memcpy(bigbuf,queue->buffer + queue->readpos * queue->itemsize,startsize);
		memcpy(bigbuf + startsize,queue->buffer,(queue->length - ending) * queue->itemsize);
	}
	free(queue->buffer);
	queue->buffer = bigbuf;
	queue->capacity = biglen;
	queue->readpos = 0;
	queue->writepos = queue->length;
}
void* QueueAppend (SpinQueue* queue, void* copyin)
{
	if (queue->length >= queue->capacity) QueueEnlarge(queue);
	memcpy(queue->buffer + queue->writepos * queue->itemsize,copyin,queue->itemsize);
	if (++queue->writepos >= queue->capacity) queue->writepos = 0;
	queue->length++;
}
void QueueClear (SpinQueue* queue, bool free_buffer)
{
	if (free_buffer)
	{
		free(queue->buffer);
		queue->capacity = 0;
	}
	queue->writepos = 0;
	queue->length = 0;
	queue->readpos = 0;
	//free(queue);
}
