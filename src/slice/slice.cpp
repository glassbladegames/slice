#include <SDL2/SDL.h>
void slDeadBeef ()
{
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,"FATAL ERROR","Memory allocation failed! The application must now crash.",NULL);
	SDL_assert(false);
	exit(0xDEADBEEF);
}
size_t slMemLimit = 0;
void slSetMemLimit (size_t limit) { slMemLimit = limit; }
void* sl_malloc (size_t n)
{
	if (slMemLimit) SDL_assert(n < slMemLimit);
	void* out = malloc(n);
    if (!out) slDeadBeef();
    return out;
}
void* sl_realloc (void* prev, size_t n)
{
	if (slMemLimit) SDL_assert(n < slMemLimit);
	void* out = realloc(prev,n);
    if (n && !out) slDeadBeef();
	return out;
}

#include <slice.h>
using namespace sl_internal;

SDL_Window* slWindow;
SDL_GLContext slUploadContext;
SDL_GLContext slContext;
void slUseContext ()
{
	SDL_GL_MakeCurrent(slWindow,slContext);
}
slScalar slWindowAspect,slInvWindowAspect;
GLfloat slWindowAspect_Narrow,slInvWindowAspect_Narrow;
slScalar slDelta = 0;
void slSort (slList* list, bool (*inorder) (void* first, void* second))
{
	void** itemptr = list->items;
	for (slBU pos = 1; pos < list->itemcount; pos++)
	{
		void** first = itemptr;
		void** second = ++itemptr;
		for (slBU backward = 0; backward < pos; backward++, first--, second--)
		{
			if (inorder(*first,*second)) break;
			if (list->indexoffset >= 0)
			{
				(*(slBU*)(*first + list->indexoffset))++;
				(*(slBU*)(*second + list->indexoffset))--;
			}
			void* temp = *first;
			*first = *second;
			*second = temp;
		}
	}
}
bool slWindowFocused = true;

void slFatal (char* msg, int returncode)
{
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,"Fatal Engine Error",msg,NULL);
	exit(returncode);
}
Uint64 slLastStep;
bool slFullscreen = false;
bool slVSync = true;
void slSetIcon (char* path)
{
	SDL_Surface* icon = IMG_Load(path);
	if (icon)
	{
		SDL_SetWindowIcon(slWindow,icon);
		SDL_FreeSurface(icon);
	}
}
void slToggleFullscreen ()
{
	slFullscreen = !slFullscreen;
	SDL_SetWindowFullscreen(slWindow,slFullscreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0);
}
void slToggleVSync ()
{
	slVSync = !slVSync;
	SDL_GL_SetSwapInterval(slVSync ? 1 : 0);
}
bool slExitReq = false;
slVec2 slMousePos = slVec2(0.5);
void (*slAppDrawStage) () = NULL;
void slDoNothing () {}
void (*slOnNextInput) (slKeyInfo input) = NULL;
Uint8 slPreviousFrameDelay = 0;
slList slPreRenderHooks("Slice: Pre-Render Hooks",slNoIndex,false);
inline void slRunPreRenders (slBox* hoverbox)
{
	for (slBU i = 0; i < slPreRenderHooks.itemcount; i++)
	{
		void (*func) (slBox*) = *(slPreRenderHooks.items + i);
		func(hoverbox);
	}
}
void slHook_PreRender (void (*func) (slBox* hoverbox))
{
	slPreRenderHooks.Add(func);
}
void slUnhook_PreRender (void (*func) (slBox* hoverbox))
{
	slPreRenderHooks.Remove(func);
}
slList slFrameConcludeHooks("Slice: Frame Conclude Hooks",slNoIndex,false);
void slHook_FrameConclude (void (*func) ())
{
	slFrameConcludeHooks.Add(func);
}
void slUnhook_FrameConclude (void (*func) ())
{
	slFrameConcludeHooks.Remove(func);
}
void (*slOnFileDrop) (char* path) = NULL;
void (*slOnMiscEvent) (SDL_Event event) = NULL;
void (*slOnAnyEvent) (SDL_Event event) = NULL;

struct slDeferCall
{
	void (*func) (void* arg);
	void* arg;
	slDeferCall* next;
};
SDL_mutex* slDefersLock;
slBlockAllocator slDefersAlloc = slBlockAllocator("Slice Deferred Calls Allocator",sizeof(slDeferCall));
slDeferCall* slDefersFirst = NULL;
slDeferCall** slDefersNextPointer = NULL;
void slDefer (void (*func) (void* arg), void* arg)
{
	SDL_LockMutex(slDefersLock);

	slDeferCall* defer = slDefersAlloc.Allocate();
	defer->func = func;
	defer->arg = arg;
	defer->next = NULL;
	*slDefersNextPointer = defer;
	slDefersNextPointer = &defer->next;

	SDL_UnlockMutex(slDefersLock);
}
void slFlushDefers ()
{
	SDL_LockMutex(slDefersLock);
	slDeferCall* defer = slDefersFirst;
	slDefersFirst = NULL;
	slDefersNextPointer = &slDefersFirst;
	SDL_UnlockMutex(slDefersLock);

	while (defer)
	{
		defer->func(defer->arg);

		slDeferCall* next = defer->next;
		SDL_LockMutex(slDefersLock);
		slDefersAlloc.Release(defer);
		SDL_UnlockMutex(slDefersLock);
		defer = next;
	}
}
void (*slTypingInputCallback) (void* userdata, int event_type, char ch) = NULL;
void* slTypingInputCallbackUserdata;
void slSetTypingInputCallback (void (*callback) (void* userdata, int event_type, char ch), void* userdata)
{
	if (slTypingInputCallback) slTypingInputCallback(slTypingInputCallbackUserdata,slTypingEvent_Deselect,0);

	slTypingInputCallback = callback;
	slTypingInputCallbackUserdata = userdata;
	if (callback) SDL_StartTextInput();
	else SDL_StopTextInput();
}
Uint8 slTypingButtonsState = 0;
#define slTypingButton_LSHIFT 1
#define slTypingButton_RSHIFT 2
#define slTypingButton_LCTRL 4
#define slTypingButton_RCTRL 8
#define slTypingButton_LMOUSE 16
#define slTypingButtonPress(button) (slTypingButtonsState |= (button))
#define slTypingButtonRelease(button) (slTypingButtonsState &= ~(button))
#define slShiftState (slTypingButtonsState & (slTypingButton_LSHIFT | slTypingButton_RSHIFT))
#define slCtrlState (slTypingButtonsState & (slTypingButton_LCTRL | slTypingButton_RCTRL))
#define slLeftMouseState (slTypingButtonsState & slTypingButton_LMOUSE)
void slCycle ()
{
    slNextFrameTasks->Flush();

	slGC_Step();

	slWindowFocused = SDL_GetWindowFlags(slWindow) & SDL_WINDOW_INPUT_FOCUS;
	if (!slWindowFocused && slDelta < 0.05f)
	{
		slPreviousFrameDelay = 50 - ((slDelta * 1000) - slPreviousFrameDelay);
		SDL_Delay(slPreviousFrameDelay);
	}
	else slPreviousFrameDelay = 0;
	int winw,winh;
	SDL_GetWindowSize(slWindow,&winw,&winh);
	slWindowAspect_Narrow = slWindowAspect = winw / (slScalar)winh;
	slInvWindowAspect_Narrow = slInvWindowAspect = 1 / slWindowAspect;
	SDL_Event event;
	slBox* box;
	while (SDL_PollEvent(&event))
	{
		if (slOnAnyEvent) slOnAnyEvent(event);

		switch (event.type)
		{
			case SDL_WINDOWEVENT:
			if (event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
			{
				glViewport(0,0,event.window.data1,event.window.data2);
				break;
			}
			if (event.window.event != SDL_WINDOWEVENT_CLOSE) break;
			// Do same thing for SDL_WINDOWEVENT_CLOSE as for SDL_QUIT.
			case SDL_QUIT:
			slExitReq = true;
			break;



			case SDL_KEYDOWN:
			switch (event.key.keysym.sym)
			{
				case SDLK_LSHIFT: slTypingButtonPress(slTypingButton_LSHIFT); break;
				case SDLK_RSHIFT: slTypingButtonPress(slTypingButton_RSHIFT); break;
				case SDLK_LCTRL: slTypingButtonPress(slTypingButton_LCTRL); break;
				case SDLK_RCTRL: slTypingButtonPress(slTypingButton_RCTRL); break;
			}
			if (slTypingInputCallback)
			{
				int event_type;
				switch (event.key.keysym.sym)
				{
					case SDLK_RETURN: event_type = slTypingEvent_Enter; break;
					case SDLK_BACKSPACE: event_type = slTypingEvent_Backspace; break;
					case SDLK_DELETE: event_type = slTypingEvent_Delete; break;
					case SDLK_END: event_type = slTypingEvent_End; break;
					case SDLK_HOME: event_type = slTypingEvent_Home; break;
					case SDLK_LEFT: event_type = slTypingEvent_LeftArrow; break;
					case SDLK_RIGHT: event_type = slTypingEvent_RightArrow; break;
					case SDLK_UP: event_type = slTypingEvent_UpArrow; break;
					case SDLK_DOWN: event_type = slTypingEvent_DownArrow; break;
					case SDLK_PAGEUP: event_type = slTypingEvent_PageUp; break;
					case SDLK_PAGEDOWN: event_type = slTypingEvent_PageDown; break;
					case SDLK_TAB: event_type = slTypingEvent_Tab; break;
					case SDLK_c:
						if (!slCtrlState) goto NOT_RELEVANT;
						event_type = slTypingEvent_Copy; break;
					case SDLK_v:
						if (!slCtrlState) goto NOT_RELEVANT;
						event_type = slTypingEvent_Paste; break;
					case SDLK_x:
						if (!slCtrlState) goto NOT_RELEVANT;
						event_type = slTypingEvent_Cut; break;
					default: goto NOT_RELEVANT;
				}
				slTypingInputCallback(slTypingInputCallbackUserdata,event_type,slShiftState);
				NOT_RELEVANT:;
			}
			else
			{
				if (!event.key.repeat)
				{
					if (slOnNextInput)
					{
						slOnNextInput(slKeyCode(event.key.keysym.sym));
						slOnNextInput = NULL;
					}
					else slCheckBoundKeysDown(slKeyCode(event.key.keysym.sym));
				}
			}
			break;



			case SDL_KEYUP:
			switch (event.key.keysym.sym)
			{
				case SDLK_LSHIFT: slTypingButtonRelease(slTypingButton_LSHIFT); break;
				case SDLK_RSHIFT: slTypingButtonRelease(slTypingButton_RSHIFT); break;
				case SDLK_LCTRL: slTypingButtonRelease(slTypingButton_LCTRL); break;
				case SDLK_RCTRL: slTypingButtonRelease(slTypingButton_RCTRL); break;
			}
			slCheckBoundKeysUp(slKeyCode(event.key.keysym.sym));
			break;



			case SDL_MOUSEBUTTONDOWN:
			if (event.button.button == SDL_BUTTON_LEFT) slTypingButtonPress(slTypingButton_LMOUSE);
			if (slOnNextInput)
			{
				slOnNextInput(slMouseButton(event.button.button));
				slOnNextInput = NULL;
			}
			else
			{
				slMousePos = slVec2(event.button.x,event.button.y) / slVec2(winw,winh);
				bool leftclick = event.button.button == SDL_BUTTON_LEFT;
				bool rightclick = event.button.button == SDL_BUTTON_RIGHT;
				if (leftclick || rightclick)
				{
					//printf(leftclick ? "left click\n" : "right click\n");
					if (leftclick && slTypingInputCallback) slSetTypingInputCallback(NULL);
					if (slOnClickUI(slMousePos,leftclick)) goto FOUND;
				}
				slCheckBoundKeysDown(slMouseButton(event.button.button));
				FOUND:
				break;
			}
			break;



			case SDL_MOUSEBUTTONUP:
			slMousePos = slVec2(event.button.x,event.button.y) / slVec2(winw,winh);
			if (event.button.button == SDL_BUTTON_LEFT)
			{
				slTypingButtonRelease(slTypingButton_LMOUSE);
				if (slTypingInputCallback) slTypingInputCallback(slTypingInputCallbackUserdata,slTypingEvent_MouseDrag,0); // last motion in mouse drag
			}
			slSlidersMouseUp(slMousePos);
			slCheckBoundKeysUp(slMouseButton(event.button.button));
			break;



			case SDL_MOUSEWHEEL:
			if (slOnNextInput)
			{
				slOnNextInput(slWheelAction(event.wheel.y > 0 ? slWheelForward : slWheelBackward));
				slOnNextInput = NULL;
			}
			else
			{
				scrScroll* hovered = scrGetHoveredScroll(slMousePos);
				while (event.wheel.y > 0)
				{
					if (hovered) hovered->ScrollUp();
					else slCheckBoundKeysDown(slWheelAction(slWheelForward));
					event.wheel.y--;
				}
				while (event.wheel.y < 0)
				{
					if (hovered) hovered->ScrollDown();
					else slCheckBoundKeysDown(slWheelAction(slWheelBackward));
					event.wheel.y++;
				}
			}
			break;



			case SDL_TEXTINPUT:
			if (slTypingInputCallback)
			{
				/// To do: make this properly handle UTF8!
				char* ch = event.text.text;
				while (*ch) slTypingInputCallback(slTypingInputCallbackUserdata,slTypingEvent_Character,*ch++);
			}
			break;



			case SDL_DROPFILE:
			if (slOnFileDrop) slOnFileDrop(event.drop.file);
			SDL_free(event.drop.file);
			break;



			case SDL_APP_LOWMEMORY:
			/// Run all garbage collection actions to try to free up some memory.
			slCollectAllGarbageNow();
			/// Maybe, even, large textures should be downsampled?
			break;



			default:
			if (slOnMiscEvent) slOnMiscEvent(event);
		}
	}
	int mousex,mousey;
	SDL_GetMouseState(&mousex,&mousey);
	slMousePos = slVec2(mousex,mousey) / slVec2(winw,winh);
	if (slTypingInputCallback && slLeftMouseState) slTypingInputCallback(slTypingInputCallbackUserdata,slTypingEvent_MouseDrag,0); // continue mouse drag
	slSlidersStep(slMousePos);

	slFlushDefers();
		/// Flush deferred calls directly after processing input,
		/// since an expected use case of slDefer is in keybind callbacks.

	// Find Hovered Box (if Any)
	slBox* hoverbox = slGetHoveredBox(slMousePos);

	slRunPreRenders(hoverbox);

	slRenderPrepareAll(hoverbox);
	/// End of Last Frame
	glFinish();
	SDL_GL_SwapWindow(slWindow);
	/// Start of Next Frame
	//glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT);
	if (slAppDrawStage) slAppDrawStage();
	else slDrawUI();
	glFlush();

	slAudioStep();

	for (slBU i = 0; i < slFrameConcludeHooks.itemcount; i++)
	{
		void (*func) () = slFrameConcludeHooks.items[i];
		func();
	}

	Uint64 now = SDL_GetPerformanceCounter();
	slDelta = (now - slLastStep) / (slScalar)SDL_GetPerformanceFrequency();
	slLastStep = now;
}
struct slWindowDims
{
	Uint32 res_x,res_y;
	Uint8 maximized;
};
char* slSettingsPath;
slWindowDims slLoadSettings ()
{
	slWindowDims out;
	out.res_x = slDefaultWindowX;
	out.res_y = slDefaultWindowY;
	out.maximized = 0;
	FILE* settings = fopen(slSettingsPath,"rb");
	if (settings)
	{
		Uint8 fs,vs,mwa;
		float vol;
		fread(&fs,sizeof(Uint8),1,settings);
		fread(&vs,sizeof(Uint8),1,settings);
		fread(&mwa,sizeof(Uint8),1,settings);
		fread(&out.maximized,sizeof(Uint8),1,settings);
		fread(&out.res_x,sizeof(Uint32),1,settings);
		fread(&out.res_y,sizeof(Uint32),1,settings);
		fread(&vol,sizeof(float),1,settings);
		slFullscreen = fs;
		slVSync = vs;
		slSetMuteWhileAway(mwa);
		slSetMasterVolume(vol);

		slLoadKeys(settings);
		fclose(settings);
	}
	return out;
}
void slSaveSettings ()
{
	FILE* settings = fopen(slSettingsPath,"wb");
	if (settings)
	{
        Uint8 maximized = (SDL_GetWindowFlags(slWindow) & SDL_WINDOW_MAXIMIZED) ? 1 : 0;
        if (maximized) SDL_RestoreWindow(slWindow);
		int resx = slDefaultWindowX, resy = slDefaultWindowY;
		if (!slFullscreen) SDL_GetWindowSize(slWindow,&resx,&resy);
        Uint8 fs = slFullscreen, vs = slVSync, mwa = slGetMuteWhileAway();
        Uint32 rx = resx, ry = resy;
        float vol = slGetMasterVolume();
        fwrite(&fs,sizeof(Uint8),1,settings);
        fwrite(&vs,sizeof(Uint8),1,settings);
        fwrite(&mwa,sizeof(Uint8),1,settings);
        fwrite(&maximized,sizeof(Uint8),1,settings);
        fwrite(&rx,sizeof(Uint32),1,settings);
        fwrite(&ry,sizeof(Uint32),1,settings);
        fwrite(&vol,sizeof(float),1,settings);

        slSaveKeys(settings);
		fclose(settings);
	}
}



struct slQueuedTask
{
    void (*task_func) (void*);
    void* task_data;
    void* next;
};
struct slQueuedCountedTask : public slQueuedTask
{
    SDL_atomic_t* items_in_queue;
    slQueuedCountedTask (SDL_atomic_t* items_in_queue)
    {
        if (items_in_queue) SDL_AtomicIncRef(items_in_queue);
        this->items_in_queue = items_in_queue;
    }
    ~slQueuedCountedTask ()
    {
        if (items_in_queue) SDL_AtomicDecRef(items_in_queue);
    }
};

slNextFrameTasksProto::slNextFrameTasksProto () : slAsyncNonblockingQueue(offsetof(slQueuedCountedTask,next)) {}
void slNextFrameTasksProto::Flush ()
{
    slQueuedCountedTask* task;
    while (task = Get())
    {
        task->task_func(task->task_data);
        delete task;
    }
}
void slNextFrameTasksProto::Push (void (*task_func) (void*), void* task_data, SDL_atomic_t* items_in_queue)
{
    slQueuedCountedTask* task = new slQueuedCountedTask(items_in_queue);
    task->task_func = task_func;
    task->task_data = task_data;
    slAsyncNonblockingQueue::Push(task);
}
slNextFrameTasksProto* slNextFrameTasks;

slBackgroundUploaderProto::slBackgroundUploaderProto () : slConsumerQueue(offsetof(slQueuedCountedTask,next)) {}
void slBackgroundUploaderProto::PreLoop ()
{
	SDL_GL_MakeCurrent(slWindow,slUploadContext);
}
void slBackgroundUploaderProto::ItemFunc (void* queue_item)
{
    slQueuedCountedTask* task = queue_item;
    task->task_func(task->task_data);
    delete task;
}
void slBackgroundUploaderProto::Push (void (*task_func) (void* task_data), void* task_data, SDL_atomic_t* items_in_queue)
{
    slQueuedCountedTask* task = new slQueuedCountedTask(items_in_queue);
    task->task_func = task_func;
    task->task_data = task_data;
    slConsumerQueue::Push(task);
}
slBackgroundUploaderProto* slBackgroundUploader;
void slWaitBackgroundUploads (SDL_atomic_t* items_in_queue)
{
    while (SDL_AtomicGet(items_in_queue)) SDL_Delay(10);
}



void slInit (char* appname, char* settings_path)
{
	slVec2 rot90 = slRotatePoint(slVec2(0,1),90); // Forces GCC not to inline this (it was causing problems).



	#ifdef _WIN32
	SDL_SetHint(SDL_HINT_WINDOWS_DISABLE_THREAD_NAMING,"1"); // Remedies debugger issue on Windows.
	#endif



	slSetDefaultFontPath("seguisym.ttf");



	slSettingsPath = settings_path;
	slWindowDims window_dims = slLoadSettings();



	srand(time(NULL));



	if (SDL_Init(SDL_INIT_EVERYTHING)) slFatal("Could not initialize SDL.",-1);



	SDL_GL_SetAttribute(SDL_GL_RED_SIZE,8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE,8);
	//SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,8);
	//SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE,32);
	//SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS,1);
	//SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES,16);

	#ifdef __APPLE__
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS,SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION,2);
    #endif

    slWindow = SDL_CreateWindow(
        appname,
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        window_dims.res_x,
        window_dims.res_y,
        SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL | (slFullscreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0) | (window_dims.maximized ? SDL_WINDOW_MAXIMIZED : 0)
    );
    if (!slWindow)
    {
        SDL_Quit();
        slFatal("Could not create window.",-2);
    }

    SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1);
    slUploadContext = SDL_GL_CreateContext(slWindow);
    if (!slUploadContext)
    {
		SDL_DestroyWindow(slWindow);
		SDL_Quit();
		slFatal("Could not create upload context.",-3);
    }
	slContext = SDL_GL_CreateContext(slWindow);
	if (!slContext)
	{
		SDL_GL_DeleteContext(slUploadContext);
		SDL_DestroyWindow(slWindow);
		SDL_Quit();
		slFatal("Could not create rendering context.",-3);
	}

	if (!gladLoadGL())
	{
		SDL_GL_DeleteContext(slUploadContext);
		SDL_GL_DeleteContext(slContext);
		SDL_DestroyWindow(slWindow);
		SDL_Quit();
		slFatal("Could not load OpenGL.",-420);
	}

	if (GLVersion.major < 3)
	{
		SDL_GL_DeleteContext(slUploadContext);
		SDL_GL_DeleteContext(slContext);
		SDL_DestroyWindow(slWindow);
		SDL_Quit();
		slFatal("Insufficient OpenGL support.",-686);
	}

	//glEnable(GL_DEPTH_CLAMP);
	SDL_GL_SetSwapInterval(slVSync ? 1 : 0);



	if (TTF_Init())
	{
		SDL_GL_DeleteContext(slUploadContext);
		SDL_GL_DeleteContext(slContext);
		SDL_DestroyWindow(slWindow);
		SDL_Quit();
		slFatal("Could not initialize SDL_ttf.",-4);
	}



	//slSetIcon("slice_logo-karen.png");

	slInitGC();



	slWorkersInit();
	slTexturesInit();
	slInitUI();
	slAudioInit();

	slTypingBoxesInit();
	slSlidersInit();



	tglInit();
	scrInit();
	animInit();
	slLinesInit();



    (slBackgroundUploader = new slBackgroundUploaderProto)->Start();
    slNextFrameTasks = new slNextFrameTasksProto;



	slLastStep = SDL_GetPerformanceCounter();
}

void slQuit ()
{
    slNextFrameTasks->Flush();

	slLinesQuit();
	animQuit();
	scrQuit();
	tglQuit();
	slSlidersQuit();
	slTypingBoxesQuit();
    slQuitUI();

    slTexturesQuit();
    delete slBackgroundUploader->Stop();
	slAudioQuit();
	slWorkersQuit();

    delete slNextFrameTasks;



	slSaveSettings(); // Must do this before closing window.
	SDL_GL_DeleteContext(slUploadContext);
	SDL_GL_DeleteContext(slContext);
	SDL_DestroyWindow(slWindow);
	// Close stuff after window deletion where possible.
	// That way the app appears to close faster.



	slPreRenderHooks.Clear(NULL);
	slKeysQuit();
	slQuitGC();



	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
	exit(0);
}





bool slGetExitReq ()
{
	return slExitReq;
}
bool slGetFullscreen ()
{
	return slFullscreen;
}
void slSetFullscreen (bool to)
{
	if (slFullscreen != to) slToggleFullscreen();
}
bool slGetVSync ()
{
	return slVSync;
}
void slSetVSync (bool to)
{
	if (slVSync != to) slToggleVSync();
}
void slSignalExit ()
{
	slExitReq = true;
}
void slRejectExit ()
{
	slExitReq = false;
}
void slSetAppDrawStage (void (*func) ())
{
	slAppDrawStage = func;
}
void slCaptureNextInput (void (*onnextinput) (slKeyInfo input))
{
	slOnNextInput = onnextinput;
}
void slGetMouse (slScalar* x, slScalar* y)
{
	if (x) *x = slMousePos.x;
	if (y) *y = slMousePos.y;
}
slVec2 slGetMouse () { return slMousePos; }
slVec2 slGetMouseMaintainAspect ()
{
    slVec2 mouse = slMousePos - 0.5;
    slScalar scaling = slGetWindowAspect() / slGetUIAspect();
    if (scaling > 1) mouse.x *= scaling;
    else mouse.y /= scaling;
    return mouse + 0.5;
}
bool slGetWindowFocused ()
{
	return slWindowFocused;
}
slScalar slGetDelta ()
{
	return slDelta;
}
slInt2 slGetWindowResolution ()
{
	int w,h;
	SDL_GetWindowSize(slWindow,&w,&h);
    return slInt2(w,h);
}
slScalar slGetWindowAspect ()
{
	int w,h;
	SDL_GetWindowSize(slWindow,&w,&h);
	return w / (slScalar)h;
}
SDL_Window* slGetWindow ()
{
	return slWindow;
}
void slSetMiscEventHandler (void (*on_misc) (SDL_Event event))
{
	slOnMiscEvent = on_misc;
}
void slSetAllEventsHandler (void (*on_any) (SDL_Event event))
{
	slOnAnyEvent = on_any;
}
void slSetFileDropHandler (void (*on_filedrop) (char* path))
{
	slOnFileDrop = on_filedrop;
}
