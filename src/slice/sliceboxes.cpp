#include <slice.h>
using namespace sl_internal;

typedef slScalar slQuadScalar __attribute__((vector_size(sizeof(slScalar)*4)));
//typedef GLfloat slQuadGLfloat __attribute__((vector_size(sizeof(GLfloat)*4)));
extern slScalar slWindowAspect,slInvWindowAspect;
extern GLfloat slWindowAspect_Narrow,slInvWindowAspect_Narrow;

slScalar slUIAspect = 1;
void slSetUIAspect (slScalar ui_aspect) { slUIAspect = ui_aspect; }
slScalar slGetUIAspect () { return slUIAspect; }

slCorners slGetBoxCorners (slBox* box)
{
	slScalar x = box->xy.x;
	slScalar y = box->xy.y;
	slScalar w = box->wh.w;
	slScalar h = box->wh.h;
	if (box->maintainaspect)
	{
        slScalar aspect_meta = slWindowAspect / slUIAspect;
		if (aspect_meta > 1)
		{
			w /= aspect_meta;
			x = ((x - 0.5f) / aspect_meta) + 0.5f;
		}
		else
		{
			h *= aspect_meta;
			y = ((y - 0.5f) * aspect_meta) + 0.5f;
		}
	}
	slQuadScalar pxes = {x,x,x,x};
	pxes += (slQuadScalar){0,w,0,w};
	slQuadScalar pyes = {y,y,y,y};
	pyes += (slQuadScalar){0,0,h,h};
	if (unlikely(box->rot))
	{
		slScalar sinvalue = slsin(slDegToRad(box->rotangle));
		slScalar cosvalue = slpow(1 - sinvalue * sinvalue,0.5);
		bool rotoffset = !box->rotcenter;
		slScalar rotpointx,rotpointy;
		if (unlikely(rotoffset))
		{
			rotpointx = box->rotpoint.x;
			rotpointy = box->rotpoint.y;
			/// No idea whether or not this is needed...
			/*if (box->maintainaspect)
			{
				// Scale rotation point.
				if (slWindowAspect > 1) rotpointx = ((rotpointx - 0.5) * slInvWindowAspect) + 0.5;
				else rotpointy = ((rotpointy - 0.5) * slWindowAspect) + 0.5;
			}*/
			/// ...should do some testing to figure that out!
			pxes -= rotpointx;
			pyes -= rotpointy;
		}
		//pxes *= slWindowAspect;
		pyes *= slInvWindowAspect;//
		slQuadScalar pxes_ = pxes;
		slQuadScalar pyes_ = pyes;
		pxes = pxes_ * cosvalue - pyes_ * sinvalue;
		pyes = pyes_ * cosvalue + pxes_ * sinvalue;
		//pxes *= slInvWindowAspect;
		pyes *= slWindowAspect;//
		if (unlikely(rotoffset))
		{
			pxes += rotpointx;
			pyes += rotpointy;
		}
	}
	return {pxes[0],pyes[0],pxes[1],pyes[1],pxes[2],pyes[2],pxes[3],pyes[3]};
}
slCorners slGetBoxTexCorners (slBox* box, slTexture* texref)
{
	slScalar x = box->xy.x;
	slScalar y = box->xy.y;
	slScalar w = box->wh.w;
	slScalar h = box->wh.h;
	slScalar aratio = (w / h) / texref->aspect;
    slScalar aspect_meta = slWindowAspect / slUIAspect;
	if (!box->maintainaspect) aratio *= aspect_meta;
	slScalar prev_dim;
	if (aratio > 1)
	{
		prev_dim = w;
		w /= aratio;
		x += (prev_dim - w) * 0.5f;
	}
	else
	{
		prev_dim = h;
		h *= aratio;
		y += (prev_dim - h) * 0.5f;
	}

	// From slGetBoxCorners...
	if (box->maintainaspect)
	{
		if (aspect_meta > 1)
		{
			w /= aspect_meta;
			x = ((x - 0.5f) / aspect_meta) + 0.5f;
		}
		else
		{
			h *= aspect_meta;
			y = ((y - 0.5f) * aspect_meta) + 0.5f;
		}
	}
	slQuadScalar pxes = {x,x,x,x};
	pxes += (slQuadScalar){0,w,0,w};
	slQuadScalar pyes = {y,y,y,y};
	pyes += (slQuadScalar){0,0,h,h};
	if (unlikely(box->rot))
	{
		slScalar sinvalue = slsin(slDegToRad(box->rotangle));
		slScalar cosvalue = slpow(1 - sinvalue * sinvalue,0.5);
		bool rotoffset = !box->rotcenter;
		slScalar rotpointx,rotpointy;
		if (unlikely(rotoffset))
		{
			rotpointx = box->rotpoint.x;
			rotpointy = box->rotpoint.y;
			pxes -= rotpointx;
			pyes -= rotpointy;
		}
		//pxes *= slWindowAspect;
		pyes *= slInvWindowAspect;//
		slQuadScalar pxes_ = pxes;
		slQuadScalar pyes_ = pyes;
		pxes = pxes_ * cosvalue - pyes_ * sinvalue;
		pyes = pyes_ * cosvalue + pxes_ * sinvalue;
		//pxes *= slInvWindowAspect;
		pyes *= slWindowAspect;//
		if (unlikely(rotoffset))
		{
			pxes += rotpointx;
			pyes += rotpointy;
		}
	}
	return {pxes[0],pyes[0],pxes[1],pyes[1],pxes[2],pyes[2],pxes[3],pyes[3]};
}
slList slBoxes("Slice: Boxes",offsetof(slBox,_index_),false);
SDL_mutex* slBoxesMutex;
slBox* slCreateBox (slTexRef tex, slTexRef hovertex)
{
	slBox* out = new slBox;
    out->SetTexRef(tex);
    out->SetHoverTexRef(hovertex);

    out->workaround_box = NULL;

    SDL_LockMutex(slBoxesMutex);
	slBoxes.Add(out);
    SDL_UnlockMutex(slBoxesMutex);
	return out;
}
void slDestroyBox (slBox* todel)
{
    SDL_LockMutex(slBoxesMutex);
	slBoxes.Remove(todel);
    SDL_UnlockMutex(slBoxesMutex);

    if (todel->workaround_box)
        todel->workaround_box->Abandon();

    delete todel;
}
bool slPointOnBox (slBox* box, slVec2 point)
{
	slCorners corners;
	if (box->rot)
	{
		slBox clone = *box;
		slScalar angle = -slDegToRad(clone.rotangle);
		slScalar angcos = slcos(angle);
		slScalar angsin = slsin(angle);
		slVec2 center = clone.xy + clone.wh * 0.5f;
		if (clone.maintainaspect)
		{
            slScalar aspect_meta = slWindowAspect / slUIAspect;
			// Scale center point.
			if (aspect_meta > 1) center.x = ((center.x - 0.5f) / aspect_meta) + 0.5f;
			else center.y = ((center.y - 0.5f) * aspect_meta) + 0.5f;
		}

		slVec2 offset;
		if (!clone.rotcenter) offset = clone.rotpoint - center;
		else offset = 0;

		point -= center;
		point -= offset;

		point.x *= slWindowAspect;

		slVec2 _point = point;
		point.x = point.x * angcos - point.y * angsin;
		point.y = _point.y * angcos + _point.y * angsin;

		point.x /= slWindowAspect;

		point += offset;
		point += center;

		clone.rot = false;
		//clone.maintainaspect = false;
		corners = slGetBoxCorners(&clone);
	}
	else corners = slGetBoxCorners(box);
	return point.x > corners.p00x && point.x < corners.p01x && point.y < corners.p10y && point.y > corners.p00y;
	/*

		Corners as seen on-screen:

		10	11
		00	01

	*/
}
void slBox::PutWithin (slBox* outer)
{
	// To do: take rotation into account.
	xy *= outer->wh;
	wh *= outer->wh;
	xy += outer->xy;
}
bool slBoxesInOrder (slBox* box1, slBox* box2)
{
	return box2->z <= box1->z;
}


namespace sl_internal
{

bool slOnClickUI (slVec2 cursor, bool leftclick)
{
	slSort(&slBoxes,slBoxesInOrder);
	slBU pos = slBoxes.itemcount;
	slBU func_offset = leftclick ? offsetof(slBox,onclick) : offsetof(slBox,onrightclick);
	while (pos)
	{
		slBox* box = slBoxes.items[--pos];
		void (*func) (slBox*) = *(void (**) (slBox*))((void*)box + func_offset);
		if (func && box->visible)
		{
			if (slPointOnBox(box,cursor))
			{
				func(box);
				return true;
			}
		}
	}
	return false;
}

}

slBox* slGetHoveredBox (slVec2 cursor)
{
	slSort(&slBoxes,slBoxesInOrder);
	for (slBU cur = slBoxes.itemcount; cur; )
	{
		slBox* box = slBoxes.items[--cur];
		if (box->visible && box->hoverable)
			if (slPointOnBox(box,cursor))
				return box;
	}
	return NULL;
}
void slBoxHoverAbsorb (slBox* box)
{
	box->hoverable = true;
	box->hoverbordercolor = box->bordercolor;
	box->hoverbackcolor = box->backcolor;
	box->hoverdrawmask = box->drawmask;
	box->hovertexbordercolor = box->texbordercolor;
    box->SetHoverTexRef(slNoTexture);
}
void slRenderPrepare (slBox* box, slBox* hoverbox)
{
    if (box->visible)
    {
        if (!box->workaround_box)
        {
            box->workaround_box = uiBoxRef()
                ->Attach()
            .ReserveGetRaw();
            box->workaround_texbox = uiBoxRef()
                ->AttachTo(box->workaround_box)
            .operator->();
            box->prev_txform.wh = 0;
            box->prev_tex_wh = 0;
            box->prev_z = 127;
        }

        if (!box->rot) box->rotangle = 0;
        if (memcmp(&box->rotangle,&box->prev_txform,sizeof(box->prev_txform)))
        {
            //printf("box update\n");
            box->workaround_box
                ->SetPos(box->xy)
                ->SetSize(box->wh)
                ->SetRotation(box->rotangle)
            ;
            memcpy(&box->prev_txform,&box->rotangle,sizeof(box->prev_txform));

            if (box->maintainaspect)
                box->workaround_box
                    ->SetTargetAspect(slUIAspect) // not bothering to detect changes in this
                    ->SetAspectAlign(box->aspect_align_x,box->aspect_align_y)
                ;
            else
                box->workaround_box->SetTargetAspect(-1);
        }

        if (box->z != box->prev_z)
        {
            box->prev_z = box->z;
            box->workaround_box->SetZ(box->z);
        }

        box->workaround_box->CapturesHover(box->hoverable);

        box->workaround_box
            ->SetBorderColor(box->bordercolor)
            ->SetBackColor(box->backcolor)
        ;

        if (memcmp(&box->tex_wh,&box->prev_tex_wh,sizeof(box->tex_wh)))
        {
            //printf("texbox update\n");
            slVec2 texgap = slVec2(1) - box->tex_wh;
            switch (box->tex_align_x)
            {
                case slAlignLeft: texgap.x = 0; break;
                case slAlignCenter: texgap.x *= 0.5;
            }
            switch (box->tex_align_y)
            {
                case slAlignTop: texgap.y = 0; break;
                case slAlignCenter: texgap.y *= 0.5;
            }
            box->workaround_texbox
                ->SetPos(texgap)
                ->SetSize(box->tex_wh)
            ;
            box->prev_tex_wh = box->tex_wh;
        }

        box->workaround_texbox
            ->SetTexAlign(box->tex_align_x,box->tex_align_y)
            ->SetTexture(box->texref_swapchain.Step())
            ->SetTexMask(box->drawmask)
        ;

        if (box->hoverable)
        {
            box->workaround_box
                ->SetHoverBackColor(box->hoverbackcolor)
                ->SetHoverBorderColor(box->hoverbordercolor)
            ;

            slTexRef hovertex = box->hovertexref_swapchain.Step();
            if (hovertex->tex)
                box->workaround_texbox->SetHoverTexture(hovertex);
            else
                box->workaround_texbox->DisableHoverTexture();
            box->workaround_texbox->SetTexMask(box->hoverdrawmask);
        }
        else
        {
            box->workaround_box
                ->DisableHoverBackColor()
                ->DisableHoverBorderColor()
            ;
            box->workaround_texbox
                ->DisableHoverTexture()
                ->DisableHoverTexMask()
            ;
        }
    }
    else if (box->workaround_box)
    {
        box->workaround_box->Abandon();
        box->workaround_box = NULL;
    }
}



slList slZHooks("Slice: Z-Hooks",slNoIndex,false);
void slAddZHook (slZHook* zhook)
{
	slZHooks.Add(zhook);
    zhook->workaround_frame = new slFrame(false);
	zhook->workaround_box = uiBoxRef()
		->SetPos(0)
		->SetSize(1)
		->SetTexture(zhook->workaround_frame->CreateColorOutput(0,true))
	.ReserveGetRaw();
}
void slRemoveZHook (slZHook* zhook)
{
	slZHooks.Remove(zhook);
	zhook->workaround_box->Abandon();
    delete zhook->workaround_frame;
}
bool slZHooksInOrder (slZHook* zhook1, slZHook* zhook2)
{
	return zhook2->z <= zhook1->z;
}



namespace sl_internal
{


void slRenderPrepareAll (slBox* hoverbox)
{
    //Uint64 start_prep = SDL_GetPerformanceCounter();

	slDoWork(&slBoxes,slRenderPrepare,hoverbox);
    /*for (slBU i = 0; i < slBoxes.itemcount; i++)
    {
        slBox* box = slBoxes.items[i];
        slRenderPrepare(box,hoverbox);
    }*/

    //Uint64 stop_prep = SDL_GetPerformanceCounter();
    //int prep_cycles = stop_prep - start_prep;
    //float prep_seconds = prep_cycles / (float)SDL_GetPerformanceFrequency();
    //printf("slBox transfer: %f seconds (%d cycles)\n",prep_seconds,prep_cycles);
}

}

void slDrawUI ()
{
	/*  Z hooks are deprecated, but continue to support them for now by sending
		their output to a normal uiBox via slTargetTexture.  */
	for (slBU i = 0; i < slZHooks.itemcount; i++)
	{
		slZHook* hook = slZHooks.items[i];
        hook->workaround_frame->Resize(slGetWindowResolution());
		hook->workaround_frame->DrawTo();
		hook->func(hook->userdata);
		hook->workaround_box->SetZ(hook->z);
	}

    slDrawToScreen();
	uiMainUI->Draw();
}



namespace sl_internal
{

void slInitUI ()
{
    uiInit();

	slInitBoxRenderPrograms();
	slBoxes.HookAutoShrink();
    slBoxesMutex = SDL_CreateMutex();
}
void slQuitUI ()
{
	slBoxes.UntilEmpty(slDestroyBox);
    SDL_DestroyMutex(slBoxesMutex);
	slZHooks.Clear(NULL);
	slQuitBoxRenderPrograms();

    uiQuit();
}

}
