#include <slice.h>



/// UI Color Program

// Source: Vert w/ Rotation
char* slColorRotateProgram_VertSrc = R"(#version 130

uniform vec4 XYWH;
in vec2 VertPos;
in vec2 VertTexCoords;
uniform vec2 RotPoint;
uniform float SinValue,CosValue;
uniform float WindowAspect;

void main ()
{
	// Generate point.
	vec2 point = XYWH.xy + (VertPos * XYWH.zw);
	// Rotate.
	point -= RotPoint;
	point.y /= WindowAspect;
	point = vec2(point.x * CosValue - point.y * SinValue,point.y * CosValue + point.x * SinValue);
	point.y *= WindowAspect;
	point += RotPoint;
	// Convert to window coordinates.
    gl_Position = vec4((point * 2) - 1,0,1);
    gl_Position.y = -gl_Position.y;
}
)";

// Uniforms: Vert w/ Rotation
namespace sl_internal
{
	GLuint
		slColorRotateProgram_XYWH,
		slColorRotateProgram_RotPoint,
		slColorRotateProgram_SinValue,
		slColorRotateProgram_CosValue,
		slColorRotateProgram_WindowAspect;
}

// Source: Frag
char* slColorProgram_FragSrc = R"(#version 130

uniform vec4 Color;

void main ()
{
    gl_FragColor = Color;
}
)";

// Uniforms: Frag
namespace sl_internal
{
	GLuint
		slColorProgram_Color,
		slColorRotateProgram_Color;
}





/// UI Texture Program

// Source: Vert w/ Rotation
char* slTexRotateProgram_VertSrc = R"(#version 130

uniform vec4 XYWH;
in vec2 VertPos;
out vec2 TexCoords;
uniform vec2 RotPoint;
uniform float SinValue,CosValue;
uniform float WindowAspect;

void main ()
{
	// Generate point.
	vec2 point = XYWH.xy + (VertPos * XYWH.zw);
	// Rotate.
	point -= RotPoint;
	point.y /= WindowAspect;
	point = vec2(point.x * CosValue - point.y * SinValue,point.y * CosValue + point.x * SinValue);
	point.y *= WindowAspect;
	point += RotPoint;
	// Convert to window coordinates.
    gl_Position = vec4((point * 2) - 1,0,1);
    gl_Position.y = -gl_Position.y;

    TexCoords = vec2(VertPos.x, 1 - VertPos.y);
}
)";

// Uniforms: Vert w/ Rotation
namespace sl_internal
{
	GLuint
		slTexRotateProgram_XYWH,
		slTexRotateProgram_RotPoint,
		slTexRotateProgram_SinValue,
		slTexRotateProgram_CosValue,
		slTexRotateProgram_WindowAspect;
}

// Source: Frag
char* slTexProgram_FragSrc = R"(#version 130

uniform sampler2D Tex;
uniform vec4 Mask;
in vec2 TexCoords;

void main ()
{
    gl_FragColor = texture(Tex,TexCoords) * Mask;
}
)";

// Uniforms: Frag
namespace sl_internal
{
	GLuint
		slTexProgram_Tex,
		slTexProgram_Mask,
		slTexRotateProgram_Tex,
		slTexRotateProgram_Mask;
}





// Shader Initialization
GLfloat slFullScreenCoords [8]
{
	0,1, // Bottom-Left
	1,1, // Bottom-Right
	1,0, // Top-Right
	0,0  // Top-Left
};
GLuint slBoxVertsBuf;
inline void slInitBoxVertsBuf ()
{
	glGenBuffers(1,&slBoxVertsBuf);
	if (!slBoxVertsBuf) slFatal("Failed to generate box vertices buffer object.",240);
	glBindBuffer(GL_ARRAY_BUFFER,slBoxVertsBuf);
	glBufferData(GL_ARRAY_BUFFER,sizeof(slFullScreenCoords),slFullScreenCoords,GL_STATIC_DRAW);
}
inline void slQuitBoxVertsBuf ()
{
	glDeleteBuffers(1,&slBoxVertsBuf);
}
GLuint slBoxRenderArray;
GLuint slBoxRenderArray_VertsIndex = 0;
inline void slInitBoxRenderArray ()
{
	glGenVertexArrays(1,&slBoxRenderArray);
	if (!slBoxRenderArray) slFatal("Failed to generate box render array.",220);
	glBindVertexArray(slBoxRenderArray);

	glEnableVertexAttribArray(slBoxRenderArray_VertsIndex);
	slInitBoxVertsBuf(); // Binds the buffer it works on.
	glVertexAttribPointer(slBoxRenderArray_VertsIndex,2,GL_FLOAT,GL_FALSE,0,0);
}
inline void slQuitBoxRenderArray ()
{
	glDeleteVertexArrays(1,&slBoxRenderArray);
	slQuitBoxVertsBuf();
}
GLuint slLocateUniform (slShaderProgram shaderprogram, char* varname)
{
    //printf("looking up \"%s\" in \"%s\"\n",varname,shaderprogram.name);

    GLuint location = glGetUniformLocation(shaderprogram.program,varname);

    if (location == -1)
        printf(
            "Cannot locate uniform \"%s\" in program \"%s\".\n",
            varname,shaderprogram.name
        );

    return location;
}
GLuint slCreateShader (char* source, GLenum type, char* name)
{
	GLuint shader = glCreateShader(type);
	if (!shader) slFatal("Couldn't generate UI color program fragment shader.",270);
	glShaderSource(shader,1,&source,NULL);
	glCompileShader(shader);
	GLint status;
	glGetShaderiv(shader,GL_COMPILE_STATUS,&status);
	if (status != GL_TRUE)
	{
		system("mkdir error");
		char* filename;
		asprintf(&filename,"error/compile-log_%lX.txt",(unsigned long)time(NULL));
		FILE* file = fopen(filename,"w");
		free(filename);
		if (!file) slFatal("Couldn't compile shader, couldn't generate log.",280);
		glGetShaderiv(shader,GL_INFO_LOG_LENGTH,&status);
		char* logstr = malloc(status);
		glGetShaderInfoLog(shader,status,NULL,logstr);
		fprintf(file,"Shader Name: \"%s\" (%s Shader)\n\n-- SOURCE CODE --\n\n%s\n\n-- COMPILE LOG --\n\n%s",
			name,
			type == GL_VERTEX_SHADER ? "Vertex" : "Fragment",
			source,
			logstr
		);
		free(logstr);
		fclose(file);
		slFatal("Couldn't compile shader, generated log file.",285);
	}
	return shader;
}
void slLinkProgram (GLuint program, char* name)
{
	glLinkProgram(program);
	GLint status;
	glGetProgramiv(program,GL_LINK_STATUS,&status);
	if (status != GL_TRUE)
	{
		system("mkdir error");
		char* filename;
		asprintf(&filename,"error/link-log_%lX.txt",(unsigned long)time(NULL));
		FILE* file = fopen(filename,"w");
		free(filename);
		if (!file) slFatal("Couldn't link shaders, couldn't generate log.",290);
		glGetProgramiv(program,GL_INFO_LOG_LENGTH,&status);
		char* logstr = malloc(status);
		glGetProgramInfoLog(program,status,NULL,logstr);
		fprintf(file,"Program Name: \"%s\"\n\n-- LINK LOG --\n\n%s",name,logstr);
		free(logstr);
		fclose(file);
		slFatal("Couldn't link shaders, generated log file.",295);
	}
}
slShaderProgram slCreateShaderProgram (char* name, char* vert_src, char* frag_src, void (*bind_attribs) (GLuint program))
{
	slShaderProgram out;

    out.name = slStrAlwaysClone(name);

	// Create Shaders
	out.vertshader = slCreateShader(vert_src,GL_VERTEX_SHADER,name);
	out.fragshader = slCreateShader(frag_src,GL_FRAGMENT_SHADER,name);

	// Create Program
	out.program = glCreateProgram();
	if (!out.program) slFatal("Couldn't generate shader program object.",250);
	//glUseProgram(out.program);

	// Attach Shaders
	glAttachShader(out.program,out.vertshader);
	glAttachShader(out.program,out.fragshader);

	// Set Attribute Indicies for VAOs
	bind_attribs(out.program);

	// Link
	slLinkProgram(out.program,name);

	glUseProgram(out.program);
	return out;
}
void slDestroyShaderProgram (slShaderProgram program)
{
    free(program.name);

	glDeleteProgram(program.program);
	glDeleteShader(program.vertshader);
	glDeleteShader(program.fragshader);
}
namespace sl_internal
{
	slShaderProgram slColorRotateProgram, slTexRotateProgram;
}
GLuint slTextureUnit_Tex = 0;
void slBindAttribs_VertPos (GLuint program)
{
	glBindAttribLocation(program,slBoxRenderArray_VertsIndex,"VertPos");
}
namespace sl_internal
{
	void slInitBoxRenderPrograms ()
	{
		// Create VAO for All Box Rendering
		slInitBoxRenderArray();


		/// UI Color Rotate Program
		slColorRotateProgram = slCreateShaderProgram("Box Color Rotate",slColorRotateProgram_VertSrc,slColorProgram_FragSrc,slBindAttribs_VertPos);

		// Get Uniforms
		slColorRotateProgram_XYWH = slLocateUniform(slColorRotateProgram,"XYWH");
		slColorRotateProgram_RotPoint = slLocateUniform(slColorRotateProgram,"RotPoint");
		slColorRotateProgram_WindowAspect = slLocateUniform(slColorRotateProgram,"WindowAspect");
		slColorRotateProgram_SinValue = slLocateUniform(slColorRotateProgram,"SinValue");
		slColorRotateProgram_CosValue = slLocateUniform(slColorRotateProgram,"CosValue");
		slColorRotateProgram_Color = slLocateUniform(slColorRotateProgram,"Color");



		/// UI Texture Rotate Program
		slTexRotateProgram = slCreateShaderProgram("Box Texture Rotate",slTexRotateProgram_VertSrc,slTexProgram_FragSrc,slBindAttribs_VertPos);

		// Get Uniforms
		slTexRotateProgram_XYWH = slLocateUniform(slTexRotateProgram,"XYWH");
		slTexRotateProgram_RotPoint = slLocateUniform(slTexRotateProgram,"RotPoint");
		slTexRotateProgram_WindowAspect = slLocateUniform(slTexRotateProgram,"WindowAspect");
		slTexRotateProgram_SinValue = slLocateUniform(slTexRotateProgram,"SinValue");
		slTexRotateProgram_CosValue = slLocateUniform(slTexRotateProgram,"CosValue");
		slTexRotateProgram_Tex = slLocateUniform(slTexRotateProgram,"Tex");
		slTexRotateProgram_Mask = slLocateUniform(slTexRotateProgram,"Mask");

		// Configure Texture Units
		glUniform1i(slTexRotateProgram_Tex,slTextureUnit_Tex);
	}
	void slQuitBoxRenderPrograms ()
	{
		slQuitBoxRenderArray();

		slDestroyShaderProgram(slColorRotateProgram);
		slDestroyShaderProgram(slTexRotateProgram);
	}
	void slSetDrawTexture (GLuint tex)
	{
		glActiveTexture(GL_TEXTURE0 + slTextureUnit_Tex);
		glBindTexture(GL_TEXTURE_2D,tex);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAX_ANISOTROPY_EXT,1);
	}
}
void slBindFullscreenArray ()
{
	glBindVertexArray(slBoxRenderArray);
}
