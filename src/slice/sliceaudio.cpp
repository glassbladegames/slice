#include <slice.h>
#define slAsyncSoundLoading
slList slSoundSources("Slice: Sound Sources",offsetof(slSoundSource,_index_),false);
slBlockAllocator slSoundSourceAllocator ("Slice: Sound Source Allocator",sizeof(slSoundSource),24);
const float slInv32768 = 1.f / 32768.f;
void slSubLoadSWAG (slSoundSource* item)
{
	FILE* file = fopen(item->loadedfrom,"rb");
	if (!file) goto FAIL_NOFILE;
	{
		Uint16 magic;
		fread(&magic,sizeof(Uint16),1,file);
		if (magic != 420) goto FAIL;
		{
			item->samplecount = 0;
			item->ready = slSoundSource_Incomplete;

			Uint32 len;
			fread(&len,sizeof(Uint32),1,file);
			fread(&item->persecond,sizeof(Uint32),1,file);

			Uint8 stereo;
			fread(&stereo,sizeof(Uint8),1,file);

			Uint32 sampletotal = len;
			if (stereo)
			{
				sampletotal = len + len;
				item->channelcount = 2;
			}
			else
			{
				sampletotal = len;
				item->channelcount = 1;
			}

			item->samples = malloc(sizeof(float) * sampletotal);

			float* bufptr = item->samples;
			Uint16 value;
			if (stereo)
			{
				while (len--)
				{
					fread(&value,sizeof(Uint16),1,file);
					*bufptr++ = (value * slInv32768) - 1.f;
					fread(&value,sizeof(Uint16),1,file);
					*bufptr++ = (value * slInv32768) - 1.f;
					item->samplecount++;
				}
			}
			else
			{
				while (len--)
				{
					fread(&value,sizeof(Uint16),1,file);
					*bufptr++ = (value * slInv32768) - 1.f;
					item->samplecount++;
				}
			}
			fclose(file);
			goto READY;
		}
		FAIL:
		fclose(file);
	}
	FAIL_NOFILE:
	item->samples = NULL;
	READY:
	item->ready = slSoundSource_Ready;
}
slSoundSource* slLoadSWAG (char* path)
{
	if (!path) return NULL;
	slSoundSource* out;
	for (slBU cur = 0; cur < slSoundSources.itemcount; cur++)
	{
		out = *(slSoundSources.items + cur);
		if (slStrEq(out->loadedfrom,path)) return out;
	}
	//out = malloc(sizeof(slSoundSource));
	out = slSoundSourceAllocator.Allocate();
	out->loadedfrom = slStrClone(path);
	out->ready = slSoundSource_Loading;
	#ifdef slAsyncSoundLoading
	SDL_Thread* loader = SDL_CreateThread(slSubLoadSWAG,"SWAG Loader",out);
	SDL_DetachThread(loader);
	#else
	slSubLoadSoundSource(out);
	#endif
	slSoundSources.Add(out);
	return out;
}
#include <vorbis/vorbisfile.h>
void slSubLoadOGG (slSoundSource* src)
{
    OggVorbis_File file;
    if (ov_fopen(src->loadedfrom,&file)) goto FAIL_NOFILE;
	{
		src->samplecount = 0;
		src->ready = slSoundSource_Incomplete;

		vorbis_info* vinfo = ov_info(&file,-1);
		src->persecond = vinfo->rate;
		int channels = vinfo->channels;
		ogg_int64_t total_samples = ov_pcm_total(&file,-1);
		int out_channels;
		switch (channels)
		{
			case 6: case 4: case 2: case 1: out_channels = channels; break;
			default: out_channels = 1;
		}
		src->samples = malloc(total_samples * sizeof(float) * out_channels);
		src->channelcount = out_channels;

		float* outbufptr = src->samples;
		int cbs = -1;
		while (true)
		{
			float** decoded = NULL;
			long samples_read = ov_read_float(&file,&decoded,16384,&cbs);
			if (samples_read)
			{
				if (samples_read < 0) continue; //printf("Error: %ld samples read\n", samples_read);

				switch (channels)
				{
					case 6: // 5.1
					for (long i = 0; i < samples_read; i++)
					{
						*outbufptr++ = decoded[0][i];
						*outbufptr++ = decoded[1][i];
						*outbufptr++ = decoded[2][i];
						*outbufptr++ = decoded[3][i];
						*outbufptr++ = decoded[4][i];
						*outbufptr++ = decoded[5][i];
					}
					break;

					case 4: // Quadrophonic.
					for (long i = 0; i < samples_read; i++)
					{
						*outbufptr++ = decoded[0][i];
						*outbufptr++ = decoded[1][i];
						*outbufptr++ = decoded[2][i];
						*outbufptr++ = decoded[3][i];
					}
					break;

					case 2: // Stereo.
					for (long i = 0; i < samples_read; i++)
					{
						*outbufptr++ = decoded[0][i];
						*outbufptr++ = decoded[1][i];
					}
					break;

					case 1: // Mono.
					for (long i = 0; i < samples_read; i++)
					{
						*outbufptr++ = decoded[0][i];
					}
					break;

					default: // Unknown channels. Average them all into a MONO stream.
					for (long i = 0; i < samples_read; i++)
					{
						float avg = 0;
						for (int c = 0; c < channels; c++) avg += decoded[c][i];
						avg /= channels;

						*outbufptr++ = avg;
					}

				}
				/*memcpy(src->samples + src->samplecount, *decoded, samples_read * sizeof(float));
				if (channels > 1) memcpy(src->samples_right + src->samplecount, decoded[1], samples_read * sizeof(float));*/
				//printf("%f\n",*(src->samples + src->samplecount));
				src->samplecount += samples_read;
				/*printf("\"%s\" - %d of %d samples read (%.2f%%).\n",
					src->loadedfrom,
					(unsigned int)src->samplecount,
					(unsigned int)total_samples,
					src->samplecount / (float)total_samples * 100
				);*/
			}
			else break;
		}
		ov_clear(&file);
		goto READY;

		//FAIL:
		//ov_clear(&file);

	}
	FAIL_NOFILE:
	src->samples = NULL;

	READY:
	src->ready = slSoundSource_Ready;
}
slSoundSource* slLoadOGG (char* path)
{
	if (!path) return NULL;
	slSoundSource* out;
	for (slBU cur = 0; cur < slSoundSources.itemcount; cur++)
	{
		out = *(slSoundSources.items + cur);
		if (slStrEq(out->loadedfrom,path)) return out;
	}
	//out = malloc(sizeof(slSoundSource));
	out = slSoundSourceAllocator.Allocate();

	out->loadedfrom = slStrClone(path);
	out->ready = slSoundSource_Loading;
	#ifdef slAsyncSoundLoading
	SDL_Thread* loader = SDL_CreateThread(slSubLoadOGG,"OGG Loader",out);
	SDL_DetachThread(loader);
	#else
	slSubLoadSoundSource(out);
	#endif
	slSoundSources.Add(out);
	return out;
}
void slFreeSoundSource (slSoundSource* src)
{
	//printf("<freeing a sound source>\n");
	slSoundSources.Remove(src);



	// Make sure the audio callback is not running, just in case...
	// Sound source has been set to NULL after mixer loads the previous value,
	// and garbage collection occurs and frees this sound source.
	// It's extremely unlikely, but possible.
	SDL_LockAudio();
	// In fact, as long as the mixer has finished since this function was called,
	// we can free this memory without breaking anything even if the mixer has started again.
	SDL_UnlockAudio();



	while (src->ready != slSoundSource_Ready) SDL_Delay(1);

	if (src->samples) free(src->samples);
	//if (src->samples_right) free(src->samples_right);
	if (src->loadedfrom) free(src->loadedfrom);

	//free(src);
	slSoundSourceAllocator.Release(src);
}
slList slSounds("Slice: Sounds",offsetof(slSound,_index_),false);
slList slSoundSourceUsedHooks("Slice: SoundSourceUsed Hooks",slNoIndex,false);
inline bool slIsSoundSourceUnused (slSoundSource* src)
{
	//printf("starting gc\n");
	slBU cur;
	for (cur = 0; cur < slSounds.itemcount; cur++) if (((slSound*)*(slSounds.items + cur))->src == src) return false;
    for (cur = 0; cur < slSoundSourceUsedHooks.itemcount; cur++)
	{
		bool (*is_used) (slSoundSource*) = *(slSoundSourceUsedHooks.items + cur);

		//printf("calling external SoundSourceUsed\n");
		if (is_used(src)) return false;
	}
	//printf("finished gc, freeing src!\n");
    return true;
}
void slHook_SoundSourceUsed (bool (*func) (slSoundSource* src))
{
	slSoundSourceUsedHooks.Add(func);
}
void slUnhook_SoundSourceUsed (bool (*func) (slSoundSource* src))
{
	slSoundSourceUsedHooks.Remove(func);
}
void slSoundSourceGC (slSoundSource* src, void* unused, slWorker* worker)
{
	//SDL_LockAudio();
	if (slIsSoundSourceUnused(src))
	//{
		//printf("freeing src!!!!!!\n");
		slSchedule(slFreeSoundSource,src,worker);
	//}
	//else printf("NOT freeing src\n");
	//SDL_UnlockAudio();
}
/*void slSingleSoundSourceGC ()
{
	if (slIsSoundSourceUnused(src)) slFreeSoundSource(src);
}*/
void slSoundSourcesGC ()
{
	SDL_LockAudio();
    slDoWork(&slSoundSources,slSoundSourceGC,NULL);
    SDL_UnlockAudio();
}
void slShrinkSoundsList ()
{
	SDL_LockAudio();
	slSounds.Shrink();
	SDL_UnlockAudio();
}
slBlockAllocator slSoundAllocator ("Slice: Sound Allocator",sizeof(slSound),32);
slSound* slCreateSound (slSoundSource* src, bool destroy, bool play, bool loop, slScalar volume, slScalar pitch)
{
	//printf("creating sound\n");
	//slSound* out = malloc(sizeof(slSound));
	SDL_LockAudio();
	slSound* out = slSoundAllocator.Allocate();
	out->src = src;
	out->pitch_modulate = NULL;
	out->cursample = 0;
	out->cursubsample = 0;
	out->loop = loop;
	out->volume = volume;
	out->pitch = pitch;
	out->playing = play;
	out->destroy = destroy;
	out->onfinish = NULL;
	slSounds.Add(out);
	SDL_UnlockAudio();
	return out;
}
void slDestroySound (slSound* sound)
{
	SDL_LockAudio();
	slSounds.Remove(sound);
	slSoundAllocator.Release(sound);
	SDL_UnlockAudio();
	//free(sound);
}
inline bool slIsSoundDead (slSound* sound)
{
	// Something explicitly tracks this object, don't destroy it.
	if (!sound->destroy) return false;

	// Definitely dead if not playing, or has no source.
	if (!sound->playing || !sound->src) return true;

	// It's playing, so if its source has samples (or is still loading and *will* have samples) it's alive.
	if (sound->src->samplecount || sound->src->ready != slSoundSource_Ready) return false;

	// Source must have failed to load, so no audio would be played. Destroy it.
	return true;
}
void slRemoveSoundIfDead (slSound* sound, void* unused, slWorker* worker)
{
	if (slIsSoundDead(sound))
		slSchedule(slDestroySound,sound,worker);
}
void slClearDeadSounds ()
{
	SDL_LockAudio();
	slDoWork(&slSounds,slRemoveSoundIfDead,NULL);
	SDL_UnlockAudio();
}
bool slMuteWhileAway = true;
#define slAudioSubRes 1000000000
SDL_AudioSpec slAudioInfo;
slScalar slMasterVolume = 1;
void (*_slCustomMixStage_) (float* buf, slBU samples, Uint8 channels, slScalar persample) = NULL;
void slSetCustomMixStage (void (*func) (float*,slBU,Uint8,slScalar))
{
	SDL_LockAudio();
	_slCustomMixStage_ = func;
	SDL_UnlockAudio();
}
float* slAudioBuf = NULL;
slBU slCustomAudioLen = 0;

#define MIX_LINEAR_RESAMPLE true

#define MIX_NOSRC\
	if (!src)\
	{\
		sound->playing = false;\
		continue;\
	}
#define MIX_CHECK_INNER\
	if (sound->cursample >= src->samplecount)\
	{\
		if (src->ready == slSoundSource_Ready)\
		{\
			if (sound->loop) sound->cursample %= src->samplecount;\
			else break;\
		}\
		else break;\
	}
#define MIX_CHECK_OUTER\
	if (sound->cursample >= src->samplecount)\
	{\
		if (src->ready == slSoundSource_Ready)\
		{\
			if (sound->loop) sound->cursample %= src->samplecount;\
			else \
			{\
				sound->playing = false;\
				sound->cursample = 0;\
				sound->cursubsample = 0;\
				goto ON_SOUND_FINISH;\
			}\
		}\
		else continue;\
	}
#define MIX_ADVANCE {\
	if (pitch_modulate) sound->cursubsample += timestep * pitch_modulate((sound->cursample + sound->cursubsample) / src->persecond);\
	else sound->cursubsample += timestep;\
	slBU advancement = sound->cursubsample;\
	sound->cursubsample -= advancement;\
	sound->cursample += advancement;\
}
#define MIX_LOOP\
	for (cur = 0; cur < slAudioInfo.samples; cur++)
#define MIX_SAMPLES\
	samples = src->samples + sound->cursample * src->channelcount;
#define MIX_GET_5PT1\
	MIX_SAMPLES\
	FrontLeft = *samples;\
	FrontRight = samples[1];\
	Center = samples[2];\
	Bass = samples[3];\
	BackLeft = samples[4];\
	BackRight = samples[5];\
	if (MIX_LINEAR_RESAMPLE)\
	{\
		if (sound->cursample + 1 < src->samplecount)\
		{\
			float next_amt = sound->cursubsample;\
			float keep_amt = 1 - next_amt;\
			FrontLeft = FrontLeft * keep_amt + samples[6] * next_amt;\
			FrontRight = FrontRight * keep_amt + samples[7] * next_amt;\
			Center = Center * keep_amt + samples[8] * next_amt;\
			Bass = Bass * keep_amt + samples[9] * next_amt;\
			BackLeft = BackLeft * keep_amt + samples[10] * next_amt;\
			BackRight = BackRight * keep_amt + samples[11] * next_amt;\
		}\
	}
#define MIX_GET_QUADPHON\
	MIX_SAMPLES\
	FrontLeft = *samples;\
	FrontRight = samples[1];\
	BackLeft = samples[2];\
	BackRight = samples[3];\
	if (MIX_LINEAR_RESAMPLE)\
	{\
		if (sound->cursample + 1 < src->samplecount)\
		{\
			float next_amt = sound->cursubsample;\
			float keep_amt = 1 - next_amt;\
			FrontLeft = FrontLeft * keep_amt + samples[4] * next_amt;\
			FrontRight = FrontRight * keep_amt + samples[5] * next_amt;\
			BackLeft = BackLeft * keep_amt + samples[6] * next_amt;\
			BackRight = BackRight * keep_amt + samples[7] * next_amt;\
		}\
	}
#define MIX_GET_STEREO\
	MIX_SAMPLES\
	FrontLeft = *samples;\
	FrontRight = samples[1];\
	if (MIX_LINEAR_RESAMPLE)\
	{\
		if (sound->cursample + 1 < src->samplecount)\
		{\
			float next_amt = sound->cursubsample;\
			float keep_amt = 1 - next_amt;\
			FrontLeft = FrontLeft * keep_amt + samples[2] * next_amt;\
			FrontRight = FrontRight * keep_amt + samples[3] * next_amt;\
		}\
	}
#define MIX_GET_MONO\
	MIX_SAMPLES\
	FrontLeft = *samples;\
	if (MIX_LINEAR_RESAMPLE)\
	{\
		if (sound->cursample + 1 < src->samplecount)\
		{\
			float next_amt = sound->cursubsample;\
			float keep_amt = 1 - next_amt;\
			FrontLeft = FrontLeft * keep_amt + samples[1] * next_amt;\
		}\
	}
#define GOTO_ON_FINISH\
	goto ON_SOUND_FINISH;
void slSoundsSilentAdvance (float by)
{
	for (slBU cur = 0; cur < slSounds.itemcount; cur++)
	{
		slSound* sound = *(slSounds.items + cur);
		if (sound->playing)
		{
			slSoundSource* src = sound->src;
			MIX_NOSRC

			float timestep = src->persecond * sound->pitch * by;
			float (*pitch_modulate) (float) = NULL;
			MIX_CHECK_OUTER
			MIX_ADVANCE
			MIX_CHECK_OUTER

			continue;
			ON_SOUND_FINISH:
			if (sound->onfinish) sound->onfinish(sound);
		}
	}
}
struct slMultiMixThread
{
	float* buf;
	slBU len;
    bool written;
};
void slMultiMixProc (slBU start, slBU count, slMultiMixThread* thread, void* unused_main_userdata, slWorker* worker)
{
	if (thread->len != slCustomAudioLen)
        thread->buf = realloc(thread->buf,
                              sizeof(float) * (thread->len = slCustomAudioLen));
    thread->written = true;
	memset(thread->buf,0,thread->len * sizeof(float));
	slBU end = start + count;

	for (slBU subcur = start; subcur < end; subcur++)
	{
		slSound* sound = *(slSounds.items + subcur);
		if (sound->playing)
		{
			slBU cur;
			float* bufptr = thread->buf;
			float* samples;
			float FrontLeft,FrontRight,Center,Bass,BackLeft,BackRight;
			slSoundSource* src = sound->src;
			MIX_NOSRC
			if (src->ready && src->samples)
			{
				float timestep = (src->persecond * sound->pitch) / slAudioInfo.freq;
				float (*pitch_modulate) (float seconds) = sound->pitch_modulate;
				float vol = sound->volume;
				switch (src->channelcount)
				{
					case 6:
					switch (slAudioInfo.channels)
					{
						case 6:
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_GET_5PT1
							// Front-Left, Front-Right, Center, Bass, Rear-Left, Rear-Right
							*bufptr++ += FrontLeft * vol;
							*bufptr++ += FrontRight * vol;
							*bufptr++ += Center * vol;
							*bufptr++ += Bass * vol;
							*bufptr++ += BackLeft * vol;
							*bufptr++ += BackRight * vol;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
						break;

						case 4:
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_GET_5PT1
							// Front-Left, Front-Right, Rear-Left, Rear-Right
							Center = (Center + Bass) * 0.25f;
							FrontLeft = FrontLeft * 0.5f + Center;
							FrontRight = FrontRight * 0.5f + Center;
							BackLeft = BackLeft * 0.5f + Center;
							BackRight = BackRight * 0.5f + Center;
							*bufptr++ = FrontLeft * vol;
							*bufptr++ = FrontRight * vol;
							*bufptr++ = BackLeft * vol;
							*bufptr++ = BackRight * vol;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
						break;

						case 2:
						vol *= 0.25f;
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_GET_5PT1
							// Left, Right
							*bufptr++ += (FrontLeft + BackLeft + Center + Bass) * vol;
							*bufptr++ += (FrontRight + BackRight + Center + Bass) * vol;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
						break;

						default:
						vol *= 0.125f;
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_SAMPLES
							MIX_GET_5PT1
							// Mono
							*bufptr++ += (FrontLeft + FrontRight + BackLeft + BackRight + Center + Center + Bass + Bass) * vol;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
					}
					break;

					case 4:
					switch (slAudioInfo.channels)
					{
						case 6:
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_GET_QUADPHON
							// Front-Left, Front-Right, Center, Bass, Rear-Left, Rear-Right
							*bufptr++ += FrontLeft * vol;
							*bufptr++ += FrontRight * vol;
							Center = (FrontLeft + FrontRight + BackLeft + BackRight) * 0.25f * vol;
							*bufptr++ += Center;
							*bufptr++ += Center;
							*bufptr++ += BackLeft * vol;
							*bufptr++ += BackRight * vol;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
						break;

						case 4:
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_GET_QUADPHON
							// Front-Left, Front-Right, Rear-Left, Rear-Right
							*bufptr++ += FrontLeft * vol;
							*bufptr++ += FrontRight * vol;
							*bufptr++ += BackLeft * vol;
							*bufptr++ += BackRight * vol;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
						break;

						case 2:
						vol *= 0.5f;
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_GET_QUADPHON
							// Left, Right
							*bufptr++ += (FrontLeft + BackLeft) * vol;
							*bufptr++ += (FrontRight + BackRight) * vol;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
						break;

						default:
						vol *= 0.25f;
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_GET_QUADPHON
							// Mono
							*bufptr++ += (FrontLeft + FrontRight + BackLeft + BackRight) * vol;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
					}
					break;

					case 2:
					switch (slAudioInfo.channels)
					{
						case 6:
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_GET_STEREO
							// Front-Left, Front-Right, Center, Bass, Rear-Left, Rear-Right
							FrontLeft *= vol;
							FrontRight *= vol;
							*bufptr++ += FrontLeft;
							*bufptr++ += FrontRight;
							Center = (FrontLeft + FrontRight) * 0.5f;
							*bufptr++ += Center;
							*bufptr++ += Center;
							*bufptr++ += FrontLeft;
							*bufptr++ += FrontRight;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
						break;

						case 4:
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_GET_STEREO
							// Front-Left, Front-Right, Rear-Left, Rear-Right
							FrontLeft *= vol;
							FrontRight *= vol;
							*bufptr++ += FrontLeft;
							*bufptr++ += FrontRight;
							*bufptr++ += FrontLeft;
							*bufptr++ += FrontRight;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
						break;

						case 2:
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_GET_STEREO
							// Left, Right
							*bufptr++ += FrontLeft * vol;
							*bufptr++ += FrontRight * vol;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
						break;

						default:
						vol *= 0.5f;
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_GET_STEREO
							// Mono
							*bufptr++ += (FrontLeft + FrontRight) * vol;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
					}
					break;

					default:
					switch (slAudioInfo.channels)
					{
						case 6:
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_GET_MONO
							// Front-Left, Front-Right, Center, Bass, Rear-Left, Rear-Right
							FrontLeft *= vol;
							*bufptr++ += FrontLeft;
							*bufptr++ += FrontLeft;
							*bufptr++ += FrontLeft;
							*bufptr++ += FrontLeft;
							*bufptr++ += FrontLeft;
							*bufptr++ += FrontLeft;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
						break;

						case 4:
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_GET_MONO
							// Front-Left, Front-Right, Rear-Left, Rear-Right
							FrontLeft *= vol;
							*bufptr++ += FrontLeft;
							*bufptr++ += FrontLeft;
							*bufptr++ += FrontLeft;
							*bufptr++ += FrontLeft;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
						break;

						case 2:
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_GET_MONO
							// Left, Right
							FrontLeft *= vol;
							*bufptr++ += FrontLeft;
							*bufptr++ += FrontLeft;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
						break;

						default:
						MIX_LOOP
						{
							MIX_CHECK_INNER
							MIX_GET_MONO
							// Mono
							*bufptr++ += FrontLeft * vol;
							MIX_ADVANCE
						}
						MIX_CHECK_OUTER
					}
				}
			}
			continue;
			/// Go here if stopped playing during this rendering frame.
			ON_SOUND_FINISH:
			if (sound->onfinish) slSchedule(sound->onfinish,sound,worker);
		}
	}
	//printf("mix loop end\n");
}
void slMultiMixThreadInit (slMultiMixThread* thread)
{
	thread->buf = NULL;
	thread->len = 0;
}
void slMultiMixThreadQuit (slMultiMixThread* thread)
{
	if (thread->buf) free(thread->buf);
}
void slMultiMixStart (void* thread_data, slBU thread_data_size, void* main_userdata, slBU core_count)
{
	while (core_count--)
	{
		slMultiMixThread* thread = thread_data;
		//if (thread->len != slCustomAudioLen)
        //    slAudioBuf = realloc(slAudioBuf,
        //                         sizeof(float) * (thread->len = slCustomAudioLen));
        thread->written = false;
		thread_data += thread_data_size;
	}
}
void slMultiMixFinish (void* thread_data, slBU thread_data_size, void* main_userdata, slBU core_count)
{
	// Copy the first array instead of zeroing and adding.
    for (; core_count--; thread_data += thread_data_size)
    {
        slMultiMixThread* thread = thread_data;
        if (thread->written)
        {
    	    memcpy(slAudioBuf,thread->buf,sizeof(float) * slCustomAudioLen);
            goto ADDING;
        }
    }

    // Nothing was copied. Zero the buffer.
    memset(slAudioBuf,0,sizeof(float) * slCustomAudioLen);
    return;

    ADDING:
	for (; core_count--; thread_data += thread_data_size)
	{
		slMultiMixThread* thread = thread_data;
        if (thread->written)
		    for (slBU cur = 0; cur < slCustomAudioLen; cur++)
                slAudioBuf[cur] += thread->buf[cur];
	}
}
slBulkJob* slMultiMixJob;
inline slForceInline void slInitMultiMix ()
{
	slBulkJobDef def;
	def.thread_userdata_size = sizeof(slMultiMixThread);
	def.thread_work = slMultiMixProc;
	def.main_prework = slMultiMixStart;
	def.main_postwork = slMultiMixFinish;
	def.init_thread_userdata = slMultiMixThreadInit;
	def.quit_thread_userdata = slMultiMixThreadQuit;
	slMultiMixJob = slCreateBulkJob(def);
	//printf("multimix intialised\n");
}
inline slForceInline void slQuitMultiMix ()
{
	slDestroyBulkJob(slMultiMixJob);
}
#define slAudioConvertTemplate(func_name,out_type,expr)\
void func_name (slBU start, slBU end, out_type* streamptr)\
{\
	/*printf("start: %llu , end: %llu\n",start,end);\
	*/\
	float* bufptr = slAudioBuf + start;\
	streamptr += start;\
	for (; start < end; start++)\
	{\
		float sample = *bufptr++;\
		*streamptr++ = (expr);\
	}\
}
slAudioConvertTemplate(slAudioCopyFloat,	float,	sample 					)
slAudioConvertTemplate(slAudioConvertS32,	Sint32, sample * 0x7FFFFFFF 	)
slAudioConvertTemplate(slAudioConvertS16,	Sint16,	sample * 0x7FFF 		)
slAudioConvertTemplate(slAudioConvertS8,	Sint8,	sample * 0x7F			)
slAudioConvertTemplate(slAudioConvertU16,	Uint16,	(sample + 1) * 0x7FFF	)
slAudioConvertTemplate(slAudioConvertU8,	Uint8,	(sample + 1) * 0x7F		)









void slFindMaxProc (slBU start, slBU count, float* thread_maxvalue, void* unused_main_userdata, slWorker* worker)
{
	float worst = 0;
	float* bufptr = slAudioBuf;
	for (slBU i = 0; i < count; i++)
	{
		float sample = *bufptr++;
		worst = fmaxf(worst,fabsf(sample));
	}
	*thread_maxvalue = worst;
}
void slFindMaxFinish (void* thread_data, slBU thread_data_size, float* max_out, slBU core_count)
{
	// There will always be at least 1 for core_count, and it makes sense to simply assign the first value instead of zeroing and adding.
	float highest = *(float*)thread_data;

	while (--core_count)
	{
		float thread_maxvalue = *(float*)(thread_data += thread_data_size);
		highest = fmaxf(highest,thread_maxvalue);
	}

	*max_out = highest;
}
slBulkJob* slFindMaxJob;
inline slForceInline void slInitFindMax ()
{
	slBulkJobDef def;
	def.thread_userdata_size = sizeof(float);
	def.thread_work = slFindMaxProc;
	def.main_prework = NULL;
	def.main_postwork = slFindMaxFinish;
	def.init_thread_userdata = NULL;
	def.quit_thread_userdata = NULL;
	slFindMaxJob = slCreateBulkJob(def);
	//printf("findmax intialised\n");
}
inline slForceInline void slQuitFindMax ()
{
	slDestroyBulkJob(slFindMaxJob);
}













float slPrevAudioScaling = 0;
void slAudioMultiplyRamp (slBU start, slBU end, float* factor_in)
{
	float ramp_target = *factor_in;
	float step = (ramp_target - slPrevAudioScaling) / slCustomAudioLen;
	float ramp = slPrevAudioScaling + start * step;
	//printf("start: %llu , end: %llu\n",start,end);
	float* bufptr = slAudioBuf + start;
	float factor = *factor_in;
	for (; start < end; start++, ramp += step) *bufptr++ *= ramp;
}
/*void slAudioMultiply (slBU start, slBU end, float* factor_in)
{
	//printf("start: %llu , end: %llu\n",start,end);
	float* bufptr = slAudioBuf + start;
	float factor = *factor_in;
	for (; start < end; start++) *bufptr++ *= factor;
}*/
extern bool slWindowFocused;
void slMixAudio (void* unused_userdata, Uint8* stream, int len)
{
	/*Uint8 samplesize;
	switch (slAudioInfo.format)
	{
		case AUDIO_F32:
		samplesize = sizeof(float);
		break;
		case AUDIO_S32:
		samplesize = sizeof(Sint32);
		break;
		case AUDIO_S16:
		samplesize = sizeof(Sint16);
		break;
		case AUDIO_S8:
		samplesize = sizeof(Sint8);
		break;
		case AUDIO_U16:
		samplesize = sizeof(Uint16);
		break;
		default:
		samplesize = sizeof(Uint8);
	}*/
	slBU samplecount = slAudioInfo.samples * slAudioInfo.channels;//len / samplesize;
	//slAudioInfo.samples = samplecount / slAudioInfo.channels;
	if (slWindowFocused || !slMuteWhileAway)
	{
		if (slCustomAudioLen != samplecount) slAudioBuf = realloc(slAudioBuf,sizeof(float) * (slCustomAudioLen = samplecount));
		//printf("actual postwork address: %llX\n",slMultiMixFinish);
		slExecuteBulkJob(slMultiMixJob,slSounds.itemcount,NULL);
		if (_slCustomMixStage_) _slCustomMixStage_(slAudioBuf,slAudioInfo.samples,slAudioInfo.channels,1. / slAudioInfo.freq);

		// Scale by master volume, and also normalize.
		float* bufptr = slAudioBuf;
		float worst;
		slExecuteBulkJob(slFindMaxJob,samplecount,&worst,10000);
		float scale_by = slMasterVolume;
		if (worst > 1) scale_by /= worst; // It's too loud, scale by a smaller value to compensate.
		slDoSimpleWork(samplecount,slAudioMultiplyRamp,&scale_by,10000);
		slPrevAudioScaling = scale_by;
		//printf("peak: %.2f    scaling by %.4f\n",worst,scale_by);

		switch (slAudioInfo.format)
		{
			case AUDIO_F32:
			slDoSimpleWork(samplecount,slAudioCopyFloat,stream,10000);
			break;

			case AUDIO_S32:
			slDoSimpleWork(samplecount,slAudioConvertS32,stream,10000);
			break;

			case AUDIO_S16:
			slDoSimpleWork(samplecount,slAudioConvertS16,stream,10000);
			break;

			case AUDIO_S8:
			slDoSimpleWork(samplecount,slAudioConvertS8,stream,10000);
			break;

			case AUDIO_U16:
			slDoSimpleWork(samplecount,slAudioConvertU16,stream,10000);
			break;

			default: // AUDIO_U8
			slDoSimpleWork(samplecount,slAudioConvertU8,stream,10000);
		}
	}
	else
	{
		slSoundsSilentAdvance(slAudioInfo.samples / (float)slAudioInfo.freq);
		memset(stream,0,len);//samplesize * samplecount);
	}
}
bool slAudioOpen = false;
void slCloseAudio ()
{
	if (slAudioOpen) SDL_CloseAudio();
	slAudioOpen = false;
}
void slOpenAudio (bool enabled, slScalar buffer_length, Uint8 channels, slBU rate, Uint32 format)
{
	slCloseAudio();
	if (enabled)
	{
		SDL_AudioSpec req;
		memset(&req,0,sizeof(req));
		req.freq = rate;
		req.format = format;
		req.channels = channels;

		int requested_samples = req.freq * buffer_length;
		req.samples = 1;
		while (req.samples < requested_samples && req.samples < 0x8000) req.samples <<= 1;



		req.callback = slMixAudio;
		if (!SDL_OpenAudio(&req,
			#ifdef _WIN32
			NULL
			#else
			&slAudioInfo
			#endif
		))
		{
			#ifdef _WIN32
			slAudioInfo = req;
			#endif

			slAudioOpen = true;
			SDL_PauseAudio(0);
			//printf("got %d channels, %d Hz, %d samples per frame\n",(int)slAudioInfo.channels,(int)slAudioInfo.freq,(int)slAudioInfo.samples);
		}
	}
}
void slPlaySound (slSound* sound)
{
	sound->cursample = 0;
	sound->cursubsample = 0;
	sound->playing = true;
}


slGC_Action slClearDeadSoundsAction;
slGC_Action slSoundSourcesGCAction;
slGC_Action slShrinkSoundsListAction;

namespace sl_internal
{

void slAudioInit ()
{
	slClearDeadSoundsAction.Hook(slClearDeadSounds);//,NULL,"clr dead sounds");
	slSoundSourcesGCAction.Hook(slSoundSourcesGC);//,NULL,"sound sources GC");

	slSoundSources.HookAutoShrink();
	slShrinkSoundsListAction.Hook(slShrinkSoundsList);//,NULL,"shrink sounds list");

	slInitMultiMix();
	slInitFindMax();
}
void slAudioQuit ()
{
	slCloseAudio();
	if (slAudioBuf) free(slAudioBuf);
	slQuitFindMax();
	slQuitMultiMix();
	slSoundSourceUsedHooks.Clear(NULL);
	slSounds.UntilEmpty(slDestroySound);
	slSoundAllocator.FreeAllBlocks();
	slSoundSources.UntilEmpty(slFreeSoundSource);
}
void slAudioStep ()
{
	if (!slAudioOpen)
	{
		SDL_LockAudio();
		slSoundsSilentAdvance(slGetDelta());
		SDL_UnlockAudio();
	}
}

}

void slSetMuteWhileAway (bool to)
{
	slMuteWhileAway = to;
}
bool slGetMuteWhileAway ()
{
	return slMuteWhileAway;
}
void slSetMasterVolume (slScalar to)
{
	slMasterVolume = to;
}
slScalar slGetMasterVolume ()
{
	return slMasterVolume;
}
