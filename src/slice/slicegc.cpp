#include <slice.h>
slScalar slGC_Interval = 10;
slScalar slGC_PerActionInterval;
slList slGC_Hooks("Slice: GC Hooks",slNoIndex,false);
slBU slGC_ActionID = 0;
slScalar slGC_Elapsed = 0;
SDL_mutex* slGC_HookingMutex;
void slGC_Action::Hook (void (*action_func) (void* arg), void* arg = NULL, char* name)
{
	this->action_func = action_func;
	this->arg = arg;
	this->name = name;
	SDL_LockMutex(slGC_HookingMutex);
	slGC_Hooks.Add(this);
	slGC_PerActionInterval = slGC_Interval / slGC_Hooks.itemcount;
	SDL_UnlockMutex(slGC_HookingMutex);
}
void slGC_Action::Unhook ()
{
	SDL_LockMutex(slGC_HookingMutex);
	slGC_Hooks.Remove(this);
	if (slGC_Hooks.itemcount) slGC_PerActionInterval = slGC_Interval / slGC_Hooks.itemcount;
	SDL_UnlockMutex(slGC_HookingMutex);
}

/*void slSetCustomGCEveryFrame (bool to)
{
	slCustomGCEveryFrame = to;
}*/
void slGC_SetInterval (slScalar to)
{
	slGC_Interval = to;
	if (slGC_Hooks.itemcount) slGC_PerActionInterval = slGC_Interval / slGC_Hooks.itemcount;
}


namespace sl_internal
{

void slGC_Step ()
{
	SDL_LockMutex(slGC_HookingMutex);
	if (!slGC_Hooks.itemcount) goto NO_GC; // Nothing to do!
	slGC_Elapsed += slGetDelta();
	if (slGC_Elapsed >= slGC_PerActionInterval)
	{
		slGC_Elapsed = 0;
		if (slGC_ActionID >= slGC_Hooks.itemcount) slGC_ActionID = 0;
		slGC_Action* action = *(slGC_Hooks.items + slGC_ActionID++);
		//printf("Action \"%s\": ",action->name);
		action->action_func(action->arg);
		//printf("OK\n");
	}
	NO_GC: // Skip to here instead of returning immediately, so that the mutex doesn't just stay locked forever.
	SDL_UnlockMutex(slGC_HookingMutex);
}
void slCollectAllGarbageNow ()
{
	SDL_LockMutex(slGC_HookingMutex);
	/// Run all of the garbage collection actions immediately, because the OS said it is running out of memory!
	for (slBU cur = 0; cur < slGC_Hooks.itemcount; cur++)
	{
		slGC_Action* action = *(slGC_Hooks.items + cur);
		action->action_func(action->arg);
	}
	SDL_UnlockMutex(slGC_HookingMutex);
}
void slInitGC ()
{

	slGC_HookingMutex = SDL_CreateMutex();


}
void slQuitGC ()
{

	SDL_DestroyMutex(slGC_HookingMutex);
	slGC_Hooks.Clear(NULL); // don't free the hooks anymore, they aren't slice-owned

}

}
