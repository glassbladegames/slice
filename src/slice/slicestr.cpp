#include <slice.h>
slBU slStrLen (char* str)
{
	if (str) return strlen(str);
	return 0;

	/*slBU len = 0;
	char cc;
	while (true)
	{
		cc = *(str + len);
		if (cc) len++;
		else return len;
	}*/
}
/*bool slStrEq (char* str1, char* str2)
{
	if (!str1 && !str2) return true;
	if (!str1 || !str2) return false;
	//return !strcmp(str1,str2);
	slBU len1 = slStrLen(str1);
	slBU len2 = slStrLen(str2);
	if (len1 != len2) return false;
	for (len2 = 0; len2 < len1; len2++) if (*(str1 + len2) != *(str2 + len2)) return false;
	return true;
}*/
bool slStrEq (char* str1, char* str2)
{
	if (str1 == str2) return true; // Same location in memory, including both NULL.
	if (str1 && str2)
	{
		while (true)
		{
			char cc1 = *str1++;
			if (cc1 != *str2++) return false; // Different characters, including one '\0' and one non '\0'.
			if (cc1) continue; // If this isn't the end of these strings, keep looking.
			return true; // Both characters indicate the end.
		}
	}
	else // One string is NULL and one is not.
		// Ensure compatibility with existing but empty strings, i.e. strings just containing a '\0' with nothing before it.
		return !*(str1 ? str1 : str2); // Return whether the first character of the non-NULL string is '\0'.
		// One string or the other will exist so this will not be an invalid dereference.
}
char* slStrClone (char* from)
{
	//if (!from) from = "";
	if (!from) return NULL;
	if (!*from) return NULL;
	slBU len = slStrLen(from) + 1;
	char* out = malloc(len);
	memcpy(out,from,len);
	return out;
}
char* slStrAlwaysClone (char* from)
{
    int bufsize = strlen(from) + 1;
    char* out = malloc(bufsize);
    memcpy(out,from,bufsize);
    return out;
}
char* slStrConcat (char* first, char* second)
{
	if (first && second)
	{
		slBU firstlen = slStrLen(first);
		slBU secondlen = slStrLen(second);
		char* out = malloc(firstlen + secondlen + 1);
		memcpy(out,first,firstlen);
		memcpy(out + firstlen,second,secondlen);
		*(out + firstlen + secondlen) = '\0';
		return out;
	}
	else if (first) return slStrClone(first);
	else if (second) return slStrClone(second);
	else return NULL;
}
char* slStrAlwaysConcat (char* str1, char* str2)
{
    int len1 = strlen(str1);
    int len2 = strlen(str2);
    int outlen = len1 + len2;
    char* outstr = malloc(outlen + 1);
    outstr[outlen] = 0;
    memcpy(outstr, str1, len1);
    memcpy(outstr + len1, str2, len2);
    return outstr;
}
char* slStrChopLast (char* str)
{
	if (!str) return NULL;
	slBU len = slStrLen(str);
	if (len < 2) return NULL;
	char* out = malloc(len);
	len--;
	memcpy(out,str,len);
	*(out + len) = '\0';
	return out;
}
char* slStrChopFirst (char* str)
{
	if (!str) return NULL;
	slBU len = slStrLen(str);
	if (len < 2) return NULL;
	char* out = malloc(len);
	memcpy(out,str + 1,len);
	return out;
}
void slStrChopLast_Replace (char** str)
{
	char* out = slStrChopLast(*str);
	free(*str);
	*str = out;
}
void slStrChopFirst_Replace (char** str)
{
	char* out = slStrChopFirst(*str);
	free(*str);
	*str = out;
}
char* slStrAppendAfter (char* str, char cc)
{
	char* out;
	if (!str)
	{
		out = malloc(2);
		*out = cc;
		*(out + 1) = '\0';
		return out;
	}
	slBU len = slStrLen(str);
	out = malloc(len + 2);
	memcpy(out,str,len);
	*(out + (len + 1)) = '\0';
	*(out + len) = cc;
	return out;
}
char* slStrAppendBefore (char* str, char cc)
{
	char* out;
	if (!str)
	{
		out = malloc(2);
		*out = cc;
		*(out + 1) = '\0';
		return out;
	}
	slBU len = slStrLen(str);
	out = malloc(len + 2);
	memcpy(out + 1,str,len + 1);
	*out = cc;
	return out;
}
void slStrAppendAfter_Replace (char** str, char cc)
{
	char* out = slStrAppendAfter(*str,cc);
	free(*str);
	*str = out;
}
void slStrAppendBefore_Replace (char** str, char cc)
{
	char* out = slStrAppendBefore(*str,cc);
	free(*str);
	*str = out;
}
const char slLowerCaseOffset = 'a' - 'A';
void slStrMakeLower (char* str)
{
	slBU len = slStrLen(str);
	while (len--)
	{
		if (*str >= 'A' && *str <= 'Z') *str += slLowerCaseOffset;
		str++;
	}
}
void slStrMakeUpper (char* str)
{
	slBU len = slStrLen(str);
	while (len--)
	{
		if (*str >= 'a' && *str <= 'z') *str -= slLowerCaseOffset;
		str++;
	}
}
slBU slScanHexU_Fast (char* str)
{
	slBU len = slStrLen(str);
	slBU out = 0;
	while (len--)
	{
		out <<= 4;
		char cc = *str++;
		if (cc >= 'a') out |= cc - ('a' - 10);
		else if (cc >= 'A') out |= cc - ('A' - 10);
		else out |= cc - '0';
	}
	return out;
}
slBS slScanHexS_Fast (char* str)
{
	if (!str) return 0;
	//if (!*str) return 0; // Empty null-terminated string. Don't need to check, b/c ('\0' != '-') and ('\0' != '+').
	bool negative = *str == '-';
	if (negative || *str == '+') str++;
	slBS out = slScanHexU_Fast(str);
	if (negative) return -out;
	return out;
}
slBU slScanDecU_Fast (char* str)
{
	slBU len = slStrLen(str);
	slBU out = 0;
	while (len--)
	{
		out *= 10;
		out += *str++ - '0';
	}
	return out;
}
slBS slScanDecS_Fast (char* str)
{
	if (!str) return 0;
	//if (!*str) return 0; // Empty null-terminated string. Don't need to check, b/c ('\0' != '-') and ('\0' != '+').
	bool negative = *str == '-';
	if (negative || *str == '+') str++;
	slBS out = slScanDecU_Fast(str);
	if (negative) return -out;
	return out;
}
slScalar slScanDecF_Fast (char* str)
{
	if (!str) return 0;
	//if (!*str) return 0; // Empty null-terminated string. Don't need to check, b/c ('\0' != '-') and ('\0' != '+').

	slBU len = slStrLen(str);

	bool sig_neg = *str == '-';
	if (sig_neg || *str == '+') str++, len--;
	slBU sig = 0;
	char cc;
	while (len)
	{
		len--;
		cc = *str++;
		if (cc == 'e' || cc == 'E') break;
		//if (cc == ',') continue; // Don't Europeans use this character as a dot?
		if (cc == '.' /* || cc == ',' */) break;
		sig *= 10;
		sig += cc - '0';
	}
	slScalar value = sig;
	if (sig_neg) value = -value;

	if (!len) return value;
	if (cc != '.') goto EXPONENT;
	{
		slBU fract = 0;
		slBU fract_len = 0;
		while (len)
		{
			len--;
			cc = *str++;
			if (cc == 'e' || cc == 'E') break;
			fract *= 10;
			fract += cc - '0';
			fract_len++;
		}
		value += fract * slpow(0.1,fract_len);

		if (!len) return value;
	}
	EXPONENT:
	bool exp_neg = *str == '-';
	if (exp_neg || *str == '+') str++, len--;
	slBU exp = 0;
	while (len--)
	{
		exp *= 10;
		exp += *(str++) - '0';
	}

	return value * slpow(exp_neg ? 0.1 : 10,exp);
}
bool slScanHexU (char* str, slBU* putin)
{
	if (!str) return true; // Empty string, non-extant.
	slBU len = slStrLen(str);
	if (!len) return true; // Empty string, null-terminated.
	slBU out = 0;
	while (len--)
	{
		out <<= 4;
		char cc = *str++;
		if (cc >= 'a' && cc <= 'f') out |= cc - ('a' - 10);
		else if (cc >= 'A' && cc <= 'F') out |= cc - ('A' - 10);
		else if (cc >= '0' && cc <= '9') out |= cc - '0';
		else return true;
	}
	*putin = out;
	return false;
}
bool slScanHexS (char* str, slBS* putin)
{
	if (!str) return true; // Empty string, non-extant.
	bool negative = *str == '-';
	if (negative || *str == '+') str++;
	slBU out;
	bool status = slScanHexU(str,&out);
	if (negative) *putin = -out;
	else *putin = out;
	return status;
}
bool slScanDecU (char* str, slBU* putin)
{
	if (!str) return true; // Empty string, non-extant.
	slBU len = slStrLen(str);
	if (!len) return true; // Empty string, null-terminated.
	slBU out = 0;
	while (len--)
	{
		out *= 10;
		char cc = *str++;
		if (cc < '0' || cc > '9') return true;
		out += cc - '0';
	}
	*putin = out;
	return false;
}
bool slScanDecS (char* str, slBS* putin)
{
	if (!str) return true; // Empty string, non-extant.
	bool negative = *str == '-';
	if (negative || *str == '+') str++;
	slBU out;
	bool status = slScanDecU(str,&out);
	if (negative) *putin = -out;
	else *putin = out;
	return status;
}
bool slScanDecF (char* str, slScalar* putin)
{
	if (!str) return true; // Empty string, non-extant.
	slBU len = slStrLen(str);
	if (!len) return true; // Empty string, null-terminated.

	bool sig_neg = *str == '-';
	if (sig_neg || *str == '+') str++, len--;
	slBU sig = 0;
	char cc;
	while (len)
	{
		len--;
		cc = *(str++);
		if (cc == 'e' || cc == 'E') break;
		//if (cc == ',') continue; // Don't Europeans use this character as a dot?
		if (cc == '.' /* || cc == ',' */) break;
		if (cc < '0' || cc > '9') return true;
		sig *= 10;
		sig += cc - '0';
	}
	slScalar value = sig;
	if (sig_neg) value = -value;

	if (!len)
	{
		*putin = value;
		return false;
	}
	if (cc != '.') goto EXPONENT;
	{
		slBU fract = 0;
		slBU fract_len = 0;
		while (len)
		{
			len--;
			cc = *str++;
			if (cc == 'e' || cc == 'E') break;
			if (cc < '0' || cc > '9') return true;
			fract *= 10;
			fract += cc - '0';
			fract_len++;
		}
		value += fract * slpow(0.1,fract_len);

		if (!len)
		{
			*putin = value;
			return false;
		}
	}
	EXPONENT:
	bool exp_neg = *str == '-';
	if (exp_neg || *str == '+') str++, len--;
	slBU exp = 0;
	while (len--)
	{
		char cc = *str++;
		if (cc < '0' || cc > '9') return true;
		exp *= 10;
		exp += cc - '0';
	}

	*putin = value * slpow(exp_neg ? 0.1 : 10,exp);
	return false;
}
bool slGetFileSegment (FILE* file, char** into, char separator)
{
	*into = NULL;
	slBU outsize = 0;
	slBU outlen = 0;
	bool more;
	while (true)
	{
		int cc = fgetc(file);
		if (cc == separator)
		{
			more = true;
			break;
		}
		if (cc == EOF)
		{
			more = false;
			break;
		}
		slBU idx = outlen++;
		if (outlen > outsize) *into = realloc(*into,outsize += 16);
		*(*into + idx) = cc;
	}
	if (*into) *(*into + outlen) = '\0';
	return more;
}
