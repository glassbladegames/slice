#include <slice.h>
void txtBlitRGBA (Uint8* dst, slBU dst_width, Uint8* src, slBU start_x, slBU start_y, slBU src_width, slBU src_height, SDL_Color modulate)
{
	dst_width *= 4; // 4 bytes per pixel
	start_x *= 4;
	for (slBU y = 0; y < src_height; y++)
	{
		Uint8* dstptr = dst + (start_y++ * dst_width) + start_x;
		for (slBU x = 0; x < src_width; x++)
		{
			*dstptr++ = (*src++ * modulate.r) / 255;
			*dstptr++ = (*src++ * modulate.g) / 255;
			*dstptr++ = (*src++ * modulate.b) / 255;
			*dstptr++ = (*src++ * modulate.a) / 255;
		}
	}
}
/*
struct txtFormatPiece
{
	char* text;
	SDL_Color color;
	slFont* font;
};
struct txtFormatLine
{
	slList textures; // list of slTexture*
	slBU maxheight;
};
slTexture* txtRenderFormatPieces (slList pieces)
{
	if (!pieces.itemcount) return NULL;
	slList lines = slListInit("txtRenderFormatPieces: lines",slNoIndex,false); // list of txtFormatLine*
	for (slBU cur = 0; cur < pieces.itemcount; cur++)
	{


	/// Ask Slice to render the text in solid white, and we will apply color by modulation instead of re-rendering.
	/// This has the potential to save memory and processing power if the same multi-line text is requested to be
	/// rendered in various different colors.

	slList lines = slListInit("txtRenderFormatPieces: lines",slNoIndex,false);
	slBU cur = 0;
	slBU start = 0;
	slBU lastheight = 0;
	slBU totalheight = 0;
	slBU openspace = 0;
	slBU maxwidth = 0;
	slTexture* linetex;
	while (true)
	{
		char cc = *(text + cur);
		if (cc == '\n' || cc == '\0')
		{
			slBU linelen = cur - start;
			if (linelen)
			{
				/// For empty lines before this one:
				totalheight += openspace;
				openspace = 0;

				char* line = malloc(linelen + 1);
				memcpy(line,text + start,linelen);
				*(line + linelen) = '\0';
				linetex = slRenderText(line,{255,255,255,255},font);
				free(line);
				slListAdd(&lines,linetex);
				lastheight = linetex->surf->h;
				totalheight += lastheight;
				if (linetex->surf->w > maxwidth) maxwidth = linetex->surf->w;
			}
			else
			{
				slListAdd(&lines,NULL);
				openspace += lastheight;
			}
			start = cur + 1;
			if (!cc) break;
		}
		cur++;
	}
	/// Now Render
	Uint8* colordata = calloc(maxwidth * totalheight * 4,1);
	totalheight = 0;
	lastheight = 0;
	openspace = 0;
	for (cur = 0; cur < lines.itemcount; cur++)
	{
		linetex = *(lines.items + cur);
		if (linetex)
		{
			/// For empty lines before this one:
			totalheight += openspace;
			openspace = 0;

			slBU xpos;
			if (align)
			{
				xpos = maxwidth - linetex->surf->w;
				if (align == txtAlignCenter) xpos >>= 1;
			}
			else xpos = 0;
			SDL_Rect out_rect;
			out_rect.x = xpos;
			out_rect.y = totalheight;
			out_rect.w = linetex->surf->w;
			out_rect.h = linetex->surf->h;
			txtBlitRGBA(colordata,maxwidth,linetex->surf->pixels,xpos,totalheight,linetex->surf->w,linetex->surf->h,color);

			/// Keep track of total height (here used as Y-offset).
			totalheight += linetex->surf->h;
			lastheight = linetex->surf->h;
		}
		else openspace += lastheight;
	}
	slListClear(&lines,NULL);

	slTexture* out = slCreateCustomTexture(maxwidth,totalheight,colordata,true);
	free(colordata);
	return out;
}*/
slTexRef txtRenderMultiline (char* text, Uint8 align, SDL_Color color, slFont* font)
{
	if (!text) return slNoTexture;

	/// Ask Slice to render the text in solid white, and we will apply color by modulation instead of re-rendering.
	/// This has the potential to save memory and processing power if the same multi-line text is requested to be
	/// rendered in various different colors.

	slList lines = slList("txtRenderMultiline: lines",slNoIndex);
	slBU cur = 0;
	slBU start = 0;
	slBU lastheight = 0;
	slBU totalheight = 0;
	slBU openspace = 0;
	slBU maxwidth = 0;
	SDL_Surface* linesurf;
	while (true)
	{
		char cc = *(text + cur);
		if (cc == '\n' || cc == '\0')
		{
			slBU linelen = cur - start;
			if (linelen)
			{
				/// For empty lines before this one:
				totalheight += openspace;
				openspace = 0;

				char* line = malloc(linelen + 1);
				memcpy(line,text + start,linelen);
				*(line + linelen) = '\0';
				linesurf = slConvertSurface_RGBA32_Free(slRenderText_Surf(line,color,font));
				free(line);
				if (linesurf)
				{
					lastheight = linesurf->h;
					totalheight += lastheight;
					if (linesurf->w > maxwidth) maxwidth = linesurf->w;
				}
			}
			else
			{
				linesurf = NULL;
				openspace += lastheight;
			}
			lines.Add(linesurf);
			start = cur + 1;
			if (!cc) break;
		}
		cur++;
	}
	/// Now Render
	Uint8* colordata = calloc(maxwidth * totalheight * 4,1);
	totalheight = 0;
	lastheight = 0;
	openspace = 0;
	for (cur = 0; cur < lines.itemcount; cur++)
	{
		linesurf = *(lines.items + cur);
		if (linesurf)
		{
			/// For empty lines before this one:
			totalheight += openspace;
			openspace = 0;

			slBU xpos;
			if (align)
			{
				xpos = maxwidth - linesurf->w;
				if (align == txtAlignCenter) xpos >>= 1;
			}
			else xpos = 0;
			SDL_Rect out_rect;
			out_rect.x = xpos;
			out_rect.y = totalheight;
			out_rect.w = linesurf->w;
			out_rect.h = linesurf->h;
			txtBlitRGBA(colordata,maxwidth,linesurf->pixels,xpos,totalheight,linesurf->w,linesurf->h,color);

			/// Keep track of total height (here used as Y-offset).
			totalheight += linesurf->h;
			lastheight = linesurf->h;
		}
		else openspace += lastheight;
	}
	lines.Clear(SDL_FreeSurface);

	slTexRef out = slCreateCustomTexture(maxwidth,totalheight,colordata);
	free(colordata);
	return out;
}
