#include <slice.h>

bool SkipLogo;
void OnSkipLogo () { SkipLogo = true; }
#define interpolate(initial,final,progress) ((initial * (1 - progress)) + (final * progress))
void SliceLogo ()
{
	slTexRef text_texture = slRenderText("SLICE");
	slBox* text = slCreateBox(text_texture);
	slScalar text_h = 0.1;
	slScalar half_text_h = text_h * 0.5;
	slSetBoxDims(text,0,0.5 - half_text_h,1,text_h,50);

	slBox* left = slCreateBox();
	left->backcolor = {26,171,234,255};
	slSetBoxDims(left,-.5,0.5 - half_text_h,0.5,text_h,40);

	slBox* right = slCreateBox();
	right->backcolor = {255,167,106,255};
	slSetBoxDims(right,1,0.5 - half_text_h,0.5,text_h,40);

	slScalar gap = half_text_h + 0.01;
	slScalar topbottomheight = 0.5 - gap;
	slBox* top = slCreateBox();
	top->backcolor = {255,246,86,255};
	slSetBoxDims(top,0,0,1,topbottomheight,40);

	slBox* bottom = slCreateBox();
	bottom->backcolor = {240,66,72,255};
	slSetBoxDims(bottom,0,1 - topbottomheight,1,topbottomheight,40);

	slBox* engine = slCreateBox(slRenderText("GAME ENGINE"));
	slSetBoxDims(engine,0,0.6,1,text_h * 0.66,50);
	engine->drawmask.a = 0;

	SkipLogo = false;
	slScalar elapsed = 0;

    slScalar text_aspect = text_texture->WaitDimsValid();

	while (elapsed < 4 && !slGetExitReq() && !SkipLogo)
	{
		elapsed += slGetDelta();

		slScalar text_w = text_h * (text_aspect * 1.35) / slGetWindowAspect();
		slScalar text_L = 0.5 - text_w * 0.5;
		slSetBoxDims(text,text_L,0.45,text_w,0.1,50);
		if (elapsed > 1)
		{
			left->xy.x = -0.5 + text_L;
			right->xy.x = 1 - text_L;
			top->xy.y = 0.42;
			top->wh.h = 0.02;
			bottom->wh.h = 0.02;
			if (elapsed > 3)
			{
				Uint8 opacity = slClamp255(1 - (elapsed - 3));
				text->drawmask.a = opacity;
				left->backcolor.a = opacity;
				right->backcolor.a = opacity;
				top->backcolor.a = opacity;
				bottom->backcolor.a = opacity;
				engine->drawmask.a = opacity;
			}
			else if (elapsed > 1)
			{
				if (elapsed < 1.5) engine->drawmask.a = slClamp255((elapsed - 1) * 2);
				else engine->drawmask.a = 255;
			}
		}
		else
		{
			left->xy.x = -0.5 + elapsed * text_L;
			right->xy.x = 1 - elapsed * text_L;
			top->xy.y = elapsed * 0.42;
			top->wh.h = 0.44 - top->xy.y;
			bottom->wh.h = top->wh.h;
			//slSetBoxDims(text,interpolate(0,text_L,elapsed),0.45,interpolate(1,text_w,elapsed),0.1,50);
		}
		slCycle();
	}
	//return;
	slDestroyBox(text);
	slDestroyBox(left);
	slDestroyBox(right);
	slDestroyBox(top);
	slDestroyBox(bottom);
	slDestroyBox(engine);
}
void SimpleLogo (char* imgpath, char* caption)
{
	slTexRef image_texture = slLoadTexture(imgpath);
	slTexRef text_texture = slRenderText(caption);
	slBox* logo = slCreateBox(image_texture);
	logo->texbordercolor = {255,255,255,255};

	slBox* text = slCreateBox(text_texture);
	text->tex_wh.h = 0.5; // Make the text less huge by restricting its height.
	text->tex_align_y = slAlignTop; // Make the text float to the top of its box.
	//text->bordercolor = {191,31,255,255}; // Debugging: show box outline.
	//text->texbordercolor = {255,223,31,255}; // Debugging: show texture outline.

	SkipLogo = false;
	//A:
	slScalar elapsed = 0;

    slScalar logo_aspect = image_texture->WaitDimsValid();
    slScalar text_aspect = text_texture->WaitDimsValid();

	while (elapsed < 4)
	{
		slScalar true_aspect = slGetWindowAspect() / logo_aspect;

		slScalar logo_w;
		if (true_aspect > 1) logo_w = 0.5 / true_aspect;
		else logo_w = 0.5;

		slScalar logo_h = (logo_w / logo_aspect) * slGetWindowAspect();
		slScalar logo_bottom = 0.5 + logo_h * 0.5;

		slSetBoxDims(logo,0.5 - logo_w * 0.5,0.5 - logo_h * 0.5,logo_w,logo_h,50);
		slSetBoxDims(text,0,logo_bottom + logo_h * 0.025,logo_w,fminl(logo_w * (slGetWindowAspect() / text_aspect),0.9 - logo_bottom),50);
		text->xy.x = 0.5 - text->wh.w * 0.5;

		Uint8 opacity;
		if (elapsed >= 3) opacity = slClamp255(1 - (elapsed - 3));
		else if (elapsed < 1) opacity = slClamp255(elapsed);
		else opacity = 255;

		logo->drawmask.a = opacity;
		/*if (logo->texref)*/ logo->texbordercolor.a = opacity;
		/*else logo->bordercolor.a = opacity;*/
		text->drawmask.a = opacity;

		slCycle();
		if (slGetExitReq()) break;
		if (SkipLogo) break;
		elapsed += slGetDelta();
	}
	//if (elapsed >= 4) goto A;

	slDestroyBox(logo);
	slDestroyBox(text);
}
