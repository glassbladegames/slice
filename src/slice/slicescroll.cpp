#include <slice.h>
slList scrScrolls("SliceScroll: Scrolls",offsetof(scrScroll,_index_),false);
slBlockAllocator scrScrollAllocator ("SliceScroll: Scroll Allocator",sizeof(scrScroll),8);
#define scrGetItemsHeight(scroll) ((1. / scroll->itemsshown) * scroll->boxes.itemcount)
#define scrShouldShowBar(scroll) (scroll->boxes.itemcount > scroll->itemsshown)
void scrScroll::ApplyOffset ()
{
	slScalar itemheight = container->wh.h / itemsshown;
	slScalar yvalue = container->xy.y;
	for (slBU cur = 0; cur < boxes.itemcount; cur++)
	{
		slBox* box = *(boxes.items + cur);
		box->visible = cur >= offset && cur < offset + itemsshown;
		if (box->visible)
		{
			box->xy.y = yvalue;
			yvalue += itemheight;
			box->visible = visi;
		}
		else box->visible = false;
	}
	bool overfilled = boxes.itemcount > itemsshown;
	bool showbar = (overfilled || !autohidebar) && visi;
	upbutton->visible = showbar;
	downbutton->visible = showbar;
	bar->mark->visible = showbar;
	bar->behind->visible = showbar;
	if (overfilled) bar->SetValue(bar->maxvalue - (offset + (offset / (bar->maxvalue - 0.99)) * 0.99));
	else bar->mark->xy.y = bar->behind->xy.y;
	if (update) update(this);
}
void scrScrollChange (slSlider* slider)
{
	scrScroll* scroll = slider->userdata;
	if (scroll->boxes.itemcount > scroll->itemsshown)
	{
		scroll->offset = scroll->bar->curvalue;
		scroll->ApplyOffset();
	}
}
void scrScroll::ScrollUp ()
{
	if (offset)
	{
		offset--;
		ApplyOffset();
	}
}
void scrScroll::ScrollDown ()
{
	if (boxes.itemcount > offset + itemsshown)
	{
		offset++;
		ApplyOffset();
	}
}
void scrOnUpClick (slBox* box)
{
	scrScroll* scroll = box->userdata;
	scroll->ScrollUp();
}
void scrOnDownClick (slBox* box)
{
	scrScroll* scroll = box->userdata;
	scroll->ScrollDown();
}
void scrScroll::Reposition ()
{
	slScalar updownheight = barwidth * 2;
	slSetBoxDims(
		upbutton,
		container->xy.x + (container->wh.w - barwidth),
		container->xy.y,
		barwidth,
		updownheight,
		container->z - 1
	);
	slSetBoxDims(
		downbutton,
		upbutton->xy.x,
		container->xy.y + (container->wh.h - updownheight),
		barwidth,
		updownheight,
		upbutton->z
	);
	slSetBoxDims(
		bar->behind,
		upbutton->xy.x,
		container->xy.y + updownheight,
		barwidth,
		container->wh.h - updownheight * 2,
		container->z
	);
	slSetBoxDims(
		bar->mark,
		upbutton->xy.x,
		bar->behind->xy.y,
		barwidth,
		bar->behind->wh.h,
		upbutton->z
	);
	if (boxes.itemcount > itemsshown) bar->mark->wh.h /= scrGetItemsHeight(this);

	slScalar item_w = container->wh.w;
	if (boxes.itemcount > itemsshown || !autohidebar) item_w -= barwidth;
	slScalar item_h = container->wh.h / itemsshown;
	for (slBU cur = 0; cur < boxes.itemcount; cur++)
	{
		slBox* box = *(boxes.items + cur);
		slSetBoxDims(
			box,
			container->xy.x,
			0,
			item_w,
			item_h,
			upbutton->z
		);
	}

	ApplyOffset();
}
scrScroll* scrCreateScroll (slBox* container, slBU itemsshown, slScalar barwidth = 0.02)
{
	//scrScroll* out = malloc(sizeof(scrScroll));
	scrScroll* out = scrScrollAllocator.Allocate();

	scrScrolls.Add(out);

	out->boxes = slList("scrCreateScroll: out->boxes",slNoIndex,true);
	out->itemsshown = itemsshown;
	out->container = container;
	out->barwidth = barwidth;
	out->update = NULL;
	out->offset = 0;
	out->visi = true;

	container->backcolor = {255,255,255,255};
	container->bordercolor = {127,127,127,255};

	slBox* barbox = slCreateBox();
	barbox->maintainaspect = container->maintainaspect;
	barbox->hoverable = true;
	barbox->backcolor = {63,63,63,255};
	barbox->hoverbackcolor = {47,47,47,255};
	barbox->hoverable = true;

	slBox* barback = slCreateBox();
	barback->maintainaspect = container->maintainaspect;
	barback->backcolor = {127,127,127,255};

	out->bar = slCreateSlider(barback,barbox,true);
	out->bar->userdata = out;
	out->bar->onchange = scrScrollChange;
	out->bar->minvalue = 0;
	out->bar->maxvalue = 0.99;

	out->upbutton = slCreateBox(slLoadTexture("tick-up.png"));
	out->upbutton->onclick = scrOnUpClick;
	out->upbutton->userdata = out;
	out->upbutton->backcolor = {79,79,79,255};
	out->upbutton->hoverbackcolor = {87,87,87,255};
	out->upbutton->hoverable = true;

	out->downbutton = slCreateBox(slLoadTexture("tick-down.png"));
	out->downbutton->onclick = scrOnDownClick;
	out->downbutton->userdata = out;
	out->downbutton->backcolor = {79,79,79,255};
	out->downbutton->hoverbackcolor = {87,87,87,255};
	out->downbutton->hoverable = true;

	out->Reposition();
	return out;
}
void scrScroll::SetVisible (bool visible)
{
	this->visi = visible;

	bool show_bars = (boxes.itemcount > itemsshown) && visi;
	upbutton->visible = show_bars;
	downbutton->visible = show_bars;
	bar->behind->visible = show_bars;
	bar->mark->visible = show_bars;
	container->visible = visi;
	slBU cur = 0;
	if (visi)
	{
		while (cur < boxes.itemcount)
		{
			((slBox*)*(boxes.items + cur))->visible = cur >= offset && cur < offset + itemsshown;
			cur++;
		}
	}
	else while (cur < boxes.itemcount) ((slBox*)*(boxes.items + cur++))->visible = false;
	if (update) update(this);
}
void scrDestroyScroll (scrScroll* scroll)
{
	scroll->boxes.Clear(slDestroyBox);
	slDestroySlider(scroll->bar);
	slDestroyBox(scroll->container);
	slDestroyBox(scroll->upbutton);
	slDestroyBox(scroll->downbutton);
	scrScrolls.Remove(scroll);
	//free(scroll);
	scrScrollAllocator.Release(scroll);
}
/*slBU scrScroll::GetRow (slBox* box)
{
	for (slBU cur = 0; cur < boxes.itemcount; cur++) if (*(boxes.items + cur) == box) return cur;
	return 0;
}*/
void scrScroll::ScrollTo (slBU row)
{
	if (offset > row) offset = row;
	else if (offset + itemsshown <= row) offset = row - (itemsshown - 1);
	ApplyOffset();
}
void scrScroll::AddBox (slBox* box)
{
	if (autohidebar && boxes.itemcount == itemsshown)
	{
		//printf("shrinking\n");
		for (slBU cur = 0; cur < boxes.itemcount; cur++)
		{
			slBox* toshrink = *(boxes.items + cur);
			toshrink->wh.w -= barwidth;
		}
	}
	boxes.Add(box);
	slSetBoxDims(box,container->xy.x,0,container->wh.w,container->wh.h / itemsshown,container->z - 1);
	if (boxes.itemcount > itemsshown)
	{
		bar->mark->wh.h = bar->behind->wh.h / scrGetItemsHeight(this);
		bar->maxvalue = (boxes.itemcount - itemsshown) + 0.99;
	}
	else if (autohidebar) goto DONTMINUSW;
	box->wh.w -= barwidth;
	DONTMINUSW:
	ApplyOffset();
}
void scrScroll::RemoveBox (slBox* box)
{
	boxes.Remove(box);
	slDestroyBox(box);
	if (boxes.itemcount > itemsshown)
	{
		bar->mark->wh.h = bar->behind->wh.h / scrGetItemsHeight(this);
		bar->maxvalue = (boxes.itemcount - itemsshown) + 0.99;
		if (offset + itemsshown > boxes.itemcount) offset = boxes.itemcount - itemsshown;
	}
	else
	{
		bar->mark->wh.h = bar->behind->wh.h;
		bar->maxvalue = 0;
		offset = 0;
	}
	if (autohidebar && boxes.itemcount == itemsshown)
	{
		//printf("expanding\n");
		for (slBU cur = 0; cur < boxes.itemcount; cur++)
		{
			slBox* toexpand = *(boxes.items + cur);
			toexpand->wh.w += barwidth;
		}
	}
	ApplyOffset();
}



bool scrScrollsInOrder (scrScroll* a, scrScroll* b)
{
	return a->container->z <= b->container->z;
}
//extern slList slBoxes;
//bool slBoxesInOrder (slBox* box1, slBox* box2);

namespace sl_internal
{
	scrScroll* scrGetHoveredScroll (slVec2 mousepos)
	{
		slSort(&scrScrolls,scrScrollsInOrder);
		//slSort(&slBoxes,slBoxesInOrder);
		//slBU lookboxpos = slBoxes.itemcount - 1;
		for (slBU cur = 0; cur < scrScrolls.itemcount; cur++)
		{
			scrScroll* scroll = scrScrolls.items[cur];
			//printf("-");
			if (scroll->visi)
			{
				//printf(">");
				slBox* scrollbox = scroll->container;
				/*slBU scrollboxpos = scrollbox->_index_;
				for (; lookboxpos > scrollboxpos; lookboxpos--)
				{
					slBox* lookbox = slBoxes.items[lookboxpos];
					if (lookbox->visible && lookbox->onclick)
						if (slPointOnBox(lookbox,mousepos))
						{
							//printf("box in the way\n");
							return NULL;
						}
				}*/
				if (slPointOnBox(scrollbox,mousepos))
				{
					//printf("scroll found\n");
					return scroll;
				}
			}
		}
		//printf("no scroll\n");
		return NULL;
	}
	void scrInit ()
	{

	}
	void scrQuit ()
	{
		scrScrolls.UntilEmpty(scrDestroyScroll);
		scrScrollAllocator.FreeAllBlocks();
	}
}
