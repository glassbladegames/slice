#include <slice.h>

slList slKeyBinds("Slice: Key Binds",slNoIndex,false); // Autoshrink off for this *and* no GC action to shrink it, because this list NEVER gets things removed!
slBlockAllocator slKeyBindAllocator ("Slice: Key Bind Allocator",sizeof(slKeyBind),32);
slList slKeyBindListeners = slList("slKeyBindListeners",slNoIndex); // Call these functions on any newly created slKeyBind.
slKeyBind* slGetKeyBind (char* name, slKeyInfo dflt)
{
	slKeyBind* out;
	for (slBU cur = 0; cur < slKeyBinds.itemcount; cur++)
	{
		out = *(slKeyBinds.items + cur);
		if (slStrEq(out->name,name)) return out;
	}
	//out = malloc(sizeof(slKeyBind));
	out = slKeyBindAllocator.Allocate();

	out->onpress = NULL;
	out->onrelease = NULL;
	out->_info_ = dflt;
	out->name = slStrClone(name);
	out->down = false;
	slKeyBinds.Add(out);

	// Notify any listeners.
	for (slBU pos = 0; pos < slKeyBindListeners.itemcount; pos++)
	{
		void (*on_event) (slKeyBind* bind, bool is_new) = slKeyBindListeners.items[pos];
		on_event(out,true);
	}

	return out;
}

void slListenKeyBinds (void (*on_event) (slKeyBind* bind, bool is_new))
{
	// Call it on all existing binds.
	for (slBU pos = 0; pos < slKeyBinds.itemcount; pos++)
		on_event(slKeyBinds.items[pos],true);

	// Register it to be called for any further binds.
	slKeyBindListeners.Add(on_event);
}
void slUnlistenKeyBinds (void (*on_event) (slKeyBind* bind, bool is_new))
{
	slKeyBindListeners.Remove(on_event);
}

inline bool slKeyInfoEq (slKeyInfo info1, slKeyInfo info2)
{
	if (info1.type != info2.type) return false;
	switch (info1.type)
	{
		case slKeyInfo_KeyCode:
		return info1.id.keycode == info2.id.keycode;
		case slKeyInfo_MouseButton:
		return info1.id.mousebutton == info2.id.mousebutton;
		default:
		return info1.id.wheelaction == info2.id.wheelaction;
	}
}
bool slBoundToSame (slKeyBind* bind0, slKeyBind* bind1)
{
	return slKeyInfoEq(bind0->_info_,bind1->_info_);
}
void sl_internal::slCheckBoundKeysDown (slKeyInfo event)
{
	for (slBU cur = 0; cur < slKeyBinds.itemcount; cur++)
	{
		slKeyBind* item = *(slKeyBinds.items + cur);
		if (slKeyInfoEq(event,item->_info_))
		{
			if (item->_info_.type != slKeyInfo_WheelAction) item->down = true;
			if (item->onpress) item->onpress(item);
		}
	}
}
void sl_internal::slCheckBoundKeysUp (slKeyInfo event)
{
	for (slBU cur = 0; cur < slKeyBinds.itemcount; cur++)
	{
		slKeyBind* item = *(slKeyBinds.items + cur);
		if (slKeyInfoEq(event,item->_info_) && item->down) // If Slice didn't know key was already pressed, don't fire un-pressing callback.
		{
			item->down = false;
			if (item->onrelease) item->onrelease(item);
		}
	}
}
char* slDescribeKeyInfo (slKeyInfo info)
{
	char* out;
	switch (info.type)
	{
		case slKeyInfo_Unbound:
		out = "Unbound";
		break;
		case slKeyInfo_WheelAction:
		out = info.id.wheelaction == slWheelForward ? "Scroll Up" : "Scroll Down";
		break;
		case slKeyInfo_KeyCode:
		out = SDL_GetKeyName(info.id.keycode);
		if (!*out) out = "Key \"?\"";
		break;
		case slKeyInfo_MouseButton:
		asprintf(&out,"Mouse %d",(int)info.id.mousebutton);
		goto DONT_COPY;
	}
	out = slStrClone(out);
	DONT_COPY:
	return out;
}
void slDeallocKeyBind (slKeyBind* item)
{
	if (item->name) free(item->name);
	//free(item);
	slKeyBindAllocator.Release(item);
}

void slKeyBind::SetInfo (slKeyInfo info)
{
	_info_ = info;

	// Notify any listeners.
	for (slBU pos = 0; pos < slKeyBindListeners.itemcount; pos++)
	{
		void (*on_event) (slKeyBind* bind, bool is_new) = slKeyBindListeners.items[pos];
		on_event(this,false);
	}
}

namespace sl_internal
{


void slLoadKeys (FILE* keysfile)
{
	Uint32 keycount;
	fread(&keycount,sizeof(Uint32),1,keysfile);
	char* name = malloc(64);
	size_t name_size = 63;
	while (keycount--)
	{
		Uint32 namelen;
		fread(&namelen,sizeof(Uint32),1,keysfile);
		if (namelen > name_size) name = realloc(name,(name_size = namelen) + 1);
		*(name + namelen) = '\0';
		fread(name,1,namelen,keysfile);
		slKeyInfo info;
		fread(&info.type,sizeof(Uint8),1,keysfile);
		switch (info.type)
		{
			case slKeyInfo_KeyCode:
			fread(&info.id.keycode,sizeof(Uint32),1,keysfile);
			break;
			case slKeyInfo_MouseButton:
			fread(&info.id.mousebutton,sizeof(Uint8),1,keysfile);
			break;
			case slKeyInfo_WheelAction:
			fread(&info.id.wheelaction,sizeof(Uint8),1,keysfile);
			// Unbound does not read anything extra.
		}
		slGetKeyBind(name,info);
	}
	free(name);
}
void slSaveKeys (FILE* keysfile)
{
	Uint32 bindcount = slKeyBinds.itemcount;
	fwrite(&bindcount,sizeof(Uint32),1,keysfile);
	for (bindcount = 0; bindcount < slKeyBinds.itemcount; bindcount++)
	{
		slKeyBind* item = *(slKeyBinds.items + bindcount);
		Uint32 namelen = slStrLen(item->name);
		fwrite(&namelen,sizeof(Uint32),1,keysfile);
		fwrite(item->name,1,namelen,keysfile);
		fwrite(&item->_info_.type,sizeof(Uint8),1,keysfile);
		switch (item->_info_.type)
		{
			case slKeyInfo_KeyCode:
			fwrite(&item->_info_.id.keycode,sizeof(Uint32),1,keysfile);
			break;
			case slKeyInfo_MouseButton:
			fwrite(&item->_info_.id.mousebutton,sizeof(Uint8),1,keysfile);
			break;
			case slKeyInfo_WheelAction:
			fwrite(&item->_info_.id.wheelaction,sizeof(Uint8),1,keysfile);
			// Unbound does not write anything extra.
		}
	}
}
void slKeysQuit ()
{
	slKeyBinds.Clear(slDeallocKeyBind);
	slKeyBindAllocator.FreeAllBlocks();
	slKeyBindListeners.Clear(NULL);
}

}
