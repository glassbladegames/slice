#include <slice.h>



class slWorkersPoolProto : public slConsumerQueue
{
public:
    slWorkersPoolProto () : slConsumerQueue(offsetof(slWorkerCall,next),SDL_GetCPUCount()) {}
private:
    void ItemFunc (void* item) override
    {
        slWorkerCall* call = item;
        call->WorkFunc();
        delete call;
    }
};
slWorkersPoolProto* slWorkersPool;
#define slWorkerCount (slWorkersPool->GetConsumerCount())
int slGetWorkerCount () { return slWorkerCount; }

slWorkerTask::slWorkerTask ()
{
    completion = SDL_CreateSemaphore(1);
}
slWorkerTask::~slWorkerTask ()
{
    SDL_SemWait(completion);
    SDL_DestroySemaphore(completion);
}

slWorkerCall::slWorkerCall (slWorkerTask* task)
{
    this->task = task;
    if (!SDL_AtomicAdd(&task->in_progress,1))
        SDL_SemWait(task->completion); // Subtract currently-posted semaphore.
}
void slWorkerCall::Send ()
{
    slWorkersPool->Push(this);
}
slWorkerCall::~slWorkerCall ()
{
    if (SDL_AtomicDecRef(&task->in_progress))
        SDL_SemPost(task->completion);
}



struct slSchedCall
{
	void (*func) (void*);
	void* arg;
    inline slSchedCall (void (*func) (void* arg), void* arg) slForceInline
    {
        this->func = func;
        this->arg = arg;
    }
    slSchedCall* next;
};

slBU slWorkerQueueAllocSize = 64;
void slSchedule (void (*func) (void* arg), void* arg, slAsyncNonblockingQueue* queue)
{
    queue->Push(new slSchedCall(func,arg));
}
void slFlushSchedules (slAsyncNonblockingQueue* queue)
{
    while (slSchedCall* call = queue->Get())
    {
        call->func(call->arg);
        delete call;
    }
}
slBU slCeil16 (slBU size)
{
	Uint8 overhang = size & 0xF;
	if (overhang) size += 0x10 - overhang;
	return size;
}
//slList slBulkJobs = slListInit("Slice Bulk Jobs",slNoIndex);
slBulkJob* slCreateBulkJob (slBulkJobDef def)
{
	def.thread_userdata_size = slCeil16(def.thread_userdata_size);
	slBU thread_data_count = slWorkerCount;
	slBulkJob* job = malloc(sizeof(slBulkJob) + def.thread_userdata_size * thread_data_count);
	//slListAdd(&slBulkJobs,job);
	job->def = def;
	if (def.init_thread_userdata)
	{
		void* udata = (void*)job + sizeof(slBulkJob);
		for (slBU cur = 0; cur < thread_data_count; cur++, udata += def.thread_userdata_size)
			def.init_thread_userdata(udata);
	}
	return job;
}
void slDestroyBulkJob (slBulkJob* job)
{
	if (job->def.quit_thread_userdata)
	{
		slBU thread_data_count = slWorkerCount;
		void* udata = (void*)job + sizeof(slBulkJob);
		for (slBU cur = 0; cur < thread_data_count; cur++, udata += job->def.thread_userdata_size)
			job->def.quit_thread_userdata(udata);
	}
	//slListRemove(&slBulkJobs,job);
	free(job);
}



struct slListWorkCall : slWorkerCall
{
    void (*peritem) (void* item, void* userdata, slWorker* worker);
    void* userdata;
    void** items;
	slBU itemcount;
    slAsyncNonblockingQueue* scheduled_calls;

    inline slListWorkCall (slWorkerTask* task,
                           void (*peritem) (void* item, void* userdata, slWorker* worker),
                           void* userdata,
                           void** items, slBU itemcount,
                           slAsyncNonblockingQueue* scheduled_calls) slForceInline
        : slWorkerCall(task)
    {
        this->peritem = peritem;
        this->userdata = userdata;
        this->items = items;
        this->itemcount = itemcount;
        this->scheduled_calls = scheduled_calls;
        Send();
    }

    void WorkFunc () override
    {
        while (itemcount--)
            peritem(*items++, userdata, scheduled_calls);
    }
};
struct slSimpleWorkCall : slWorkerCall
{
    void (*work_func) (slBU start, slBU end, void* userdata, slWorker* worker);
    void* userdata;
    slBU start;
	slBU itemcount;
    slAsyncNonblockingQueue* scheduled_calls;

    inline slSimpleWorkCall (slWorkerTask* task,
                             void (*work_func) (slBU start, slBU end, void* userdata, slWorker* worker),
                             void* userdata,
                             slBU start, slBU itemcount,
                             slAsyncNonblockingQueue* scheduled_calls) slForceInline
        : slWorkerCall(task)
    {
        this->work_func = work_func;
        this->userdata = userdata;
        this->start = start;
        this->itemcount = itemcount;
        this->scheduled_calls = scheduled_calls;
        Send();
    }

    void WorkFunc () override
    {
		work_func(start, start + itemcount, userdata, scheduled_calls);
    }
};
struct slArrayWorkCall : slWorkerCall
{
    void (*peritem) (void* item, void* userdata, slWorker* worker);
    void* userdata;
    void* array;
	slBU itemcount;
	slBU item_size; // Size of array items.
    slAsyncNonblockingQueue* scheduled_calls;

    inline slArrayWorkCall (slWorkerTask* task,
                            void (*peritem) (void* item, void* userdata, slWorker* worker),
                            void* userdata,
                            void* array, slBU itemcount, slBU item_size,
                            slAsyncNonblockingQueue* scheduled_calls) slForceInline
        : slWorkerCall(task)
    {
        this->peritem = peritem;
        this->userdata = userdata;
        this->array = array;
        this->itemcount = itemcount;
        this->item_size = item_size;
        this->scheduled_calls = scheduled_calls;
        Send();
    }

    void WorkFunc () override
    {
        while (itemcount--)
        {
            peritem(array, userdata, scheduled_calls);
            array += item_size;
        }
    }
};
struct slJobWorkCall : slWorkerCall
{
    slBulkJob* job;
    void* userdata;
    slBU start;
	slBU itemcount;
    slAsyncNonblockingQueue* scheduled_calls;
    int thread_id;

    inline slJobWorkCall (slWorkerTask* task,
                          slBulkJob* job, void* userdata,
                          slBU start, slBU itemcount,
                          slAsyncNonblockingQueue* scheduled_calls,
                          int thread_id) slForceInline
        : slWorkerCall(task)
    {
        this->job = job;
        this->userdata = userdata;
        this->start = start;
        this->itemcount = itemcount;
        this->scheduled_calls = scheduled_calls;
        this->thread_id = thread_id;
        Send();
    }

    void WorkFunc () override
    {
        void* thread_userdata =   (void*)job
                                + sizeof(slBulkJob)
                                +     job->def.thread_userdata_size
                                    * thread_id;
        job->def.thread_work(start,itemcount,thread_userdata,userdata,scheduled_calls);
    }
};



namespace sl_internal
{
    void slWorkersInit ()
    {
        (slWorkersPool = new slWorkersPoolProto)->Start();
    }
    void slWorkersQuit ()
    {
        delete slWorkersPool->Stop();
    }
}

void slExecuteBulkJob (slBulkJob* job, slBU count, void* main_userdata, slBU threading_threshold)
{
	//if (!count) return;
	// Always do the start & finish functions.

	void* all_userdata = (void*)job + sizeof(slBulkJob);

	if (job->def.main_prework)
        job->def.main_prework(
            all_userdata,
            job->def.thread_userdata_size,
            main_userdata,
            slWorkerCount
        );

    slAsyncNonblockingQueue scheduled_calls(offsetof(slSchedCall,next));

    /* Destructor call barrier. (task) */
    {
        slWorkerTask task;

    	slBU perthread = count / slWorkerCount;
        slBU extra = count - perthread * slWorkerCount;
        slBU offset = 0;
    	for (slBU cur = 0; cur < slWorkerCount; cur++)
    	{
            slBU herecount = perthread;
            if (cur < extra) herecount++;
            //if (!herecount) break;
        	// Always execute the job function.

            new slJobWorkCall(
                &task,
                job, main_userdata,
                offset, herecount,
                &scheduled_calls,
                cur
            );

            offset += herecount;
    	}
    }

	slFlushSchedules(&scheduled_calls); // Do stuff the workers have scheduled to happen upon completion.

	if (job->def.main_postwork)
        job->def.main_postwork(
            all_userdata,
            job->def.thread_userdata_size,
            main_userdata,
            slWorkerCount
        );
}
void slDoArrayWork (void* array, slBU count, slBU item_size, void (*peritem) (void* item, void* userdata, slWorker* worker), void* userdata, slBU threading_threshold)
{
	if (!count) return;

    slAsyncNonblockingQueue scheduled_calls(offsetof(slSchedCall,next));

    /* Destructor call barrier. (task) */
    {
        slWorkerTask task;

    	slBU perthread = count / slWorkerCount;
        slBU extra = count - perthread * slWorkerCount;
        slBU offset = 0;
    	for (slBU cur = 0; cur < slWorkerCount; cur++)
    	{
            slBU herecount = perthread;
            if (cur < extra) herecount++;
            if (!herecount) break;

            new slArrayWorkCall(
                &task,
                peritem, userdata,
                array + item_size * offset, herecount, item_size,
                &scheduled_calls
            );

            offset += herecount;
    	}
    }

	slFlushSchedules(&scheduled_calls); // Do stuff the workers have scheduled to happen upon completion.
}
void slDoSimpleWork (slBU count, void (*work_func) (slBU start, slBU end, void* userdata, slWorker* worker), void* userdata, slBU threading_threshold = 16)
{
	if (!count) return;

    slAsyncNonblockingQueue scheduled_calls(offsetof(slSchedCall,next));

    /* Destructor call barrier. (task) */
    {
        slWorkerTask task;

    	slBU perthread = count / slWorkerCount;
        slBU extra = count - perthread * slWorkerCount;
        slBU offset = 0;
    	for (slBU cur = 0; cur < slWorkerCount; cur++)
    	{
            slBU herecount = perthread;
            if (cur < extra) herecount++;
            if (!herecount) break;

            new slSimpleWorkCall(
                &task,
                work_func, userdata,
                offset, herecount,
                &scheduled_calls
            );

            offset += herecount;
    	}
    }

	slFlushSchedules(&scheduled_calls); // Do stuff the workers have scheduled to happen upon completion.
}
void slDoWork (slList* list, void (*peritem) (void* item, void* userdata, slWorker* worker), void* userdata, slBU threading_threshold)
{
	if (!list->itemcount) return;

    slAsyncNonblockingQueue scheduled_calls(offsetof(slSchedCall,next));

    /* Destructor call barrier. (task) */
    {
        slWorkerTask task;

    	slBU perthread = list->itemcount / slWorkerCount;
        slBU extra = list->itemcount - perthread * slWorkerCount;
        slBU offset = 0;
    	for (slBU cur = 0; cur < slWorkerCount; cur++)
    	{
            slBU herecount = perthread;
            if (cur < extra) herecount++;
            if (!herecount) break;

            new slListWorkCall(
                &task,
                peritem, userdata,
                list->items + offset, herecount,
                &scheduled_calls
            );

            offset += herecount;
    	}
    }

	slFlushSchedules(&scheduled_calls); // Do stuff the workers have scheduled to happen upon completion.
}
