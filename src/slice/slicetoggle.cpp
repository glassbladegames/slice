#include <slice.h>
void tglToggler::SetState (bool new_state)
{
	if (state != new_state)
	{
		state = new_state;
		animating = true;
		if (state)
		{
			toggle_front->backcolor = on_color;
			toggle_front->SetTexRef(on_tex);
		}
		else
		{
			toggle_front->backcolor = off_color;
			toggle_front->SetTexRef(off_tex);
		}
		if (onchange) onchange(this);
	}
}
void tglToggleToggler (slBox* box)
{
	tglToggler* toggler = box->userdata;
	tglSetToggler(toggler,!toggler->state);
}
slList tglTogglers("SliceToggle: Togglers",offsetof(tglToggler,index));
slBlockAllocator tglTogglerAllocator ("SliceToggle: Toggler Allocator",sizeof(tglToggler));
tglToggler* tglCreateToggler (char* text, slBox* parent, bool initial_state, slScalar text_ratio = 0.8, slTexRef on_tex, slTexRef off_tex, SDL_Color on_color, SDL_Color off_color)
{
	//tglToggler* out = malloc(sizeof(tglToggler));
	tglToggler* out = tglTogglerAllocator.Allocate();

	out->on_tex = on_tex.ReserveGetRaw();
	out->off_tex = off_tex.ReserveGetRaw();
	out->on_color = on_color;
	out->off_color = off_color;

	tglTogglers.Add(out);
	out->text_area = slCreateBox(slRenderText(text));
	out->toggle_back = slCreateBox();
	out->toggle_front = slCreateBox();

	slSetBoxDims(out->text_area,0,0,text_ratio,1,parent->z);
	slRelBoxDims(out->text_area,parent);
	out->text_area->tex_align_x = slAlignLeft;

	slScalar toggle_w = 1 - text_ratio;
	slScalar toggle_sub_offset = toggle_w * 0.1;
	toggle_w *= 0.8;
	slSetBoxDims(out->toggle_back,text_ratio + toggle_sub_offset,0.1,toggle_w,0.8,parent->z - 1);
	slRelBoxDims(out->toggle_back,parent);

	toggle_sub_offset += toggle_w * 0.05;
	toggle_w *= 0.9;
	slSetBoxDims(out->toggle_front,text_ratio + toggle_sub_offset,0.15,toggle_w * 0.5,0.7,parent->z - 2);
	slRelBoxDims(out->toggle_front,parent);

	out->toggle_front_left_pos = out->toggle_front->xy.x;
	out->state = initial_state;
	out->toggle_back->backcolor = {127,127,127,255};
	out->toggle_back->bordercolor = {63,63,63,255};
	if (initial_state)
	{
		out->toggle_front->xy.x += out->toggle_front->wh.w;
		out->animation_progress = 1;
		out->toggle_front->backcolor = on_color;
        out->toggle_front->SetTexRef(on_tex);
	}
	else
	{
		out->animation_progress = 0;
		out->toggle_front->backcolor = off_color;
        out->toggle_front->SetTexRef(off_tex);
	}
	out->animating = false;
	out->toggle_back->onclick = tglToggleToggler;
	out->toggle_back->userdata = out;
	out->toggle_front->onclick = tglToggleToggler;
	out->toggle_front->userdata = out;
	out->onchange = NULL;
	return out;
}
void tglDestroyToggler (tglToggler* toggler)
{
    toggler->on_tex->Abandon();
    toggler->off_tex->Abandon();

	tglTogglers.Remove(toggler);
	slDestroyBox(toggler->text_area);
	slDestroyBox(toggler->toggle_front);
	slDestroyBox(toggler->toggle_back);
	//free(toggler);
	tglTogglerAllocator.Release(toggler);
}
void tglStepToggler (tglToggler* toggler)
{
	if (toggler->animating)
	{
		slScalar rate = slGetDelta() * 3;
		if (toggler->state)
		{
			toggler->animation_progress += rate;
			if (toggler->animation_progress >= 1)
			{
				toggler->animation_progress = 1;
				toggler->animating = false;
			}
		}
		else
		{
			toggler->animation_progress -= rate;
			if (toggler->animation_progress <= 0)
			{
				toggler->animation_progress = 0;
				toggler->animating = false;
			}
		}
		toggler->toggle_front->xy.x = toggler->toggle_front_left_pos + toggler->toggle_front->wh.w * toggler->animation_progress;
	}

}
void tglStepAllTogglers ()
{
	slDoWork(&tglTogglers,tglStepToggler,NULL);
}

namespace sl_internal
{
	void tglInit ()
	{
		slHook_PreRender(tglStepAllTogglers);
	}
	void tglQuit ()
	{
		slUnhook_PreRender(tglStepAllTogglers);
		tglTogglers.UntilEmpty(tglDestroyToggler);
		tglTogglerAllocator.FreeAllBlocks();
	}
}
