#include <slice.h>

slBlockAllocator::slBlockAllocator (char* name, slBU item_size, slBU first_block_items)
{
	/// When something is not allocated, it's got a pointer value telling where the next open space is after it.
	/// So, the size of an item must be at least the size of a pointer.
	if (item_size < sizeof(void*)) item_size = sizeof(void*);

	// Can't have 0 items per block, so allow 0 to mean a default number.
	if (!first_block_items) first_block_items = slDefaultFirstBlockItems;
	// It doesn't have to be a multiple of 8 anymore with the new management technique.
	// In fact the code is simpler overall.



	this->name = name;
	this->item_size = item_size;
	this->first_block_items = first_block_items;
	blocks = NULL;
	block_count = 0;
	next_available = NULL;
}
void slBlockAllocator::CreateNewBlock ()
{
	/// Allocate new memory.
	slBU next_block_items = first_block_items << block_count;
	slBU blockcount_prev = block_count++;
	void* mem = malloc(next_block_items * item_size);



	/// Keep track of the new block, ensuring the array of blocks remains sorted.
	blocks = realloc(blocks,sizeof(void*) * block_count);
	blocks[blockcount_prev] = mem;



	/// Set up the linked list to include all of the newly allocated spaces.
	void** prev = &(next_available); // This function wouldn't be called is next_available weren't NULL.
	for (slBU i = 0; i < next_block_items; i++, mem += item_size)
	{
		*prev = mem; // Overwrite pointer value. (It goes to here.)
		prev = mem; // Set prev. (Remember that we were here.)
	}
	*prev = NULL;



	next_block_items <<= 1; // If we need to allocate more space, allocate double the amount allocated this time.
}
void* slBlockAllocator::Allocate ()
{
	if (!next_available) CreateNewBlock();
	void* available = next_available;
	next_available = *(void**)available;
	return available;
}
void slBlockAllocator::Release (void* item)
{
	*(void**)item = next_available;
	next_available = item;
}
void slBlockAllocator::ReleaseAllItems ()
{
	slBU block_items = first_block_items; // if block_count is 1, blocks[0] contains next_block_items >> 1 items.

	void** prev = &(next_available); // Similar logic to "This function wouldn't be called is next_available weren't NULL."
	for (slBU block_id = 0; block_id < block_count; block_id++, block_items <<= 1)
	{
		void* mem = blocks[block_id];

		/// Set up the linked list to include all of the newly allocated spaces.
		for (slBU i = 0; i < block_items; i++, mem += item_size)
		{
			*prev = mem; // Overwrite pointer value. (It goes to here.)
			prev = mem; // Set prev. (Remember that we were here.)
		}

		block_items <<= 1;
	}
	*prev = NULL;
}
void slBlockAllocator::FreeAllBlocks ()
{
	if (blocks)
	{
		for (slBU block_id = 0; block_id < block_count; block_id++) free(blocks[block_id]);
		free(blocks);
		blocks = NULL;
		block_count = 0;
		next_available = NULL;
	}
}
