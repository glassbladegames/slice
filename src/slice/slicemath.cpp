#include <slice.h>

SDL_Color slHSVAtoRGBA (slHSVA in)
{
	float r,g,b;
	float h = in.h * slInv255;
	float s = in.s * slInv255;
	float v = in.v * slInv255;

	float f = h * 6;
	Uint8 i = f;
	f -= i;
	float p = v * (1 - s);
	float q = v * (1 - (f * s));
	float t = v * (1 - (1 - f) * s);
	switch (i)
	{
		case 0: r = v; g = t; b = p; break;
		case 1: r = q; g = v; b = p; break;
		case 2: r = p; g = v; b = t; break;
		case 3: r = p; g = q; b = v; break;
		case 4: r = t; g = p; b = v; break;
		default: r = v; g = p; b = q;
	}
	return (SDL_Color){r * 255, g * 255, b * 255, in.a};
}
slHSVA slRGBAtoHSVA (SDL_Color rgba)
{
	slHSVA out;
	out.a = rgba.a;
	if ((out.v = rgba.r > rgba.g ? rgba.r : rgba.g) < rgba.b) out.v = rgba.b;
	if (out.v)
	{
		Uint8 diff;
		if ((diff = rgba.r < rgba.g ? rgba.r : rgba.g) > rgba.b) diff = rgba.b;
		diff = out.v - diff;
		if (diff)
		{
			out.s = diff * 255 / out.v;
			if (rgba.r == out.v) out.h = (rgba.g - rgba.b) * 42 / diff;
			else if (rgba.g == out.v) out.h = 85 + (rgba.b - rgba.r) * 42 / diff;
			else out.h = 170 + (rgba.r - rgba.g) * 42 / diff;
			return out;
		}
	}
	out.s = 0;
	out.h = 0;
	return out;
}
Uint8 slClamp255 (slScalar n)
{
	if (n < 0) return 0;
	if (n > 1) return 255;
	return n * 255;
}
extern slScalar slWindowAspect,slInvWindowAspect;
void slAspectifyPoint (slScalar* x, slScalar* y)
{
	if (x && slWindowAspect > 1) *x = ((*x - 0.5) * slWindowAspect) + 0.5;
	else if (y && slWindowAspect < 1) *y = ((*y - 0.5) * slInvWindowAspect) + 0.5;
}
void slDeAspectifyPoint (slScalar* x, slScalar* y)
{
	if (x && slWindowAspect > 1) *x = ((*x - 0.5) * slInvWindowAspect) + 0.5;
	else if (y && slWindowAspect < 1) *y = ((*y - 0.5) * slWindowAspect) + 0.5;
}
void slAspectifyPoint (slVec2* point)
{
	if (slWindowAspect > 1) point->x = ((point->x - 0.5f) * slWindowAspect) + 0.5f;
	else point->y = ((point->y - 0.5f) * slInvWindowAspect) + 0.5f;
}
void slDeAspectifyPoint (slVec2* point)
{
	if (slWindowAspect > 1) point->x = ((point->x - 0.5f) * slInvWindowAspect) + 0.5f;
	else point->y = ((point->y - 0.5f) * slWindowAspect) + 0.5f;
}


slVec2 slRotatePoint (slVec2 point, slScalar ang)
{
	slScalar cosvalue = -slDegToRad(ang);
	slScalar sinvalue = slsin(cosvalue);
			 cosvalue = slcos(cosvalue);
	return slVec2(point.x,point.y) * cosvalue + slVec2(-point.y,point.x) * sinvalue;
	/*
	return slVec2(
		point.x * cosvalue - point.y * sinvalue,
		point.y * cosvalue + point.x * sinvalue
	);
	*/
}
slVec2 slRotatePointAroundPoint (slVec2 point, slScalar ang, slVec2 center)
{
	point -= center;
	point = slRotatePoint(point,ang);
	point += center;
	return point;
}
