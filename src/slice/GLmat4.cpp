#include <slice/GLmat4.h>

#ifdef __APPLE__
void sincosf(float x, float *s, float *c)
{
	*s = sinf(x);
	*c = cosf(x);
}
#endif

void GLmat4::print ()
{
	printf(
		"\n"
		"     |\t%.3f\t%.3f\t%.3f\t%.3f\t|\n"
		"     |\t%.3f\t%.3f\t%.3f\t%.3f\t|\n"
		"     |\t%.3f\t%.3f\t%.3f\t%.3f\t|\n"
		"     |\t%.3f\t%.3f\t%.3f\t%.3f\t|\n"
		"\n",
		data[0],data[1],data[2],data[3],
		data[4],data[5],data[6],data[7],
		data[8],data[9],data[10],data[11],
		data[12],data[13],data[14],data[15]
	);
}
void GLvec4::print ()
{
	printf("     (\t%.3f,\t%.3f,\t%.3f,\t%.3f\t)\n",x,y,z,w);
}
GLmat4 GLmat4_mul (GLmat4& a, GLmat4& b)
{
	GLvec4_raw col1 = { b.data[0], b.data[4], b.data[8],  b.data[12] };
	GLvec4_raw col2 = { b.data[1], b.data[5], b.data[9],  b.data[13] };
	GLvec4_raw col3 = { b.data[2], b.data[6], b.data[10], b.data[14] };
	GLvec4_raw col4 = { b.data[3], b.data[7], b.data[11], b.data[15] };
	GLmat4 out;
	out.entire_vec = (GLvec16_raw)
	{
		GLvec4(a.row1 * col1).sum4(),	GLvec4(a.row1 * col2).sum4(),	GLvec4(a.row1 * col3).sum4(),	GLvec4(a.row1 * col4).sum4(),
		GLvec4(a.row2 * col1).sum4(),	GLvec4(a.row2 * col2).sum4(),	GLvec4(a.row2 * col3).sum4(),	GLvec4(a.row2 * col4).sum4(),
		GLvec4(a.row3 * col1).sum4(),	GLvec4(a.row3 * col2).sum4(), 	GLvec4(a.row3 * col3).sum4(), 	GLvec4(a.row3 * col4).sum4(),
		GLvec4(a.row4 * col1).sum4(),	GLvec4(a.row4 * col2).sum4(), 	GLvec4(a.row4 * col3).sum4(),	GLvec4(a.row4 * col4).sum4()
	};
	return out;
}
GLmat4 GLmat4::transpose ()
{
	GLmat4 out;
	out.entire_vec = (GLvec16_raw)
	{
		data[0],	data[4],	data[8],	data[12],
		data[1],	data[5],	data[9],	data[13],
		data[2],	data[6],	data[10],	data[14],
		data[3],	data[7],	data[11],	data[15]
	};
	return out;
}
#define GLmat4_swap(a,b) temp = data[a]; data[a] = data[b]; data[b] = temp;
void GLmat4::transpose_in_place ()
{
	GLfloat temp;
	GLmat4_swap(1,4)
	GLmat4_swap(2,8)
	GLmat4_swap(3,12)
	GLmat4_swap(6,9)
	GLmat4_swap(7,13)
	GLmat4_swap(11,14)
}
GLmat4 GLmat4_scaling (GLvec4 scalevec)
{
	GLmat4 out;
	out.entire_vec = (GLvec16_raw)
	{
		scalevec.x,		0,				0,				0,
		0,				scalevec.y,		0,				0,
		0,				0,				scalevec.z,		0,
		0,				0,				0,				scalevec.w
	};
	return out;
}
GLmat4 GLmat4_xrotate (GLfloat rads)
{
	GLmat4 out;
	/**
		Y component is not affected by X, since we're rotating around it
		Y component begins to include Z, so use cosine
		Y component stops including itself, so use sine

		Actually cosine starts at one and sine starts are zero, so invert all these
	**/
	GLfloat sinf_rads = sinf(rads);
	GLfloat cosf_rads = cosf(rads);
	out.entire_vec = (GLvec16_raw){
		1,				0,				0,				0,
		0,				cosf_rads,		-sinf_rads,		0,
		0,				sinf_rads,		cosf_rads,		0,
		0,				0,				0,				1
	};
	return out;
}
GLmat4 GLmat4_yrotate (GLfloat rads)
{
	GLmat4 out;
	/**
		X component not affected by Y since we're rotating around it.
		X component begins to include Z so use cosine
	**/
	GLfloat sinf_rads = sinf(rads);
	GLfloat cosf_rads = cosf(rads);
	out.entire_vec = (GLvec16_raw)
	{
		cosf_rads,		0,				-sinf_rads,		0,
		0,				1,				0,				0,
		sinf_rads,		0,				cosf_rads,		0,
		0,				0,				0,				1
	};
	return out;
}
GLmat4 GLmat4_zrotate (GLfloat rads)
{
	GLmat4 out;
	/**
		X not affected by Z, rotating around
		X begins to have Y so use cosine
	**/
	GLfloat sinf_rads = sinf(rads);
	GLfloat cosf_rads = cosf(rads);
	out.entire_vec = (GLvec16_raw)
	{
		cosf_rads,		-sinf_rads,		0,				0,
		sinf_rads,		cosf_rads,		0,				0,
		0,				0,				1,				0,
		0,				0,				0,				1
	};
	return out;
}
GLmat4 GLmat4_offset (GLfloat x, GLfloat y, GLfloat z)
{
	GLmat4 out;
	out.entire_vec = (GLvec16_raw)
	{
		1,				0,				0,				x,
		0,				1,				0,				y,
		0,				0,				1,				z,
		0,				0,				0,				1
	};
	return out;
}
GLmat4 GLmat4_ortho (GLfloat left, GLfloat right, GLfloat bottom, GLfloat top, GLfloat near_clip, GLfloat far_clip)
{
	GLfloat dx = left - right;
	GLfloat dy = bottom - top;
	GLfloat dz = near_clip - far_clip;
	GLmat4 out;
	out.entire_vec = (GLvec16_raw)
	{
		-2 / dx,	0,			0,			(right + left) / dx,
		0,			-2 / dy,	0,			(top + bottom) / dy,
		0,			0,			2 / dz,		(far_clip + near_clip) / dz,
		0,			0,			0,			1
	};
	return out;
}
/*GLmat4 GLmat4_ortho_depth (GLfloat near_z, GLfloat far_z)
{
	GLmat4 out;
	//GLfloat zm = 2 / (near_z - far_z);
	//GLfloat za = (far_z + near_z) / (near_z - far_z);
	out.entire_vec = (GLvec16_raw)
	{
		1,	0,	0,	0,
		0,	1,	0,	0,
		0,	0,	1,	0,//za,
		0,	0,	0,	1
	};
	return out;
}*/
slForceInline inline GLfloat cotangent (GLfloat x)
{
	GLfloat sinval,cosval;
	sincosf(x,&sinval,&cosval);
	return cosval / sinval;
}
GLmat4 GLmat4_perspective (GLfloat fov, GLfloat znear, GLfloat zfar)
{
	GLfloat f = cotangent(slDegToRad_F(fov) / 2);
	GLfloat zmul = (zfar + znear) / (znear - zfar);
	GLfloat zadd = (2 * zfar * znear) / (znear - zfar);
	GLmat4 out;
	out.entire_vec = (GLvec16_raw)
	{
		f,			0,			0,			0,
		0,			f,			0,			0,
		0,			0,			zmul,		zadd,
		0,			0,			-1,			0
	};
	return out;
}
