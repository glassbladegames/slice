#include <slice.h>



slAsyncNonblockingQueue::slAsyncNonblockingQueue (int offsetof_next)
{
    this->offsetof_next = offsetof_next;
    sync = SDL_CreateMutex();
}
slAsyncNonblockingQueue::~slAsyncNonblockingQueue ()
{
    SDL_DestroyMutex(sync);
}

void slAsyncNonblockingQueue::Push (void* item)
{
    SDL_LockMutex(sync);

    *last_ptr = item;
    last_ptr = item + offsetof_next;
    *last_ptr = NULL;

    SDL_UnlockMutex(sync);
}
void* slAsyncNonblockingQueue::Get () // Will immediately return NULL if there is nothing.
{
    SDL_LockMutex(sync);

    void* out = first;
    if (first)
    {
        // We are pulling this item out of the queue.
        void** next_ptr = first + offsetof_next;
        first = *next_ptr;

        // If we pulled out the last item, we have to reset last_ptr.
        if (!first) last_ptr = &first;
    }

    SDL_UnlockMutex(sync);

    return out;
}



slAsyncBlockingQueue::slAsyncBlockingQueue (int offsetof_next)
    : slAsyncNonblockingQueue(offsetof_next)
{
    waiting = SDL_CreateSemaphore(0);
}
slAsyncBlockingQueue::~slAsyncBlockingQueue ()
{
    SDL_DestroySemaphore(waiting);
}

void slAsyncBlockingQueue::Push (void* item)
{
    if (item) slAsyncNonblockingQueue::Push(item);
    SDL_SemPost(waiting);
}
void* slAsyncBlockingQueue::Get ()
{
    SDL_SemWait(waiting);
    return slAsyncNonblockingQueue::Get();
}



slConsumerQueue::slConsumerQueue (int offsetof_next, int consumer_count = 1)
    : slAsyncBlockingQueue(offsetof_next)
{
    this->consumer_count = consumer_count;
}
void slConsumerQueue::Start ()
{
    for (int i = 0; i < consumer_count; i++)
        SDL_DetachThread(SDL_CreateThread(Consume_Wrap,"slAsyncQueue consumer loop",this));
}
slConsumerQueue* slConsumerQueue::Stop ()
{
    shutdown_complete = SDL_CreateSemaphore(0);

    for (int i = 0; i < consumer_count; i++)
        slAsyncBlockingQueue::Push(NULL);

    SDL_SemWait(shutdown_complete);
    SDL_DestroySemaphore(shutdown_complete);

    return this;
}
slConsumerQueue::~slConsumerQueue () {}

void slConsumerQueue::Push (void* item)
{
    slAsyncBlockingQueue::Push(item);
}

void slConsumerQueue::PreLoop () {}
void slConsumerQueue::PostLoop () {}

__thread int slConsumerQueue::consumer_id;
void slConsumerQueue::Consume ()
{
    consumer_id = SDL_AtomicAdd(&next_consumer_id,1);
	srand(time(NULL) + consumer_id); // Thread local random seed.

    PreLoop();
    while (true)
    {
        void* item = slAsyncBlockingQueue::Get();
        if (!item) break;
        ItemFunc(item);
    }
    PostLoop();

    if (SDL_AtomicDecRef(&next_consumer_id)) SDL_SemPost(shutdown_complete);
}
static void slConsumerQueue::Consume_Wrap (slConsumerQueue* self)
{
    self->Consume();
}
