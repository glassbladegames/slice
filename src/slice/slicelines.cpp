#include <slice.h>
char* slLinesVertSrc = R"(#version 130

in vec2 VertPos;

uniform vec2 CamPos;
uniform vec2 InvHalfCamDims;

uniform float CamRotCos,CamRotSin;

void main ()
{
	// Generate point.
	vec2 point = VertPos;
	// Translate by camera position.
	point = point - CamPos;
    // Rotate around camera.
    point = vec2(point.x * CamRotCos - point.y * CamRotSin,point.y * CamRotCos + point.x * CamRotSin);
    // Scale by dimensions.
	gl_Position = vec4(point * InvHalfCamDims,0,1);

	/*
	// UI coords mode:
	point = point * 2 - 1; // 0,1 -> -1,1
	gl_Position = vec4(point,0,1);
	*/

	//gl_Position = vec4(VertPos,0,1);
}
)";
char* slLinesFragSrc = R"(#version 130
uniform vec4 LineColor;
void main ()
{
	gl_FragColor = LineColor;
}
)";
GLuint
	slLines_CamPos,
	slLines_InvHalfCamDims,
	slLines_CamRotCos,
	slLines_CamRotSin,
	slLines_LineColor;
slShaderProgram slLinesProgram;


void slLines_BindAttribs (GLuint program)
{
	glBindAttribLocation(program,0,"VertPos");
}

namespace sl_internal
{
	void slLinesInit ()
	{
		slLinesProgram = slCreateShaderProgram("slLines",slLinesVertSrc,slLinesFragSrc,slLines_BindAttribs);

		slLines_CamPos = slLocateUniform(slLinesProgram,"CamPos");
		slLines_InvHalfCamDims = slLocateUniform(slLinesProgram,"InvHalfCamDims");
		slLines_CamRotCos = slLocateUniform(slLinesProgram,"CamRotCos");
		slLines_CamRotSin = slLocateUniform(slLinesProgram,"CamRotSin");
		slLines_LineColor = slLocateUniform(slLinesProgram,"LineColor");

		//slHook_PreRender(PrepareAllLineGroups);
	}
	void slLinesQuit ()
	{
		slDestroyShaderProgram(slLinesProgram);
	}
}
slCoordsTransform slUICoordsTransform ()
{
	slCoordsTransform out;
	out.view_xy = 0.5;
	out.view_wh = 1;
	out.view_rotate = 0;
	return out;
}
slCoordsTransform slUIAspectCoordsTransform ()
{
	slCoordsTransform out;
	out.view_xy = 0.5;
	out.view_wh = 1;
	slScalar aspect = slGetWindowAspect();
	if (aspect > 1) // wide screen
		out.view_wh.w *= aspect;
	else // tall screen
		out.view_wh.h /= aspect;
	out.view_rotate = 0;
	return out;
}
void slLineGroup::Clear ()
{
	lines_usage = 0;
	stale_start = 0;
}
void slLineGroup::Draw (GLvec2 p0, GLvec2 p1)
{
	if (lines_usage >= lines_capacity)
	{
		lines_buf = realloc(lines_buf,sizeof(slLine) * (lines_capacity <<= 1));
	}
	slLine* line = lines_buf + lines_usage++;
	line->p0 = p0;
	line->p1 = p1;
}
void slDrawLineGroup (slLineGroup* group)
{
	if (!group->visible) return;
	glUseProgram(slLinesProgram.program);
	glBindVertexArray(group->vao);
	glBindBuffer(GL_ARRAY_BUFFER,group->glbuf);
	slBU stale_count = group->lines_usage - group->stale_start;
	if (stale_count)
	{
		const size_t linesize = sizeof(float) * 4;
		if (group->lines_usage > group->glbuf_capacity)
		{
			group->glbuf_capacity = group->lines_usage;
			glBufferData(GL_ARRAY_BUFFER,linesize * group->lines_usage,group->lines_buf,GL_DYNAMIC_DRAW);
		}
		else
		{
			glBufferSubData(GL_ARRAY_BUFFER,linesize * group->stale_start,linesize * stale_count,group->lines_buf + group->stale_start);
		}
		group->stale_start = group->lines_usage;
	}
	slUniformFromColor(slLines_LineColor,group->color);



	slCoordsTransform transform = group->get_transform();
	transform.view_wh = GLvec2(2) / transform.view_wh;
	glUniform2fv(slLines_InvHalfCamDims,1,(GLfloat*)&transform.view_wh);
	glUniform2fv(slLines_CamPos,1,(GLfloat*)&transform.view_xy);
	transform.view_rotate = slDegToRad_F(transform.view_rotate);
	GLfloat camcos = cosf(transform.view_rotate);
	GLfloat camsin = sinf(transform.view_rotate);
	glUniform1f(slLines_CamRotCos,camcos);
	glUniform1f(slLines_CamRotSin,camsin);



	glDrawArrays(GL_LINES,0,group->lines_usage * 2);
}
slLineGroup* slCreateLineGroup (Uint8 z, SDL_Color color)
{
	slLineGroup* out = malloc(sizeof(slLineGroup));
	out->lines_capacity = 16;
	out->lines_buf = malloc(sizeof(slLine) * out->lines_capacity);
	out->lines_usage = 0;
	glGenVertexArrays(1,&out->vao);
	glBindVertexArray(out->vao);
	glEnableVertexAttribArray(0);
	glGenBuffers(1,&out->glbuf);
	glBindBuffer(GL_ARRAY_BUFFER,out->glbuf);
	glVertexAttribPointer(0,2,GL_FLOAT,false,sizeof(float) * 2,0);
	out->glbuf_capacity = 0;
	out->stale_start = 0;
	out->zhook.func = slDrawLineGroup;
	out->zhook.userdata = out;
	out->zhook.z = z;
	slAddZHook(&out->zhook);
	out->color = color;
	out->visible = true;
	out->get_transform = slUICoordsTransform;
	return out;
}
void slDestroyLineGroup (slLineGroup* group)
{
	slRemoveZHook(&group->zhook);
	free(group->lines_buf);
	glDeleteVertexArrays(1,&group->vao);
	glDeleteBuffers(1,&group->glbuf);
	free(group);
}
