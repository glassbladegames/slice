#include <slice.h>
void slListExecShrink (slList* list) { list->Shrink(); }
slList::slList (char* name, slBS indexoffset, bool preserveorder)
{
	this->name = name;
	this->indexoffset = indexoffset;
	this->preserveorder = preserveorder;
	items = NULL;
	capacity = 0;
	itemcount = 0;
}
void slList::Add (void* item)
{
	#ifdef SL_LIST_INDEXED_DUPLICATE_CHECK
	if (indexoffset >= 0)
	{
		slBU prev_index = *(slBU*)(item + indexoffset);
		if (prev_index < itemcount)
		{
			if (items[prev_index] == item)
			{
				printf("<List \"%s\"> Attempted to add already-resident item!\n",name);
				return;
			}
		}
	}
	#endif

	if (itemcount >= capacity)
	{
		if (!itemcount) capacity = 16;
		else capacity <<= 1;
		items = realloc(items,sizeof(void*) * capacity);
	}
	items[itemcount] = item;
	if (indexoffset >= 0) *(slBU*)(item + indexoffset) = itemcount;
	itemcount++;
}
void slList::RemoveIndex (slBU index)
{
	#ifdef SL_LIST_INDEX_CHECK
	if (index >= itemcount)
	{
		printf("<List \"%s\"> Attempt to remove invalid index.\n",name);
		return;
	}
	#endif
	itemcount--;
	if (preserveorder)
	{
		// Shift back.
		for (slBU i = index; i < itemcount; i++)
			items[i] = items[i + 1];

		// Update all affected index values.
		if (indexoffset >= 0)
			for (slBU i = index; i < itemcount; i++)
				(*(slBU*)(items[i] + indexoffset))--;
	}
	else
	{
		// Move last item here.
		items[index] = items[itemcount];

		// Update index of moved item.
		if (indexoffset >= 0)
			*(slBU*)(items[index] + indexoffset) = index;
	}
}
void slList::Remove (void* item)
{
	slBU index;
	if (indexoffset >= 0)
	{
		index = *(slBU*)(item + indexoffset);
		#ifdef SL_LIST_INDEX_CHECK
		if (index < itemcount)
		{
			if (items[index] != item)
			{
				printf("<List \"%s\"> Item at removal index wasn't the item to remove!\n",name);
				return;
			}
		}
		#endif
	}
	else
	{
		for (index = 0; index < itemcount; index++)
			if (items[index] == item)
				goto FOUND;
		printf("<List \"%s\"> Item to remove was not found in the list.\n",name);
		return;
		FOUND:;
	}
	RemoveIndex(index);
}
bool slList::Has (void* item)
{
	if (indexoffset >= 0)
	{
		slBU index = *(slBU*)(item + indexoffset);
        if (index >= itemcount) return false;
        return items[index] == item;
	}
	else
	{
		for (slBU index = 0; index < itemcount; index++)
			if (items[index] == item)
                return true;
		return false;
	}
}
void slList::Shrink ()
{
	if (capacity)
	{
		if (itemcount)
		{
			slBU twiceitems = itemcount << 1;
			if (capacity >= twiceitems && capacity > SL_LIST_STARTING_CAPACITY)
			{
				do
				{
					capacity >>= 1;
				}
				while (capacity >= twiceitems && capacity > SL_LIST_STARTING_CAPACITY);
				items = realloc(items,sizeof(void*) * capacity);
			}
		}
		else
		{
			free(items);
			items = NULL;
			capacity = 0;
		}
	}
}
void slList::Clear (void (*clr_func) (void* item), bool free_buffer)
{
	if (capacity)
	{
		if (clr_func) for (slBU i = 0; i < itemcount; i++) clr_func(items[i]);
		itemcount = 0;
		if (free_buffer)
		{
			free(items);
			items = NULL;
			capacity = 0;
		}
	}
}
void slList::UntilEmpty (void (*clr_func) (void* item), bool free_buffer)
{
	while (itemcount) clr_func(*items);
	if (capacity && free_buffer)
	{
		free(items);
		items = NULL;
		capacity = 0;
	}
}
void slList::HookAutoShrink ()
{
	autoshrink_action.Hook(slListExecShrink,this);
}
void slList::UnhookAutoShrink ()
{
	autoshrink_action.Unhook();
}
