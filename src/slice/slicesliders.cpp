#include <slice.h>

slList slSliders("Slice: Sliders",offsetof(slSlider,_index_),false);
slBlockAllocator slSliderAllocator ("Slice: Slider Allocator",sizeof(slSlider),16);
slSlider* slActiveSlider = NULL;
void slOnSliderClick (slBox* box)
{
	slActiveSlider = box->userdata;
}
slSlider* slCreateSlider (slBox* behind, slBox* mark, bool vertical, bool isbar, bool invert)
{
	//slSlider* slider = malloc(sizeof(slSlider));
	slSlider* slider = slSliderAllocator.Allocate();
	slider->behind = behind;
	slider->mark = mark;
	slider->vertical = vertical;
	slider->isbar = isbar;
	behind->onclick = slOnSliderClick;
	behind->userdata = slider;
	mark->onclick = slOnSliderClick;
	mark->userdata = slider;
	slSliders.Add(slider);
	slider->onchange = NULL;
	slider->invert = invert;
	return slider;
}
void slDestroySlider (slSlider* slider)
{
	slDestroyBox(slider->behind);
	slDestroyBox(slider->mark);
	slSliders.Remove(slider);
	//free(slider);
	slSliderAllocator.Release(slider);
}
void slSlider::SetPos (slScalar to)
{
	slScalar fardim,closedim;
	if (vertical)
	{
		fardim = behind->xy.y + behind->wh.h;
		if (isbar)
		{
			mark->wh.h = to - mark->xy.y;
			if (!invert)
			{
				mark->wh.h += (mark->xy.y - behind->xy.y);
				mark->wh.h = behind->wh.h - mark->wh.h;
			}
			if (mark->wh.h > behind->wh.h)
			{
				mark->wh.h = behind->wh.h;
				curvalue = maxvalue;
			}
			else if (mark->wh.h < 0)
			{
				mark->wh.h = 0;
				curvalue = minvalue;
			}
			else curvalue = minvalue + ((maxvalue - minvalue) * (mark->wh.h / behind->wh.h));
			if (!invert) mark->xy.y = fardim - mark->wh.h;
		}
		else
		{
			mark->xy.y = to;
			closedim = mark->wh.h * 0.5f;
			fardim -= closedim;
			closedim += behind->xy.y;
			if (mark->xy.y > fardim)
			{
				mark->xy.y = fardim;
				curvalue = maxvalue;
			}
			else if (mark->xy.y < closedim)
			{
				mark->xy.y = closedim;
				curvalue = minvalue;
			}
			else curvalue = minvalue + ((maxvalue - minvalue) * ((mark->xy.y - closedim) / (behind->wh.h - mark->wh.h)));
			mark->xy.y -= mark->wh.h * 0.5f;
		}
		if (invert) curvalue = minvalue + (maxvalue - curvalue);
	}
	else
	{
		fardim = behind->xy.x + behind->wh.w;
		if (isbar)
		{
			mark->wh.w = to - mark->xy.x;
			if (invert)
			{
				mark->wh.w += (mark->xy.x - behind->xy.x);
				mark->wh.w = behind->wh.w - mark->wh.w;
			}
			if (mark->wh.w > behind->wh.w)
			{
				mark->wh.w = behind->wh.w;
				curvalue = maxvalue;
			}
			else if (mark->wh.w < 0)
			{
				mark->wh.w = 0;
				curvalue = minvalue;
			}
			else curvalue = minvalue + ((maxvalue - minvalue) * (mark->wh.w / behind->wh.w));
			if (invert) mark->xy.x = fardim - mark->wh.w;
		}
		else
		{
			mark->xy.x = to;
			closedim = mark->wh.w * 0.5f;
			fardim -= closedim;
			closedim += behind->xy.x;
			if (mark->xy.x > fardim)
			{
				mark->xy.x = fardim;
				curvalue = maxvalue;
			}
			else if (mark->xy.x < closedim)
			{
				mark->xy.x = closedim;
				curvalue = minvalue;
			}
			else curvalue = minvalue + ((maxvalue - minvalue) * ((mark->xy.x - closedim) / (behind->wh.w - mark->wh.w)));
			mark->xy.x -= mark->wh.w * 0.5f;
		}
		if (invert) curvalue = minvalue + (maxvalue - curvalue);
	}
}
void slSlider::SetValue (slScalar to)
{
	slScalar place = (to - minvalue) / (maxvalue - minvalue);
	if (vertical)
	{
		if (!invert) place = 1 - place;
		if (isbar) SetPos(behind->xy.y + (behind->wh.h * place));
		else SetPos((behind->xy.y + mark->wh.h * 0.5f) + ((behind->wh.h - mark->wh.h) * place));
	}
	else
	{
		if (invert) place = 1 - place;
		if (isbar) SetPos(behind->xy.x + (behind->wh.w * place));
		else SetPos((behind->xy.x + mark->wh.w * 0.5f) + ((behind->wh.w - mark->wh.w) * place));
	}
}

namespace sl_internal
{

void slSlidersMouseUp (slVec2 mouse)
{
	if (slActiveSlider)
	{
		slActiveSlider->SetPos(slActiveSlider->vertical ? mouse.y : mouse.x);
		if (slActiveSlider->onchange) slActiveSlider->onchange(slActiveSlider);
		slActiveSlider = NULL;
	}
}
void slSlidersStep (slVec2 mouse)
{
	if (slActiveSlider)
	{
		slActiveSlider->SetPos(slActiveSlider->vertical ? mouse.y : mouse.x);
		if (slActiveSlider->onchange) slActiveSlider->onchange(slActiveSlider);
	}
}




void slSlidersInit ()
{
	slSliders.HookAutoShrink();
}
void slSlidersQuit ()
{

}

}
