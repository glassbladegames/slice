#include <slice.h>
slList animAnimations("SliceAnim: Animations",offsetof(animAnimation,_index_),false);
slBlockAllocator animAllocator ("SliceAnim: Animation Allocator",sizeof(animAnimation),16);
/*bool animIsTextureUsed (slTexture* tex)
{
	for (slBU cur = 0; cur < animAnimations.itemcount; cur++)
	{
		animAnimation* anim = *(animAnimations.items + cur);
		for (slBU i = 0; i < anim->framecount; i++) if (*(anim->frames + i) == tex) return true;
	}
	return false;
}*/
void animRawTexChange (slTexture** texptr, slTexture* set_to)
{
	slTexture* was = *texptr;
	*texptr = set_to;
	if (was) was->Abandon();
	if (set_to) set_to->Reserve();
}
animAnimation* animCreateAnimation_Internal (void* reftoset, char* path_fmtstr, slBU framecount, slScalar framerate, bool ref_is_swapchain)
{
	//animAnimation* out = malloc(sizeof(animAnimation));
	animAnimation* out = animAllocator.Allocate();
	out->reftoset = reftoset;
	out->pos = 0;
	out->framerate = framerate;
	out->framecount = framecount;
	out->frames = malloc(sizeof(slTexture*) * framecount);
	for (slBU cur = 0; cur < framecount; cur++)
	{
		char* path;
		asprintf(&path,path_fmtstr,(unsigned int)cur);
		slTexRef tex = slLoadTexture(path);
		out->frames[cur] = tex.ReserveGetRaw();
		free(path);
	}
	if (framecount)
	{
		if (ref_is_swapchain) ((slTexSwapChain*)reftoset)->Queue(*out->frames);
		else animRawTexChange(reftoset,*out->frames);
	}
	out->ref_is_swapchain = ref_is_swapchain;
	animAnimations.Add(out);
	return out;
}
animAnimation* animCreateAnimation (slTexture** reftoset, char* path_fmtstr, slBU framecount, slScalar framerate)
{
	return animCreateAnimation_Internal(reftoset,path_fmtstr,framecount,framerate,false);
}
animAnimation* animCreateAnimation (slTexSwapChain* swapchain, char* path_fmtstr, slBU framecount, slScalar framerate)
{
	return animCreateAnimation_Internal(swapchain,path_fmtstr,framecount,framerate,true);
}
void animDestroyAnimation (animAnimation* anim)
{
	if (!anim->ref_is_swapchain) animRawTexChange(anim->reftoset,NULL);
	for (slBU cur = 0; cur < anim->framecount; cur++) anim->frames[cur]->Abandon();
	animAnimations.Remove(anim);
	free(anim->frames);
	animAllocator.Release(anim);
	//free(anim);
}
slScalar animDelta;
void animCycleAnimation (animAnimation* anim)
{
	if (!anim->framecount) return;
	anim->pos += animDelta;
	slBU frameid = anim->pos * anim->framerate;
	if (frameid >= anim->framecount)
	{
		frameid = 0;
		anim->pos = 0;
	}
	slTexture* set_to = *(anim->frames + frameid);
	if (anim->ref_is_swapchain) ((slTexSwapChain*)anim->reftoset)->Queue(set_to);
	else animRawTexChange(anim->reftoset,set_to);
}
void animCycleAll ()
{
	animDelta = slGetDelta();
	slDoWork(&animAnimations,animCycleAnimation,NULL);
}
namespace sl_internal
{
	void animInit ()
	{
		slHook_PreRender(animCycleAll);
		//slHook_TextureUsed(animIsTextureUsed);
	}
	void animQuit ()
	{
		slUnhook_PreRender(animCycleAll);
		//slUnhook_TextureUsed(animIsTextureUsed);
		animAnimations.UntilEmpty(animDestroyAnimation);
		animAllocator.FreeAllBlocks();
	}
}
