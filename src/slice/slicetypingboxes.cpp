#include <slice/slicetypingboxes.h>
slTypingChar::slTypingChar (char ch, SDL_Color text_color, slFont* font, slBox* scissor_box, bool sensitive)
{
	char str [4];
	if (sensitive)
	{
		str[0] = 0xE2;
		str[1] = 0x97;
		str[2] = 0x8F;
		str[3] = 0;
	}
	else
	{
		str[0] = ch;
		str[1] = 0;
	}
	slTexRef tex = slRenderText(str,{0xFF,0xFF,0xFF,0xFF},font);

	this->ch = ch;
	box = slCreateBox(tex);
	box->maintainaspect = true;
	box->scissor_box = scissor_box;
	box->drawmask = text_color;

    aspect = tex->WaitDimsValid();
}
slTypingChar::~slTypingChar ()
{
	slDestroyBox(box);
}
void slTypingText::InsertChar (slBU pos, char ch)
{
	line.insert(line.begin() + pos,new slTypingChar(ch,text_color,font,scissor_box,sensitive));
}
void slTypingText::DeleteChar (slBU pos)
{
	delete line[pos];
	line.erase(line.begin() + pos);
}
void slTypingText::DeleteRangeChars (slBU del_start, slBU del_stop)
{
	for (slBU i = del_start; i < del_stop; i++) delete line[i];
	line.erase(line.begin() + del_start, line.begin() + del_stop);
}
void slTypingText::Clear ()
{
	for (slBU i = 0; i < line.size(); i++)
		delete line[i];
	line.clear();
}
char* slTypingText::GenText ()
{
	slBU len = line.size();
	char* out = malloc(len + 1);
	for (slBU i = 0; i < len; i++) out[i] = line[i]->ch;
	out[len] = 0;
	return out;
}
char* slTypingText::GenRangeText (slBU start, slBU stop)
{
	char* out = malloc(stop - start + 1);
	slBU writepos = 0;
	for (slBU i = start; i < stop; i++) out[writepos++] = line[i]->ch;
	out[writepos] = 0;
	return out;
}
void slTypingBox::SetText (char* str)
{
	text.Clear();
    cursor_strpos = 0;
    cursor_offset = 0;
    select_strpos = 0;
    select_offset = 0;
    view_offset = 0;

	slBU line_pos = 0;
	char cc;
	while (cc = *str++)
	{
		if (cc == '\n') continue; // Skip new line, it's not valid in this type of typing box.
		else if (cc == '\t') for (int i = 0; i < 4; i++) text.InsertChar(line_pos++,' ');
		else text.InsertChar(line_pos++,cc);
	}
	UpdateCells();
}
char* slTypingBox::GenText ()
{
	return text.GenText();
}
slTypingBox::slTypingBox (slBox* container, SDL_Color text_color, slFont* font)
{
	this->container = container;
	text.text_color = text_color;
	text.font = font;
	container->onclick = OnContainerClick;
	container->userdata = this;
	container->maintainaspect = true;
	container->tex_align_x = slAlignLeft;
	caret = slCreateBox();
	caret->backcolor = text_color;
	caret->visible = false;
	caret->maintainaspect = true;
	highlight = slCreateBox();
	highlight->backcolor = {0x7F,0x7F,0x7F,0x7F};
	highlight->visible = false;
	highlight->maintainaspect = true;
	highlight->scissor_box = text.scissor_box;
	onenter = NULL;
	ontextchange = NULL;
	ontab = NULL;
	onshifttab = NULL;
	onselect = NULL;
	onabandon = NULL;

	//SetText("");
}
void slTypingBox::SetHint (char* str, SDL_Color color)
{
	if (hint_tex) hint_tex->Abandon();
	hint_tex = slRenderText(str,color,text.font).ReserveGetRaw();
	UpdateCells();
}
void slTypingBox::CheckCursorBounds ()
{
	if (cursor_offset < view_offset) view_offset = cursor_offset;
	else
	{
		slScalar right = view_offset + container->wh.w / container->wh.h;
		if (cursor_offset > right) view_offset += cursor_offset - right;
	}
}
void slTypingBox::CursorToMouse ()
{
	slScalar cursor_offset = (slGetMouse().x - container->xy.x) / (container->wh.h * container->tex_wh.h);
	slScalar remaining = cursor_offset;
	cursor_strpos = 0;
	for (int i = 0; i < text.line.size(); i++)
	{
		slBox* charbox = text.line[i]->box;
		slScalar aspect = text.line[i]->aspect;
		if (remaining > aspect * 0.5)
		{
			cursor_strpos++;
			remaining -= aspect;
		}
		else break;
	}
	cursor_offset -= remaining;
}
static void slTypingBox::OnContainerClick (slBox* box)
{
	slTypingBox* code_area = box->userdata;
	code_area->CursorToMouse();
	code_area->select_strpos = code_area->cursor_strpos;
	code_area->select_offset = code_area->cursor_offset;
	code_area->Focus();
}
void slTypingBox::ShowCursor ()
{
	caret->visible = true;
	caret_blink_elapsed = 0;
}
void slTypingBox::Focus ()
{
	slSetTypingInputCallback(OnTextInput_Wrap,this);
	focused = true;
	ShowCursor();
	UpdateCells(); // to highlight current line
	if (onselect) onselect(this);
}

void slTypingBox::HighlightStartEnd (slBU* start_strpos, slScalar* start_offset, slBU* end_strpos, slScalar* end_offset)
{
	if (cursor_strpos < select_strpos)
	{
		// cursor < select
		*start_strpos = cursor_strpos;
		*start_offset = cursor_offset;
		*end_strpos = select_strpos;
		*end_offset = select_offset;
	}
	else
	{
		// select < cursor
		*start_strpos = select_strpos;
		*start_offset = select_offset;
		*end_strpos = cursor_strpos;
		*end_offset = cursor_offset;
	}
}
void slTypingBox::SetTextColor (SDL_Color color)
{
	text.text_color = color;
	for (int i = 0; i < text.line.size(); i++)
	{
		slTypingChar* ch = text.line[i];
		ch->box->drawmask = color;
	}
}
void slTypingBox::UpdateCells ()
{
	text.scissor_box = container;

	CheckCursorBounds();

	slBU highlight_start_strpos,highlight_end_strpos;
	slScalar highlight_start_offset,highlight_end_offset;
	HighlightStartEnd(&highlight_start_strpos,&highlight_start_offset,&highlight_end_strpos,&highlight_end_offset);
	highlight->visible = highlight_start_strpos != highlight_end_strpos;
	if (highlight->visible)
	{
		highlight->xy.x = container->xy.x + (highlight_start_offset - view_offset) * container->wh.h;
		highlight->xy.y = container->xy.y;
		highlight->wh.w = (highlight_end_offset - highlight_start_offset) * container->wh.h;
		highlight->wh.h = container->wh.h;
	}

	caret->xy.x = container->xy.x + (cursor_offset - view_offset) * container->wh.h;
	caret->xy.y = container->xy.y;
	caret->wh.w = container->wh.h * caret_width;
	caret->wh.h = container->wh.h;
	caret->z = container->z - 2;

	Uint8 z = container->z - 1;
	slVec2 xy = container->xy;
	xy.x -= view_offset * container->wh.h;
	for (int i = 0; i < text.line.size(); i++)
	{
		slTypingChar* ch = text.line[i];
		slVec2 wh = slVec2(container->wh.h * ch->aspect,container->wh.h);
		ch->box->SetDims(xy,wh,z);
		xy.x += wh.w;
	}

	bool show_hint = !text.line.size() && hint_tex;// && !focused;
	container->SetTexRef(show_hint ? slTexRef(hint_tex) : slNoTexture);
}

void slTypingBox::Step ()
{
	if (focused)
	{
		caret_blink_elapsed += slGetDelta();
		while (caret_blink_elapsed > caret_blink_interval)
		{
			caret_blink_elapsed -= caret_blink_interval;
			caret->visible = !caret->visible;
		}
	}
	else caret->visible = false;
}
char* slTypingBox::GenSelectedText ()
{
	if (cursor_strpos == select_strpos) return NULL;

	slBU highlight_start_strpos,highlight_end_strpos;
	slScalar highlight_start_offset,highlight_end_offset;
	HighlightStartEnd(&highlight_start_strpos,&highlight_start_offset,&highlight_end_strpos,&highlight_end_offset);

	return text.GenRangeText(highlight_start_strpos,highlight_end_strpos);
}
bool slTypingBox::DeleteSelection ()
{
	if (cursor_strpos == select_strpos) return false;

	slBU highlight_start_strpos,highlight_end_strpos;
	slScalar highlight_start_offset,highlight_end_offset;
	HighlightStartEnd(&highlight_start_strpos,&highlight_start_offset,&highlight_end_strpos,&highlight_end_offset);

	text.DeleteRangeChars(highlight_start_strpos,highlight_end_strpos);

	cursor_offset = highlight_start_offset;
	cursor_strpos = highlight_start_strpos;

	return true;
}
void slTypingBox::PutCharAtCursor (char ch)
{
	text.InsertChar(cursor_strpos,ch);
	cursor_offset += text.line[cursor_strpos++]->aspect;
}
void slTypingBox::NoSelection ()
{
	select_strpos = cursor_strpos;
	select_offset = cursor_offset;
}
void slTypingBox::OnTextInput (int event_type, char ch)
{
	ShowCursor();
	if (event_type == slTypingEvent_Tab)
	{
		if (ch)
		{
			if (onshifttab) onshifttab(this);
		}
		else
		{
			if (ontab) ontab(this);
		}
	}
	else if (event_type == slTypingEvent_Enter)
	{
		if (onenter) onenter(this);
		/// TO DO: fire text submitted callback
	}
	else if (event_type == slTypingEvent_Character)
	{
		DeleteSelection();

		PutCharAtCursor(ch);

		NoSelection();

		UpdateCells();

		if (ontextchange) ontextchange(this);
	}
	else if (event_type == slTypingEvent_Backspace)
	{
		if (!DeleteSelection())
		{
			if (cursor_strpos)
			{
				// Remove character.
				text.DeleteChar(--cursor_strpos);
				cursor_offset -= text.line[cursor_strpos]->aspect;
			}
			else return;
		}

		NoSelection();

		UpdateCells();

		if (ontextchange) ontextchange(this);
	}
	else if (event_type == slTypingEvent_Delete)
	{
		if (!DeleteSelection())
		{
			if (cursor_strpos < text.line.size()) text.DeleteChar(cursor_strpos);
			else return;
		}

		NoSelection();

		UpdateCells();

		if (ontextchange) ontextchange(this);
	}
	else if (event_type == slTypingEvent_LeftArrow)
	{
		if (cursor_strpos)
		{
			cursor_strpos--;
			cursor_offset -= text.line[cursor_strpos]->aspect;
		}
		else return;

		if (!ch) NoSelection(); // shift key

		UpdateCells();
	}
	else if (event_type == slTypingEvent_RightArrow)
	{
		if (cursor_strpos < text.line.size())
			cursor_offset += text.line[cursor_strpos++]->aspect;
		else return;

		if (!ch) NoSelection(); // shift key

		UpdateCells();
	}
	else if (event_type == slTypingEvent_Home)
	{
		cursor_strpos = 0;
		cursor_offset = 0;

		if (!ch) NoSelection(); // shift key

		UpdateCells();
	}
	else if (event_type == slTypingEvent_End)
	{
		while (cursor_strpos < text.line.size())
			cursor_offset += text.line[cursor_strpos++]->aspect;

		if (!ch) NoSelection(); // shift key

		UpdateCells();
	}
	else if (event_type == slTypingEvent_Deselect)
	{
		focused = false;
		if (onabandon) onabandon(this);
	}
	else if (event_type == slTypingEvent_Copy)
	{
		char* text = GenSelectedText();
		if (text)
		{
			SDL_SetClipboardText(text);
			free(text);
		}
	}
	else if (event_type == slTypingEvent_Cut)
	{
		OnTextInput(slTypingEvent_Copy,0);

		DeleteSelection();

		NoSelection();

		UpdateCells();

		if (ontextchange) ontextchange(this);
	}
	else if (event_type == slTypingEvent_Paste)
	{
		char* text = SDL_GetClipboardText();
		if (text)
		{
			DeleteSelection();

			// to do: support UTF8
			/// to do: don't do UpdateCells for each character inserted by the loop
			char cc;
			int pos = 0;
			while (cc = text[pos++])
			{
				if (cc == '\n') PutCharAtCursor(' ');
				else if (cc == '\t') for (int i = 0; i < 4; i++) PutCharAtCursor(' '); // to do: real tabs
				else if (cc != '\r') PutCharAtCursor(cc);
			}
			SDL_free(text);

			NoSelection();

			UpdateCells();

			if (ontextchange) ontextchange(this);
		}
	}
	else if (event_type == slTypingEvent_MouseDrag)
	{
		CursorToMouse();
		ShowCursor();
		UpdateCells();
	}
	// to do: support page up & page down
	// to do: when cursor is moved, if it's out of the current view, adjust the view offsets
}
static void slTypingBox::OnTextInput_Wrap (slTypingBox* code_area, int event_type, char ch)
{
	code_area->OnTextInput(event_type,ch);
}
slTypingBox::~slTypingBox ()
{
	text.Clear();
	if (hint_tex) hint_tex->Abandon();
	slDestroyBox(caret);
	slDestroyBox(container);
	slDestroyBox(highlight);
}





slList slTypingBoxes("Slice: Typing Boxes",offsetof(slTypingBox,_index_),false);
void slTypingBoxesStep ()
{
	for (slBU i = 0; i < slTypingBoxes.itemcount; i++)
	{
		slTypingBox* typing = slTypingBoxes.items[i];
		typing->Step();
	}
}
namespace sl_internal
{
	void slTypingBoxesInit ()
	{
		slHook_PreRender(slTypingBoxesStep);
		slTypingBoxes.HookAutoShrink();
	}
	void slTypingBoxesQuit ()
	{
		slUnhook_PreRender(slTypingBoxesStep);
		slTypingBoxes.UnhookAutoShrink();
		slTypingBoxes.UntilEmpty(slDestroyTypingBox);
	}
}
slTypingBox* slCreateTypingBox (slBox* from, SDL_Color text_color, slFont* font)
{
	slTypingBox* out = new slTypingBox(from,text_color,font);
	slTypingBoxes.Add(out);
	return out;
}
void slDestroyTypingBox (slTypingBox* todel)
{
	slTypingBoxes.Remove(todel);
	delete todel;
}
