#include <slice2d.h>
#include <slice2d/world_shaders.h>
using namespace s2d_internal;
slList s2dInstances("Slice2D: Instances",offsetof(s2dInstance,_index_),false);
SDL_mutex* s2dInstanceMutex;
s2dInstance* s2dCreateInstance (slTexRef tex, s2dTexInfo* texinfo)
{
	s2dInstance* out = new s2dInstance;
    out->SetTexRef(tex);
    out->texinfo = texinfo;
    SDL_LockMutex(s2dInstanceMutex);
	s2dInstances.Add(out);
    SDL_UnlockMutex(s2dInstanceMutex);
	return out;
}
void s2dDestroyInstance (s2dInstance* todel)
{
    SDL_LockMutex(s2dInstanceMutex);
	s2dInstances.Remove(todel);
    SDL_UnlockMutex(s2dInstanceMutex);
    delete todel;
}
typedef slScalar __attribute__((vector_size(sizeof(slScalar)*4))) s2dQuadScalar;
slCorners s2dGetInstCorners (s2dInstance* item)
{
	slVec2 halfdims = item->wh * 0.5f;
	slVec2 mins = item->xy;
	slVec2 maxs = mins;
	mins -= halfdims;
	maxs += halfdims;
	s2dQuadScalar pxes = {mins.x,maxs.x,mins.x,maxs.x};
	s2dQuadScalar pyes = {maxs.y,maxs.y,mins.y,mins.y};
	if (item->rot)
	{
		slScalar cosvalue = -slDegToRad(item->rotangle);
		slScalar sinvalue = slsin(cosvalue);
				 cosvalue = slcos(cosvalue);
		slVec2 rotpoint;
		if (item->rotcenter) rotpoint = item->xy;
		else rotpoint = item->rotpoint;
		pxes -= rotpoint.x;
		pyes -= rotpoint.y;
		s2dQuadScalar pxes_ = pxes;
		s2dQuadScalar pyes_ = pyes;
		pxes = (pxes * cosvalue) - (pyes * sinvalue);
		pyes = (pyes_ * cosvalue) + (pxes_ * sinvalue);
		pxes += rotpoint.x;
		pyes += rotpoint.y;
	}
	return {pxes[0],pyes[0],pxes[1],pyes[1],pxes[2],pyes[2],pxes[3],pyes[3]};
}
slCorners s2dGetInstTexCorners (s2dInstance* inst)
{
	s2dInstance clone = *inst;
	slScalar aratio = (clone.wh.w / clone.wh.h) / (inst->texref_swapchain.Step()->aspect);
	if (aratio > 1) clone.wh.w /= aratio;
	else clone.wh.h *= aratio;
	return s2dGetInstCorners(&clone);
}
bool s2dPointOnInstance (s2dInstance* inst, slVec2 point)
{
	slCorners corners;
	if (inst->rot)
	{
		slScalar angle = -slDegToRad(inst->rotangle);
		slScalar angcos = slcos(angle);
		slScalar angsin = slsin(angle);

		slVec2 offset;
		if (!inst->rotcenter) offset = inst->rotpoint - inst->xy;

		point -= inst->xy;
		point -= offset;

		slVec2 _point = point;
		point.x = point.x * angcos - point.y * angsin;
		point.y = _point.y * angcos + _point.x * angsin;

		point += offset;
		point += inst->xy;

		s2dInstance clone = *inst;
		clone.rot = false;
		corners = s2dGetInstCorners(&clone);
	}
	else corners = s2dGetInstCorners(inst);
	return point.x > corners.p00x && point.x < corners.p01x && point.y > corners.p10y && point.y < corners.p00y;
	/*

		Corners as seen on-screen:

		10	11
		00	01

	*/
}
void s2dInstance::PutWithin (s2dInstance* other)
{
	xy *= other->wh;
	wh *= other->wh;
	xy += other->xy;
}
slVec2 s2dCamXY = slVec2(0);
slScalar s2dCamSize = 10;
slVec2 _s2dCamWH_;
void s2dScreenToWorld (slBox* screen, s2dInstance* world)
{
	world->wh = screen->wh * _s2dCamWH_;
	world->xy = (slVec2(screen->xy.x,1 - screen->xy.y) - 0.5f) * _s2dCamWH_ + s2dCamXY + world->wh * slVec2(0.5f,-0.5f);

	world->rot = screen->rot;
	world->rotangle = screen->rotangle;
	world->rotcenter = screen->rotcenter;

	world->rotpoint = (screen->rotpoint - 0.5f) * _s2dCamWH_ + s2dCamXY;
}
void s2dWorldToScreen (s2dInstance* world, slBox* screen)
{
	screen->wh = world->wh / _s2dCamWH_;
	screen->xy = ((world->xy + world->wh * slVec2(-0.5f,0.5f)) - s2dCamXY) / _s2dCamWH_ + 0.5f;
	screen->xy.y = 1 - screen->xy.y;

	screen->rot = world->rot;
	screen->rotangle = world->rotangle;
	screen->rotcenter = world->rotcenter;

	screen->rotpoint = (world->rotpoint - s2dCamXY) / _s2dCamWH_ + 0.5f;
	screen->rotpoint.y = 1 - screen->rotpoint.y;
}
slVec2 s2dRelMousePos(0);
Uint8 s2dCamWH_Mode = s2dCamWH_PadArea;
inline GLxywh s2dCalcInstXYWH (s2dInstance* inst)
{
	GLvec2 xy(inst->xy);
	GLvec2 wh(inst->wh);
	xy -= wh * 0.5f;
	return {xy.x,xy.y,wh.w,wh.h};
}
inline GLxywh s2dCalcTexXYWH (s2dInstance* inst)
{
	GLvec2 xy(inst->xy);
	GLvec2 wh(inst->wh);
	if (inst->keepratio)
	{
		GLfloat aratio = (wh.w / wh.h) / (GLfloat)inst->texref_swapchain.Step()->aspect;
		if (aratio > 1) wh.w /= aratio;
		else wh.h *= aratio;
	}
	xy -= wh * 0.5f;
	return {xy.x,xy.y,wh.w,wh.h};
}
bool s2dInstancesInOrder (s2dInstance* inst1, s2dInstance* inst2)
{
	return inst2->z <= inst1->z;
}
slList s2dZHooks("Slice2D: Z-Hooks",slNoIndex,false);
void s2dAddZHook (slZHook* zhook)
{
	s2dZHooks.Add(zhook);
}
void s2dRemoveZHook (slZHook* zhook)
{
	s2dZHooks.Remove(zhook);
}
slScalar s2dCamRot = 0;
void s2dUpdateRelMouse ()
{
	slVec2 mouse = slGetMouse();
	mouse.y = 1 - mouse.y;
	s2dRelMousePos = (mouse - 0.5f) * _s2dCamWH_;
	s2dRelMousePos = slRotatePoint(s2dRelMousePos,s2dCamRot);
	s2dRelMousePos += s2dCamXY;
}
void s2dUpdateCamWH (slVec2 target_wh)
{
    slScalar window_aspect = target_wh.w / target_wh.h;
	if (s2dCamWH_Mode == s2dCamWH_SetWto1)
	{
		_s2dCamWH_.w = s2dCamSize;
		_s2dCamWH_.h = s2dCamSize / window_aspect;
	}
	else if (s2dCamWH_Mode == s2dCamWH_SetHto1)
	{
		_s2dCamWH_.w = s2dCamSize * window_aspect;
		_s2dCamWH_.h = s2dCamSize;
	}
	else if (s2dCamWH_Mode == s2dCamWH_CutArea)
	{
		if (window_aspect < 1)
		{
			_s2dCamWH_.w = s2dCamSize * window_aspect;
			_s2dCamWH_.h = s2dCamSize;
		}
		else
		{
			_s2dCamWH_.w = s2dCamSize;
			_s2dCamWH_.h = s2dCamSize / window_aspect;
		}
	}
	else
	{
		if (window_aspect < 1)
		{
			_s2dCamWH_.w = s2dCamSize;
			_s2dCamWH_.h = s2dCamSize / window_aspect;
		}
		else
		{
			_s2dCamWH_.w = s2dCamSize * window_aspect;
			_s2dCamWH_.h = s2dCamSize;
		}
	}
}
slVec2 s2dScreenBL,s2dScreenTR;
void s2dRenderPrepare (s2dInstance* inst)
{
    s2dInstanceRender* render = &inst->render;
	render->texref = inst->texref_swapchain.Step();
    if (inst->visible && render->texref->ready && render->texref->tex)
    {
		slScalar radius = inst->wh.len();
		slVec2 inst_bottomleft = inst->xy - radius;
		slVec2 inst_topright = inst->xy + radius;
		render->draw =
                 inst_topright.x >= s2dScreenBL.x
            &&   inst_topright.y >= s2dScreenBL.y
            && inst_bottomleft.x <= s2dScreenTR.x
            && inst_bottomleft.y <= s2dScreenTR.y;
		if (render->draw)
        {
    		if (inst->rot)
    		{
    			GLfloat angle = inst->rotangle;
    			angle = -slDegToRad_F(angle);
    			render->cosvalue = cosf(angle);
    			render->sinvalue = sinf(angle);
    			if (!inst->rotcenter)
    			{
    				render->rotpoint = GLvec2(inst->rotpoint);
    				goto OFFCENTER;
    			}
    		}
    		else
    		{
    			render->cosvalue = 1;
    			render->sinvalue = 0;
    		}
    		render->rotpoint = GLvec2(inst->xy);
    		OFFCENTER:

    		if (inst->texinfo)
    		{
    			render->xywh = s2dCalcInstXYWH(inst);
    			slScalar texrotdeg = inst->texinfo->rot;
    			//if (inst->rot) texrotdeg -= inst->rotangle;
    			GLfloat texrot = slDegToRad_F((GLfloat)texrotdeg);
    			render->texinfocos = cosf(texrot);
    			render->texinfosin = sinf(texrot);
    		}
    		else render->xywh = s2dCalcTexXYWH(inst);
        }
	}
    else render->draw = false;
}
extern slList s2dBatches;
void s2dRenderPrepareAll (slVec2 target_wh)
{
	s2dUpdateCamWH(target_wh); // This must be done before culling maths.
	s2dUpdateRelMouse(); // This can be done here so that is happens prior to glFinish in slCycle.

	// Calculate culling boundaries for this frame.
	slVec2 half = _s2dCamWH_ * 0.5f;
	slScalar camrot = slDegToRad(s2dCamRot);
	slScalar camcos = slfabs(slcos(camrot));
	slScalar camsin = slfabs(slsin(camrot));
	slVec2 _half = half;
	half.w = camcos * half.w + camsin * half.h;
	half.h = camcos * _half.h + camsin * _half.w;
	s2dScreenBL = s2dCamXY - half;
	s2dScreenTR = s2dCamXY + half;

    SDL_LockMutex(s2dInstanceMutex);
	slDoWork(&s2dInstances,s2dRenderPrepare,NULL);
    SDL_UnlockMutex(s2dInstanceMutex);
	//slDoWork(&s2dBatches,s2dBatchPreRender,NULL);
	#ifdef s2dEnableBatchCulling
	for (slBU cur = 0; cur < s2dBatches.itemcount; cur++)
	{
		//s2dBatchPreRender(s2dBatches.items[cur]);

		s2dBatch* batch = s2dBatches.items[cur];
		slDoArrayWork(batch->active_members,batch->active_member_count,sizeof(s2dBatchMember*),s2dBatchCheckCurrentlyActive,NULL);
		slDoArrayWork(batch->dormant_members,batch->dormant_member_count,sizeof(s2dBatchMember*),s2dBatchCheckCurrentlyDormant,NULL);
	}
	#endif
	//s2dPushBatchBuffers();
}
bool s2dBatchesInOrder (s2dBatch* batch1, s2dBatch* batch2)
{
	return batch1->z <= batch2->z;
}
SDL_Color s2dGlobalDrawMask = {255,255,255,255};
void s2dSetGlobalDrawMask (SDL_Color drawmask)
{
	s2dGlobalDrawMask = drawmask;
	// To do: make this actually have an effect.
}
void s2dActuallyRender (slVec2 target_wh)
{
    s2dRenderPrepareAll(target_wh);
    // For now do this here. When actual "camera" objects
    // are implemented, go back to doing this before the
    // actual rendering, for each camera.

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	s2dLoadWorldOrtho();
	slBindFullscreenArray(); // In case this has been clobbered.

	slSort(&s2dInstances,s2dInstancesInOrder);
	slSort(&s2dZHooks,slZHooksInOrder);
	slSort(&s2dBatches,s2dBatchesInOrder);
	GLuint last_texid = 0;

	slBU zhook_id = 0;
	slZHook* zhook;
	if (s2dZHooks.itemcount) zhook = *s2dZHooks.items;
	else zhook = NULL;

	slBU batch_id = 0;
	s2dBatch* batch;
	if (s2dBatches.itemcount) batch = *s2dBatches.items;
	else batch = NULL;

	GLuint CurrentProgram = 0;
    SDL_LockMutex(s2dInstanceMutex);
	for (slBU cur = 0; cur < s2dInstances.itemcount; cur++)
	{
		s2dInstance* inst = *(s2dInstances.items + cur);

		if (zhook)
		{
			if (inst->z < zhook->z)
			{
				DO_ZHOOK:
				zhook->func(zhook->userdata);
				if (++zhook_id < s2dZHooks.itemcount)
				{
					zhook = *(s2dZHooks.items + zhook_id);
					if (inst->z < zhook->z) goto DO_ZHOOK;
				}
				else zhook = NULL;

				slBindFullscreenArray(); // In case this has been clobbered.
				CurrentProgram = 0;
				last_texid = 0;
			}
		}

		if (batch)
		{
			if (inst->z < batch->z)
			{
				DO_BATCH:
            	if (batch->active_member_count) last_texid = s2dRenderBatch(batch);

				if (++batch_id < s2dBatches.itemcount)
				{
					batch = *(s2dBatches.items + batch_id);
					if (inst->z < batch->z) goto DO_BATCH;
				}
				else batch = NULL;

				slBindFullscreenArray();
				CurrentProgram = 0;
			}
		}

        s2dInstanceRender* render = &inst->render;
		if (render->draw)
		{
			GLuint uniform_drawmask,uniform_xywh;
			GLuint uniform_rotpoint,uniform_sinvalue,uniform_cosvalue;

			if (inst->texinfo) /* COULD BE THREAD-UNSAFE if app changes this
            between now and uploading referenced memory as uniform to GL. */
			{
				if (CurrentProgram != s2dTiledRotateProgram.program) glUseProgram(CurrentProgram = s2dTiledRotateProgram.program);
				glUniform4fv(s2dTiledRotateProgram_TexXYWH,1,(GLfloat*)inst->texinfo);
				glUniform2fv(s2dTiledRotateProgram_TexCosSin,1,(GLfloat*)&render->texinfocos);
				//glUniform1fv(s2dTiledRotateProgram_resbias,1,(GLfloat*)&render->texinforesbias);
				//glUniform1f(s2dTiledRotateProgram_resbias,1./64);
				uniform_drawmask = s2dTiledRotateProgram_Mask;
				uniform_xywh = s2dTiledRotateProgram_XYWH;
				uniform_sinvalue = s2dTiledRotateProgram_SinValue;
				uniform_cosvalue = s2dTiledRotateProgram_CosValue;
				uniform_rotpoint = s2dTiledRotateProgram_RotPoint;
			}
			else
			{
				if (CurrentProgram != s2dWorldRotateProgram.program) glUseProgram(CurrentProgram = s2dWorldRotateProgram.program);
				uniform_drawmask = s2dWorldRotateProgram_Mask;
				uniform_xywh = s2dWorldRotateProgram_XYWH;
				uniform_sinvalue = s2dWorldRotateProgram_SinValue;
				uniform_cosvalue = s2dWorldRotateProgram_CosValue;
				uniform_rotpoint = s2dWorldRotateProgram_RotPoint;
			}
			// Rotation
			glUniform1f(uniform_sinvalue,render->sinvalue);
			glUniform1f(uniform_cosvalue,render->cosvalue);
			glUniform2fv(uniform_rotpoint,1,(GLfloat*)&render->rotpoint);

			// Texture & Drawmask
			GLuint texid = render->texref->tex;
			if (texid != last_texid)
			//{
				s2dSetDrawTexture(last_texid = texid);
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
			//}
			slSetTextureClamping(inst->texinfo ? GL_REPEAT : GL_CLAMP_TO_EDGE);

			s2dUniformFromColor(uniform_drawmask,inst->drawmask);

			// XYWH
			glUniform4fv(uniform_xywh,1,(GLfloat*)&render->xywh);

			// Draw
			glDrawArrays(GL_TRIANGLE_FAN,0,4);
		}
	}
    SDL_UnlockMutex(s2dInstanceMutex);
	//printf("skipped %u out of %u\n",(unsigned)skipped,(unsigned)s2dInstances.itemcount);
	while (zhook_id < s2dZHooks.itemcount)
	{
		zhook = *(s2dZHooks.items + zhook_id++);
		zhook->func(zhook->userdata);
		//slBindFullscreenArray(); // In case this has been clobbered.
	}
	while (batch_id < s2dBatches.itemcount)
	{
		batch = *(s2dBatches.items + batch_id++);
		if (batch->active_member_count) s2dRenderBatch(batch);
	}
}
void s2dRender ()
{
    slDrawToScreen();
    s2dActuallyRender(slGetWindowResolution());
}
void s2dRender (slFrame* target_frame)
{
    target_frame->DrawTo();
    slVec2 target_wh = target_frame->GetDims();
    glClear(GL_COLOR_BUFFER_BIT);
    s2dActuallyRender(target_wh);
}
/*bool s2dIsTextureUsed (slTexture* tex)
{
    for (slBU cur = 0; cur < s2dInstances.itemcount; cur++) if (((s2dInstance*)*(s2dInstances.items + cur))->texref == tex) return true;
    return false;
}*/
slBU s2dInitCount = 0;
void s2dInit ()
{
	if (s2dInitCount++) return;

    s2dInstanceMutex = SDL_CreateMutex();
	s2dInitWorldProgram();
	//slHook_TextureUsed(s2dIsTextureUsed);
	//slHook_PreRender(s2dRenderPrepareAll);

	physInit();
	ptclInit();
}
void s2dQuit ()
{
	//if (!s2dInitCount) return; // This shouldn't happen unless someone's an idiot. So, let it burn.
	if (--s2dInitCount) return;

	ptclQuit();
	physQuit();

	s2dInstances.UntilEmpty(s2dDestroyInstance);
    SDL_DestroyMutex(s2dInstanceMutex);
	s2dBatches.UntilEmpty(s2dDestroyBatch);
	s2dZHooks.Clear(NULL);
	s2dQuitWorldProgram();
	//slUnhook_TextureUsed(s2dIsTextureUsed);
	//slUnhook_PreRender(s2dRenderPrepareAll);
}
void s2dGetRelMouse (slScalar* relx, slScalar* rely)
{
	if (relx) *relx = s2dRelMousePos.x;
	if (rely) *rely = s2dRelMousePos.y;
}
slVec2 s2dGetRelMouse ()
{
	return s2dRelMousePos;
}
void s2dSetCamXY (slVec2 xy)
{
	s2dCamXY = xy;
}
void s2dSetCamXY (slScalar x, slScalar y)
{
	s2dSetCamXY(slVec2(x,y));
}
void s2dSetCamSize (slScalar w)
{
	s2dCamSize = w;
}
void s2dSetCamWH_Mode (Uint8 mode)
{
	s2dCamWH_Mode = mode;
}
void s2dGetCamXY (slScalar* x, slScalar* y)
{
	if (x) *x = s2dCamXY.x;
	if (y) *y = s2dCamXY.y;
}
slVec2 s2dGetCamXY ()
{
	return s2dCamXY;
}
slScalar s2dGetCamSize ()
{
	return s2dCamSize;
}
void s2dGetCamWH (slScalar* w, slScalar* h)
{
	if (w) *w = _s2dCamWH_.w;
	if (h) *h = _s2dCamWH_.h;
}
slVec2 s2dGetCamWH ()
{
	return _s2dCamWH_;
}
void s2dSetCamRot (slScalar rot)
{
	s2dCamRot = rot;
}
slScalar s2dGetCamRot ()
{
	return s2dCamRot;
}

slCoordsTransform s2dWorldCoordsTransform ()
{
	slCoordsTransform out;
	out.view_xy = s2dCamXY;
	out.view_wh = _s2dCamWH_;
	out.view_rotate = s2dCamRot;
	return out;
}
