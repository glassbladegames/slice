#include <slice2d.h>
slList ptclLayers("SliceParticles: Layers",slNoIndex,false);
void ptclRenderLayerParticles (ptclLayer* layer);
void* ptclUserdata (void* particle);
slBU ptclParticleAllocRate = 256;
void ptclShrinkParticleArray (ptclLayer* layer)
{
	slBU remove = ((layer->ptcl_capacity - layer->ptcl_usage) / ptclParticleAllocRate) * ptclParticleAllocRate;
	if (remove)
	{
		layer->ptcl_array = realloc(layer->ptcl_array,layer->ptcl_size * (layer->ptcl_capacity -= remove));
		//layer->render_states = realloc(layer->render_states,layer->ptcl_capacity);
	}
}
const slBU ptclRawVAOSizeUnit = sizeof(GLfloat) * 36;
#define slWorkerCount (slGetWorkerCount())
extern GLuint slBoxVertsBuf;
ptclLayer* ptclCreateLayer (Uint8 z, bool (*step_particle) (ptclParticle* particle, float delta), slBU userdata_size)
{
	ptclLayer* out = malloc(sizeof(ptclLayer));
	ptclLayers.Add(out);
	out->ptcl_array = NULL;
	out->ptcl_capacity = 0;
	out->ptcl_usage = 0;
	out->ptcl_size = sizeof(ptclParticle) + slCeil16(userdata_size); // Align on 16 bytes.
	//out->render_states = NULL;
	out->zhook.z = z;
	out->zhook.func = ptclRenderLayerParticles;
	out->zhook.userdata = out;
	out->step_particle = step_particle;
	s2dAddZHook(&out->zhook);
	out->gc_action.Hook(ptclShrinkParticleArray,out);

	out->draw_buffers = malloc(sizeof(ptclDrawBuffer) * slWorkerCount);
	out->draw_buffers_used = 0;
	for (slBU cur = 0; cur < slWorkerCount; cur++)
	{
		ptclDrawBuffer* data = out->draw_buffers + cur;
		glGenBuffers(1,&data->buf);
		if (!data->buf) slFatal("Failed to generate a layer's thread-local vertex buffer.",970);
		glGenVertexArrays(1,&data->vao);
		if (!&data->vao) slFatal("Failed to generate particles render array.",960);

		glBindVertexArray(data->vao);
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		//glBindBuffer(GL_ARRAY_BUFFER,slBoxVertsBuf);
		//glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,0,0);

		glBindBuffer(GL_ARRAY_BUFFER,data->buf);
		void* offset = 0;
		const size_t vertsize = ptclRawVAOSizeUnit / 6;
		glVertexAttribPointer(0,2,GL_FLOAT,false,vertsize,offset); offset += sizeof(GLfloat) * 2;
		glVertexAttribPointer(1,3,GL_FLOAT,false,vertsize,offset); offset += sizeof(GLfloat) * 3;
		glVertexAttribPointer(2,1,GL_FLOAT,false,vertsize,offset); offset += sizeof(GLfloat) * 1;

		data->buf_available = 0;





	}

	return out;
}
void ptclDestroyLayer (ptclLayer* layer)
{
	for (slBU cur = 0; cur < slWorkerCount; cur++)
	{
		ptclDrawBuffer* data = layer->draw_buffers + cur;
		glDeleteVertexArrays(1,&data->vao);
		glDeleteBuffers(1,&data->buf);
	}
	free(layer->draw_buffers);

	s2dRemoveZHook(&layer->zhook);
	if (layer->ptcl_array) free(layer->ptcl_array);
	//if (layer->render_states) free(layer->render_states);
	ptclLayers.Remove(layer);
	layer->gc_action.Unhook();
	free(layer);
}
ptclParticle* ptclSpawnParticle (ptclLayer* layer)
{
	//putchar('.');
	if (layer->ptcl_usage >= layer->ptcl_capacity)
	{
		//printf("allocating\n");
		layer->ptcl_array = realloc(layer->ptcl_array,layer->ptcl_size * (layer->ptcl_capacity += ptclParticleAllocRate));
		//printf("size increased to %llu\n",layer->ptcl_capacity);
		//layer->render_states = realloc(layer->render_states,layer->ptcl_capacity);
	}
	slBU idx = layer->ptcl_usage++;
	ptclParticle* out = layer->ptcl_array + idx * layer->ptcl_size;

	//out->index = idx;
	//out->parent = layer;
	//printf("particle index %llu\n",idx);

	return out;
}
slBU ptclRemovesGrow = 64;
struct ptclRemoves
{
	slBU* queue;
	slBU capacity,usage;
	slGC_Action shrink_action;
	slBU max_usage;
	GLuint vertex_array_object;
};
void ptclShrinkRemoves (ptclRemoves* removes)
{
	/*if (removes->queue)
	{
		free(removes->queue);
		removes->queue = NULL;
		removes->capacity = 0;
	}*/
	slBU remove = ((removes->capacity - removes->max_usage) / ptclRemovesGrow) * ptclRemovesGrow;
	if (remove) removes->queue = realloc(removes->queue,sizeof(slBU) * (removes->capacity -= remove));
	removes->max_usage = 0;
}
void ptclInitThreadRemoves (ptclRemoves* removes)
{
	removes->queue = NULL;
	removes->capacity = 0;
	removes->shrink_action.Hook(ptclShrinkRemoves,removes);
	removes->max_usage = 0;
}
void ptclQuitThreadRemoves (ptclRemoves* removes)
{
	if (removes->queue) free(removes->queue);
	removes->shrink_action.Unhook();
}
ptclRemoves ptclFinalRemoves;
struct ptclThreadData
{
	ptclRemoves removes;
	GLfloat* raw_vao;
	size_t raw_vao_capacity;
	size_t raw_vao_maxusage;
	size_t raw_vao_usage;
	slGC_Action raw_vao_shrink;
};
slBU ptclThreadDataShrinkThreshold = 64;
void ptclShrinkRawVAO (ptclThreadData* data)
{
	if (data->raw_vao_maxusage + ptclThreadDataShrinkThreshold <= data->raw_vao_capacity)
		data->raw_vao = realloc(data->raw_vao,ptclRawVAOSizeUnit * (data->raw_vao_capacity = data->raw_vao_maxusage));
	data->raw_vao_maxusage = 0;
}
void ptclInitThreadData (ptclThreadData* data)
{
	ptclInitThreadRemoves(&data->removes);

	data->raw_vao = NULL;
	data->raw_vao_capacity = 0;
	data->raw_vao_maxusage = 0;
	data->raw_vao_shrink.Hook(ptclShrinkRawVAO,data);
}
void ptclQuitThreadData (ptclThreadData* data)
{
	ptclQuitThreadRemoves(&data->removes);
	if (data->raw_vao) free(data->raw_vao);
	data->raw_vao_shrink.Unhook();
}
/*void ptclZeroRemoves (void* all_threads_userdata, slBU userdata_size, void* unused, slBU core_count)
{
	for (slBU cur = 0; cur < core_count; cur++)
	{
		ptclRemoves* removes = all_threads_userdata + userdata_size * cur;
		removes->usage = 0;
	}
}*/
void ptclPostWork (void* all_threads_userdata, slBU userdata_size, ptclLayer* layer, slBU core_count)
{
	//printf("CopyRemoves layer: 0x%llX [%llu]\n",layer->ptcl_array,layer->ptcl_usage);
	ptclThreadData* thread_data;
	slBU required = 0;
	for (slBU cur = 0; cur < core_count; cur++)
	{
		/// Count removes.
		thread_data = all_threads_userdata + userdata_size * cur;
		ptclRemoves* removes = &thread_data->removes;
		required += removes->usage;
		//if (removes->max_usage < removes->usage) removes->max_usage = removes->usage;

		/// Upload vertex buffer from memory to the GL.
		layer->draw_buffers[cur].buf_len = thread_data->raw_vao_usage;
		if (thread_data->raw_vao_usage)
		{
			glBindBuffer(GL_ARRAY_BUFFER,layer->draw_buffers[cur].buf);
			if (layer->draw_buffers[cur].buf_available < thread_data->raw_vao_usage)
			{
				layer->draw_buffers[cur].buf_available = thread_data->raw_vao_usage;
				glBufferData(GL_ARRAY_BUFFER,thread_data->raw_vao_usage * ptclRawVAOSizeUnit,thread_data->raw_vao,GL_DYNAMIC_DRAW);
			}
			else glBufferSubData(GL_ARRAY_BUFFER,0,thread_data->raw_vao_usage * ptclRawVAOSizeUnit,thread_data->raw_vao);
		}
	}
	layer->draw_buffers_used = core_count;
	//printf("full removes queue: %llu\n",required);
	if (required > ptclFinalRemoves.capacity) ptclFinalRemoves.queue = realloc(ptclFinalRemoves.queue,sizeof(slBU) * (ptclFinalRemoves.capacity = required));
	required = 0;
	for (slBU cur = 0; cur < core_count; cur++)
	{
		thread_data = all_threads_userdata + userdata_size * cur;
		ptclRemoves* removes = &thread_data->removes;
		memcpy(ptclFinalRemoves.queue + required,removes->queue,sizeof(slBU) * removes->usage);
		required += removes->usage;
	}
	ptclFinalRemoves.usage = required;
	if (ptclFinalRemoves.usage > ptclFinalRemoves.max_usage) ptclFinalRemoves.max_usage = ptclFinalRemoves.usage;
}
float ptclDelta;
/*void ptclStepParticle (ptclParticle* part, ptclLayer* layer, slWorker* worker)
{
	layer->step_particle(part,ptclDelta,worker);
}*/
GLvec2 ptclScreenBL,ptclScreenTR;
ptclLayer* ptclCurrentLayer;
slForceInline inline void ptclKillParticle (ptclParticle* particle, ptclRemoves* removes)
{
	//printf("kill particle\n");
	//printf("removing particle %llu\n",particle->index);
	if (removes->usage >= removes->capacity)
	//{
		removes->queue = realloc(removes->queue,sizeof(slBU) * (removes->capacity += ptclRemovesGrow));
		//printf("expanded removes buffer to %llu\n",ptclRemoveCapacity);
	//}
	slBU index = ((slBU)particle - (slBU)ptclCurrentLayer->ptcl_array) / ptclCurrentLayer->ptcl_size;
	removes->queue[removes->usage++] = index;//particle->index;
}
void ptclStepParticles (slBU start, slBU count, ptclThreadData* thread_data, ptclLayer* layer, slWorker* worker)
{
	//printf("step function\n");
	ptclRemoves* removes = &thread_data->removes;
	removes->usage = 0;
	void* ptcl_ptr = layer->ptcl_array + (start * layer->ptcl_size);
	void* first_particle = ptcl_ptr;
	//bool* render_state = layer->render_states + start;
	slBU n_drawn = 0;
	for (start = 0; start < count; start++, ptcl_ptr += layer->ptcl_size)
	{
		ptclParticle* part = ptcl_ptr;
		if (layer->step_particle(part,ptclDelta))
		{
			part->render = false;
			ptclKillParticle(part,removes);
		}
		else
		{
			GLfloat halfr = part->radius * 0.5f;
			GLvec2 part_topright = part->pos + halfr;
			GLvec2 part_bottomleft = part->pos - halfr;
			//*render_state++ =
			part->render = part_topright.x >= ptclScreenBL.x && part_topright.y >= ptclScreenBL.y && part_bottomleft.x <= ptclScreenTR.x && part_bottomleft.y <= ptclScreenTR.y;
			if (part->render) n_drawn++;
		}
	}
	if (removes->usage > removes->max_usage) removes->max_usage = removes->usage;


	thread_data->raw_vao_usage = n_drawn;
	if (!n_drawn) return;

	if (thread_data->raw_vao_capacity < n_drawn)
		thread_data->raw_vao = realloc(thread_data->raw_vao,ptclRawVAOSizeUnit * (thread_data->raw_vao_capacity = n_drawn));
	if (thread_data->raw_vao_maxusage < n_drawn) thread_data->raw_vao_maxusage = n_drawn;

	GLfloat* bufptr = thread_data->raw_vao;
	ptcl_ptr = first_particle;
	while (n_drawn--)
	{
		SKIP:
		ptclParticle* part = ptcl_ptr;
		ptcl_ptr += layer->ptcl_size;
		if (!part->render) goto SKIP;

		#define ptclPushVertexData\
			*bufptr++ = part->pos.x;\
			*bufptr++ = part->pos.y;\
			*bufptr++ = part->red;\
			*bufptr++ = part->green;\
			*bufptr++ = part->blue;\
			*bufptr++ = part->radius;
		ptclPushVertexData
		ptclPushVertexData
		ptclPushVertexData
		ptclPushVertexData
		ptclPushVertexData
		ptclPushVertexData
	}
}
slBulkJob* ptclJob;
inline void ptclCreateJob ()
{
	slBulkJobDef def;
	def.thread_userdata_size = sizeof(ptclThreadData);//sizeof(ptclRemoves);
	def.init_thread_userdata = ptclInitThreadData;//ptclInitThreadRemoves;
	def.quit_thread_userdata = ptclQuitThreadData;//ptclQuitThreadRemoves;
	def.main_prework = NULL;//ptclZeroRemoves;
	def.main_postwork = ptclPostWork;//ptclCopyRemoves;
	def.thread_work = ptclStepParticles;
	ptclJob = slCreateBulkJob(def);
}






/*
void ptclKillParticle (ptclParticle* particle)
{
	printf("kill particle (ptr %llX) idx %llu\n",particle,particle->index);
	ptclLayer* layer = particle->parent;
	layer->ptcl_usage--;
	slBU index = particle->index;
	if (index < layer->ptcl_usage)
	{
		memcpy(
			(void*)layer->ptcl_array + index * layer->ptcl_size,
			(void*)layer->ptcl_array + layer->ptcl_usage * layer->ptcl_size,
			layer->ptcl_size
		);
		particle->index = index; // Another particle moved to this pointer, give it the correct index.
	}
}*/
void ptclFlushRemoves (ptclLayer* layer)
{
	//printf("FlushRemoves layer: 0x%llX [%llu]\n",layer->ptcl_array,layer->ptcl_usage);
	//printf("about to remove %llu particles\n",ptclRemoveUsage);
	void* array = layer->ptcl_array;
	slBU size = layer->ptcl_size;
	for (slBU cur = 0; cur < ptclFinalRemoves.usage; )
	{
		slBU idx = ptclFinalRemoves.queue[cur++];
		//printf("removing %llu; ",idx);
		slBU last = --layer->ptcl_usage;
		//printf("last: %llu\n",last);
		if (idx != last)
		{
			for (slBU sub = cur; sub < ptclFinalRemoves.usage; sub++)
			{
				if (ptclFinalRemoves.queue[sub] == last)
				{
					ptclFinalRemoves.queue[sub] = idx;
					//printf("relocated future remove\n");
					break;
				}
				//else if (ptclRemoveQueue[sub] > idx) ptclRemoveQueue[sub]--;
			}
			//if (idx > last) printf("OHHHHHH SHIT idx=%llu (last: %llu)\n",idx,last);
			ptclParticle* moved = array + idx * size;
			memcpy(
				moved,
				array + last * size,
				size
			);
			//moved->index = idx;
		}
	}
	ptclFinalRemoves.usage = 0; // In case this wasn't overwritten because no particles were stepped in a layer.
}
//GLuint ptclPos;
//GLuint ptclRadius;
//GLuint ptclColor;
GLuint ptclCamPos;
GLuint ptclCamHalfDims;
GLuint ptclCamRotCos;
GLuint ptclCamRotSin;
slShaderProgram ptclProgram;
void ptclStepAllParticles ()
{
	glUseProgram(ptclProgram.program);

	GLvec2 camwh(s2dGetCamWH());
	GLvec2 camxy(s2dGetCamXY());

	GLvec2 camhalfdims = GLvec2(2) / camwh;
	glUniform2fv(ptclCamHalfDims,1,(GLfloat*)&camhalfdims);
	glUniform2fv(ptclCamPos,1,(GLfloat*)&camxy);

	GLfloat camrot = s2dGetCamRot();
	camrot = slDegToRad_F(camrot);
	GLfloat camcos = cosf(camrot);
	GLfloat camsin = sinf(camrot);
	glUniform1f(ptclCamRotCos,camcos);
	glUniform1f(ptclCamRotSin,camsin);

	GLvec2 half = camwh * 0.5f;
	camcos = fabsf(camcos);
	camsin = fabsf(camsin);
	GLvec2 _half = half;
	half.w = camcos * half.w + camsin * half.h;
	half.h = camcos * _half.h + camsin * _half.w;

	ptclScreenBL = camxy - half;
	ptclScreenTR = camxy + half;

	ptclDelta = slGetDelta();

	for (slBU cur = 0; cur < ptclLayers.itemcount; cur++)
	{
		ptclLayer* layer = *(ptclLayers.items + cur);
		ptclCurrentLayer = layer;
		//slListShrink(&layer->particles); // Shrink once per frame, before particles removed and after particles spawned.
		//slDoArrayWork(layer->ptcl_array,layer->ptcl_usage,layer->ptcl_size,ptclStepParticle,layer);
		slExecuteBulkJob(ptclJob,layer->ptcl_usage,layer);
		ptclFlushRemoves(layer);
	}
}
slScalar Deviation (slScalar lowest, slScalar highest);



/*char* ptclVertSrc = R"(
#version 130
uniform vec2 CamPos;
uniform vec2 InvHalfCamDims;
uniform float CamRotCos,CamRotSin;
in vec2 ParticlePos;
in vec3 ParticleColor;
in float ParticleRadius;
out vec3 Color;
void main ()
{

	gl_PointSize = 100;
	gl_Position = vec4(ParticlePos,0,1);//vec4(0,0,0,1);
	Color = vec3(1,1,1);
	return;



	// Generate point.
	//vec2 point = ParticlePos + ((VertPos * 2 - 1) * ParticleRadius);
	vec2 point = ParticlePos;
	// Translate by camera position.
	point = point - CamPos;
    // Rotate around camera.
    point = vec2(point.x * CamRotCos - point.y * CamRotSin,point.y * CamRotCos + point.x * CamRotSin);
    // Scale by dimensions.
    //FragPos = point * InvHalfCamDims;
	gl_Position = vec4(point * InvHalfCamDims,0,1);

	//gl_Position = vec4(VertPos,0,1);

	gl_PointSize = ParticleRadius * min(InvHalfCamDims.x,InvHalfCamDims.y);
	//ParticleSize = ParticleRadius * InvHalfCamDims;

	Color = ParticleColor;
}
)";



char* ptclFragSrc = R"(
#version 130
in vec3 Color;
void main ()
{
	)" gl_FragColor R"( = vec4(1,1,1,1);
	return;



	float dist = length(gl_PointCoord * 2 - 1);
	//float dist = length((FragPos - ParticlePoint) / ParticleSize);
	//gl_FragColor = vec4(1,1,1,1); return;
	//if (dist > 1) discard;
	)" gl_FragColor R"( = vec4(Color,max(0,1 - dist));
}
)";*/



char* ptclVertSrc = R"(#version 130

in vec2 ParticlePos;
in vec3 ParticleColor;
in float ParticleRadius;
flat out vec3 FragmentColor;

uniform vec2 CamPos;
uniform vec2 InvHalfCamDims;
uniform float CamRotCos,CamRotSin;

out vec2 NormalizedCoords;

void main ()
{
	// Get vertex position.
	int vid = gl_VertexID % 6;
	if (vid >= 3) vid -= 2;
	vec2 VertPos = vec2((vid & 1) << 1,vid & 2) - 1;


	// Pass these things to the fragment shader.
	NormalizedCoords = VertPos;
	FragmentColor = ParticleColor;


	// Generate point.
	vec2 point = ParticlePos + VertPos * ParticleRadius;


	// Translate by camera position.
	point -= CamPos;


    // Rotate around camera.
    point = vec2(point.x * CamRotCos - point.y * CamRotSin,point.y * CamRotCos + point.x * CamRotSin);


    // Scale by dimensions.
    point *= InvHalfCamDims;
	gl_Position = vec4(point,0,1);
}
)";



char* ptclFragSrc = R"(#version 130

in vec2 NormalizedCoords;
flat in vec3 FragmentColor;

void main ()
{
	// Linear distance:
	float dist = length(NormalizedCoords);

	// Alternate distance:
	//vec2 coords = NormalizedCoords * NormalizedCoords;
	//float dist = coords.x + coords.y;

	gl_FragColor = vec4(FragmentColor,max(1 - dist,0));
}
)";



void ptclBindAttribs (GLuint program)
{
	glBindAttribLocation(program,0,"ParticlePos");
	glBindAttribLocation(program,1,"ParticleColor");
	glBindAttribLocation(program,2,"ParticleRadius");
}
namespace s2d_internal
{
	void ptclInit ()
	{
		ptclProgram = slCreateShaderProgram("Particles",ptclVertSrc,ptclFragSrc,ptclBindAttribs);
		//ptclPos = slLocateUniform(ptclProgram,"ParticlePos");
		//ptclRadius = slLocateUniform(ptclProgram,"ParticleRadius");
		//ptclColor = slLocateUniform(ptclProgram,"ParticleColor");
		ptclCamPos = slLocateUniform(ptclProgram,"CamPos");
		ptclCamHalfDims = slLocateUniform(ptclProgram,"InvHalfCamDims");
		ptclCamRotCos = slLocateUniform(ptclProgram,"CamRotCos");
		ptclCamRotSin = slLocateUniform(ptclProgram,"CamRotSin");

		slHook_PreRender(ptclStepAllParticles);

		ptclCreateJob();
		ptclInitThreadRemoves(&ptclFinalRemoves);

	}
	void ptclQuit ()
	{
		ptclLayers.UntilEmpty(ptclDestroyLayer);
		slDestroyShaderProgram(ptclProgram);

		slUnhook_PreRender(ptclStepAllParticles);

		slDestroyBulkJob(ptclJob);
		ptclQuitThreadRemoves(&ptclFinalRemoves);
	}
}
void ptclRenderLayerParticles (ptclLayer* layer)
{
	if (!layer->ptcl_usage) return; // Nothing to draw.

	//glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
	//glPointSize(15);

	glUseProgram(ptclProgram.program);

	//bool* render_state = layer->render_states;

	for (slBU core = 0; core < layer->draw_buffers_used; core++)
	{
		ptclDrawBuffer drawbuf = layer->draw_buffers[core];
		if (drawbuf.buf_len)
		{
			glBindVertexArray(drawbuf.vao);
			//glBindBuffer(GL_ARRAY_BUFFER,drawbuf.buf);
			glDrawArrays(GL_TRIANGLES,0,drawbuf.buf_len * 6);
			//printf("rendering %llu points (core %llu, buffer %u, vao %u)\n",drawbuf.buf_len,core,drawbuf.buf,drawbuf.vao);
		}
	}

	/*
	void* partptr = layer->ptcl_array;
	for (slBU cur = 0; cur < layer->ptcl_usage; cur++, partptr += layer->ptcl_size)
	{
		ptclParticle* part = partptr;
		if (part->render)
		//if (*render_state++)
		{
			glUniform2fv(ptclPos,1,(GLfloat*)&part->pos);
			glUniform1f(ptclRadius,part->radius);
			glUniform3fv(ptclColor,1,&part->red);
			glDrawArrays(GL_TRIANGLE_FAN,0,4);
		}
	}*/
}
struct ptclRayCallback : b2RayCastCallback
{
	bool hit = false;
	b2Vec2 hitpoint;
	b2Vec2 hitnormal;
	float32 hitfraction;
	bool (*ignore) (b2Fixture* fxt);
	float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction)
	{
		if (ignore) if (ignore(fixture)) return -1;
		hit = true;
		hitpoint = point;
		hitnormal = normal;
		hitfraction = fraction;
		return fraction;
	}
};
void ptclPhysAdvance (ptclParticle* part, float delta, bool (*ignore) (b2Fixture* fxt), float restitution, void (*oncollide) (ptclParticle* particle))
{
	float moveby = delta;
	//int hits = 0;
	while (true)
	{
		b2Vec2 origin = b2Vec2(part->pos.xy);
		b2Vec2 destination = origin + b2Vec2(part->vel.xy) * moveby;
		/*slVec2 moveto = part->pos;
		moveto.xy += part->vel.xy * moveby;
		b2Vec2 origin = b2Vec2(part->pos.x,part->pos.y);
		b2Vec2 destination = b2Vec2(moveto.x,moveto.y);*/
		ptclRayCallback ray;
		ray.ignore = ignore;
		if ((destination - origin).LengthSquared() > 0.0f) physGetWorld()->RayCast(&ray,origin,destination);
		if (!ray.hit)
		{
			part->pos = GLvec2(destination.xy);
			break;
		}
		//if (hits > 5) printf("hit %u\n",++hits);
		part->pos = GLvec2(ray.hitpoint.xy);
		float speed = sqrtf(part->vel.x * part->vel.x + part->vel.y * part->vel.y);

		speed *= restitution;
		//if (speed < 0.01) speed = 0.01;

		float in_ang = atan2f(part->vel.y,part->vel.x) + M_PI_F;
		//printf("in: %f\n",in_ang);
		float norm_ang = atan2f(ray.hitnormal.y,ray.hitnormal.x);
		//printf("norm: %f\n",norm_ang);
		float out_ang = norm_ang + (norm_ang - in_ang);
		//printf("out: %f\n\n",out_ang);

		part->vel = GLvec2(cosf(out_ang),sinf(out_ang)) * speed;
		moveby *= (1 - ray.hitfraction);
		if (oncollide) oncollide(part);
	}
}
