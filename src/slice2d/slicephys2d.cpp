#include <slice2d.h>
slScalar physInterval = 1. / 120.;
void physSetInterval (slScalar to)
{
	physInterval = to;
}
slScalar physGetInterval ()
{
	return physInterval;
}
slScalar physRateMul = 1;
void physSetRateMul (slScalar mul)
{
	physRateMul = mul;
}
inline b2Vec2 physAdvancePosition (b2Vec2 pos, b2Vec2 vel, slScalar t)
{
	return pos + (vel * t);
}
b2World* physWorld;
#define physShape_Rectangle 0
#define physShape_Circle 1
slBlockAllocator physInstanceAllocator ("SlicePhys2D: PhysInstance Allocator",sizeof(physInstance),1024);
slList physInstances("SlicePhys2D: PhysInstances",offsetof(physInstance,_index_),false);
b2PolygonShape physRectShape (s2dInstance* visual, slTexture* shrink_to, b2Vec2* capture)
{
	b2PolygonShape shape;
	float32 wmul = 0.5f, hmul = 0.5f;
	if (shrink_to && !visual->texinfo)
	{
		while (!shrink_to->dims_valid) SDL_Delay(0);
		if (shrink_to->aspect > 1) hmul /= shrink_to->aspect;
		else wmul *= shrink_to->aspect;
	}
	b2Vec2 wh(visual->wh.wh);
	wh.xy *= (b2RawVec2){wmul,hmul};
	shape.SetAsBox(wh.x,wh.y);
	if (capture) *capture = wh;
	return shape;
}
physInstance* physCreateInstance (s2dInstance* visual, b2PolygonShape shape, bool canmove, physCollInfo collinfo, bool canrotate, float32 friction, float32 density)
{
	/// Initialization info for the body.
	b2BodyDef body_def;
	body_def.position = visual->xy.xy;//.Set(visual->xy.x,visual->xy.y);
	if (visual->rot) body_def.angle = slDegToRad(-visual->rotangle); // Does not support non-centered rotation.
	else body_def.angle = 0;
	if (canmove) body_def.type = b2_dynamicBody;
	body_def.fixedRotation = !canrotate;

	/// Create the body within the world.
	b2Body* body = physWorld->CreateBody(&body_def);
	//if (!body) printf("< FAILED TO CREATE PHYSICS OBJECT >\n");

	/// Create the fixture from shape and attach to the body.
	b2FixtureDef fixture;
	fixture.shape = &shape;
	fixture.density = density;
	fixture.friction = friction;
	fixture.filter.categoryBits = collinfo.objtype;
	fixture.filter.maskBits = collinfo.collidewith;
	/*b2Fixture* fix = */body->CreateFixture(&fixture);
	//if (!fix) printf("< Body Created, FIXTURE NOT CREATED >\n");

	physInstance* out = physInstanceAllocator.Allocate();
	out->visual = visual;
	out->canmove = canmove;
	out->canrotate = canrotate;
	out->body = body;

	// Prevent weird stuff from happening if smoothing is enabled.
	out->prev_pos = out->visual->xy;
	out->next_pos = out->visual->xy;
	out->prev_rot = out->visual->rotangle;
	out->next_rot = out->visual->rotangle;

	physInstances.Add(out);

	visual->rot = canrotate;
	visual->rotcenter = true;

	return out;
}
void physDestroyInstance (physInstance* instance)
{
	physWorld->DestroyBody(instance->body);
	s2dDestroyInstance(instance->visual);
	physInstances.Remove(instance);
	physInstanceAllocator.Release(instance);
}
bool physSmoothMode = true;
bool physFirstAccumulate = true;
void physSetSmoothMode (bool smooth_mode)
{
	if (!physSmoothMode) physFirstAccumulate = true;
	physSmoothMode = smooth_mode;
}
void physUpdateInstance (physInstance* inst)
{
	// update what's necessary
	//if (inst->canmove)
	//{
		// update visual object's position to reflect new location of the physics object
		b2Vec2 pos = inst->body->GetPosition();
		inst->visual->xy = slVec2(pos.xy);
		//printf("updating position (%f,%f)\n",pos.x,pos.y);
	//}
	//if (inst->canrotate)
	//{
		// update visual object's rotation to reflect new orientation of the physics object

		inst->visual->rotangle = -slRadToDeg(inst->body->GetAngle());
	//}
}
slList physHooks_PreCycle("SlicePhys2D: Pre-Cycle Hooks",slNoIndex,false);
void physHook_PreCycle (void (*func) ())
{
	physHooks_PreCycle.Add(func);
}
void physUnhook_PreCycle (void (*func) ())
{
	physHooks_PreCycle.Remove(func);
}
slList physHooks_PreStep("SlicePhys2D: Pre-Step Hooks",slNoIndex,false);
void physHook_PreStep (void (*func) ())
{
	physHooks_PreStep.Add(func);
}
void physUnhook_PreStep (void (*func) ())
{
	physHooks_PreStep.Remove(func);
}
slList physHooks_PostCycle("SlicePhys2D: Post-Cycle Hooks",slNoIndex,false);
void physHook_PostCycle (void (*func) ())
{
	physHooks_PostCycle.Add(func);
}
void physUnhook_PostCycle (void (*func) ())
{
	physHooks_PostCycle.Remove(func);
}
slScalar physElapsed = 0;
//void physCaptureInstanceBeforeState (physInstance* inst)
//{
	/*b2Vec2 pos = inst->body->GetPosition();
	inst->prev_pos = slVec2(pos.xy);
	inst->prev_rot = -slRadToDeg(inst->body->GetAngle());*/
	/*inst->prev_pos = inst->next_pos;
	inst->prev_rot = inst->next_rot;*/
//}
void physCaptureInstanceAfterState (physInstance* inst)
{
	inst->prev_pos = inst->next_pos;
	inst->prev_rot = inst->next_rot;

	b2Vec2 pos = inst->body->GetPosition();
	inst->next_pos = slVec2(pos.xy);
	inst->next_rot = -slRadToDeg(inst->body->GetAngle());
}
slScalar physInterpolationProgress,physInterpolationNegative;
void physInterpolateInstance (physInstance* inst)
{
	inst->visual->xy = (inst->prev_pos * physInterpolationNegative) + (inst->next_pos * physInterpolationProgress);
	//printf("%lf * %lf + %lf * %lf = %lf\n",inst->prev_pos.x,physInterpolationNegative,inst->next_pos.x,physInterpolationProgress,inst->visual->xy.x);
	//inst->visual->rotangle = -slRadToDeg(inst->prev_rads * physInterpolationNegative + inst->next_rads * physInterpolationProgress);
	inst->visual->rotangle = inst->prev_rot * physInterpolationNegative + inst->next_rot * physInterpolationProgress;
}
/*slForceInline void physCaptureBefores ()
{
	slDoWork(&physInstances,physCaptureInstanceBeforeState,NULL);
}*/
slForceInline inline void physCaptureAfters ()
{
	slDoWork(&physInstances,physCaptureInstanceAfterState,NULL);
}
slForceInline inline void physInterpolate ()
{
	slDoWork(&physInstances,physInterpolateInstance,NULL);
}
slForceInline inline void physUnsmoothUpdate ()
{
	slDoWork(&physInstances,physUpdateInstance,NULL); // could skip this is no steps were performed...
}
slScalar physMaxCycleTime = 1./30;
void physCycle ()
{
	Uint64 cycle_begin = SDL_GetPerformanceCounter();

	physElapsed += slGetDelta() * physRateMul;

	if (physElapsed >= physInterval) /// if no physics steps, we don't need to perform updates or process the pre-cycle/post-cycle callbacks.
	{
		/// Hooks to be run once before potentially several physics steps, for updating physics based on input very recently gathered during early slCycle.
		/// Unlike the Slice pre-render hook (which physCycle runs as) these hooks are guaranteed to happen
		/// before physics calculations (hooking into every rendered frame could introduce undesirable latency),
		/// and can be trimmed out when no physics calculations are going to occur during a frame
		/// (so hooking into every rendered frame could be more wasteful as well).
		for (slBU cur = 0; cur < physHooks_PreCycle.itemcount; cur++)
		{
			void (*percycle) () = *(physHooks_PreCycle.items + cur);
			percycle();
		}
		bool will_update_again;
		do
		{
			physElapsed -= physInterval;
			will_update_again = physElapsed >= physInterval;
			slScalar cycle_time = (SDL_GetPerformanceCounter() - cycle_begin) / (slScalar)SDL_GetPerformanceFrequency();
			if (cycle_time >= physMaxCycleTime)
			{
				physElapsed = 0;
				will_update_again = false;
			}

			/// Allow hooking into this so that some things can be easily done per actual physics step.
			for (slBU cur = 0; cur < physHooks_PreStep.itemcount; cur++)
			{
				void (*PreStep) () = *(physHooks_PreStep.items + cur);
				PreStep();
			}

			bool smooth_capture = physSmoothMode && !will_update_again;
			if (smooth_capture) physCaptureAfters();

			physWorld->Step(physInterval,8,3);

			if (smooth_capture) physCaptureAfters();
			physFirstAccumulate = false;
		}
		while (will_update_again);

		if (!physSmoothMode)
        {
            physUnsmoothUpdate();

    		for (slBU cur = 0; cur < physHooks_PostCycle.itemcount; cur++)
    		{
    			void (*percycle) () = *(physHooks_PostCycle.items + cur);
    			percycle();
    		}
        }
	}

	/// If smooth mode is enabled, need to do interpolation even if no discrete physics step happened.
	if (physSmoothMode)
	{
		if (physFirstAccumulate) physUnsmoothUpdate();
		else
		{
			physInterpolationProgress = physElapsed / physInterval;
			physInterpolationNegative = 1 - physInterpolationProgress;
			physInterpolate();
		}

		for (slBU cur = 0; cur < physHooks_PostCycle.itemcount; cur++)
		{
			void (*percycle) () = *(physHooks_PostCycle.items + cur);
			percycle();
		}
	}
}
b2World* physGetWorld ()
{
	return physWorld;
}
namespace s2d_internal
{
	void physInit ()
	{
		slHook_PreRender(physCycle);
		physWorld = new b2World(b2Vec2(0,-9.8));
	}
	void physQuit ()
	{
		slUnhook_PreRender(physCycle);
		physInstances.UntilEmpty(physDestroyInstance);
		physInstanceAllocator.FreeAllBlocks();
		physHooks_PreCycle.Clear(NULL);
		physHooks_PreStep.Clear(NULL);
		physHooks_PostCycle.Clear(NULL);
		delete physWorld;
	}
}
