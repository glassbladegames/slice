#include <slice2d.h>
slList s2dBatches("Slice2D Batch-Draw Groups",slNoIndex);
void s2dShrinkBatch (s2dBatch* batch)
{
	/*if (batch->rawbuf_capacity > s2dBatchInitialBufferSize)
	{
		if ((batch->buf_member_count << 1) < batch->rawbuf_capacity)
		{
			batch->rawbuf_capacity >>= 1;
			batch->raw_vertices = realloc(batch->raw_vertices,sizeof(GLfloat) * 48 * batch->rawbuf_capacity);
		}
	}*/
}
void s2dBatchMember_OnVertexModify (s2dBatchMember* member);
void s2dBatchExpandStorage (s2dBatch* batch)
{
	//batch->vbo_capacity += s2dBatchBufferExpandRate;
	//printf("vbo expanded to %llu items\n",batch->vbo_capacity);
	/*glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,batch->index_vbo);
	if (batch->vbo_capacity > 65536)
	{
		batch->index_type = GL_UNSIGNED_INT;
		slBU len = sizeof(GLuint) * 6 * batch->vbo_capacity;
		GLuint* temp_buf = malloc(len);
		GLuint* buf_ptr = temp_buf;
		for (slBU cur = 0; cur < batch->vbo_capacity; cur++)
		{
			*buf_ptr++ = cur;
			*buf_ptr++ = cur;
			*buf_ptr++ = cur;
			*buf_ptr++ = cur;
			*buf_ptr++ = cur;
			*buf_ptr++ = cur;
		}
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,len,temp_buf,GL_STATIC_DRAW);
		free(temp_buf);
	}
	else if (batch->vbo_capacity > 256)
	{
		batch->index_type = GL_UNSIGNED_SHORT;
		slBU len = sizeof(GLushort) * 6 * batch->vbo_capacity;
		GLushort* temp_buf = malloc(len);
		GLushort* buf_ptr = temp_buf;
		for (slBU cur = 0; cur < batch->vbo_capacity; cur++)
		{
			*buf_ptr++ = cur;
			*buf_ptr++ = cur;
			*buf_ptr++ = cur;
			*buf_ptr++ = cur;
			*buf_ptr++ = cur;
			*buf_ptr++ = cur;
		}
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,len,temp_buf,GL_STATIC_DRAW);
		free(temp_buf);
	}
	else
	{
		batch->index_type = GL_UNSIGNED_BYTE;
		slBU len = sizeof(GLubyte) * 6 * batch->vbo_capacity;
		GLubyte* temp_buf = malloc(len);
		GLubyte* buf_ptr = temp_buf;
		for (slBU cur = 0; cur < batch->vbo_capacity; cur++)
		{
			*buf_ptr++ = cur;
			*buf_ptr++ = cur;
			*buf_ptr++ = cur;
			*buf_ptr++ = cur;
			*buf_ptr++ = cur;
			*buf_ptr++ = cur;
		}
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,len,temp_buf,GL_STATIC_DRAW);
		free(temp_buf);
	}*/
	/*slBU len = sizeof(GLfloat) * 8 * batch->vbo_capacity * 6;
	GLfloat* temp_buf = malloc(len);
	GLfloat* buf_ptr = temp_buf;
	for (slBU cur = 0; cur < batch->active_member_count; cur++)
	{
		s2dBatchMember* member = batch->active_members[cur];
		memcpy(buf_ptr,member->vertexdata,sizeof(GLfloat) * 8);
		buf_ptr += 8;
		memcpy(buf_ptr,member->vertexdata,sizeof(GLfloat) * 8);
		buf_ptr += 8;
		memcpy(buf_ptr,member->vertexdata,sizeof(GLfloat) * 8);
		buf_ptr += 8;
		memcpy(buf_ptr,member->vertexdata,sizeof(GLfloat) * 8);
		buf_ptr += 8;
		memcpy(buf_ptr,member->vertexdata,sizeof(GLfloat) * 8);
		buf_ptr += 8;
		memcpy(buf_ptr,member->vertexdata,sizeof(GLfloat) * 8);
		buf_ptr += 8;
	}
	// Ending area can remain junk data.
	glBindBuffer(GL_ARRAY_BUFFER,batch->vbo);
	glBufferData(GL_ARRAY_BUFFER,len,temp_buf,GL_DYNAMIC_DRAW);
	free(temp_buf);*/
	slBU len = sizeof(GLfloat) * 8 * 6 * batch->vbo_capacity;
	glBindBuffer(GL_ARRAY_BUFFER,batch->vbo);
	glBufferData(GL_ARRAY_BUFFER,len,NULL,GL_DYNAMIC_DRAW);
	GLfloat* offset = 0;
	for (slBU cur = 0; cur < batch->active_member_count; cur++)
	{
		s2dBatchMember* member = batch->active_members[cur];
		for (Uint8 i = 0; i < 6; i++)
		{
			glBufferSubData(GL_ARRAY_BUFFER,offset,sizeof(GLfloat) * 8,member->vertexdata);
			offset += 8;
		}
	}
	//printf("offset stopped at %lli\n",offset);
	for (int i = 0; i < batch->active_member_count; i++)
	{
		s2dBatchMember* member = batch->active_members[i];
		s2dBatchMember_OnVertexModify(member);
	}
}
GLxywh s2dBatchMember::GetTexXYWH ()
{
	GLvec2 xy(this->xy);
	GLvec2 wh(this->wh);
	if (parent_batch->keepratio)
	{
		GLfloat aratio = (wh.w / wh.h) / parent_batch->tex->aspect;
		if (aratio > 1) wh.w /= aratio;
		else wh.h *= aratio;
	}
	xy -= wh * 0.5f;
	return {xy.x,xy.y,wh.w,wh.h};
}
void s2dRecalcBatchMemberXYWH (s2dBatchMember** member_ptr)
{
	s2dBatchMember* member = *member_ptr;
	GLxywh xywh = member->GetTexXYWH();
	member->vertexdata[0] = xywh.x;
	member->vertexdata[1] = xywh.y;
	member->vertexdata[2] = xywh.w;
	member->vertexdata[3] = xywh.h;
}
void s2dBatchNewTexture (s2dBatch* batch)
{
	if (batch->tex)
	{
        batch->tex->WaitDimsValid();
		slDoArrayWork(batch->active_members,batch->active_member_count,sizeof(s2dBatchMember*),s2dRecalcBatchMemberXYWH,NULL);
		for (slBU cur = 0; cur < batch->active_member_count; cur++)
			s2dBatchMember_OnVertexModify(batch->active_members[cur]);
	}
}
#define s2dBatchArraysInitialCapacity 64
s2dBatch* s2dCreateBatch (slTexRef tex_ref, Uint8 z)
{
	s2dBatch* batch = malloc(sizeof(s2dBatch));
	batch->allocator = slBlockAllocator("Slice2D Batch-Draw Member Allocator",sizeof(s2dBatchMember));
	//batch->members = slListInit("Slice2D Batch-Draw Members List",offsetof(s2dBatchMember,_index_));
	s2dBatches.Add(batch);
	batch->tex = NULL;
	batch->z = z;
	//batch->raw_vertices = NULL;
	//batch->rawbuf_capacity = s2dBatchInitialBufferSize;
	//batch->raw_vertices = malloc(sizeof(GLfloat) * 48 * s2dBatchInitialBufferSize);
	//batch->buf_member_count = 0;

	glGenVertexArrays(1,&batch->vao);
	glGenBuffers(1,&batch->vbo);
	//glGenBuffers(1,&batch->index_vbo);

	glBindVertexArray(batch->vao);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);

	glBindBuffer(GL_ARRAY_BUFFER,batch->vbo);
	void* offset = 0;
	const size_t vertsize = sizeof(GLfloat) * 8;
	glVertexAttribPointer(0,4,GL_FLOAT,false,vertsize,offset); offset += sizeof(GLfloat) * 4;
	glVertexAttribPointer(1,1,GL_FLOAT,false,vertsize,offset); offset += sizeof(GLfloat) * 1;
	glVertexAttribPointer(2,1,GL_FLOAT,false,vertsize,offset); offset += sizeof(GLfloat) * 1;
	glVertexAttribPointer(3,2,GL_FLOAT,false,vertsize,offset); offset += sizeof(GLfloat) * 2;

	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,batch->index_vbo);

	batch->active_member_capacity = s2dBatchArraysInitialCapacity;
	batch->dormant_member_capacity = s2dBatchArraysInitialCapacity;
	batch->active_members = malloc(sizeof(s2dBatchMember*) * s2dBatchArraysInitialCapacity);
	batch->dormant_members = malloc(sizeof(s2dBatchMember*) * s2dBatchArraysInitialCapacity);
	batch->active_member_count = 0;
	batch->dormant_member_count = 0;

	batch->vbo_capacity = s2dBatchArraysInitialCapacity;
	s2dBatchExpandStorage(batch);

    batch->SetTexture(tex_ref);

	//batch->vertex_data_lock = SDL_CreateMutex();

	return batch;
}
void s2dDestroyBatch (s2dBatch* batch)
{
	//SDL_DestroyMutex(batch->vertex_data_lock);

	if (batch->tex) batch->tex->Abandon();
	glDeleteVertexArrays(1,&batch->vao);
	glDeleteBuffers(1,&batch->vbo);
	//glDeleteBuffers(1,&batch->index_vbo);
	batch->allocator.FreeAllBlocks();
	//slListClear(&batch->members,NULL);
	s2dBatches.Remove(batch);
	/*if (batch->raw_vertices)*/ //free(batch->raw_vertices);
	free(batch->active_members);
	free(batch->dormant_members);
	free(batch);
}
void s2dBatchPrepareActiveInsertion (s2dBatch* batch)
{
	if (batch->active_member_count >= batch->active_member_capacity)
	{
		batch->active_member_capacity <<= 1;
		batch->active_members = realloc(batch->active_members,sizeof(s2dBatchMember*) * batch->active_member_capacity);
	}
	if (batch->active_member_count >= batch->vbo_capacity)
	{
		batch->vbo_capacity <<= 1;
		s2dBatchExpandStorage(batch);
	}
}
void s2dBatchPrepareDormantInsertion (s2dBatch* batch)
{
	if (batch->dormant_member_count >= batch->dormant_member_capacity)
	{
		batch->dormant_member_capacity <<= 1;
		batch->dormant_members = realloc(batch->dormant_members,sizeof(s2dBatchMember*) * batch->dormant_member_capacity);
	}
}
s2dBatchMember* s2dCreateBatchMember (s2dBatch* batch)
{
	s2dBatchMember* member = batch->allocator.Allocate();

	// Initialize these to no rotation so that SetRotation doesn't always have to be called.
	member->vertexdata[4] = 1;
	member->vertexdata[5] = 0;



	//slListAdd(&batch->members,member);

	#ifdef s2dEnableBatchCulling
	s2dBatchPrepareDormantInsertion(batch);
	member->index = batch->dormant_member_count++;
	batch->dormant_members[member->index] = member;
	member->is_active = false;
	#else
	s2dBatchPrepareActiveInsertion(batch);
	member->index = batch->active_member_count++;
	batch->active_members[member->index] = member;
	member->is_active = true;
	/// Don't push junk data, there is no point.
	//s2dBatchMember_OnVertexModify(member);
	#endif

	member->rot = false;
	member->rotcenter = true;
	member->visible = true;
	member->is_active = true;
	member->parent_batch = batch;
	return member;
}





void s2dBatchMember_OnVertexModify (s2dBatchMember* member)
{
	if (member->is_active)
	{
		//SDL_LockMutex(member->parent_batch->vertex_data_lock);

		//slUseContext(); // this crap doesn't work anyway

		//printf("0");
		//printf("updating a member's vertex information\n");
		glBindBuffer(GL_ARRAY_BUFFER,member->parent_batch->vbo);
		GLfloat* offset = member->index * sizeof(GLfloat) * 8 * 6;
		for (Uint8 i = 0; i < 6; i++)
		{
			glBufferSubData(GL_ARRAY_BUFFER,offset,sizeof(GLfloat) * 8,member->vertexdata);
			offset += 8;
		}
		/*while (true)
		{
			int err = glGetError();
			printf("[err=%d]\n",glGetError());
			if (!err) break;
		}*/
		//printf("1");
		//printf("%llf\n",member->vertexdata[0]);



		//SDL_UnlockMutex(member->parent_batch->vertex_data_lock);
	}
}
void s2dDestroyBatchMember (s2dBatch* batch, s2dBatchMember* member)
{
	//slListRemove(&batch->members,member);
	if (member->is_active)
	{
		slBU last_index = --batch->active_member_count;
		if (member->index < last_index)
		{
			batch->active_members[member->index] = batch->active_members[last_index];
			batch->active_members[member->index]->index = member->index;

			// Push the new occupant's vertex info.
			s2dBatchMember_OnVertexModify(batch->active_members[member->index]);
		}
	}
	else
	{
		slBU last_index = --batch->dormant_member_count;
		if (member->index < last_index)
		{
			batch->dormant_members[member->index] = batch->dormant_members[last_index];
			batch->dormant_members[member->index]->index = member->index;
		}
	}
	batch->allocator.Release(member);
}
void s2dBatchMember_MoveToActive (s2dBatchMember* member)
{
	s2dBatch* batch = member->parent_batch;

	slBU last_index = --batch->dormant_member_count;
	if (member->index < last_index)
	{
		batch->dormant_members[member->index] = batch->dormant_members[last_index];
		batch->dormant_members[member->index]->index = member->index;
	}

	s2dBatchPrepareActiveInsertion(batch);
	member->index = batch->active_member_count++;
	batch->active_members[member->index] = member;

	member->is_active = true;

	// Push vertex info.
	s2dBatchMember_OnVertexModify(member);
}
void s2dBatchMember_MoveToDormant (s2dBatchMember* member)
{
	s2dBatch* batch = member->parent_batch;

	slBU last_index = --batch->active_member_count;
	if (member->index < last_index)
	{
		batch->active_members[member->index] = batch->active_members[last_index];
		batch->active_members[member->index]->index = member->index;

		// Push the new occupant's vertex info.
		s2dBatchMember_OnVertexModify(batch->active_members[member->index]);
	}

	s2dBatchPrepareDormantInsertion(batch);
	member->index = batch->dormant_member_count++;
	batch->dormant_members[member->index] = member;

	member->is_active = false;
}
/*void s2dBatchMember::SetPosition (slVec2 pos)
{
	this->xy = pos;
	/ *GLvec2 pos_float = GLvec2(pos);
	vertexdata[0] = pos_float.x;
	vertexdata[1] = pos_float.y;
	if (rotcenter)
	{
		vertexdata[6] = pos_float.x;
		vertexdata[7] = pos_float.y;
	}* /

	s2dBatchMember* this_real = this;
	s2dRecalcBatchMemberXYWH(&this_real);

	if (rotcenter)
	{
		GLvec2 pos_float = GLvec2(pos);
		vertexdata[6] = pos_float.x;
		vertexdata[7] = pos_float.y;
	}

	s2dBatchMember_OnVertexModify(this);
}
void s2dBatchMember::SetDimensions (slVec2 dims)
{
	this->wh = dims;
	/ *GLvec2 dims_float = GLvec2(dims);
	vertexdata[2] = dims_float.x;
	vertexdata[3] = dims_float.y;* /

	s2dBatchMember* this_real = this;
	s2dRecalcBatchMemberXYWH(&this_real);

	s2dBatchMember_OnVertexModify(this);
}*/
void s2dBatchMember::SetPosAndDims (slVec2 pos, slVec2 dims)
{
	this->xy = pos;
	this->wh = dims;

	s2dBatchMember* this_real = this;
	s2dRecalcBatchMemberXYWH(&this_real);

	if (rotcenter)
	{
		GLvec2 pos_float = GLvec2(pos);
		vertexdata[6] = pos_float.x;
		vertexdata[7] = pos_float.y;
	}

	s2dBatchMember_OnVertexModify(this);
}
void s2dBatchMember::SetRotation (bool rot, slScalar rotangle, bool rotcenter, slVec2 rotpoint)
{
	this->rot = rot;
	this->rotangle = rotangle;
	this->rotcenter = rotcenter;
	this->rotpoint = rotpoint;

	GLvec2 rotpoint_float;
	GLfloat cosvalue,sinvalue;
	if (rot)
	{
		GLfloat angle = rotangle;
		angle = -slDegToRad_F(angle);
		cosvalue = cosf(angle);
		sinvalue = sinf(angle);
		if (!rotcenter)
		{
			rotpoint_float = GLvec2(rotpoint);
			goto OFFCENTER;
		}
	}
	else
	{
		cosvalue = 1;
		sinvalue = 0;
	}
	rotpoint_float = GLvec2(xy);
	OFFCENTER:

	vertexdata[4] = cosvalue;
	vertexdata[5] = sinvalue;
	vertexdata[6] = rotpoint_float.x;
	vertexdata[7] = rotpoint_float.y;

	s2dBatchMember_OnVertexModify(this);
}
void s2dBatchMember::SetVisible (bool visible)
{
	this->visible = visible;
	#ifndef s2dEnableBatchCulling
	if (visible && !is_active)
	{
		s2dBatchMember_MoveToActive(this);
	}
	else if (is_active && !visible)
	{
		s2dBatchMember_MoveToDormant(this);
	}
	#endif
}
/*void s2dSetBatchMemberDims (s2dBatchMember* member, slScalar x, slScalar y, slScalar w, slScalar h)
{
	member->xy = slVec2(x,y);
	member->wh = slVec2(w,h);
}
void s2dSetBatchMemberDims (s2dBatchMember* member, slVec2 xy, slVec2 wh)
{
	member->xy = xy;
	member->wh = wh;
}
void s2dRelBatchMemberDims (s2dBatchMember* member, s2dBatchMember* rel)
{
	member->xy *= rel->wh;
	member->wh *= rel->wh;
	member->xy += rel->xy;
}
void s2dSetBatchMemberRots (s2dBatchMember* member, bool rot, slScalar rotangle, bool rotcenter, slVec2 rotpoint)
{
	member->rot = rot;
	member->rotangle = rotangle;
	member->rotcenter = rotcenter;
	member->rotpoint = rotpoint;
}*/
extern slVec2 s2dScreenBL,s2dScreenTR;
#ifdef s2dEnableBatchCulling
void s2dBatchCheckCurrentlyActive (s2dBatchMember** member_ptr, void* unused, slWorker* worker)
{
	s2dBatchMember* member = *member_ptr;
	if (!member->visible) goto MAKE_DORMANT;

	//goto NO_CULL;

	slScalar radius = member->wh.len();
	slVec2 inst_bottomleft = member->xy - radius;
	slVec2 inst_topright = member->xy + radius;
	bool offscreen = inst_topright.x < s2dScreenBL.x || inst_topright.y < s2dScreenBL.y || inst_bottomleft.x > s2dScreenTR.x || inst_bottomleft.y > s2dScreenTR.y;
	if (offscreen) goto MAKE_DORMANT;

	//NO_CULL:

	return; // Stay active.
	MAKE_DORMANT:
	slSchedule(s2dBatchMember_MoveToDormant,member,worker);
}
void s2dBatchCheckCurrentlyDormant (s2dBatchMember** member_ptr, void* unused, slWorker* worker)
{
	s2dBatchMember* member = *member_ptr;
	if (!member->visible) return; // Stay dormant.

	//goto NO_CULL;

	slScalar radius = member->wh.len();
	slVec2 inst_bottomleft = member->xy - radius;
	slVec2 inst_topright = member->xy + radius;
	bool offscreen = inst_topright.x < s2dScreenBL.x || inst_topright.y < s2dScreenBL.y || inst_bottomleft.x > s2dScreenTR.x || inst_bottomleft.y > s2dScreenTR.y;
	if (offscreen) return; // Stay dormant.

	//NO_CULL:

	slSchedule(s2dBatchMember_MoveToActive,member,worker);
}
#endif
/*void s2dBatchPreRenderMember (s2dBatchMember** member_ptr)
{
	s2dBatchMember* member = *member_ptr;
	slScalar radius = member->wh.len();
	slVec2 inst_bottomleft = member->xy - radius;
	slVec2 inst_topright = member->xy + radius;
	bool offscreen = inst_topright.x < s2dScreenBL.x || inst_topright.y < s2dScreenBL.y || inst_bottomleft.x > s2dScreenTR.x || inst_bottomleft.y > s2dScreenTR.y;
	bool should_appear = member->visible && !offscreen;
	if (member->is_active && !should_appear)
		slSchedule(s2dBatchMember_MoveToDormant,member);
	else if (should_appear && !member->is_active)
		slSchedule(s2dBatchMember_MoveToActive,member);
}*/

/*void s2dBatchPreRender (s2dBatch* batch)
{
	slDoArrayWork(batch->active_members,batch->active_member_count,sizeof(s2dBatchMember*),s2dBatchCheckCurrentlyActive,NULL);
	slDoArrayWork(batch->dormant_members,batch->dormant_member_count,sizeof(s2dBatchMember*),s2dBatchCheckCurrentlyDormant,NULL);

	//printf("active: %llu, dormant: %llu, total: %llu\n",batch->active_member_count,batch->dormant_member_count,batch->active_member_count + batch->dormant_member_count);






	/ *batch->buf_member_count = 0;
	if (!batch->tex) return;
	if (!batch->tex->ready) return;
	GLfloat* bufptr = batch->raw_vertices;
	for (slBU cur = 0; cur < batch->members.itemcount; cur++)
	{
		s2dBatchMember* member = *(batch->members.items + cur);
		if (!member->visible) continue;

		slScalar radius = member->wh.len();
		slVec2 inst_bottomleft = member->xy - radius;
		slVec2 inst_topright = member->xy + radius;
		if (inst_topright.x < s2dScreenBL.x || inst_topright.y < s2dScreenBL.y || inst_bottomleft.x > s2dScreenTR.x || inst_bottomleft.y > s2dScreenTR.y) continue;

		GLvec2 rotpoint;
		GLfloat cosvalue,sinvalue;
		if (member->rot)
		{
			GLfloat angle = member->rotangle;
			angle = -slDegToRad_F(angle);
			cosvalue = cosf(angle);
			sinvalue = sinf(angle);
			if (!member->rotcenter)
			{
				rotpoint = GLvec2(member->rotpoint);
				goto OFFCENTER;
			}
		}
		else
		{
			cosvalue = 1;
			sinvalue = 0;
		}
		rotpoint = GLvec2(member->xy);
		OFFCENTER:

		GLxywh xywh = s2dBatchMemberTexXYWH(member,batch);
		//xywh.x = 0;
		//xywh.y = 0;
		//xywh.w = 0.1;
		//xywh.h = 0.1;
		//cosvalue = cosf(slDegToRad_F(45));
		//sinvalue = sinf(slDegToRad_F(45));

        if (batch->buf_member_count >= batch->rawbuf_capacity)
		{
			//printf("batch capacity was %llu",batch->rawbuf_capacity);
			batch->rawbuf_capacity <<= 1;
			//printf(", increased to %llu\n",batch->rawbuf_capacity);
			batch->raw_vertices = realloc(batch->raw_vertices,sizeof(GLfloat) * 48 * batch->rawbuf_capacity);
			bufptr = batch->raw_vertices + batch->buf_member_count * 48;
		}
		#define s2dPushBatchVertexData\
			*bufptr++ = xywh.x;\
			*bufptr++ = xywh.y;\
			*bufptr++ = xywh.w;\
			*bufptr++ = xywh.h;\
			*bufptr++ = cosvalue;\
			*bufptr++ = sinvalue;\
			*bufptr++ = rotpoint.x;\
			*bufptr++ = rotpoint.y;
		s2dPushBatchVertexData
		s2dPushBatchVertexData
		s2dPushBatchVertexData
		s2dPushBatchVertexData
		s2dPushBatchVertexData
		s2dPushBatchVertexData
		batch->buf_member_count++;
	}
	//printf("generated %llu instances in this batch, out of %llu.\n",batch->buf_member_count,batch->members.itemcount);
	/ *for (int i = 0; i < 6; i++)
	{
		for (int i = 0; i < 8; i++) printf("\t%llf\n",batch->raw_vertices[i]);
		printf("\n");
	}* /
	* /
}*/
/*void s2dPushBatchBuffers ()
{
	for (slBU cur = 0; cur < s2dBatches.itemcount; cur++)
	{
		//printf("pushing buffer\n");
		s2dBatch* batch = s2dBatches.items[cur];
		if (!batch->buf_member_count) continue;
		glBindBuffer(GL_ARRAY_BUFFER,batch->vbo);
		if (batch->buf_member_count > batch->vbo_capacity)
		{
			batch->vbo_capacity = batch->buf_member_count;
			glBufferData(GL_ARRAY_BUFFER,batch->buf_member_count * sizeof(GLfloat) * 48,batch->raw_vertices,GL_DYNAMIC_DRAW);
		}
		else glBufferSubData(GL_ARRAY_BUFFER,0,batch->buf_member_count * sizeof(GLfloat) * 48,batch->raw_vertices);
	}
}
*/

namespace s2d_internal
{
	GLuint s2dRenderBatch (s2dBatch* batch)
	{
		//printf("drawing %llu members\n",batch->active_member_count);
        GLuint tex = batch->tex->tex;
		glUseProgram(s2dBatchProgram.program);
		s2dSetDrawTexture(tex);
		slSetTextureClamping(GL_CLAMP_TO_EDGE);
		glBindVertexArray(batch->vao);
		glBindBuffer(GL_ARRAY_BUFFER,batch->vbo);
		glDrawArrays(GL_TRIANGLES,0,batch->active_member_count * 6);

		//glBindBuffer(GL_ARRAY_BUFFER,batch->vbo);
		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,batch->index_vbo);
		//printf("drawing %llu members\n",batch->active_member_count);
		//glDrawElements(GL_TRIANGLES,batch->active_member_count * 6,batch->index_type,0);
		//glDrawArrays(GL_TRIANGLES,0,batch->active_member_count);

        return tex;
	}
}
