#include <slice2d.h>



/// World Texture Program

// Source: Vert w/ Rotation
char* s2dWorldRotateProgram_VertSrc = R"(#version 130

uniform vec4 XYWH;
in vec2 VertPos;
out vec2 TexCoords;
uniform vec2 CamPos;
uniform vec2 InvHalfCamDims;
uniform vec2 RotPoint;
uniform float SinValue,CosValue;
uniform float CamRotCos,CamRotSin;

void main ()
{
	// Generate point.
	vec2 point = XYWH.xy + (VertPos * XYWH.zw);
	// Rotate.
	point -= RotPoint;
	point = vec2(point.x * CosValue - point.y * SinValue,point.y * CosValue + point.x * SinValue);
	point += RotPoint;
	// Translate by camera position.
	point = point - CamPos;
    // Rotate around camera.
    point = vec2(point.x * CamRotCos - point.y * CamRotSin,point.y * CamRotCos + point.x * CamRotSin);
    // Scale by dimensions.
    gl_Position = vec4(point * InvHalfCamDims,0,1);

    TexCoords = VertPos;
}
)";

// Uniforms: Vert w/ Rotation
namespace s2d_internal
{
	GLuint
		s2dWorldRotateProgram_XYWH,
		s2dWorldRotateProgram_CamPos,
		s2dWorldRotateProgram_InvHalfCamDims,
		s2dWorldRotateProgram_CamRotCos,
		s2dWorldRotateProgram_CamRotSin,
		s2dWorldRotateProgram_SinValue,
		s2dWorldRotateProgram_CosValue,
		s2dWorldRotateProgram_RotPoint;
}

// Source: Vert w/ Rotation, Batched
char* s2dBatchProgram_VertSrc = R"(#version 130

in vec4 XYWH;
in float CosValue,SinValue;
in vec2 RotPoint;
out vec2 TexCoords;
uniform vec2 CamPos;
uniform vec2 InvHalfCamDims;
uniform float CamRotCos,CamRotSin;

void main ()
{
	// Get vertex position.
	int vid = gl_VertexID % 6;
	//int vid = gl_InstanceID % 6;
	if (vid >= 3) vid -= 2;
	vec2 VertPos = vec2(vid & 1,(vid & 2) >> 1);


	// Generate point.
	vec2 point = XYWH.xy + (VertPos * XYWH.zw);


	// Rotate.
	point -= RotPoint;
	point = vec2(point.x * CosValue - point.y * SinValue,point.y * CosValue + point.x * SinValue);
	point += RotPoint;


	// Translate by camera position.
	point = point - CamPos;


    // Rotate around camera.
    point = vec2(point.x * CamRotCos - point.y * CamRotSin,point.y * CamRotCos + point.x * CamRotSin);


    // Scale by dimensions.
    gl_Position = vec4(point * InvHalfCamDims,0,1);


    TexCoords = VertPos;
}
)";

// Uniforms: Vert w/ Rotation, Batched
namespace s2d_internal
{
	GLuint
		s2dBatchProgram_CamPos,
		s2dBatchProgram_InvHalfCamDims,
		s2dBatchProgram_CamRotCos,
		s2dBatchProgram_CamRotSin;
}

// Source: Texture-Tiled Vert w/ Rotation
char* s2dTiledRotateProgram_VertSrc = R"(#version 130

//#extension GL_ARB_gpu_shader_fp64 : enable
uniform vec4 XYWH;
in vec2 VertPos;
out vec2 TexCoords;
uniform vec2 CamPos;
uniform vec2 InvHalfCamDims;
uniform vec2 RotPoint;
uniform float SinValue,CosValue;
uniform vec4 TexXYWH;
uniform float CamRotCos,CamRotSin;
uniform vec2 TexCosSin;
//uniform float resbias;

void main ()
{
	// Generate point.
	//vec2 VertPos = vec2(gl_VertexID % 2,gl_VertexID % 1) * 2 - 1;
	vec2 point = XYWH.xy + (VertPos * XYWH.zw);
	// Rotate.
	//TexCoords = point;
	point -= RotPoint;
	point = vec2(point.x * CosValue - point.y * SinValue,point.y * CosValue + point.x * SinValue);
	point += RotPoint;
	//dvec2 DTXCoords = point;
	TexCoords = point;
	// Translate by camera position.
	point = point - CamPos;
    // Rotate around camera.
    point = vec2(point.x * CamRotCos - point.y * CamRotSin,point.y * CamRotCos + point.x * CamRotSin);
    // Scale by dimensions.
    gl_Position = vec4(point * InvHalfCamDims,0,1);

    TexCoords -= TexXYWH.xy;
    TexCoords = vec2(TexCoords.x * TexCosSin.x - TexCoords.y * TexCosSin.y,TexCoords.y * TexCosSin.x + TexCoords.x * TexCosSin.y);
    TexCoords /= TexXYWH.zw;
    //TexCoords = vec2(DTXCoords) + resbias;
}
)";

// Uniforms: Texture-Tiled Vert w/ Rotation
namespace s2d_internal
{
	GLuint
		s2dTiledRotateProgram_XYWH,
		s2dTiledRotateProgram_CamPos,
		s2dTiledRotateProgram_InvHalfCamDims,
		s2dTiledRotateProgram_CamRotCos,
		s2dTiledRotateProgram_CamRotSin,
		s2dTiledRotateProgram_SinValue,
		s2dTiledRotateProgram_CosValue,
		s2dTiledRotateProgram_RotPoint,
		s2dTiledRotateProgram_TexXYWH,
		s2dTiledRotateProgram_TexCosSin;
}

// Source: Frag
char* s2dWorldProgram_FragSrc = R"(#version 130

uniform sampler2D Tex;
uniform vec4 Mask;
in vec2 TexCoords;

void main ()
{
    gl_FragColor = texture(Tex,TexCoords) * Mask;
}
)";

// Uniforms: Frag
namespace s2d_internal
{
	GLuint
		s2dWorldRotateProgram_Tex,
		s2dWorldRotateProgram_Mask,
		s2dBatchProgram_Tex,
		s2dBatchProgram_Mask,
		s2dTiledRotateProgram_Tex,
		s2dTiledRotateProgram_Mask;
}





// Shader Initialization

    extern GLuint slBoxRenderArray;
    extern GLuint slBoxRenderArray_VertsIndex;
    void slBindAttribs_VertPos (GLuint program);

GLuint s2dTextureUnit_Tex = 0;
void s2dBindBatchProgramAttribs (GLuint program)
{
	glBindAttribLocation(program,0,"XYWH");
	glBindAttribLocation(program,1,"CosValue");
	glBindAttribLocation(program,2,"SinValue");
	glBindAttribLocation(program,3,"RotPoint");
}
namespace s2d_internal
{
	slShaderProgram s2dWorldRotateProgram, s2dBatchProgram, s2dTiledRotateProgram;
	void s2dInitWorldProgram ()
	{
		/// World Texture Program w/ Rotation
		s2dWorldRotateProgram = slCreateShaderProgram("World Rotate",s2dWorldRotateProgram_VertSrc,s2dWorldProgram_FragSrc,slBindAttribs_VertPos);
		// Get Uniforms
		s2dWorldRotateProgram_CamPos = slLocateUniform(s2dWorldRotateProgram,"CamPos");
		s2dWorldRotateProgram_InvHalfCamDims = slLocateUniform(s2dWorldRotateProgram,"InvHalfCamDims");
		s2dWorldRotateProgram_Tex = slLocateUniform(s2dWorldRotateProgram,"Tex");
		s2dWorldRotateProgram_Mask = slLocateUniform(s2dWorldRotateProgram,"Mask");
		s2dWorldRotateProgram_SinValue = slLocateUniform(s2dWorldRotateProgram,"SinValue");
		s2dWorldRotateProgram_CosValue = slLocateUniform(s2dWorldRotateProgram,"CosValue");
		s2dWorldRotateProgram_RotPoint = slLocateUniform(s2dWorldRotateProgram,"RotPoint");
		s2dWorldRotateProgram_XYWH = slLocateUniform(s2dWorldRotateProgram,"XYWH");
		s2dWorldRotateProgram_CamRotCos = slLocateUniform(s2dWorldRotateProgram,"CamRotCos");
		s2dWorldRotateProgram_CamRotSin = slLocateUniform(s2dWorldRotateProgram,"CamRotSin");
		// Configure Texture Units
		glUniform1i(s2dWorldRotateProgram_Tex,s2dTextureUnit_Tex);

		/// World Texture Program w/ Rotation, Batched
		s2dBatchProgram = slCreateShaderProgram("World Rotate, Batched",s2dBatchProgram_VertSrc,s2dWorldProgram_FragSrc,s2dBindBatchProgramAttribs);
		s2dBatchProgram_CamPos = slLocateUniform(s2dBatchProgram,"CamPos");
		s2dBatchProgram_InvHalfCamDims = slLocateUniform(s2dBatchProgram,"InvHalfCamDims");
		s2dBatchProgram_Tex = slLocateUniform(s2dBatchProgram,"Tex");
		s2dBatchProgram_Mask = slLocateUniform(s2dBatchProgram,"Mask");
		s2dBatchProgram_CamRotCos = slLocateUniform(s2dBatchProgram,"CamRotCos");
		s2dBatchProgram_CamRotSin = slLocateUniform(s2dBatchProgram,"CamRotSin");
		// Configure texture units & mask to 1's since mask modulation not supported for batches
		//glUseProgram(s2dBatchProgram.program);
		glUniform1i(s2dBatchProgram_Tex,s2dTextureUnit_Tex);
		glUniform4f(s2dBatchProgram_Mask,1.f,1.f,1.f,1.f);

		/// World Tiled Texture Program w/ Rotation
		s2dTiledRotateProgram = slCreateShaderProgram("World Tiled Rotate",s2dTiledRotateProgram_VertSrc,s2dWorldProgram_FragSrc,slBindAttribs_VertPos);
		// Get Uniforms
		s2dTiledRotateProgram_CamPos = slLocateUniform(s2dTiledRotateProgram,"CamPos");
		s2dTiledRotateProgram_InvHalfCamDims = slLocateUniform(s2dTiledRotateProgram,"InvHalfCamDims");
		s2dTiledRotateProgram_Tex = slLocateUniform(s2dTiledRotateProgram,"Tex");
		s2dTiledRotateProgram_Mask = slLocateUniform(s2dTiledRotateProgram,"Mask");
		s2dTiledRotateProgram_SinValue = slLocateUniform(s2dTiledRotateProgram,"SinValue");
		s2dTiledRotateProgram_CosValue = slLocateUniform(s2dTiledRotateProgram,"CosValue");
		s2dTiledRotateProgram_RotPoint = slLocateUniform(s2dTiledRotateProgram,"RotPoint");
		s2dTiledRotateProgram_XYWH = slLocateUniform(s2dTiledRotateProgram,"XYWH");
		s2dTiledRotateProgram_TexXYWH = slLocateUniform(s2dTiledRotateProgram,"TexXYWH");
		s2dTiledRotateProgram_CamRotCos = slLocateUniform(s2dTiledRotateProgram,"CamRotCos");
		s2dTiledRotateProgram_CamRotSin = slLocateUniform(s2dTiledRotateProgram,"CamRotSin");
		s2dTiledRotateProgram_TexCosSin = slLocateUniform(s2dTiledRotateProgram,"TexCosSin");
		//s2dTiledRotateProgram_resbias = slLocateUniform(s2dTiledRotateProgram,"resbias");
		// Configure Texture Units
		glUniform1i(s2dTiledRotateProgram_Tex,s2dTextureUnit_Tex);
	}
	void s2dQuitWorldProgram ()
	{
		slDestroyShaderProgram(s2dWorldRotateProgram);
		slDestroyShaderProgram(s2dTiledRotateProgram);
	}
	void s2dSetDrawTexture (GLuint tex)
	{
		glActiveTexture(GL_TEXTURE0 + s2dTextureUnit_Tex);
		glBindTexture(GL_TEXTURE_2D,tex);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MAX_ANISOTROPY_EXT,1);
	}
	void s2dLoadWorldOrtho ()
	{
		GLvec2 ihcd(s2dGetCamWH());
		GLvec2 cam_xy(s2dGetCamXY());
		ihcd = slVec2(2) / ihcd;
		/*GLfloat ihcdw = 2 / (GLfloat)_s2dCamW_;
		GLfloat ihcdh = 2 / (GLfloat)_s2dCamH_;*/

		GLfloat camrotcos = slDegToRad(s2dGetCamRot());
		GLfloat camrotsin = sinf(camrotcos);
				camrotcos = cosf(camrotcos);

		glUseProgram(s2dWorldRotateProgram.program);
		glUniform2fv(s2dWorldRotateProgram_CamPos,1,(GLfloat*)&cam_xy);
		glUniform2fv(s2dWorldRotateProgram_InvHalfCamDims,1,(GLfloat*)&ihcd);
		glUniform1f(s2dWorldRotateProgram_CamRotCos,camrotcos);
		glUniform1f(s2dWorldRotateProgram_CamRotSin,camrotsin);

		glUseProgram(s2dBatchProgram.program);
		glUniform2fv(s2dBatchProgram_CamPos,1,(GLfloat*)&cam_xy);
		glUniform2fv(s2dBatchProgram_InvHalfCamDims,1,(GLfloat*)&ihcd);
		glUniform1f(s2dBatchProgram_CamRotCos,camrotcos);
		glUniform1f(s2dBatchProgram_CamRotSin,camrotsin);
		//glUniform4f(s2dBatchProgram_Mask,1.f,1.f,1.f,1.f);

		glUseProgram(s2dTiledRotateProgram.program);
		glUniform2fv(s2dTiledRotateProgram_CamPos,1,(GLfloat*)&cam_xy);
		glUniform2fv(s2dTiledRotateProgram_InvHalfCamDims,1,(GLfloat*)&ihcd);
		glUniform1f(s2dTiledRotateProgram_CamRotCos,camrotcos);
		glUniform1f(s2dTiledRotateProgram_CamRotSin,camrotsin);
	}
	#define s2dInv255 (1 / 255.f)
	void s2dUniformFromColor (GLuint uniform, SDL_Color color)
	{
		glUniform4f(uniform,color.r * s2dInv255,color.g * s2dInv255,color.b * s2dInv255,color.a * s2dInv255);
	}
}
